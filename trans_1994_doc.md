# L'autonomie de la personne vieillissante dans son environnement socio-culturel

«L'autonomie de la personne vieillissante dans son environnement socio-culturel» est une enquête transversale sur la vie des personnes âgées en Valais et à Genève, réalisée en 1994 par le CIG (Centre interfacultaire de gérontologie), ancêtre du CIGEV. Elle fait suite à une précédente enquête transversale, menée en 1979, présentée dans un autre jeu de données, et a été poursuivie par une étude longitudinale sur 10 ans, entre 1994 et 2004. Les considérations ci-dessous y font souvent référence.

La recherche porte sur des facteurs (internes et environnementaux) et des processus qui favorisent ou soutiennent l'autonomie de la personne vieillisante, son intégrité physique et psychique et sa participation à la vie en société, ou au contraire qui entravent ou portent atteinte à celles-ci. Cet objectif est poursuivi au moyen d'une enquête transversale auprès d'un échantillon représentatif de la population âgée de 63 ans et plus de deux régions typées (canton de Genève et Valais central) en 1979. Le choix des deux régions d'enquête s'explique par le fait qu'elles constituent typologiquement les deux extrêmes de la "fourchette" représentant la diversité des situations des personnes âgées en Suisse. Cette enquête transversale est répétée en 1994 et complétée par une enquête longitudinale (1994-1999) sur la cohorte des 80-84 ans vivant à domicile au moment du passage de l'enquête transversale de 1994. Ce design autorise une comparaison permettant d'établir l'évolution et les changements de la condition âgée sur 15 ans.

La recherche s'inscrit dans la perspective théorique du parcours de vie (life course). À la stratification sociale "horizontale" (de classes) s'articule une stratification "verticale", une organisation sociale du temps de vie, définissant étapes et transitions. L'hypothèse est que la position occupée dans le parcours de vie est un meilleur indicateur que l'âge tant pour définir des situations et problèmes-types que par rapport aux pratiques des personnes vieillissantes. L'idée est donc de construire et de valider un indicateur de position dans le parcours de vie (IPV) combinant la position individuelle dans trois dimensions: le parcours de santé, la trajectoire familiale, la trajectoire professionnelle.

Par les résultats de la recherche, on veut contribuer à une réflexion sur les fondements éthiques de la recherche en gérontologie et d'une politique de la vieillesse, à la définition des besoins présents et à venir des personnes vieillissantes, et à la mise en lumière des possibles articulations entre le soutien que peuvent apporter la famille ou les réseaux d'entraide, et les diverses formes d'aides institutionnelles.

## Citation

(FR)
> Lalive d'Épinay, C. (dir.). (1994). *L'autonomie de la personne vieillissante dans son environnement socio-culturel* [Jeu de données et documentation]. Tiré de https://gitlab.unige.ch/cigev/

(EN)
> Lalive d'Épinay, C. (Dir.). (1994). *L'autonomie de la personne vieillissante dans son environnement socio-culturel* [Data file and documentation]. Retrieved from https://gitlab.unige.ch/cigev/

## Auteur·e·s

* **Christian Lalive d'Épinay** – *Chef de projet*
* **Eric Fuchs**
* **Eugen Horber**
* **Jean-Pierre Michel**
* **André Rougemont**

### Collaboratrices et collaborateurs

* **Christine Bétemps**
* **Jean-François Bickel**
* **Matthias Brunner**
* **Cornelia Hummel**
* **Carole Maystre**
* **Jean-François Riand**
* **Dario Spini**
* **Astrid Stuckelberger**
* **Manfred Urben**
* **Nathalie Vollenwyder**

## Financement

Fonds national suisse de la recherche scientifique (FNS), Programme national de recherche no 32 ([PNR 32](http://www.snf.ch/fr/pointrecherche/programmes-nationaux-de-recherche/pnr32-vieillesse/Pages/default.aspx)): «Vieillesse».

## Thèmes

Interviewé: commune de résidence, sexe, date de naissance, état civil, confession, lieu où l'interviewé(e) à passé l'enfance, année du: mariage, veuvage, divorce, remariage; nombre d'enfants et de petits-enfants.

Pour chaque enfant de l'interviewé(e): sexe, année de naissance, année du décès, activité principale, activité principale du conjoint, lieu de résidence, nombre d'enfant(s).

Famille de l'interviewé: nombre de frères et soeurs à l'âge de 20 ans, nombre de frères /de soeurs encore vivant aujourd'hui; rang de l'interviewé dans la fratrie, nombre de (petits-)neveux et (petites-)nièces âgés de moins de 15 ans.

Conjoint(e): année de naissance, situation professionnelle: actuellement, à l'âge de 60ans; retraite: à l'âge légal, repoussée, anticipée; changements positifs/négatifs liés à la retraite du conjoint(e).

Jeunesse: langue parlée à la maison, lieu de résidence, profession exercée par le père, dernière école fréquentée, diplômes obtenus, année du premier emploi régulier et profession alors exercée; situation à 45 ans: langue parlée, commune de résidence, statut professionnel, profession exercée, taux et secteur d'activité, situation entre 45 et 50 ans: type de changements professionnels; situation à 60 ans: profession exercée et statut professionnel, taux et secteur d'activité, nombre de subordonnés, jugement global sur ce travail; retraite: à l'âge légal, repoussée, anticipée; raisons à la base de: la poursuite de l'activité professionnelle au-delà de l'âge légal, la retraite anticipée; si poursite d'une activité rémunérée: type, taux et raisons; manque ressenti depuis la retraite: la compagnie, le sentiment d'être utile, les rentrées d'argent, etc.

Veuves et veufs: statut professionnel du conjoint à l'époque qui a précédé son décès; si conjoint/e décédé/e était à l'AI: année de la prise en charge par l'AI, invalidité totale ou partielle, invalidité résultant d'un accident ou d'une maladie; circonstances du décès du conjoint/e: décès subit, grave maladie et durée de celle-ci, séjour à l'hôpital ou à la maison; maladie ayant induit beaucoup de travail pour l'interviewé, soutien et aide reçus avant le décès; suite au décès du conjoint degré de gravité de divers problèmes: problèmes financiers, manque de contacts sociaux, solitude, etc.; personnes ayant aidé à faire face à ces problèmes.

Voiture et transports: propre voiture de l'interviewé ou de son conjoint, fréquence d'utilisation de la voiture par l'interviewé, personne conduisant habituellement la voiture; moyen de transport utilisé pour aller en ville ou dans un autre lieu.

Assurances: assurance(s) que l'interviewé/le conjoint a souscrite(s): assurance-maladie, assurance-accident, assurance-vie.

Santé: autoévaluation de l'état de santé à 45 ans; maladie/suites d'un accident ou d'une opération dont l'interviewé souffre actuellement et âge auquel cette maladie/cet accident/cette opération est survenu/e; hospitalisation au cours des 12 derniers mois: nombre de séjours, lieu; personne qui s'est principalement occupée de l'interviewé lors de son hospitalisation; au cours des 3 derniers mois maladie ou accident ayant obligé l'interviewé à garder le lit: durée de l'immobilisation, personne qui s'est principalement occupée de l'interviewé; au cours des 12 derniers mois nombre de visites chez: le médecin, le dentiste; troubles et affections dont l'interviewé a souffert au cours des quatre dernières semaines et conséquences de ceux-ci sur ses activités journalières habituelles; port de lunettes correctives ou de lentilles de contact; troubles de la vue même avec le port de lunettes ou de lentilles; troubles de l'ouïe, appareil auditif; chute au cours de la dernière année écoulée et si oui combien; prise de médicaments au cours de la dernière semaine et nombre de médicaments différents; aide extérieure lors de la prise des médicaments; si l'interviewé fume: nombre de cigarettes/cigarillos/cigares/pipes par jour; consommation d'alcool; difficulté à accomplir seul/e certaines activités: se déplacer d'une pièce à l'autre, monter ou descendre un escalier, se déplacer à l'extérieur du logement, etc.; problèmes d'incontinence; perte de mémoire; propre réaction face à une fracture du fémur; au cours des trois derniers mois type d'aide(s) reçue(s) par l'interviewé/le conjoint/une autre personne vivant avec l'interviewé et fréquence de cette/ces aide(s); propre état de santé: évaluation, comparaison avec celui des personnes du même âge, évolution au cours des trois derniers mois; évaluation de l'état de santé du conjoint et comparaison avec le propre état de santé; emploi d'un/e homme/femme de ménage; opinion sur la santé et la maladie.

Médias: fréquence à laquelle l'interviewé: écoute la radio, regarde la télévision, lit un journal, lit des livres ou des revues; types d'émissions: les plus écoutées à la radio, les plus regardées à la télévision; rubriques les plus lues dans les journaux ou hebdomadaires; genres de livres lus de préférence.

Logement: chez soi (propriétaire/locataire), nombre de pièces, satisfaction (grandeur, équipement, bruit, etc.); loger seul/avec (conjoint, enfant, famille, ami, autre), équipement du logement: eau chaude, WC dans l'appartement, téléphone, etc.; nombre de personnes vivant dans le même logement que l'interviewé; personnes vivant seules: souhait de partager le logement et personnes avec lesquelles l'interviewé aimerait vivre; années de résidence dans: le logement actuel, la commune actuelle (en ville: le quartier); si moins de 15 ans de résidence dans l'actuelle commune/ l'actuel quartier de domicile: lieu de résidence précédent, raisons ayant motivé le déménagement; durée du trajet à pied entre le domicile et: le magasin d'alimentation dans lequel l'interviewé se sert habituellement, l'arrêt de transport public le plus proche, le café ou le restaurant que l'interviewé fréquente le plus souvent.

Vie en société: association(s) dont l'interviewé fait partie, responsabilités au cœur de l'association, nombre de réunions par mois auxquelles l'interviewé participe; au cours des trois derniers mois aide ou soins apportés par l'interviewé à une personne de son entourage: bénéficitaire, fréquence; participation aux votations; événements survenus dans le monde/en Suisse qui ont le plus marqué l'interviewé et pour quelle raison.

Réseau familial: types et fréquence des relations, fréquence des services rendus à un membre de la famille: faire le ménage, préparation de repas, courses, etc.; personne de la famille qui profite le plus de ces services; fréquence des services obtenus d'un membre de la famille; existence d'au moins une personne de la famille de l'interviewé qui: lui demande son avis, aime passer de longs moments avec lui/elle, a une affection profonde pour lui/elle, etc.; membres de la famille auxquels l'interviewé(e) est le plus attaché(e): lieu de résidence, fréquence des contacts.

Réseau social: fréquence des services rendus à un/e ami/e, connaissance, voisin/e: faire le ménage, repas, bricolage, etc.; fréquence des services obtenus du réseau social: apporter ou préparer des repas, aide dans la toilette, etc.; nombre des amis et connaissances auxquels l'interviewé/e est très attaché(e); résidence et fréquence de contact avec les deux amis les plus proches; évaluation des rapports humains personnels.

Activités et loisirs: pratique actuelle de diverses activités: sport, aller au café/restaurant, aller au cinéma/concert/théâtre, etc.; personnes qui partagent principalement ces activités, activités pratiquées dans des clubs ou associations.

Situation financière: revenu individuel; fortune; source(s) du revenu: salaire, retraite, intérêts d'épargne, revenu d'une location, etc.; évaluation du montant du revenu des retraités en comparaison avec celui des personnes actives.

Opinions: sur la retraite: l'estime pour les personnes retraitées en comparaison avec les actives, la condition économique; qualités les plus importantes que les parents devraient chercher à encourager chez leurs enfants; problèmes les plus importants pour la Suisse; estimation du degré de liberté dans les choix de la propre vie, etc.

Interviewer: habitation de l'interviewé: type, lieu de résidence, étage, nombre de logements dans le bâtiment; accès à l'habitation: escalier, ascenceur; évaluation de l'interviewé: problèmes de mémoire, difficultés d'audition, difficultés d'élocution, autre problème de santé, attitude de l'interviewé pendant l'interview; présence d'un tiers pendant à l'entretien, intervention du tiers, influence de l'intervention.

## Univers de référence

Personnes âgées de 60 ans et plus et vivant en Valais central et à Genève.

## Échantillonnage

Tirage aléatoire dans la population âgées de 60 ans et plus, en stratifiant selon la région (deux régions: canton de Genève et Valais central), le sexe et la classe d'âge quinquennale.

Pour la région «Genève», la population provient (après tirage aléatoire) de tout le canton de Genève, pour la région «Valais central», la population provient (après tirage aléatoire également) des quatre districts de Conthey, Hérence, Sion et Sierre.

Les non-réponses et les refus ont été remplacés par la méthode «des jumeaux» (remplacement d'une personne de l'échantillon original qui se révèle impossible à joindre par une autre, présentant les mêmes caractéristiques connues sur la base des données des registres de population).

Participation: échantillon de 2101 individus, dont 1071 à Genève et 1030 en Valais central. Les différentes sous-populations, en fonction de l'âge, du lieu de domicile et de l'aptitude à répondre au questionnaire, sont présentées dans [un document annexe](trans_1994_annexe_souspopulations.pdf).

## Récolte des données

Instrument: questionnaire. Une moitié du questionnaire a été remplie par la personne âgée seule, l'autre l'a été au cours d'une entrevue avec un·e enquêteur/trice.

Les enquêteurs/trices ont été mandatés et formés par le CIG.

Date du relevé: entre février et juillet 1994.

## Métadonnées

* Relevé effectué par: Université de Genève, CIG.
* Documentation: questionnaires, codebook, annexes, liste de publications.
* Cas: 2101.
* Variables: 1460.
* Format: CSV + syntaxe SPS pour importation dans SPSS.

## Licence

[CC-BY-SA (Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International)](LICENSE)

## Rapport de traitement

### Réalisation

* [SIDOS](http://forscenter.ch/) – 2005
* [CIGEV](https://cigev.unige.ch/) (Grégoire Métral) – 2019-2020

### Mini-contrôle

#### Remarques générales
Les données dont il est question ici (trans_1994_cig) font partie d'une enquête plus vaste, qui comprend d'autres sous-projets. D'une part, une enquête a déjà été réalisée en 1979 avec le même type de population (voir trans_1979_gugrispa). Ces données sont également archivées chez FORS. D'autre part, une étude longitudinale, Swiss Interdisciplinary Longitudinal Study on the Oldest Old (SWILSOO), a débuté en 1994 avec une cohorte issue du jeu de données présenté ici et s'est poursuivie jusqu'en 2004 (les 5 premières vagues sont disponibles chez FORS). Une troisième enquête transversale, Vivre-Leben-Vivere (VLV) a été réalisée en 2011, suivie d'une étude longitudinale (VLV2) en 2017 basée sur une partie des répondant·e·s de 2011.

#### Description du projet et de la méthode
La description du projet de recherche et de la méthode se trouvent dans l'ouvrage *Vieillesses au fil du temps*, chapitre 1 (voir la première référence des [publications](#publications) ci-dessous). Un autre ouvrage a été écrit pour décrire plus en détail le déroulement de l'enquête: *Journal d'une enquête* (voir la seconde référence des [publications](#publications) ci-dessous).

Des extraits de ces ouvrages se trouvent dans une annexe au format PDF.

#### Questionnaires
Les données ont été collectées uniquement en Suisse romande et il n'existe donc que des questionnaires en français – mais ceux-ci sont disponibles dans différentes versions, adaptées à l'âge et à la situation de vie des répondants. Les différentes versions sont destinées: aux personnes à leur domicile aptes à répondre au questionnaire, aux personnes à leur domicile mais inaptes à répondre seules au questionnaire, ainsi qu'aux personnes en institution (aptes ou non-aptes). De plus, le questionnaire pour les personnes aptes à domicile a fait l'objet d'une version légèrement simplifiée pour les personnes de plus de 80 ans. Pour toutes les personnes aptes à répondre au questionnaire, ce dernier a été subdivisé en deux fascicules (auto-administré et entretien face-à-face).

Au final, il existe 7 documents correspondant aux variantes suivantes (les liens mènent directement à ces questionnaires):
* [Q1 (bleu)](trans_1994_q1bleu.pdf): questionnaire auto-administré, personnes aptes à domicile
* [Q1 (vert)](trans_1994_q1vert.pdf): questionnaire face-à-face (avec enquêteur/trice), personnes aptes à domicile
* [Q2 (bleu)](trans_1994_q2bleu.pdf): version simplifiée de Q1 bleu, personnes de plus de 80 ans, avec enquêteur/trice
* [Q2 (vert)](trans_1994_q2vert.pdf): version simplifiée de Q1 vert, personnes de plus de 80 ans, avec enquêteur/trice
* [Q3](trans_1994_q3.pdf): questionnaire auto-administré, personnes en institution
* [Q4](trans_1994_q4.pdf): questionnaire rempli par le personnel pour une personne dans une institution
* [Q5](trans_1994_q5.pdf): questionnaire rempli par un·e proche pour une personne à domicile

Dans le langage des responsables de l'enquête, les 5 variantes du questionnaire sont nommées:
* Q1: domicile apte moins de 80 ans
* Q2: domicile apte plus de 80 ans
* Q3: institution apte
* Q4: institution non-apte
* Q5: domicile non-apte (proxi)

Les deux parties du questionnaire Q1 constituent le modèle de base pour tous les questionnaires. Dans les questionnaires Q2, Q3, Q4 et Q5, certaines questions sont supprimées, modifiées ou ajoutées. La [liste détaillée des variables et différences pour chaque questionnaire](trans_1994_annexe_variablesquestionnaires.ods) est mise à disposition par l'équipe de recherche. Afin de présenter ces variations, chaque variable est précédée d'une ou plusieurs lettres qui permettent de savoir rapidement dans quel questionnaire on se trouve (voir [Explication des noms de variables](#explication-des-noms-de-variables) ci-dessous).

#### Instructions aux enquêteurs/trices
Les questionnaires sont agrémentés de nombreuses instructions pour les enquêteurs/trices. Une formation avait également été donnée insistant sur ces points. L'ouvrage *Journal d'une enquête* revient en détail sur le processus d'encadrement des enquêteurs (p. 69-71 et p. 157).

#### Matériel annexe
Dans les questionnaires administrés par l'enquêteur, des cartes sont mentionnées. Malheureusement ces cartes ne sont plus à notre disposition. Toutefois, les catégories sont indiquées dans le codebook.

#### Évaluation de la documentation
La documentation globale est suffisante.

#### Évaluation du ou des fichier(s) de données
Le jeu de données fourni pas les chercheurs ne suivait pas la logique des questionnaires. Les variables ont été réordonnées partiellement par le SIDOS, puis le nettoyage a été appronfondi par le CIGEV.

L'état général des données est bon.

Les questionnaires papier sont encore disponibles. Au cours de la vérification, il est apparu que certains questionnaires semblent manquants; dans certains cas, le questionnaire se trouve dans les archives SWILSOO (étude longitudinale se basant sur un sous-échantillon de cette enquête transversale); il est possible que dans d'autres cas, les questionnaires soient mal classés; pour le reste, il est possible que des questionnaires soient perdus; si les qusetionnaires sont un jour systématiquement scannés et correctement nommés, on retrouvera peut-être des informations.

### Variables

Le SIDOS a reçu un jeu de données de bonne qualité, avec des annexes (codebook, explications sur les noms des variables, comparaison 1979-1994, etc.) permettant de bien appréhender l'étude. Sur la base des premiers contrôles effectués, le CIGEV a complété le nettoyage des variables et des valeurs.

Une [la liste thématique des variables](trans_1994_annexe_listethematiquevariables.pdf) a été fournie par l'équipe de recherche.

#### Explication des noms de variables
Les deux parties du questionnaire Q1 constituent le modèle de base pour tous les questionnaires. Dans les questionnaires Q2, Q3, Q4 et Q5, certaines questions sont supprimées, modifiées ou ajoutées. Afin de présenter ces variations, chaque variable est précédée d'une ou plusieurs lettres qui permettent de savoir rapidement dans quel questionnaire on se trouve:

* b: Q1 première partie (bleu)
* v: Q1 deuxième partie (vert)
* i: seulement dans Q3
* j: seulement dans Q4
* d: seulement dans Q5
* s: dans Q1 et Q2 (donc domicile apte, tout âge)
* u: dans Q1 et Q3 (auto-administré, apte)
* m: dans Q1, Q2 et Q3 (personnes aptes)
* p: dans Q1, Q2 et Q5 (personnes à domicile)
* n: dans Q1, Q2, Q3 et Q4
* l: dans Q1, Q2, Q3 et Q5
* o: dans Q3 et Q4 (personnes en institution)
* k: dans Q3 et Q5
* q: dans Q3, Q4 et Q5
* r: dans Q4 et Q5 (personnes non-aptes)
* t: dans tous (parfois aucune mention)

Les numéros correspondent aux numéros des questions du questionnaire (les questionnaires auto-administrés et face-à-face commencent tous deux avec la question numéro 1). Comme les variables du jeu de données suivent les questionnaires, il est assez facile de s'y retrouver.

Dans les cas où, dans le questionnaire, il y a deux sortes de sous-questions pour la même question, on a ajouté des a, b, c... Tous les noms de variables ont été corrigés par des noms ne comportant que des caractères non accentués en minuscules et des chiffres.

Nous avons gardé la variable tb172a et supprimé les variables mb172a et d172a correspondantes, qui n'affichaient que les réponses pour certains questionnaires (il est précisé dans le jeu de données à quel questionnaire les personnes répondent); voir les questionnaires pour les libellés qui varient sensiblement (idem pour 173).

Pour la variable v35: seules les 15 personnes ayant répondu "oui" à la question 34 devraient répondre; or 12 parmi elles répondent, mais à cela s'ajoutent 26 personnes ayant soit répondu "non", soit n'étant pas parmi les répondants de la question 34...

Pour les questionnaires verts (face-à-face), les questions en 2 parties ont fait l'objet d'un renommage pour les noms de variables, afin que le suffixe "a" soit appliqué à la partie de base de la question et le suffixe "b" à la partie complémentaire (p. ex. âge de la retraite); cela concerne les questions 36, 38, 47, 50, 54, 56, 58, 65, 72, 75, 78, etc.

Éliminé la variable sv40i (toutes les réponses à "0": la possibilité supplémentaire n'a pas été mentionnée. Même chose pour les questions (face-à-face) 49, 52, 73, 74, 76: les variables v49g, v52g, v73h, v74g, v76g, v87h ont été éliminées car la modalité supplémentaire proposée, "besoin entreprise", n'a pas été mentionnée ni codée.

Les noms des variables nbvismed et nbvisden ont été changés en sv105ar et sv105br (pour suivre la logique de la question 107). Cela concerne la question 105 des questionnaires verts.

À partir des variables issues des questions Q3 et suivants, il semble que ne sont présentes que les variables complétant les autres questionnaires (p. ex. la langue parlée dans la jeunesse, i2b, est indiquée "2e langue", donc la première langue doit correspondre à la variable tb2 présente plus haut); dans le questionnaire, ces variables sont surlignées en bleu (sauf exceptions ou oublis, p. ex. Q4, questions 172-173).

Les questions des dates font l'objet de variables peu précises. Nous avons systématiquement corrigé (pour utiliser une date sur 8 caractères) ou complété (pour avoir plusieurs variables indiquant le mois et l'année, comme pour la variable i91).

#### Présence des labels de variables
Avec la syntaxe fournie, tous les labels de variables sont indiqués sous forme du libellé de la question, repris du questionnaire. Les labels sont parfois tronqués, mais la formulation complète peut etre retrouvée dans le questionnaire correspondant.

Dans les questionnaires proxi, les questions sur les enfants ne sont parfois pas reformulées ("votre fils/fille" plutôt que "son fils/fille" puisqu'on parle à un proxi); nous avons corrigé partiellement dans le jeu de données, indiquant des labels qui peuvent dès lors différer des questionnaires papier.

#### Variables construites et corrections
Catégories socioprofessionnelles: le choix des catégories et leur numération est expliqué dans
l'annexe [trans_1994_annexe_csp.pdf](trans_1994_annexe_csp.pdf).

De nombreuses autres corrections pour des individus particuliers ont été appliquées si les données étaient manifestements erronnées, après vérification dans les questionnaires papier (et dans la mesure du possible).

#### Variables contextuelles
Les variables du début du jeu de données: type de questionnaire, canton, sexe, classe d'âge, date de l'entretien.

#### Variable(s) de pondération
Il y a une variable de pondération à la fin de l'enregistrement. Sa construction et son application sont abordées dans le livre "Journal d'une enquête" (p. 129-131). Nous [citons ci-après](#pondération) les éléments principaux.

Le nom de la variable signifie: "pond" pour la pondération (pondération), "rep" pour les répondants et "5" représente la version du coefficient. La variable pondrep5 est la dernière variable de l'ensemble de données.

#### Variables disparues
Les réponses des questions ouvertes ne sont pas systématiquement codés, et ne sont en tout cas pas transcrits. Dans la mesure où l'essentiel des questionnaires papier sont encore disponibles, il sera possible de récupérer ces informations en cas de besoin.

Quelques valeurs indiquées dans les rubriques "autre" ont été codées conformément aux [instructions données](#questions-autres).

### Valeurs

Tous les labels de valeurs ont été insérés selon le questionnaire.

Dans la mesure du possible, nous avons essayé de systématiser les codes "non/oui" (ou "non/autre chose") en mettant le code "0" pour "non" et "1" pour "oui". Cela concerne les question (questionnaires verts) numéro 10, 12, 16, 18, 31, 34, 37, 71, 79, 91, 94, 96-99, 106, 108, 111, 131, 139, etc.

Les filtres («si oui, passer à la question xxx») ont été pris en compte dans la mesure du possible et les INAP ont été recodés.

Les codes sauvages constatés ont été corrigés ou mis en missing (voir ci-dessous). P. ex. EGO prend une retraite anticipée à 84 ans...

Pour les mesures (scale, ordinal, nominal): le type a été indiqué systématiquement.

Quelques tests de plausibilité ont été effectués, notamment sur les années, mais pas systématiquement.

Tous les codes de lieux ont été vérifiés et (re-)codés. Cela concerne les questions nb3/Q1b-Q2b, v6/Q1v-Q2v, i93/Q3, j12c/Q4 j23c/Q4 j34c/Q4 j45c/Q4, d93/Q5). Les codes inconnus (ou questionnaire absent ou pas trouvé) ont été recodés en -9.

Un certain nombre de valeurs ne sont pas documentées et il conviendrait de reprendre les questionnaires papier. Par exemple:

* variable sb197: valeur "6" non documentée
* variable sb198: valeur "5" non documentée
* variable sb209: valeurs "5" et "6" non documentées
* variable sb219: valeur "6" non documentée
* variable sb220: valeur "5" non documentée

Pour certaines variables, nous avons recréé des labels de valeurs en fonction des valeurs et des indications des questionnaires (p. ex. variable v14 pour les valeurs 3 et 4).

Les établissements hospitaliers de la question 110 comportaient souvent des dénominations peu précises dans le document fourni. Nous avons transformé en essayant de reprendre les dénominations qui devaient exister en 1994 (p. ex. "Hôpital cantonal de Genève", qui n'était pas encore les HUG), et fusionné quelques catégories comme suggéré sur la feuille manuscrite; le code 80 n'était pas documenté (il s'agit de la clinique La Lignière). Peut-être faudrait-il passer à travers les codes 99 pour voir de quoi il s'agit. Le résultat est présenté dans [l'annexe ci-dessous](#établissements-hospitaliers-retenus).

Question 141 du questionnaire vert: renommé la dernière variable mv141bb en mv141ab (pour faire suite à mv141aa).

Pour les variables i1_18, i1_19 et i1_21 (premier enfant, idem pour les enfants suivants): les codes ne sont pas documentés; dans le questionnaire, les codes sont partiels (il y en a davantage dans le jeu de données); une vérification plus systématique serait intéressante.

Pour les variables d1_18 et d2_18: trop de catégories par rapport au questionnaire (il faudrait vérifier avec les questions correspondantes dans les autres questionnaires).

Nous avons conservé les variables construites intéressantes, mais certaines comportaient également des valeurs manifestement fausses. P. ex. agemari1 (âge au mariage): les données 0 et 1 ont été recodées en -9 (idem pour variable mb5 qui était trop proche de tb1).

#### Missing
Les non réponses ou données manquantes n'étaient pas documentées. Nous les avons systématiquement indiquées avec des valeurs négatives selon la logique suivante:

* -1 = Ne sait pas (NSP) – dans les rares cas où un item "ne sait pas" était précisé, nous l'avons converti en "-1"
* -2 = Non réponse (NR)
* -7 = Inapproprié (INAP), en raison d'un filtre
* -9 = Valeur manquante non documentée (Missing)

Souvent, seule une croix était demandée si le cas s'appliquait (p. ex. dans les choix multiples). L'absence de croix peut dès lors signifier autant "non" que "ne sait pas" ou "pas de réponse". Les codes rencontrés dans ces cas sont parfois "0", parfois "-2", parfois des SYSMIS. La distinction entre "0" et "-2" n'est pas évidente, et n'a pas été documentée par l'équipe de recherche.

De nombreuses questions filtres n'ont manifestement pas été respectées. Un passage systématique serait souhaitable: il reste de nombreux codes -2 (NR) qui devraient être corrigés en -7 (INAP), notamment en fonction du questionnaire et des filtres internes.

D'autres valeurs négatives sont présentes lorsque la valeur indiquée était manifestement erronnée.

La syntaxe fournie permet de fixer en "missing" toutes les valeurs négatives.

## Codebook

* [trans_1994_codebook.ods](trans_1994_codebook.ods) – codebook de ce jeu de données
* [trans_1994_annexe_codebook79-94.ods](trans_1994_annexe_codebook79-94.ods) – codebook annexe permettant la comparaison entre les enquêtes de 1979 et 1994

## Publications

### Rapports

* Lalive d'Épinay, Christian; Bickel, Jean-François; Maystre,Carole; Vollenwyder, Nathalie. (2000). *Vieillesses au fil du temps. 1979-1994. Une révolution tranquille*. Lausanne: Réalités sociales.
* Bétemps, Christine; Bickel, Jean-François; Brunner, Matthias; Hummel, Cornelia. (1997). *Journal d'une enquête: La récolte des données dans le cadre d'une recherche sur échantillon aléatoire*. Lausanne: Réalités sociales.

### Autres publications sélectionnées

* Lalive d'Epinay, Christian; Bickel, Jean-François; Maystre, Carole; Riand, Jean-François; Vollenwyder, Nathalie. (1997). Les personnes âgées à Genève, 1979-1994. *Cahier de la santé, 8*, Genève, DASS.
* Lalive d'Épinay, Christian; Maystre, Carole; Bickel, Jean-François; Hagmann, Hermann-Michel; Michel, Jean-Pierre; Riand, Jean-François. (1997). Un bilan de santé de la population âgée: comparaison entre deux régions de Suisse et analyse des changements sur quinze ans (1979-1994). *Cahiers médico-sociaux, 41*, 109-131.
* Spini, Dario, Lalive d'Épinay, Christian. (2000). A comparison of attitudes towards self, life and society among Swiss elders between 1979 and 1994: Disentangling aging and sociocultural factors. *Comparer ou prédire: exemples de recherches comparatives en psychologie aujourd'hui*, Fribourg: Ed. universitaires, 177-186.

## Annexes

### Professions et catégories socio-professionnelles (PCS), nomenclature INSEE

Voir aussi les [explications sur la catégorie socio-professionnelle en 1994, en comparaison avec l'enquête de 1979](trans_1994_annexe_csp.pdf).

    10	Agriculteurs exploitants
    11	Agriculteurs exploitants sur petite exploitation
    12	Agriculteurs exploitants sur moyenne exploitation
    13	Agriculteurs exploitants sur grande exploitation
    20	Artisans, commerçants et chefs d'entreprise
    21	Artisans
    22	Commerçants et assimilés
    23	Chefs d'entreprise de 10 salariés ou plus
    30	Cadres et professions intellectuelles supérieures
    31	Professions libérales
    33	Cadres de la fonction publique
    34	Professeurs, professions scientifiques
    35	Professions de l'information, des arts et des spectacles
    37	Cadres administratifs et commerciaux d'entreprise
    38	Ingénieurs et cadres techniques d'entreprise
    40	Professions intermédiaires
    42	Instituteurs et assimilés
    43	Professions intermédiaires de la santé et du travail social
    44	Clergé, religieux
    45	Professions intermédiaires administratives de la fonction publique
    46	Professions intermédiaires administratives et commerciales d'entreprise
    47	Techniciens (sauf techniciens tertiaires)
    48	Contremaîtres, agents de maîtrise (maîtrise administrative exclue)
    50	Employés
    52	Employés civils et agents de service de la fonction publique
    53	Policiers et militaires
    54	Employés administratifs d'entreprise
    55	Employés de commerce
    56	Personnels des services directs aux particuliers
    60	Ouvriers
    62	Ouvriers qualifiés de type industriel
    63	Ouvriers qualifiés de type artisanal
    64	Chauffeurs
    65	Ouvriers qualifiés de la manutention, du magasinage et des transports
    67	Ouvriers non qualifiés de type industriel
    68	Ouvriers non qualifiés de type artisanal
    69	Ouvriers agricoles et assimilés
    70	Population inactive
    71	Anciens agriculteurs
    72	Anciens artisans, commerçants, chefs d'entreprise
    80	Autres

### Établissements hospitaliers retenus

    1	Hôpital cantonal de Genève (Genève)
    2	Hôpital de gériatrie (Thônex)
    3	Clinique La Colline (Genève)
    4	Clinique ophtalmologique (Genève)
    5	Clinique Vert-Pré (Chêne-Bougeries)
    6	Clinique de Joli-Mont (Genève)
    7	Hôpital de la Tour (Meyrin)
    8	Hôpital Beau-Séjour (Genève)
    9	Clinique Générale Beaulieu (Genève)
    10	Centre de Soins Continus (Collonge-Bellerive)
    11	Clinique des Grangettes (Chêne-Bougeries)
    12	Clinique de l'Arve (Carouge)
    13	Clinique La Lignière (Gland)
    40	Hôpital de Sion (Sion)
    41	Clinique genevoise d'altitude (Montana)
    42	Hôpital gériatrique de Gravelone (Sion)
    43	Hôpital régional de Sierre (Sierre)
    45	Clinique de Valère (Sion)
    46	Hôpital régional de Martigny (Martigny)
    50	Centre valaisan de pneumologie (Montana)
    53	Clinique de Loèche-les-Bains (Reumaclinic, Leukerbad)
    98	Étranger
    99	Autre canton

Codes fusionnés selon suggestions manuscrites:

    44	Hôpital de Champsec (Sion)
    47	Hôpital de district Sion, Hérens, Conthey (Sion)
    	=> code 40

    48	Hôpital de Sierre (Sierre)
    51	Ancien Hôpital de Sierre (Sierre)
    52	Hôpital d'arrondissement de Sierre (Sierre)
    	=> code 43

    49	Sana Val (Montana)
    	=> code 50

### Lieux de résidence

Les codes OFS ont été utilisés pour les communes de Genève et du Valais. Ils ont été complétés par des codes pour les cantons (lorsque la commune n'était pas disponible ou pas pertinente):

    7801	Zurich
    7802	Berne
    7803	Lucerne
    7804	Uri
    7805	Schwyz
    7806	Obwald
    7807	Nidwald
    7808	Glaris
    7809	Zoug
    7810	Fribourg
    7811	Soleure
    7812	Bâle-Ville
    7813	Bâle-Campagne
    7814	Schaffhouse
    7815	Appenzell Rhodes Extérieures
    7816	Appenzell Rhodes Intérieures
    7817	Saint-Gall
    7818	Grisons
    7819	Argovie
    7820	Thurgovie
    7821	Tessin
    7822	Vaud
    7823	Valais
    7824	Neuchâtel
    7825	Genève
    7826	Jura

S'y ajoutent également des codes pour les pays (la provenance de ces codes n'est pas documentée, et certains sont étranges, comme le Salvador perdu au milieu des pays d'Asie):

    Europe:
    7101	Albanie
    7102	Belgique
    7103	Bulgarie
    7104	Danemark
    7105	Allemagne
    7106	Finlande
    7107	France
    7108	Royaume-Uni
    7109	Irlande
    7110	Islande
    7111	Italie
    7112	Yougoslavie
    7113	Luxembourg
    7114	Malte
    7115	Pays-Bas
    7116	Norvège
    7117	Autriche
    7118	Pologne
    7119	Portugal
    7120	Roumanie
    7121	Suède
    7122	Suisse
    7123	Espagne
    7124	Tchécoslovaquie
    7125	Turquie
    7127	Hongrie
    7128	Chypre
    7129	Grèce
    7130	Monaco
    7131	Liechtenstein
    7132	Arménie
    7133	Azerbaïdjan
    7134	Biélorussie
    7135	Estonie
    7136	Géorgie
    7137	Lettonie
    7138	Lituanie
    7139	Moldavie
    7140	Russie
    7141	Ukraine
    7150	Départements français de l'Ain et de la Haute-Savoie

    Asie:
    7201	Afghanistan
    7203	Bangladesh
    7204	Bhoutan
    7205	Birmanie
    7206	Brunei
    7207	Taïwan
    7208	Chine
    7209	Inde
    7210	Indonésie
    7211	Irak
    7212	Iran
    7213	Israël
    7214	Japon
    7217	Jordanie
    7218	Cambodge
    7219	Qatar
    7220	Corée du Nord
    7221	Corée du Sud
    7222	Koweït
    7223	Laos
    7224	Liban
    7225	Malaisie
    7227	Mongolie
    7228	Népal
    7229	Oman
    7230	Pakistan
    7231	Philippines
    7232	Arabie Saoudite
    7234	Sri Lanka
    7235	Syrie
    7236	Thaïlande
    7237	Émirats Arabes Unis
    7238	Vietnam
    7242	Salvador
    7243	Sarawak
    7244	Singapour
    7245	Yémen du Nord
    7246	Yémen du Sud
    7247	Hong-Kong
    7248	Maldives
    7249	Kazakhstan
    7250	Kirghizstan
    7251	Ouzbékistan
    7252	Tadjikistan
    7253	Turkménistan

    Afrique:
    7301	Égypte
    7302	Algérie
    7303	Angola
    7305	Éthiopie
    7306	Bénin
    7307	Botswana
    7309	Burkina Faso
    7310	Burundi
    7312	Djibouti
    7313	Côte-d'Ivoire
    7314	Gabon
    7315	Gambie
    7316	Ghana
    7317	Guinée
    7318	Guinée-Bissau
    7319	Cameroun
    7321	Kenya
    7322	Comores
    7323	Congo
    7325	Liberia
    7326	Libye
    7327	Madagascar
    7328	Malawi
    7329	Mali
    7330	Maroc
    7331	Mauritanie
    7332	Île Maurice
    7333	Mozambique
    7334	Namibie
    7335	Niger
    7336	Nigeria
    7337	Rwanda
    7339	Zambie
    7340	Sénégal
    7341	Sierra Leone
    7342	Zimbabwe
    7343	Somalie
    7344	Afrique du Sud
    7345	Soudan
    7346	Swaziland
    7347	Tanzanie
    7348	Togo
    7350	Tchad
    7351	Tunisie
    7352	Ouganda
    7354	Zaïre
    7355	République Centrafricaine
    7356	Lesotho

    Amériques:
    7401	Argentine
    7402	Bahamas
    7403	Barbade
    7404	Belize
    7405	Bolivie
    7406	Brésil
    7407	Chili
    7408	Costa Rica
    7409	République Dominicaine
    7410	Équateur
    7413	Guatemala
    7414	Guyana
    7415	Haïti
    7416	Honduras
    7417	Jamaïque
    7418	Canada
    7419	Colombie
    7420	Cuba
    7421	Mexique
    7422	Nicaragua
    7423	Panama
    7424	Paraguay
    7425	Pérou
    7426	Sainte Lucie
    7428	Surinam
    7430	Uruguay
    7431	Venezuela
    7432	États-Unis d'Amérique
    7433	Îles Falkland
    7434	Guyane
    7435	Porto Rico

    Océanie:
    7501	Australie
    7502	Nouvelle-Zélande
    7512	Papouasie
    7513	Îles Salomon
    7600	Groenland
    7685	Trinité
    7686	Tasmanie
    7688	Réunion
    7691	Nouvelle-Calédonie
    7692	Martinique
    7693	Madère
    7695	Îles Canaries
    7696	Guadeloupe
    7697	Bornéo
    7698	Guinée Équatoriale
    7699	Alaska

    8600	Autre

### Questions "autres"

Principes généraux:
* Si un faux questionnaire a été rempli (p. ex. Q1 pour Q5), il faut retranscrire les informations dans le questionnaire approprié.
* Coder la commune dans le Qbleu p. 25 question 93 et dans le Q5 p. 9.
* Les personnes en chômage partiel sont considérées comme actives.

#### Qvert
    page 3	question 2	classer "école ménagère" sous secondaire inférieur
    page 5	question 7	rajouter une catégorie "patois" (code 7)
    page 8	question 20	rajouter une catégorie "AI" (code 5)
    page 9	question 24	les fonctionnaires sont reclassés dans la catégorie 3
    page 14	question 39	rajouter: nécessité de l'entreprise (h)
    page 15	question 40	rajouter: décès du conjoint (i)
    page 18	question 49	rajouter: nécessité de l'entreprise (g)
    page 19	question 52	rajouter: nécessité de l'entreprise (g)
    page 20	question 59	rajouter une catégorie "AI" (code 6)
    page 22	question 66	rajouter: nécessité de l'entreprise (f)
    page 25	question 73	rajouter: nécessité de l'entreprise (h)
    page 25	question 74	rajouter: nécessité de l'entreprise (h)
    page 26	question 76	rajouter: nécessité de l'entreprise (g)
    page 28	question 86	rajouter: nécessité de l'entreprise (g)
    page 29	question 87	rajouter: nécessité de l'entreprise (h)

#### Qbleu
    page 2	question 2	rajouter une catégorie "patois" (code 8)
    page 6	question 18	rajouter une catégorie "retraité-e" (code 5)
    page 7	question 19	rajouter une catégorie "retraité-e" (code 5)
    (idem pour tous les enfants)
    page 23	question 85	rajouter une catégorie "couvent-monastère" (code 6)
    (si "Home", alors remplir le questionnaire correspondant)
    page 24	question 90	rajouter la catégorie "avec un compagnon" (code 4)
    page 25	question 94	rajouter la catégorie "décès du conjoint" (k)
    page 30	questions 109-110	rajouter une catégorie "ouvrages/journaux spirituels" (code 9)
    page 51	question 176	si autres = clubs d'aînés ou assimilés, les laisser comme autres et mettre une croix sous la colonne C à la rubrique "autre"
    page 55	question 186	même situation que question 176

#### Q3
    page 3	questions 1-2	rajouter une catégorie "patois" (code 7)
    page 4	question II.2	école ménagère à reclasser sous secondaire inférieur
    page 6-9	questions *_18 et *_19	rajouter une catégorie "retraité-e" (code 4)
    page 14	question II.39	rajouter: nécessité de l'entreprise (f)
    page 33	questions 109-110	rajouter une catégorie "ouvrages/journaux spirituels" (code 9)

#### Q4
    page 5	questions 1-2	rajouter une catégorie "patois" (code 9)
    page 6	question II.2	école ménagère à reclasser sous secondaire inférieur

#### Q5
    page 4	questions 1-2	rajouter une catégorie "patois" (code 9)
    page 6-8	questions *_18 et *_19	rajouter une catégorie "retraité-e" (code 5)
    page 9	coder la commune

### Pondération

Stratification et nécessité de pondérer (tiré de *Journal d'une enquête*, p. 129-131)

Comme nous l'avons déjà mentionné dans les chapitres III et IX, nous avons fait un choix qui a d'importantes conséquences sur la représentativité de nos données: celui de stratifier notre échantillon. Dans cette troisième partie, nous voudrions mettre en évidence la nature du biais de représentativité qui est engendré par la stratification ainsi que la façon la plus commune d'y remédier. En effet, dans la mesure où nos échantillons valaisans et genevois sont stratifiés en fonction de la classe d'âge et du sexe, il est évident que leur structure générale ne correspond pas à celle des populations concernées: étant moins nombreux dans la population, les hommes et les classes d'âge supérieures sont sur-représentées par rapport à la réalité. [Nous ne donnerons pas les coefficients de pondération pour les Valaisans par rapport aux Genevois car l'analyse sera centrée sur la composition régionale: dans la mesure où nous avons affaire à une comparaison d'ordre typologique selon l'axe urbain-rural, il n'est en effet pas question d'inférer nos résultats à l'ensemble de la population helvétique, la moyenne n'étant pas égale à la moyenne entre les extrêmes.]

Nous commencerons par mesurer la sur- ou sous-représentation de nos échantillons, pour ensuite élaborer des coefficients de pondération rectifiant les biais que celle-ci introduirait dans l'inférence de nos résultats aux populations de référence.

Le tableau 12 permet de comparer le nombre de personnes interrogées et le nombre réel de personnes résidant dans les deux populations de référence.

*Tableau X-12: personnes interrogées et populations de référence (nombres absolus)*

| | GE | GE | GE | GE | GE | GE | VS | VS | VS | VS | VS | VS |
| :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| | F | F | H | H | F+H | F+H | F | F | H | H | F+H | F+H |
| Classe d'âge | Ech. | Pop. | Ech. | Pop. | Ech. | Pop. | Ech. | Pop. | Ech. | Pop. | Ech. | Pop. |
| 60-64 ans | 86 | 9775 | 83 | 8611 | 169 | 18386 | 71 | 2388 | 77 | 2159 | 148 | 4547 |
| 65-69 ans | 89 | 8652 | 84 | 6653 | 173 | 15305 | 88 | 2279 | 83 | 1857 | 171 | 4136 |
| 70-74 ans | 88 | 6487 | 85 | 4522 | 173 | 11009 | 87 | 1790 | 86 | 1321 | 173 | 3111 |
| 75-79 ans | 79 | 6436 | 91 | 3530 | 170 | 9966 | 82 | 1484 | 88 | 1015 | 170 | 2499 |
| 80-84 ans | 109 | 5421 | 102 | 2548 | 211 | 7969 | 97 | 1013 | 96 | 652 | 193 | 1665 |
| 85 ans + | 91 | 4957 | 90 | 1626 | 181 | 6583 | 90 | 710 | 89 | 315 | 179 | 1025 |
| Total | 542 | 41728 | 535 | 27490 | 1077 | 69218 | 515 | 9664 | 519 | 7319 | 1034 | 16983 |

Pour avoir une stratification correcte c'est-à-dire qui corresponde à la réalité, il convient à ce stade de donner les coefficients de pondération. Ces derniers permettront de rectifier le poids de chaque groupe l'un par rapport à l'autre et rendront possible, lors de l'analyse, une inférence de nos résultats à l'ensemble des pesronnes de 60 ans et plus résidant en Valais central ou dans le canton de Genève.

*Tableau X-13: coefficients de pondération par classe d'âge et par région*

| | GE | GE | GE | VS | VS | VS |
| :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| Classe d'âge | F | H | F+H | F | H | F+H |
| 60-64 ans | 1.769 | 1.614 | 1.693 | 2.048 | 1.707 | 1.871 |
| 65-69 ans | 1.513 | 1.232 | 1.377 | 1.577 | 1.362 | 1.473 |
| 70-74 ans | 1.147 | 0.828 | 0.990 | 1.253 | 0.935 | 1.095 |
| 75-79 ans | 1.268 | 0.604 | 0.912 | 1.102 | 0.702 | 0.895 |
| 80-84 ans | 0.774 | 0.389 | 0.588 | 0.636 | 0.414 | 0.525 |
| 85 ans + | 0.848 | 0.281 | 0.566 | 0.480 | 0.215 | 0.349 |
| Total | 1.198 | 0.799 | 1.000 | 1.142 | 0.859 | 1.000 |

Le tableau 13 présente les coefficients de pondération pour chacune des cases qui permettront de rectifier leur poids par rapport aux deux populations concernées.

Comme le montre le tableau 14, une fois les valeurs pondérées, le nombre total des personnes sélectionnées est toujours le même, seule la répartition des personnes à travers les différentes strates se trouve modifiée.

*Tableau X-14: nombre pondéré de répondants*

| | GE | GE | GE | VS | VS | VS |
| :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| Classe d'âge | F | H | F+H | F | H | F+H |
| 60-64 ans | 152 | 134 | 286 | 145 | 131 | 277 |
| 65-69 ans | 135 | 104 | 238 | 139 | 113 | 252 |
| 70-74 ans | 101 | 70 | 171 | 109 | 80 | 189 |
| 75-79 ans | 100 | 55 | 155 | 90 | 62 | 152 |
| 80-84 ans | 84 | 40 | 124 | 62 | 40 | 101 |
| 85 ans + | 77 | 25 | 102 | 43 | 19 | 62 |
| Total | 649 | 428 | 1077 | 588 | 446 | 1034 |

N.B.: Ici, il est question d'individu; avec l'arrondi il y a quelques discordances dans les additions en ligne/colonne.

Le tableau 14 met aussi en évidence la raison pour laquelle nous avons srtatifié nos échantillons valaisans et genevois par le sexe et la classe d'âge: dans cette répartition, le nombre de personnes dans les classes d'âges suipérieures et en particulier pour le sexe masculin est insuffisant pour permettre une analyse statistique valide.

Par ailleurs, lorsqu'il s'agira d'analyser nos données, il ne sera pas toujours nécessaire de travailler avec des valeurs pondérées. En effet, lors de l'analyse, deux types d'interrogations au moins opurronr se présenter: une interrogation sur l'ensemble de la population des personnes de 60 ans et plus résidant dans l'une ou l'autre des régions concernées et une interrogation sur les groupes d'âge et de sexe. La pondération sera nécessaire uniquement dans le premier cas. L'encadré qui suit explique cela plus en détail.
