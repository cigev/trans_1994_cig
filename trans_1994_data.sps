﻿* Encoding: UTF-8.
* Commande pour l'importation du fichier CSV dans SPSS

* Attention: modifier la ligne ci-dessous commençant par /FILE
* afin d'indiquer le chemin complet du jeu de données
* (p.ex. C:\Users\FooBar\trans_1994_data.csv)

* Importation du fichier

SET UNICODE=ON.
GET DATA
 /TYPE=TXT
 /ENCODING='UTF8'
 /FILE='trans_1994_data.csv'
 /DELCASE=LINE
 /DELIMITERS=','
 /QUALIFIER='"'
 /ARRANGEMENT=DELIMITED
 /FIRSTCASE=2
 /IMPORTCASE=ALL
 /VARIABLES=
icode	F5
typeq	F1
canton	F1
sexe	F1
clage	F1
dateentr	F8
tb1	F4
tb2	F1
nb3	F4
tb4	F1
mb5	F4
mb6a	F4
mb6b	F4
mb7a	F4
mb7b	F4
mb8a	F4
mb8b	F4
mb9	F4
sb10	F1
sb11	F2
mb12	F1
mb13a	F4
sb13b	F4
mb14	F1
mb15	F1
b16	F2
mb17	F2
sb18	F1
sb19	F1
sb20	F1
sb21	F1
b22	F1
mb23	F1
mb24a	F4
sb24b	F4
mb25	F1
mb26	F1
b27	F2
mb28	F2
sb29	F1
sb30	F1
sb31	F1
sb32	F1
b33	F1
mb34	F1
mb35a	F4
sb35b	F4
mb36	F1
mb37	F1
b38	F2
mb39	F2
sb40	F1
sb41	F1
sb42	F1
sb43	F1
b44	F1
mb45	F1
mb46a	F4
sb46b	F4
mb47	F1
mb48	F1
b49	F2
mb50	F2
sb51	F1
sb52	F1
sb53	F1
sb54	F1
b55	F1
b56a	F1
b56b	F4
b56c	F4
b57	F1
b58	F1
b59a	F1
b59b	F1
b59c	F1
b59d	F1
b59e	F1
b59f	F1
b60	F1
b61	F1
b62a	F1
b62b	F4
b62c	F4
b63	F1
b64	F1
b65a	F1
b65b	F1
b65c	F1
b65d	F1
b65e	F1
b65f	F1
b66	F1
b67	F1
b68a	F1
b68b	F4
b68c	F4
b69	F1
b70	F1
b71a	F1
b71b	F1
b71c	F1
b71d	F1
b71e	F1
b71f	F1
b72	F1
b73	F1
b74a	F1
b74b	F4
b74c	F4
b75	F1
b76	F1
b77a	F1
b77b	F1
b77c	F1
b77d	F1
b77e	F1
b77f	F1
b78	F1
b79	F1
sb80	F2
sb81a	F2
sb81b	F2
sb81ab	F2
sb82	F2
sb83a	F2
mb83b	F2
mb83c	F2
mb83d	F2
mb83e	F2
mb83f	F2
mb83g	F2
sb84	F1
pb85	F1
pb86	F1
pb87	F2
pb88a	F1
pb88b	F1
q88c	F1
pb88d	F1
pb88e	F1
pb88f	F1
pb88g	F1
sb89	F1
sb90	F1
sb91	F1
sb92	F1
sb93	F4
pb94a	F1
pb94b	F1
pb94c	F1
pb94d	F1
pb94e	F1
pb94f	F1
pb94g	F1
pb94h	F1
pb94i	F1
pb94j	F1
sb94k	F1
pb95	F1
mb96a	F1
mb96b	F1
mb96c	F1
mb96d	F1
mb96e	F1
mb96f	F1
mb97a	F1
mb97b	F1
sb97c	F1
pb98a	F1
pb98b	F1
pb98c	F1
pb98d	F1
pb98e	F1
pb98f	F1
pb98g	F1
pb98h	F1
sb98i	F1
pb98j	F1
sb99	F1
sb100a	F1
sb100b	F1
sb100c	F1
sb100d	F1
sb100e	F1
sb100f	F1
sb100g	F1
sb100h	F1
sb100i	F1
sb100j	F1
sb100k	F1
sb101a	F1
sb101b	F1
sb101c	F1
sb101d	F1
sb101e	F1
sb101f	F1
sb101g	F1
sb101h	F1
sb101i	F1
sb101j	F1
sb101k	F1
sb102a	F1
sb102b	F1
sb102c	F1
sb102d	F1
tb103	F1
sb104	F1
tb105	F1
tb106	F1
mb107a	F1
mb107b	F1
mb107c	F1
mb107d	F1
mb107e	F1
mb107f	F1
mb108a	F1
mb108b	F1
mb108c	F1
mb108d	F1
mb108e	F1
mb108f	F1
mb109a	F1
mb109b	F1
mb109c	F1
mb109d	F1
mb109e	F1
mb109f	F1
mb109g	F1
mb109h	F1
mb110a	F1
mb110b	F1
mb110c	F1
mb110d	F1
mb110e	F1
mb110f	F1
mb110g	F1
mb110h	F1
mb110i	F1
sb111	F1
sb112	F1
sb113	F1
sb113a	F1
sb113b	F1
sb113c	F1
sb113d	F1
sb114	F1
b115	F4
b116a	F1
b116b	F1
b116c	F1
b116d	F1
tb117a	F1
tb117b	F1
tb117c	F1
tb117d	F1
mb118a	F1
mb118b	F1
mb118c	F1
mb119a	F1
mb119b	F1
mb119c	F1
b120	F1
b121	F4
b122	F1
b123	F4
mb124	F1
b126	F1
b127	F2
b128	F4
b129	F1
b130	F1
b131	F1
tb132aa	F1
tb132ab	F1
tb132ac	F1
tb132ad	F1
tb132ae	F1
tb132af	F1
tb132ag	F1
tb132ah	F1
tb132ai	F1
tb132aj	F1
tb132ak	F1
sb132ba	F1
sb132bb	F1
sb132bc	F1
sb132bd	F1
sb132be	F1
sb132bf	F1
sb132bg	F1
sb132bh	F1
sb132bi	F1
sb132bj	F1
sb132bk	F1
tb133	F1
tb134	F1
tb135	F1
tb136	F1
tb137	F1
tb138	F1
tb139	F1
tb140	F1
tb141	F2
sb142	F1
tb143	F2
nbmedic	F1
mb144	F1
sb146	F1
sb147a	F2
sb147b	F2
sb147c	F2
sb147d	F2
sb148	F1
tb149	F1
tb150	F1
tb151	F1
tb152	F1
tb153	F1
tb154	F1
tb155	F1
tb156	F1
tb157	F1
tb158	F1
tb159	F1
sb160	F1
sb161a	F1
sb161b	F1
sb161c	F1
sb161d	F1
sb161e	F1
sb161f	F1
sb161g	F1
sb161h	F1
sb161i	F1
sb161j	F1
sb162a	F1
sb162b	F1
sb162c	F1
sb162d	F1
sb162e	F1
sb162f	F1
sb162g	F1
mb163	F1
mb164	F1
mb165	F1
mb166	F1
mb167	F1
sb168a	F1
sb168b	F1
pb169aa	F1
pb169ab	F1
pb169ac	F1
pb169ad	F1
pb169ae	F1
pb169af	F1
pb169ag	F1
pb169ah	F1
pb169ba	F1
pb169bb	F1
pb169bc	F1
pb169bd	F1
pb169be	F1
pb169bf	F1
pb169bg	F1
pb169bh	F1
sb170aa	F1
sb170ab	F1
sb170ac	F1
sb170ad	F1
sb170ae	F1
sb170af	F1
sb170ag	F1
sb170ah	F1
sb170ba	F1
sb170bb	F1
sb170bc	F1
sb170bd	F1
sb170be	F1
sb170bf	F1
sb170bg	F1
sb170bh	F1
mb171a	F1
mb171b	F1
mb171c	F1
mb171d	F1
mb171e	F1
mb171f	F1
tb172a	F1
mb172b	F1
lb172c	F1
mb172d	F1
mb172e	F1
mb172f	F1
tb173a	F1
mb173b	F1
lb173c	F1
mb173d	F1
mb173e	F1
b174	F1
sb175a	F1
sb175b	F1
sb175c	F1
sb175d	F1
sb175e	F1
sb176aa	F1
sb176ba	F1
sb176ca	F1
sb176ab	F1
sb176bb	F1
sb176cb	F1
sb176ac	F1
sb176bc	F1
sb176cc	F1
sb176ad	F1
sb176bd	F1
sb176cd	F1
sb176ae	F1
sb176be	F1
sb176ce	F1
sb176af	F1
sb176bf	F1
sb176cf	F1
sb176ag	F1
sb176bg	F1
sb176cg	F1
sb176ah	F1
sb176bh	F1
sb176ch	F1
sb176ai	F1
sb176bi	F1
sb176ci	F1
sb176aj	F1
sb176bj	F1
sb176cj	F1
sb176ak	F1
sb176bk	F1
sb176ck	F1
sb176al	F1
sb176bl	F1
sb176cl	F1
sb176am	F1
sb176bm	F1
sb176cm	F1
sb177	F1
b178	F1
b179	F1
b180	F1
b181	F1
sb182aa	F1
sb182ba	F1
sb182ca	F1
b182ab	F1
b182bb	F1
b182cb	F1
sb182ac	F1
sb182bc	F1
sb182cc	F1
sb182ad	F1
sb182bd	F1
sb182cd	F1
sb182ae	F1
sb182be	F1
sb182ce	F1
sb182af	F1
sb182bf	F1
sb182cf	F1
sb182ag	F1
sb182bg	F1
sb182cg	F1
sb183aa	F1
sb183ba	F1
sb183ab	F1
sb183bb	F1
sb183ac	F1
sb183bc	F1
sb183ad	F1
sb183bd	F1
sb184	F1
b185	F1
b186aa	F1
b186ba	F1
b186ab	F1
b186bb	F1
b186ac	F1
b186bc	F1
b186ad	F1
b186bd	F1
b186ae	F1
b186be	F1
b186af	F1
b186bf	F1
b186ag	F1
b186bg	F1
b186ah	F1
b186bh	F1
b186ai	F1
b186bi	F1
b186aj	F1
b186bj	F1
b186ak	F1
b186bk	F1
b186al	F1
b186bl	F1
b186am	F1
b186bm	F1
b187a	F1
b187b	F1
b187c	F1
b187d	F1
b187e	F1
b187f	F1
b187g	F1
b187h	F1
b187i	F1
b187j	F1
b187k	F1
b187l	F1
b187m	F1
tb188	F1
mb191	F1
mb192a	F4
sb192b	F4
mb193	F1
mb194	F1
sb195	F2
mb196	F2
sb197	F1
sb198	F1
sb199	F1
sb200	F1
sb201	F1
mb202	F1
mb203a	F4
sb203b	F4
mb204	F1
mb205	F1
sb206	F2
mb207	F2
sb208	F1
sb209	F1
sb210	F1
sb211	F1
sb212	F1
mb213	F1
mb214a	F4
sb214b	F4
mb215	F1
mb216	F1
sb217	F2
mb218	F2
sb219	F1
sb220	F1
sb221	F1
sb222	F1
sb223	F1
mb224	F1
mb225a	F4
sb225b	F4
mb226	F1
mb227	F1
sb228	F2
mb229	F2
sb230	F1
sb231	F1
sb232	F1
sb233	F1
sb234	F1
mv1	F2
tv2	F1
mv3a	F1
mv3b	F1
mv3c	F1
mv3d	F1
mv3e	F1
mv4	F4
mv5	F2
v6	F4
sv7	F1
v8	F1
v9	F1
v10	F1
v11	F1
v12	F1
v13	F1
v14	F1
v15	F1
v16	F1
v17	F1
v18	F1
v19	F1
sv20	F1
v21	F1
sv22	F2
sv23	F1
sv24	F1
sv25	F1
sv26	F1
v27a	F1
v27b	F1
v27c	F1
v27d	F1
v27e	F1
v27f	F1
v28a	F1
v28b	F1
v29	F1
v30	F2
v31	F1
v32a	F1
v32b	F1
v32c	F1
v32d	F1
v32e	F1
mv33	F1
v34	F1
v35	F1
v36a	F1
v36b	F2
v37	F1
mv38a	F1
mv38b	F2
mv39a	F1
mv39b	F1
mv39c	F1
mv39d	F1
mv39e	F1
mv39f	F1
mv40a	F1
mv40b	F1
mv40c	F1
mv40d	F1
mv40e	F1
mv40f	F1
mv40g	F1
mv40h	F1
v41a	F1
v41b	F1
v41c	F1
v42	F1
v43	F1
v44a	F1
v44b	F1
v44c	F1
v44d	F1
v44e	F1
v44f	F1
v44g	F1
v45a	F1
v45b	F1
v45c	F1
v45d	F1
v45e	F1
v45f	F1
v45g	F1
v46	F1
v47a	F1
v47b	F2
v48a	F1
v48b	F1
v48c	F1
v48d	F1
v48e	F1
v48f	F1
v48g	F1
v49a	F1
v49b	F1
v49c	F1
v49d	F1
v49e	F1
v49f	F1
v50a	F1
v50b	F2
v51a	F1
v51b	F1
v51c	F1
v52a	F1
v52b	F1
v52c	F1
v52d	F1
v52e	F1
v52f	F1
v53	F1
v54a	F1
v54b	F2
v55	F1
v56a	F1
v56b	F1
v57	F1
v58a	F1
v58b	F2
sv59	F1
sv60	F2
sv61	F1
sv62	F1
sv63	F1
sv64	F1
sv65a	F1
sv65b	F2
v66a	F1
v66b	F1
v66c	F1
v66d	F1
v66e	F1
v66f	F1
v67a	F1
v67b	F1
v67c	F1
v67d	F1
v67e	F1
v67f	F1
v67g	F1
v67h	F1
v68	F1
v69	F1
v70a	F1
v70b	F1
v70c	F1
v70d	F1
v71	F1
v72a	F1
v72b	F2
v73a	F1
v73b	F1
v73c	F1
v73d	F1
v73e	F1
v73f	F1
v73g	F1
v74a	F1
v74b	F1
v74c	F1
v74d	F1
v74e	F1
v74f	F1
v75a	F1
v75b	F2
v76a	F1
v76b	F1
v76c	F1
v76d	F1
v76e	F1
v76f	F1
v77	F1
v78a	F1
v78b	F2
v79	F1
v80	F1
mv81	F1
mv82	F4
mv83	F1
mv84	F1
mv85	F1
v86a	F1
v86b	F1
v86c	F1
v86d	F1
v86e	F1
v86f	F1
v86g	F1
v87a	F1
v87b	F1
v87c	F1
v87d	F1
v87e	F1
v87f	F1
v87g	F1
mv88	F1
mv89	F1
mv90	F1
mv91	F1
mv92a	F1
uv92b	F1
mv92c	F1
mv92d	F1
mv92e	F1
mv92f	F1
mv92g	F1
mv92h	F1
mv93a	F1
mv93b	F1
mv93c	F1
mv93d	F1
mv93e	F1
mv93f	F1
mv93g	F1
mv93h	F1
mv94	F1
mv95a	F1
uv95b	F1
mv95c	F1
mv95d	F1
mv95e	F1
mv95f	F1
mv95g	F1
mv95h	F1
v96	F1
v97	F1
v98	F1
v99	F1
v100	F1
mv101	F1
mv102	F1
v103a	F1
v103b	F1
v103c	F1
v103d	F1
v103e	F1
v103f	F1
v103g	F1
v103h	F1
v103i	F1
v103j	F1
v103k	F1
v103l	F1
v103m	F1
v103n	F1
v103o	F1
v103p	F1
tv104aa	F1
sv104ca	F2
tv104ab	F1
sv104cb	F2
tv104ac	F1
sv104cc	F2
sv105aa	F1
sv105ba	F2
sv105ab	F1
sv105bb	F2
sv105ar	F2
sv105br	F2
lv106	F1
sv107	F2
sv107r	F1
sv108	F1
sv109aa	F1
sv109ab	F1
sv109ac	F1
v109ad	F1
sv109ae	F1
sv109af	F1
sv109ag	F1
sv109ah	F1
sv109ai	F1
sv109aj	F1
sv109ba	F1
sv109bb	F1
sv109bc	F1
v109bd	F1
sv109be	F1
sv109bf	F1
sv109bg	F1
sv109bh	F1
sv109bi	F1
sv109bj	F1
sv109ca	F1
sv109cb	F1
sv109cc	F1
v109cd	F1
sv109ce	F1
sv109cf	F1
sv109cg	F1
sv109ch	F1
sv109ci	F1
sv109cj	F1
sv110a	F2
sv110b	F2
sv110c	F2
mv111	F1
mv112	F1
mv113	F1
sv114aa	F1
sv114ab	F1
sv114ac	F1
v114ad	F2
sv114ae	F1
sv114af	F1
sv114ag	F1
sv114ah	F1
sv114ai	F1
sv114aj	F1
sv114ak	F1
sv114ba	F1
sv114bb	F1
sv114bc	F1
v114bd	F1
sv114be	F1
sv114bf	F1
sv114bg	F1
sv114bh	F1
sv114bi	F1
sv114bj	F1
sv114bk	F1
sv114ca	F1
sv114cb	F1
sv114cc	F1
v114cd	F1
sv114ce	F1
sv114cf	F1
sv114cg	F1
sv114ch	F1
sv114ci	F1
sv114cj	F1
sv114ck	F1
sv114d	F1
mv115a	F1
mv115b	F1
mv115c	F1
mv115d	F1
mv115e	F1
mv115f	F1
mv115g	F1
mv115h	F1
mv115i	F1
mv115j	F1
mv115k	F1
mv115l	F1
mv115m	F1
tv116	F1
sv117	F1
sv118a	F1
sv118b	F1
sv118c	F1
sv118d	F1
sv118e	F1
sv118f	F1
sv118g	F1
sv118h	F1
sv118i	F1
sv118j	F1
v119a	F1
v119b	F1
v119c	F1
v119d	F1
v119e	F1
v119f	F1
sv120a	F1
sv120b	F1
sv120c	F1
sv120d	F1
sv120e	F1
sv120f	F1
sv120g	F1
sv120h	F1
v121a	F1
sv121b	F1
sv121c	F1
sv121d	F1
sv121e	F1
sv121f	F1
sv121g	F1
sv122a	F1
sv122b	F1
sv122c	F1
sv122d	F1
sv122e	F1
sv122f	F1
sv122g	F1
sv123a	F1
sv123b	F1
sv123c	F1
sv123d	F1
sv123e	F1
sv123f	F1
sv124a	F1
sv124b	F1
sv124c	F1
sv124d	F1
sv124e	F1
sv124f	F1
sv124g	F1
sv124h	F1
sv124i	F1
mv125a	F1
mv125b	F1
mv125c	F1
mv125d	F1
v125e	F1
v125f	F1
mv125g	F1
mv125h	F1
mv125i	F1
mv125j	F1
mv125k	F1
mv125l	F1
sv126a	F1
sv126b	F1
mv127a	F1
mv127b	F1
tv128	F1
sv129a	F1
sv129b	F1
sv129c	F1
sv129d	F1
sv129e	F1
sv129f	F1
sv129g	F1
sv130a	F1
sv130b	F1
sv130c	F1
sv130d	F1
sv130e	F1
sv130f	F1
sv131	F1
sv132a	F1
sv132b	F1
sv133a	F1
sv133b	F1
mv134	F1
tv135aa	F1
sv135b1a	F1
sv135b2a	F1
sv135b3a	F2
sv135b4a	F1
sv135ca	F1
v135da	F1
v135ea	F1
sv135ab	F1
sv135b1b	F1
sv135b2b	F1
sv135b3b	F1
sv135b4b	F1
sv135cb	F1
v135db	F1
v135eb	F1
tv135ac	F1
sv135b1c	F1
sv135b2c	F1
sv135b3c	F1
sv135b4c	F1
sv135cc	F1
v135dc	F1
v135ec	F1
sv135ad	F1
sv135b1d	F1
sv135b2d	F1
sv135b3d	F1
sv135b4d	F1
sv135cd	F1
v135dd	F1
v135ed	F1
sv135ae	F1
sv135b1e	F1
sv135b2e	F1
sv135b3e	F1
sv135b4e	F1
sv135ce	F1
v135de	F1
v135ee	F1
sv135af	F1
sv135b1f	F1
sv135b2f	F1
sv135b3f	F1
sv135b4f	F1
sv135cf	F1
v135df	F1
v135ef	F1
tv135ag	F1
sv135b1g	F1
sv135b2g	F1
sv135b3g	F1
sv135b4g	F1
sv135cg	F1
v135dg	F1
v135eg	F1
sv135ah	F1
sv135b1h	F1
sv135b2h	F1
sv135b3h	F1
sv135b4h	F1
sv135ch	F1
v135dh	F1
v135eh	F1
sv135ai	F1
sv135b1i	F1
sv135b2i	F1
sv135b3i	F1
sv135b4i	F1
sv135ci	F1
v135di	F1
v135ei	F1
sv135aj	F1
sv135b1j	F1
sv135b2j	F1
sv135b3j	F1
sv135b4j	F1
sv135cj	F1
v135dj	F1
v135ej	F1
sv135ak	F1
sv135b1k	F1
sv135b2k	F1
sv135b3k	F1
sv135b4k	F1
sv135ck	F1
v135dk	F1
v135ek	F1
tv135al	F1
sv135b1l	F1
sv135b2l	F1
sv135b3l	F1
sv135b4l	F1
sv135cl	F1
v135dl	F1
v135el	F1
tv135am	F1
sv135b1m	F1
sv135b2m	F1
sv135b3m	F1
sv135b4m	F1
sv135cm	F1
v135dm	F1
v135em	F1
tv135an	F1
sv135b1n	F1
sv135b2n	F1
sv135b3n	F1
sv135b4n	F1
sv135cn	F1
v135dn	F1
v135en	F1
tv135ao	F1
sv135b1o	F1
sv135b2o	F1
sv135b3o	F1
sv135b4o	F1
sv135co	F1
v135do	F1
v135eo	F1
sv135ap	F1
sv135b1p	F1
sv135b2p	F1
sv135b3p	F1
sv135b4p	F1
sv135cp	F1
v135dp	F1
v135ep	F1
sv135aq	F1
sv135b1q	F1
sv135b2q	F1
sv135b3q	F1
sv135b4q	F1
sv135cq	F1
v135dq	F1
v135eq	F1
tv135ar	F1
sv135b1r	F1
sv135b2r	F1
sv135b3r	F1
sv135b4r	F1
sv135cr	F1
v135dr	F1
v135er	F1
tv135as	F1
sv135b1s	F1
sv135b2s	F1
sv135b3s	F1
sv135b4s	F1
sv135cs	F1
v135ds	F1
v135es	F1
tv135at	F1
sv135b1t	F1
sv135b2t	F1
sv135b3t	F1
sv135b4t	F1
sv135ct	F1
v135dt	F1
v135et	F1
pv136	F1
sv137	F1
sv138a	F1
sv138b	F1
sv138c	F1
sv138d	F1
sv138e	F1
sv138f	F1
sv138g	F1
sv138h	F1
sv138i	F1
sv138j	F1
sv138k	F1
sv139	F1
sv140	F1
mv141a	F1
v141b	F1
mv141c	F1
mv141d	F1
mv141e	F1
mv141f	F1
v141g	F1
mv141h	F1
mv141i	F1
mv141j	F1
mv141k	F1
v141l	F1
mv141m	F1
mv141n	F1
mv141o	F1
mv141p	F1
mv141q	F1
mv141r	F1
v141s	F1
mv141t	F1
mv141u	F1
mv141v	F1
mv141w	F1
v141x	F1
mv141y	F1
mv141z	F1
mv141aa	F1
mv141ab	F1
sv142	F2
mv143aa	F1
mv143ab	F1
mv143ac	F1
mv143ad	F1
mv143ae	F1
mv143af	F1
mv143ag	F1
mv143ah	F1
mv143ai	F1
mv143aj	F1
mv143ba	F1
mv143bb	F1
mv143bc	F1
mv143bd	F1
mv143be	F1
mv143bf	F1
mv143bg	F1
mv143bh	F1
mv143bi	F1
mv143bj	F1
mv143ca	F1
mv143cb	F1
mv143cc	F1
mv143cd	F1
mv143ce	F1
mv143cf	F1
mv143cg	F1
mv143ch	F1
mv143ci	F1
mv143cj	F1
mv144aa	F1
mv144ab	F1
mv144ac	F1
mv144ad	F1
mv144ae	F1
mv144af	F1
mv144ag	F1
mv144ah	F1
mv144ai	F1
mv144aj	F1
mv144ba	F1
mv144bb	F1
mv144bc	F1
mv144bd	F1
mv144be	F1
mv144bf	F1
mv144bg	F1
mv144bh	F1
mv144bi	F1
mv144bj	F1
mv144ca	F1
mv144cb	F1
mv144cc	F1
mv144cd	F1
mv144ce	F1
mv144cf	F1
mv144cg	F1
mv144ch	F1
mv144ci	F1
mv144cj	F1
mv145	F1
svf1	F1
svf2	F1
svf3	F2
svf4	F1
svf5	F1
mvf6a	F1
mvf6b	F1
mvf6c	F1
mvf7	F1
mvf8	F1
mvf9	F1
mvf9a	F1
mvf9b	F1
mvf9c	F1
mvf9d	F1
mvf10	F1
mvf11	F1
svfdate	F4
ix1a	F1
ix1b	F1
i2b	F1
i10	F1
i11	F2
i1_18	F1
i1_19	F1
i1_21	F1
i2_18	F1
i2_19	F1
i2_21	F1
i3_18	F1
i3_19	F1
i3_21	F1
i4_18	F1
i4_19	F1
i4_21	F1
i5_18	F1
i5_19	F1
i5_21	F1
i6_18	F1
i6_19	F1
i6_21	F1
i7_18	F1
i7_19	F1
i7_21	F1
i8_18	F1
i8_19	F1
i8_21	F1
ix2a	F1
ix2b	F1
i81	F1
i83a	F2
i83b	F2
i83c	F2
i83d	F2
i83e	F2
i83f	F2
i83g	F2
i84	F1
i91	F1
i91mois	F2
i91annee	F4
i93	F4
i95	F1
ix3a	F1
ix3b	F1
i117d	F1
ix4	F1
i22	F2
i24	F1
i25	F1
i26	F1
ix5	F1
i60	F2
i62	F1
i63	F1
i64	F1
i65	F1
i59	F2
i136	F1
ix6a	F1
ix6b	F1
ix6c	F1
i98	F1
i175a	F1
i175b	F1
i175c	F1
i175d	F1
i182aa	F1
i182ba	F1
i182ac	F1
i182bc	F1
i182ad	F1
i182bd	F1
i182ae	F1
i182be	F1
i182ag	F1
i182bg	F1
i125a	F1
i125l	F1
i135e	F1
i135f	F1
i135j	F1
rjpb	F2
rjpc	F2
jpd	F1
jpg	F1
j9	F2
j12a	F1
j12b	F2
j12c	F4
j23a	F1
j23b	F2
j23c	F4
j34a	F1
j34b	F2
j34c	F4
j45a	F1
j45b	F2
j45c	F4
j26	F1
j115a	F1
j115b	F1
j115c	F1
j115d	F1
j115e	F1
j115f	F1
j115g	F1
j115h	F1
j115i	F1
j115j	F1
j115k	F1
j115m	F1
j172a	F1
j172b	F1
dpd	F1
dpe	F1
dpf	F1
dpg	F1
dph	F1
dpi	F1
dpl	F4
d1_12	F1
d1_13	F2
d1_14	F1
d1_15	F2
d1_18	F1
d1_19	F1
d1_21	F1
d2_12	F1
d2_13	F2
d2_14	F1
d2_15	F2
d2_18	F1
d2_19	F1
d2_21	F1
dx7	F1
d26	F1
d91	F1
d93	F4
d96	F1
dx8a	F1
dx8b	F1
dx8c	F1
dx8d	F1
dx8e	F1
dx8f	F1
dx8g	F1
dx8h	F1
senfviv	F2
sansfam	F1
snfratri	F2
lgenfviv	F1
lgfratri	F1
ptsenfan	F1
frersoeu	F1
parents	F1
bparents	F1
agemere	F2
agepere	F2
agebmere	F2
agebpere	F2
ageascd	F2
ageascal	F2
agmari1	F2
agveuv1	F2
pondrep5	F4.2
.
CACHE.
EXECUTE.

* Définition des labels de variables

VARIABLE LABELS icode "Code ego".
VARIABLE LABELS typeq "Type de questionnaire".
VARIABLE LABELS canton "Région d'enquête".
VARIABLE LABELS sexe "Sexe".
VARIABLE LABELS clage "Classe d'âge".
VARIABLE LABELS dateentr "Date de l'entretien (d'après svfdate et dpl)".
VARIABLE LABELS tb1 "En quelle année êtes-vous né/e ?".
VARIABLE LABELS tb2 "Dans votre jeunesse, quelle langue avez-vous parlé à la maison ?".
VARIABLE LABELS nb3 "Où avez-vous passé la plus grande partie de votre enfance (avant 16 ans) ?".
VARIABLE LABELS tb4 "Quel est votre état civil ?".
VARIABLE LABELS mb5 "En quelle année vous êtes-vous marié/e ?".
VARIABLE LABELS mb6a "En quelle année vous êtes-vous retrouvé/e veuf/ve ?".
VARIABLE LABELS mb6b "En quelle année vous êtes-vous retrouvé/e veuf/ve ? [2e fois]".
VARIABLE LABELS mb7a "En quelle année vous êtes-vous séparé‎/e ou divorcé‎/e ?".
VARIABLE LABELS mb7b "En quelle année vous êtes-vous séparé‎/e ou divorcé‎/e ? [2e fois]".
VARIABLE LABELS mb8a "En quelle année vous êtes-vous remarié/e ?".
VARIABLE LABELS mb8b "En quelle année vous êtes-vous remarié/e ? [2e fois]".
VARIABLE LABELS mb9 "Quelle est (était) l'année de naissance de votre conjoint/e ?".
VARIABLE LABELS sb10 "Avez-vous eu des enfants ?".
VARIABLE LABELS sb11 "Combien d'enfants avez-vous eu[s] ?".
VARIABLE LABELS mb12 "Fils / Fille [Enfant 1]".
VARIABLE LABELS mb13a "Année de naissance [Enfant 1]".
VARIABLE LABELS sb13b "Si décédé/e, année de décès [Enfant 1]".
VARIABLE LABELS mb14 "Quel est son état civil, aujourd'hui ? [Enfant 1]".
VARIABLE LABELS mb15 "A-t-il/elle des enfants ? (nombre) [Enfant 1]".
VARIABLE LABELS b16 "Combien de ses enfants ont moins de 15 ans ? [Enfant 1]".
VARIABLE LABELS mb17 "A-t-il/elle des petits-enfants (vos arrière-petits-enfants) ? (nombre) [Enfant 1]".
VARIABLE LABELS sb18 "Quelle est l'activité principale de votre fils/fille ? [Enfant 1]".
VARIABLE LABELS sb19 "Quelle est l'activité principale de son/sa conjoint/e ? [Enfant 1]".
VARIABLE LABELS sb20 "Votre enfant habite : [Enfant 1]".
VARIABLE LABELS sb21 "Par rapport à vous, votre enfant habite : [Enfant 1]".
VARIABLE LABELS b22 "Combien de temps vous faut-il pour vous rendre chez votre enfant ? (selon le moyen le plus commode) [Enfant 1]".
VARIABLE LABELS mb23 "Fils / Fille [Enfant 2]".
VARIABLE LABELS mb24a "Année de naissance [Enfant 2]".
VARIABLE LABELS sb24b "Si décédé/e, année de décès [Enfant 2]".
VARIABLE LABELS mb25 "Quel est son état civil, aujourd'hui ? [Enfant 2]".
VARIABLE LABELS mb26 "A-t-il/elle des enfants ? (nombre) [Enfant 2]".
VARIABLE LABELS b27 "Combien de ses enfants ont moins de 15 ans ? [Enfant 2]".
VARIABLE LABELS mb28 "A-t-il/elle des petits-enfants (vos arrière-petits-enfants) ? (nombre) [Enfant 2]".
VARIABLE LABELS sb29 "Quelle est l'activité principale de votre fils/fille ? [Enfant 2]".
VARIABLE LABELS sb30 "Quelle est l'activité principale de son/sa conjoint/e ? [Enfant 2]".
VARIABLE LABELS sb31 "Votre enfant habite : [Enfant 2]".
VARIABLE LABELS sb32 "Par rapport à vous, votre enfant habite : [Enfant 2]".
VARIABLE LABELS b33 "Combien de temps vous faut-il pour vous rendre chez votre enfant ? (selon le moyen le plus commode) [Enfant 2]".
VARIABLE LABELS mb34 "Fils / Fille [Enfant 3]".
VARIABLE LABELS mb35a "Année de naissance [Enfant 3]".
VARIABLE LABELS sb35b "Si décédé/e, année de décès [Enfant 3]".
VARIABLE LABELS mb36 "Quel est son état civil, aujourd'hui ? [Enfant 3]".
VARIABLE LABELS mb37 "A-t-il/elle des enfants ? (nombre) [Enfant 3]".
VARIABLE LABELS b38 "Combien de ses enfants ont moins de 15 ans ? [Enfant 3]".
VARIABLE LABELS mb39 "A-t-il/elle des petits-enfants (vos arrière-petits-enfants) ? (nombre) [Enfant 3]".
VARIABLE LABELS sb40 "Quelle est l'activité principale de votre fils/fille ? [Enfant 3]".
VARIABLE LABELS sb41 "Quelle est l'activité principale de son/sa conjoint/e ? [Enfant 3]".
VARIABLE LABELS sb42 "Votre enfant habite : [Enfant 3]".
VARIABLE LABELS sb43 "Par rapport à vous, votre enfant habite : [Enfant 3]".
VARIABLE LABELS b44 "Combien de temps vous faut-il pour vous rendre chez votre enfant ? (selon le moyen le plus commode) [Enfant 3]".
VARIABLE LABELS mb45 "Fils / Fille [Enfant 4]".
VARIABLE LABELS mb46a "Année de naissance [Enfant 4]".
VARIABLE LABELS sb46b "Si décédé/e, année de décès [Enfant 4]".
VARIABLE LABELS mb47 "Quel est son état civil, aujourd'hui ? [Enfant 4]".
VARIABLE LABELS mb48 "A-t-il/elle des enfants ? (nombre) [Enfant 4]".
VARIABLE LABELS b49 "Combien de ses enfants ont moins de 15 ans ? [Enfant 4]".
VARIABLE LABELS mb50 "A-t-il/elle des petits-enfants (vos arrière-petits-enfants) ? (nombre) [Enfant 4]".
VARIABLE LABELS sb51 "Quelle est l'activité principale de votre fils/fille ? [Enfant 4]".
VARIABLE LABELS sb52 "Quelle est l'activité principale de son/sa conjoint/e ? [Enfant 4]".
VARIABLE LABELS sb53 "Votre enfant habite : [Enfant 4]".
VARIABLE LABELS sb54 "Par rapport à vous, votre enfant habite : [Enfant 4]".
VARIABLE LABELS b55 "Combien de temps vous faut-il pour vous rendre chez votre enfant ? (selon le moyen le plus commode) [Enfant 4]".
VARIABLE LABELS b56a "Avez-vous encore votre mère ?".
VARIABLE LABELS b56b "Si non, année de décès [mère]".
VARIABLE LABELS b56c "Si oui, année de naissance [mère]".
VARIABLE LABELS b57 "Quel est l'état civil de votre mère, aujourd'hui ?".
VARIABLE LABELS b58 "Où habite votre mère ?".
VARIABLE LABELS b59a "Personne, elle vit seule [Qui partage son logement ? (qui vit avec elle?)] [mère]".
VARIABLE LABELS b59b "Son mari [Qui partage son logement ? (qui vit avec elle?)] [mère]".
VARIABLE LABELS b59c "Un ou plusieurs enfants [Qui partage son logement ? (qui vit avec elle?)] [mère]".
VARIABLE LABELS b59d "Un ou plusieurs autres membres de la famille [Qui partage son logement ? (qui vit avec elle?)] [mère]".
VARIABLE LABELS b59e "Un/e ou des ami/e(s) [Qui partage son logement ? (qui vit avec elle?)] [mère]".
VARIABLE LABELS b59f "Autre(s) personne(s) [Qui partage son logement ? (qui vit avec elle?)] [mère]".
VARIABLE LABELS b60 "Par rapport à vous, votre mère habite :".
VARIABLE LABELS b61 "Combien de temps vous faut-il pour vous rendre chez votre mère (selon le moyen le plus commode) ?".
VARIABLE LABELS b62a "Avez-vous encore votre père ?".
VARIABLE LABELS b62b "Si non, année de décès [père]".
VARIABLE LABELS b62c "Si oui, année de naissance [père]".
VARIABLE LABELS b63 "Quel est l'état civil de votre père, aujourd'hui ?".
VARIABLE LABELS b64 "Où habite votre père ?".
VARIABLE LABELS b65a "Personne, il vit seul [Qui partage son logement ? (qui vit avec lui?)] [père]".
VARIABLE LABELS b65b "Sa femme [Qui partage son logement ? (qui vit avec lui?)] [père]".
VARIABLE LABELS b65c "Un ou plusieurs enfants [Qui partage son logement ? (qui vit avec lui?)] [père]".
VARIABLE LABELS b65d "Un ou plusieurs autres membres de la famille [Qui partage son logement ? (qui vit avec lui?)] [père]".
VARIABLE LABELS b65e "Un/e ou des ami/e(s) [Qui partage son logement ? (qui vit avec lui?)] [père]".
VARIABLE LABELS b65f "Autre(s) personne(s) [Qui partage son logement ? (qui vit avec lui?)] [père]".
VARIABLE LABELS b66 "Par rapport à vous, votre père habite :".
VARIABLE LABELS b67 "Combien de temps vous faut-il pour vous rendre chez votre père (selon le moyen le plus commode) ?".
VARIABLE LABELS b68a "Avez-vous encore votre belle-mère ?".
VARIABLE LABELS b68b "Si non, année de décès [belle-mère]".
VARIABLE LABELS b68c "Si oui, année de naissance [belle-mère]".
VARIABLE LABELS b69 "Quel est l'état civil de votre belle-mère, aujourd'hui ?".
VARIABLE LABELS b70 "Où habite votre belle-mère ?".
VARIABLE LABELS b71a "Personne, elle vit seule [Qui partage son logement ? (qui vit avec elle?)] [belle-mère]".
VARIABLE LABELS b71b "Son mari [Qui partage son logement ? (qui vit avec elle?)] [belle-mère]".
VARIABLE LABELS b71c "Un ou plusieurs enfants [Qui partage son logement ? (qui vit avec elle?)] [belle-mère]".
VARIABLE LABELS b71d "Un ou plusieurs autres membres de la famille [Qui partage son logement ? (qui vit avec elle?)] [belle-mère]".
VARIABLE LABELS b71e "Un/e ou des ami/e(s) [Qui partage son logement ? (qui vit avec elle?)] [belle-mère]".
VARIABLE LABELS b71f "Autre(s) personne(s) [Qui partage son logement ? (qui vit avec elle?)] [belle-mère]".
VARIABLE LABELS b72 "Par rapport à vous, votre belle-mère habite :".
VARIABLE LABELS b73 "Combien de temps vous faut-il pour vous rendre chez votre belle-mère (selon le moyen le plus commode) ?".
VARIABLE LABELS b74a "Avez-vous encore votre beau-père ?".
VARIABLE LABELS b74b "Si non, année de décès [beau-père]".
VARIABLE LABELS b74c "Si oui, année de naissance [beau-père]".
VARIABLE LABELS b75 "Quel est l'état civil de votre beau-père, aujourd'hui ?".
VARIABLE LABELS b76 "Où habite votre beau-père ?".
VARIABLE LABELS b77a "Personne, il vit seul [Qui partage son logement ? (qui vit avec lui?)] [beau-père]".
VARIABLE LABELS b77b "Sa femme [Qui partage son logement ? (qui vit avec lui?)] [beau-père]".
VARIABLE LABELS b77c "Un ou plusieurs enfants [Qui partage son logement ? (qui vit avec lui?)] [beau-père]".
VARIABLE LABELS b77d "Un ou plusieurs autres membres de la famille [Qui partage son logement ? (qui vit avec lui?)] [beau-père]".
VARIABLE LABELS b77e "Un/e ou des ami/e(s) [Qui partage son logement ? (qui vit avec lui?)] [beau-père]".
VARIABLE LABELS b77f "Autre(s) personne(s) [Qui partage son logement ? (qui vit avec lui?)] [beau-père]".
VARIABLE LABELS b78 "Par rapport à vous, votre beau-père habite :".
VARIABLE LABELS b79 "Combien de temps vous faut-il pour vous rendre chez votre beau-père (selon le moyen le plus commode) ?".
VARIABLE LABELS sb80 "Combien aviez-vous de frères et soeurs lorsque vous aviez 20 ans environ ?".
VARIABLE LABELS sb81a "Frères (vivants) [Et maintenant, combien avez-vous de:]".
VARIABLE LABELS sb81b "Soeurs (vivantes) [Et maintenant, combien avez-vous de:]".
VARIABLE LABELS sb81ab "Nombre de frères et soeurs".
VARIABLE LABELS sb82 "Par rapport à vos frères et soeurs, vous êtes le combientième ?".
VARIABLE LABELS sb83a "Le même appartement/villa que vous [Pouvez-vous nous dire combien de frères et de soeurs habitent:]".
VARIABLE LABELS mb83b "Le même quartier/village que vous (mais pas le même logement) [Pouvez-vous nous dire combien de frères et de soeurs habitent:]".
VARIABLE LABELS mb83c "La même commune que vous (mais pas le même quartier/village) [Pouvez-vous nous dire combien de frères et de soeurs habitent:]".
VARIABLE LABELS mb83d "Le même canton que vous (mais pas la même commune) [Pouvez-vous nous dire combien de frères et de soeurs habitent:]".
VARIABLE LABELS mb83e "Un autre canton [Pouvez-vous nous dire combien de frères et de soeurs habitent:]".
VARIABLE LABELS mb83f "Un autre pays [Pouvez-vous nous dire combien de frères et de soeurs habitent:]".
VARIABLE LABELS mb83g "Je ne sais pas [Pouvez-vous nous dire combien de frères et de soeurs habitent:]".
VARIABLE LABELS sb84 "Avez-vous actuellement un ou plusieurs neveux ou nièces (petits-neveux ou petites-nièces) âgés de moins de 15 ans ?".
VARIABLE LABELS pb85 "Habitez-vous :".
VARIABLE LABELS pb86 "Habitez-vous seul/e ?".
VARIABLE LABELS pb87 "Combien de personnes vivent avec vous dans le même logement (vous non compris) ?".
VARIABLE LABELS pb88a "Conjoint/e [Quelles sont les personnes qui vivent avec vous ?]".
VARIABLE LABELS pb88b "Enfant(s) [Quelles sont les personnes qui vivent avec vous ?]".
VARIABLE LABELS q88c "Parent(s), beaux-parents [Quelles sont les personnes qui vivent avec vous ?]".
VARIABLE LABELS pb88d "Autre(s) membre(s) de la famille [Quelles sont les personnes qui vivent avec vous ?]".
VARIABLE LABELS pb88e "Ami(s), amie(s) [Quelles sont les personnes qui vivent avec vous ?]".
VARIABLE LABELS pb88f "Employé/e(s) [Quelles sont les personnes qui vivent avec vous ?]".
VARIABLE LABELS pb88g "Autre [Quelles sont les personnes qui vivent avec vous ?]".
VARIABLE LABELS sb89 "Souhaiteriez-vous vivre avec quelqu'un d'autre ?".
VARIABLE LABELS sb90 "Avec qui souhaiteriez-vous vivre ? Est-ce avec vos enfants, des parents, des amis, dans une pension ou une institution pour les personnes âgées ou avec d'autres personnes encore ?".
VARIABLE LABELS sb91 "Depuis combien de temps environ habitez-vous votre logement actuel ?".
VARIABLE LABELS sb92 "Depuis combien d'années environ habitez-vous dans la commune (en ville: dans le quartier) où vous habitez actuellement ?".
VARIABLE LABELS sb93 "Vous avez déménagé dans les 15 dernières années: où habitiez-vous avant votre dernier déménagement ?".
VARIABLE LABELS pb94a "Raison liée au travail [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS pb94b "Raison liée à la retraite [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS pb94c "Pour se rapprocher des enfants (ou aller vivre avec eux) [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS pb94d "Pour se rapprocher de père/mère/beau-père/belle-mère (ou aller vivre avec eux) [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS pb94e "Pour se rapprocher d'autres parents ou amis (ou aller vivre avec eux) [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS pb94f "Pour raison de santé [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS pb94g "Pour raison financière [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS pb94h "Expropriation, bail non renouvelable ou coût de loyer [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS pb94i "Commodité matérielle (appartement trop grand, peu confortable, trop éloigné des équipements collectifs) [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS pb94j "Autre [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS sb94k "Autre: décès du conjoint [Quelles sont la ou les raisons qui ont motivé ce déménagement ?]".
VARIABLE LABELS pb95 "Combien de pièces y a-t-il dans votre appartement sans compter la cuisine et la salle de bains ?".
VARIABLE LABELS mb96a "Grandeur [Votre domicile actuel, du point de vue de sa grandeur, de son équipement, du bruit, etc., vous satisfait-il ?]".
VARIABLE LABELS mb96b "Équipement [Votre domicile actuel, du point de vue de sa grandeur, de son équipement, du bruit, etc., vous satisfait-il ?]".
VARIABLE LABELS mb96c "Bruit [Votre domicile actuel, du point de vue de sa grandeur, de son équipement, du bruit, etc., vous satisfait-il ?]".
VARIABLE LABELS mb96d "Adaptation à vos besoins [Votre domicile actuel, du point de vue de sa grandeur, de son équipement, du bruit, etc., vous satisfait-il ?]".
VARIABLE LABELS mb96e "Contact avec voisins [Votre domicile actuel, du point de vue de sa grandeur, de son équipement, du bruit, etc., vous satisfait-il ?]".
VARIABLE LABELS mb96f "Sécurité [Votre domicile actuel, du point de vue de sa grandeur, de son équipement, du bruit, etc., vous satisfait-il ?]".
VARIABLE LABELS mb97a "À l'épicerie ou au magasin d'alimentation où vous vous servez habituellement [Combien de temps vous faut-il pour vous rendre à pied, à votre allure habituelle, de votre logement ?]".
VARIABLE LABELS mb97b "À l'arrêt de bus (tram), de car postal ou autre transport public le plus proche [Combien de temps vous faut-il pour vous rendre à pied, à votre allure habituelle, de votre logement ?]".
VARIABLE LABELS sb97c "Au café ou au restaurant que vous fréquentez le plus souvent [Combien de temps vous faut-il pour vous rendre à pied, à votre allure habituelle, de votre logement ?]".
VARIABLE LABELS pb98a "Eau chaude à volonté [Avez-vous dans votre logement ?]".
VARIABLE LABELS pb98b "W.-C. [Avez-vous dans votre logement ?]".
VARIABLE LABELS pb98c "Salle de bain [Avez-vous dans votre logement ?]".
VARIABLE LABELS pb98d "Frigo [Avez-vous dans votre logement ?]".
VARIABLE LABELS pb98e "Congélateur [Avez-vous dans votre logement ?]".
VARIABLE LABELS pb98f "Four à micro-ondes [Avez-vous dans votre logement ?]".
VARIABLE LABELS pb98g "Chauffage central [Avez-vous dans votre logement ?]".
VARIABLE LABELS pb98h "Lave-linge [Avez-vous dans votre logement ?]".
VARIABLE LABELS sb98i "Établi [Avez-vous dans votre logement ?]".
VARIABLE LABELS pb98j "Téléphone [Avez-vous dans votre logement ?]".
VARIABLE LABELS sb99 "Si vous devez téléphoner d'urgence, pouvez-vous le faire:".
VARIABLE LABELS sb100a "Un jardin d'agrément [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb100b "Un potager (même petit) [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb100c "Quelques arbres fruitiers [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb100d "Une vigne [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb100e "Un poulailler [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb100f "Un ou des lapins [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb100g "Du petit bétail [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb100h "Du gros bétail [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb100i "Un ou des chats [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb100j "Un ou des chiens [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb100k "Un autre animal domestique [Avez-vous actuellement (ou disposez-vous de) ?]".
VARIABLE LABELS sb101a "Un jardin d'agrément [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb101b "Un potager (même petit) [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb101c "Quelques arbres fruitiers [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb101d "Une vigne [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb101e "Un poulailler [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb101f "Un ou des lapins [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb101g "Du petit bétail [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb101h "Du gros bétail [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb101i "Un ou des chats [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb101j "Un ou des chiens [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb101k "Un autre animal domestique [Vous en occupez-vous vous-même ?]".
VARIABLE LABELS sb102a "Un tourne-disque, un enregistreur à cassette ou un lecteur de disque compact [Avez-vous également:]".
VARIABLE LABELS sb102b "Une radio [Avez-vous également:]".
VARIABLE LABELS sb102c "Une télévision [Avez-vous également:]".
VARIABLE LABELS sb102d "Une vidéo (magnétoscope) [Avez-vous également:]".
VARIABLE LABELS tb103 "À quelle fréquence écoutez-vous la radio ?".
VARIABLE LABELS sb104 "À quelle fréquence regardez-vous la télévision ?".
VARIABLE LABELS tb105 "À quelle fréquence lisez-vous le journal ?".
VARIABLE LABELS tb106 "À quelle fréquence lisez-vous les livres ou les revues (sans compter les quotidiens) ?".
VARIABLE LABELS mb107a "Les informations, les émissions d'actualité [Si vous écoutez la radio, qu'écoutez-vous le plus ?]".
VARIABLE LABELS mb107b "Les documentaires, reportages, récits de voyage [Si vous écoutez la radio, qu'écoutez-vous le plus ?]".
VARIABLE LABELS mb107c "Les émissions culturelles [Si vous écoutez la radio, qu'écoutez-vous le plus ?]".
VARIABLE LABELS mb107d "La musique [Si vous écoutez la radio, qu'écoutez-vous le plus ?]".
VARIABLE LABELS mb107e "Les sports [Si vous écoutez la radio, qu'écoutez-vous le plus ?]".
VARIABLE LABELS mb107f "Un peu tout (je laisse le poste ouvert) [Si vous écoutez la radio, qu'écoutez-vous le plus ?]".
VARIABLE LABELS mb108a "Les informations, les émissions d'actualité [Si vous regardez la télévision, que regardez-vous avant tout ?]".
VARIABLE LABELS mb108b "Les documentaires, reportages, récits de voyage [Si vous regardez la télévision, que regardez-vous avant tout ?]".
VARIABLE LABELS mb108c "Les émissions culturelles [Si vous regardez la télévision, que regardez-vous avant tout ?]".
VARIABLE LABELS mb108d "La musique [Si vous regardez la télévision, que regardez-vous avant tout ?]".
VARIABLE LABELS mb108e "Les sports [Si vous regardez la télévision, que regardez-vous avant tout ?]".
VARIABLE LABELS mb108f "Un peu tout (je laisse le poste ouvert) [Si vous regardez la télévision, que regardez-vous avant tout ?]".
VARIABLE LABELS mb109a "Les informations locales et nationales [Si vous lisez les journaux ou les hebdomadaires, que lisez-vous de préférence ?]".
VARIABLE LABELS mb109b "Les informations internationales [Si vous lisez les journaux ou les hebdomadaires, que lisez-vous de préférence ?]".
VARIABLE LABELS mb109c "La rubrique sportive [Si vous lisez les journaux ou les hebdomadaires, que lisez-vous de préférence ?]".
VARIABLE LABELS mb109d "Les analyses économiques [Si vous lisez les journaux ou les hebdomadaires, que lisez-vous de préférence ?]".
VARIABLE LABELS mb109e "La chronique culturelle (critique de livres, de spectacles) [Si vous lisez les journaux ou les hebdomadaires, que lisez-vous de préférence ?]".
VARIABLE LABELS mb109f "Les annonces de naisssances, décès, mariages [Si vous lisez les journaux ou les hebdomadaires, que lisez-vous de préférence ?]".
VARIABLE LABELS mb109g "L'horoscope [Si vous lisez les journaux ou les hebdomadaires, que lisez-vous de préférence ?]".
VARIABLE LABELS mb109h "Spiritualité [Si vous lisez les journaux ou les hebdomadaires, que lisez-vous de préférence ?]".
VARIABLE LABELS mb110a "Romans policiers [Si vous lisez des livres, quel genre de livres lisez-vous de préférence ?]".
VARIABLE LABELS mb110b "Romans divertissants [Si vous lisez des livres, quel genre de livres lisez-vous de préférence ?]".
VARIABLE LABELS mb110c "Littérature moderne et classique [Si vous lisez des livres, quel genre de livres lisez-vous de préférence ?]".
VARIABLE LABELS mb110d "Guides pratiques (voyage, bricolage, jardinage) [Si vous lisez des livres, quel genre de livres lisez-vous de préférence ?]".
VARIABLE LABELS mb110e "Ouvrages scientifiques [Si vous lisez des livres, quel genre de livres lisez-vous de préférence ?]".
VARIABLE LABELS mb110f "Livres sur la politique [Si vous lisez des livres, quel genre de livres lisez-vous de préférence ?]".
VARIABLE LABELS mb110g "Livres sur l'art [Si vous lisez des livres, quel genre de livres lisez-vous de préférence ?]".
VARIABLE LABELS mb110h "Livres historiques, biographiques [Si vous lisez des livres, quel genre de livres lisez-vous de préférence ?]".
VARIABLE LABELS mb110i "Livres de spiritualité [Si vous lisez des livres, quel genre de livres lisez-vous de préférence ?]".
VARIABLE LABELS sb111 "Est-ce que vous ou votre conjoint/e possède une voiture ?".
VARIABLE LABELS sb112 "Si vous disposez d'une voiture, à quelle fréquence l'utilisez-vous (seul/e ou avec votre conjoint/e), en temps normal ?".
VARIABLE LABELS sb113 "Qui conduit d'habitude la voiture ?".
VARIABLE LABELS sb113a "Conducteur: moi".
VARIABLE LABELS sb113b "Conducteur: conjoint‎/e".
VARIABLE LABELS sb113c "Conducteur: les deux".
VARIABLE LABELS sb113d "Conducteur: autre personne".
VARIABLE LABELS sb114 "Si vous ne disposez pas de voiture: en avez-vous possédé une ?".
VARIABLE LABELS b115 "Si oui: en quelle année environ l'avez-vous abandonnée ?".
VARIABLE LABELS b116a "Je n'en avais plus besoin [Pour quelle raison l'avez-vous abandonnée ?]".
VARIABLE LABELS b116b "Pour des raisons de santé [Pour quelle raison l'avez-vous abandonnée ?]".
VARIABLE LABELS b116c "Pour des raisons financières [Pour quelle raison l'avez-vous abandonnée ?]".
VARIABLE LABELS b116d "Autre [Pour quelle raison l'avez-vous abandonnée ?]".
VARIABLE LABELS tb117a "Les transports publics [Quand vous vous déplacez pour aller en ville ou dans un autre lieu, utilisez-vous:]".
VARIABLE LABELS tb117b "Le taxi [Quand vous vous déplacez pour aller en ville ou dans un autre lieu, utilisez-vous:]".
VARIABLE LABELS tb117c "La voiture d'un parent, ami, connaissance [Quand vous vous déplacez pour aller en ville ou dans un autre lieu, utilisez-vous:]".
VARIABLE LABELS tb117d "Vous ne vous déplacez que très rarement hors de chez vous et de votre quartier‎/village [Quand vous vous déplacez pour aller en ville ou dans un autre lieu, utilisez-vous:]".
VARIABLE LABELS mb118a "Assurance-maladie [Est-ce que vous avez actuellement les assurances suivantes:]".
VARIABLE LABELS mb118b "Assurance-accident [Est-ce que vous avez actuellement les assurances suivantes:]".
VARIABLE LABELS mb118c "Assurance-vie [Est-ce que vous avez actuellement les assurances suivantes:]".
VARIABLE LABELS mb119a "Assurance-maladie [Est-ce que votre conjoint‎/e a actuellement les assurances suivantes:]".
VARIABLE LABELS mb119b "Assurance-accident [Est-ce que votre conjoint‎/e a actuellement les assurances suivantes:]".
VARIABLE LABELS mb119c "Assurance-vie [Est-ce que votre conjoint‎/e a actuellement les assurances suivantes:]".
VARIABLE LABELS b120 "Au cours de votre vie professionnelle, avez-vous cotisé à une caisse de retraite (deuxième pilier)?".
VARIABLE LABELS b121 "Si oui, depuis quelle année avez-vous cotisé?".
VARIABLE LABELS b122 "Et votre conjoint/e, au cours de sa vie professionnelle, a-t-il/elle cotisé à une caisse de retraite?".
VARIABLE LABELS b123 "Si oui, depuis quelle année a-t-il/elle cotisé?".
VARIABLE LABELS mb124 "Comment évaluez-vous votre état de santé lorsque vous aviez 45 ans environ?".
VARIABLE LABELS b126 "Entre l'âge de 45 ans et maintenant, est-ce que vous avez dû renoncer à exercer vos activités habituelles pour des raisons de santé (durant deux mois au moins)?".
VARIABLE LABELS b127 "Combien de fois environ? [Nombre]".
VARIABLE LABELS b128 "Pensez à la dernière interruption pour raison de santé: Quand est-ce arrivé? [Année (environ)]".
VARIABLE LABELS b129 "S'agissait-il d'un accident ou d'une maladie?".
VARIABLE LABELS b130 "Êtes-vous aujourd'hui parfaitement remis(e) ou non de cette maladie (ou accident) et de ses suites?".
VARIABLE LABELS b131 "Cette interruption a-t-elle provoqué un déséquilibre de votre budget dans les mois, voire les années qui ont suivi?".
VARIABLE LABELS tb132aa "Douleurs, crampes, tremblements ou enflure des membres inférieurs (pieds, jambes, genoux, hanches..) [Est-ce que vous souffrez de?]".
VARIABLE LABELS tb132ab "Même chose pour les membres supérieurs (mains, poignets, coudes, bras, épaules) [Est-ce que vous souffrez de?]".
VARIABLE LABELS tb132ac "Maux de tête ou autres douleurs au visage [Est-ce que vous souffrez de?]".
VARIABLE LABELS tb132ad "Maux de dos, de reins [Est-ce que vous souffrez de?]".
VARIABLE LABELS tb132ae "Irrégularités cardiaques: palpitations et douleurs [Est-ce que vous souffrez de?]".
VARIABLE LABELS tb132af "Difficultés respiratoires, asthme, toux chroniques, maux de gorge [Est-ce que vous souffrez de?]".
VARIABLE LABELS tb132ag "Maux d'estomac, maux de ventre (ballottements), diarrhée, constipation [Est-ce que vous souffrez de?]".
VARIABLE LABELS tb132ah "Douleurs, gêne ou mauvais fonctionnement des organes génitaux ou urinaires [Est-ce que vous souffrez de?]".
VARIABLE LABELS tb132ai "Douleurs dans la poitrine, sensation de pression [Est-ce que vous souffrez de?]".
VARIABLE LABELS tb132aj "Fièvres [Est-ce que vous souffrez de?]".
VARIABLE LABELS tb132ak "Autres [Est-ce que vous souffrez de?]".
VARIABLE LABELS sb132ba "Douleurs, crampes, tremblements ou enflure des membres inférieurs (pieds, jambes, genoux, hanches..) [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS sb132bb "Même chose pour les membres supérieurs (mains, poignets, coudes, bras, épaules) [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS sb132bc "Maux de tête ou autres douleurs au visage [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS sb132bd "Maux de dos, de reins [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS sb132be "Irrégularités cardiaques: palpitations et douleurs [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS sb132bf "Difficultés respiratoires, asthme, toux chroniques, maux de gorge [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS sb132bg "Maux d'estomac, maux de ventre (ballottements), diarrhée, constipation [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS sb132bh "Douleurs, gêne ou mauvais fonctionnement des organes génitaux ou urinaires [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS sb132bi "Douleurs dans la poitrine, sensation de pression [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS sb132bj "Fièvres [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS sb132bk "Autres [Ces troubles vous gênent-ils dans vos activités journalières habituelles?]".
VARIABLE LABELS tb133 "Portez-vous des lunettes correctives ou des lentilles de contact?".
VARIABLE LABELS tb134 "Est-ce que votre vue est suffisamment bonne pour vous permettre de lire un texte normalement imprimé dans un journal (avec vos lunettes ou lentilles de contact, si vous en avez)?".
VARIABLE LABELS tb135 "Avez-vous des troubles de la vue même avec le port de lunettes ou de lentilles?".
VARIABLE LABELS tb136 "Utilisez-vous un appareil auditif (de poche, derrière ou dans l'oreille, dans le canal auditif)?".
VARIABLE LABELS tb137 "Pouvez-vous entendre ce que vous dit une autre personne au cours d'une conversation normale avec elle seule (avec votre appareil auditif si vous en avez un)?".
VARIABLE LABELS tb138 "Pouvez-vous suivre une conversation à laquelle participent plusieurs personnes (avec votre appareil auditif si vous en avez un)?".
VARIABLE LABELS tb139 "Avez-vous des troubles de l'ouïe, même avec le port d'un appareil auditif?".
VARIABLE LABELS tb140 "Vous est-il arrivé de tomber au cours de la dernière année?".
VARIABLE LABELS tb141 "Combien de fois environ?".
VARIABLE LABELS sb142 "Avez-vous pris des médicaments au cours de cette dernière semaine?".
VARIABLE LABELS tb143 "Combien de médicaments différents avez-vous pris cette dernière semaine? [Nombre]".
VARIABLE LABELS nbmedic "Nombre de médicaments différents corrigé".
VARIABLE LABELS mb144 "Quand vous devez prendre des médicaments, les prenez-vous:".
VARIABLE LABELS sb146 "Est-ce que vous fumez?".
VARIABLE LABELS sb147a "Cigarettes [Pouvez-vous dire combien, à peu près, vous fumez par jour de:]".
VARIABLE LABELS sb147b "Cigarillos‎/petits cigares [Pouvez-vous dire combien, à peu près, vous fumez par jour de:]".
VARIABLE LABELS sb147c "Cigares [Pouvez-vous dire combien, à peu près, vous fumez par jour de:]".
VARIABLE LABELS sb147d "Pipes [Pouvez-vous dire combien, à peu près, vous fumez par jour de:]".
VARIABLE LABELS sb148 "Est-ce que vous buvez de l'alcool?".
VARIABLE LABELS tb149 "Vous déplacez-vous seul/e d'une pièce à l'autre ?".
VARIABLE LABELS tb150 "Montez-vous ou descendez-vous seul/e un escalier ?".
VARIABLE LABELS tb151 "Vous déplacez-vous seul/e à l'extérieur de votre logement?".
VARIABLE LABELS tb152 "Parcourez-vous seul/e à pied 200 mètres au moins?".
VARIABLE LABELS tb153 "Vous couchez-vous et vous levez-vous seul/e?".
VARIABLE LABELS tb154 "Vous habillez-vous et vous déshabillez-vous seul/e?".
VARIABLE LABELS tb155 "Prenez-vous seul/e soin de votre apparence (par ex. vous coiffer, vous raser, etc.)?".
VARIABLE LABELS tb156 "Mangez-vous sans aide et coupez-vous vous-mêmes vos aliments (viande ou fruits par exemple)?".
VARIABLE LABELS tb157 "Faites-vous seul/e votre toilette complète (vous laver entièrement)?".
VARIABLE LABELS tb158 "Vous coupez-vous seul/e les ongles des pieds?".
VARIABLE LABELS tb159 "Est-ce que vous avez des problèmes d'incontinence, d'urine ou de selle (ce qu'on appelle des 'petits accidents')?".
VARIABLE LABELS sb160 "Parmi les personnes qui vivent avec vous, est-ce qu'il y a quelqu'un qui a des problèmes d'incontinence, d'urine ou de selle?".
VARIABLE LABELS sb161a "Se déplacer d'une pièce à l'autre? [Parmi les personnes qui vivent avec vous, y a-t-il quelqu'un qui a besoin de l'aide d'une autre personne pour:]".
VARIABLE LABELS sb161b "Monter et descendre un escalier? [Parmi les personnes qui vivent avec vous, y a-t-il quelqu'un qui a besoin de l'aide d'une autre personne pour:]".
VARIABLE LABELS sb161c "Se déplacer à l'extérieur de sa maison‎/son appartement? [Parmi les personnes qui vivent avec vous, y a-t-il quelqu'un qui a besoin de l'aide d'une autre personne pour:]".
VARIABLE LABELS sb161d "Parcourir à pied 200 m. au moins? [Parmi les personnes qui vivent avec vous, y a-t-il quelqu'un qui a besoin de l'aide d'une autre personne pour:]".
VARIABLE LABELS sb161e "Se coucher et se lever? [Parmi les personnes qui vivent avec vous, y a-t-il quelqu'un qui a besoin de l'aide d'une autre personne pour:]".
VARIABLE LABELS sb161f "S'habiller et se déshabiller? [Parmi les personnes qui vivent avec vous, y a-t-il quelqu'un qui a besoin de l'aide d'une autre personne pour:]".
VARIABLE LABELS sb161g "Prendre soin de son apparence, par ex. se coiffer, se raser, etc.? [Parmi les personnes qui vivent avec vous, y a-t-il quelqu'un qui a besoin de l'aide d'une autre personne pour:]".
VARIABLE LABELS sb161h "Manger et couper ses aliments (viande ou fruits par exemple)? [Parmi les personnes qui vivent avec vous, y a-t-il quelqu'un qui a besoin de l'aide d'une autre personne pour:]".
VARIABLE LABELS sb161i "Se faire une toilette complète (se laver entièrement)? [Parmi les personnes qui vivent avec vous, y a-t-il quelqu'un qui a besoin de l'aide d'une autre personne pour:]".
VARIABLE LABELS sb161j "Se couper les ongles des pieds? [Parmi les personnes qui vivent avec vous, y a-t-il quelqu'un qui a besoin de l'aide d'une autre personne pour:]".
VARIABLE LABELS sb162a "Conjoint/e [Pouvez-vous nous dire de qui il s'agit?]".
VARIABLE LABELS sb162b "Père, mère [Pouvez-vous nous dire de qui il s'agit?]".
VARIABLE LABELS sb162c "Beau-père, belle-mère [Pouvez-vous nous dire de qui il s'agit?]".
VARIABLE LABELS sb162d "Fils, fille (belle-fille, beau-fils) [Pouvez-vous nous dire de qui il s'agit?]".
VARIABLE LABELS sb162e "Autre parent [Pouvez-vous nous dire de qui il s'agit?]".
VARIABLE LABELS sb162f "Ami/e [Pouvez-vous nous dire de qui il s'agit?]".
VARIABLE LABELS sb162g "Autre personne [Pouvez-vous nous dire de qui il s'agit?]".
VARIABLE LABELS mb163 "Tout compte fait, comment évaluez-vous votre état de santé? Diriez-vous qu'il est:".
VARIABLE LABELS mb164 "Si vous comparez votre état de santé à celui des personnes qui ont le même âge que vous, le vôtre est-il:".
VARIABLE LABELS mb165 "Au cours de ces trois derniers mois, estimez-vous que votre état de santé s'est:".
VARIABLE LABELS mb166 "Comment évaluez-vous l'état de santé de votre conjoint(e)?".
VARIABLE LABELS mb167 "Diriez-vous que, par rapport au vôtre, l'état de santé de votre conjoint/e est:".
VARIABLE LABELS sb168a "Une femme, un homme de ménage [Avez-vous?]".
VARIABLE LABELS sb168b "Un/e employé/e de maison [Avez-vous?]".
VARIABLE LABELS pb169aa "Infirmière / aide à domicile [Au cours des trois derniers mois, avez-vous reçu de l'aide de?]".
VARIABLE LABELS pb169ab "Aide ménagère / aide familiale [Au cours des trois derniers mois, avez-vous reçu de l'aide de?]".
VARIABLE LABELS pb169ac "Physiothérapeute / ergothérapeute [Au cours des trois derniers mois, avez-vous reçu de l'aide de?]]".
VARIABLE LABELS pb169ad "Assistante sociale [Au cours des trois derniers mois, avez-vous reçu de l'aide de?]".
VARIABLE LABELS pb169ae "Repas à domicile [Au cours des trois derniers mois, avez-vous reçu de l'aide de?]".
VARIABLE LABELS pb169af "Hôpital de jour [Au cours des trois derniers mois, avez-vous reçu de l'aide de?]".
VARIABLE LABELS pb169ag "Bénévoles [Au cours des trois derniers mois, avez-vous reçu de l'aide de?]".
VARIABLE LABELS pb169ah "Autre [Au cours des trois derniers mois, avez-vous reçu de l'aide de?]".
VARIABLE LABELS pb169ba "Infirmière / aide à domicile [À quelle fréquence? Ego]".
VARIABLE LABELS pb169bb "Aide ménagère / aide familiale [À quelle fréquence? Ego]".
VARIABLE LABELS pb169bc "Physiothérapeute / ergothérapeute [À quelle fréquence? Ego]".
VARIABLE LABELS pb169bd "Assistante sociale [À quelle fréquence? Ego]".
VARIABLE LABELS pb169be "Repas à domicile [À quelle fréquence? Ego]".
VARIABLE LABELS pb169bf "Hôpital de jour [À quelle fréquence? Ego]".
VARIABLE LABELS pb169bg "Bénévoles [À quelle fréquence? Ego]".
VARIABLE LABELS pb169bh "Autre [À quelle fréquence? Ego]".
VARIABLE LABELS sb170aa "Infirmière / aide à domicile [Au cours des trois derniers mois, votre conjoint‎/e ou une autre personne a-t-il‎/elle reçu de l'aide de?]".
VARIABLE LABELS sb170ab "Aide ménagère / aide familiale [Au cours des trois derniers mois, votre conjoint‎/e ou une autre personne a-t-il‎/elle reçu de l'aide de?]".
VARIABLE LABELS sb170ac "Physiothérapeute / ergothérapeute [Au cours des trois derniers mois, votre conjoint‎/e ou une autre personne a-t-il‎/elle reçu de l'aide de?]".
VARIABLE LABELS sb170ad "Assistante sociale [Au cours des trois derniers mois, votre conjoint‎/e ou une autre personne a-t-il‎/elle reçu de l'aide de?]".
VARIABLE LABELS sb170ae "Repas à domicile [Au cours des trois derniers mois, votre conjoint‎/e ou une autre personne a-t-il‎/elle reçu de l'aide de?]".
VARIABLE LABELS sb170af "Hôpital de jour [Au cours des trois derniers mois, votre conjoint‎/e ou une autre personne a-t-il‎/elle reçu de l'aide de?]".
VARIABLE LABELS sb170ag "Bénévoles [Au cours des trois derniers mois, votre conjoint‎/e ou une autre personne a-t-il‎/elle reçu de l'aide de?]".
VARIABLE LABELS sb170ah "Autre [Au cours des trois derniers mois, votre conjoint‎/e ou une autre personne a-t-il‎/elle reçu de l'aide de?]".
VARIABLE LABELS sb170ba "Infirmière / aide à domicile [À quelle fréquence? Conjoint‎/e]".
VARIABLE LABELS sb170bb "Aide ménagère / aide familiale [À quelle fréquence? Conjoint‎/e]".
VARIABLE LABELS sb170bc "Physiothérapeute / ergothérapeute [À quelle fréquence? Conjoint‎/e]".
VARIABLE LABELS sb170bd "Assistante sociale [À quelle fréquence? Conjoint‎/e]".
VARIABLE LABELS sb170be "Repas à domicile [À quelle fréquence? Conjoint‎/e]".
VARIABLE LABELS sb170bf "Hôpital de jour [À quelle fréquence? Conjoint‎/e]".
VARIABLE LABELS sb170bg "Bénévoles [À quelle fréquence? Conjoint‎/e]".
VARIABLE LABELS sb170bh "Autre [À quelle fréquence? Conjoint‎/e]".
VARIABLE LABELS mb171a "La santé dépend avant tout de moi [Dites- nous si vous êtes d'accord, en partie d'accord ou pas d'accord, avec les avis suivants:]".
VARIABLE LABELS mb171b "La santé est un cadeau, un don [Dites- nous si vous êtes d'accord, en partie d'accord ou pas d'accord, avec les avis suivants:]".
VARIABLE LABELS mb171c "La santé n'est plus aujourd'hui un gros problème grâce à la médecine [Dites- nous si vous êtes d'accord, en partie d'accord ou pas d'accord, avec les avis suivants:]".
VARIABLE LABELS mb171d "Les maladies font partie de la vieillesse, c'est le destin [Dites- nous si vous êtes d'accord, en partie d'accord ou pas d'accord, avec les avis suivants:]".
VARIABLE LABELS mb171e "Les maladies peuvent être évitées si on fait attention et si on prend soin de soi [Dites- nous si vous êtes d'accord, en partie d'accord ou pas d'accord, avec les avis suivants:]".
VARIABLE LABELS mb171f "Les maladies peuvent être évitées grâce à des visites régulières chez le médecin [Dites- nous si vous êtes d'accord, en partie d'accord ou pas d'accord, avec les avis suivants:]".
VARIABLE LABELS tb172a "Recevez-vous, chez vous, des membres de votre famille? [En ce qui concerne votre famille (enfants, parents, frères et soeurs, cousins, neveux, etc.), pourriez-vous me dire comment et à quelle fréquence vous communiquez avec ".
VARIABLE LABELS mb172b "Allez vous chez des membres de votre famille? [En ce qui concerne votre famille (enfants, parents, frères et soeurs, cousins, neveux, etc.), pourriez-vous me dire comment et à quelle fréquence vous communiquez avec les ".
VARIABLE LABELS lb172c "Téléphonez-vous à des membres de votre famille ou recevez-vous des téléphones de leur part? [En ce qui concerne votre famille (enfants, parents, frères et soeurs, cousins, neveux, etc.), pourriez-vous me dire comment et à ".
VARIABLE LABELS mb172d "Écrivez-vous des lettres ou des cartes postales à des membres de votre famille ou en recevez-vous de leur part? [En ce qui concerne votre famille (enfants, parents, frères et soeurs, cousins, neveux, etc.), pourriez-vous me ".
VARIABLE LABELS mb172e "Recevez-vous vos petits-enfants ou des parents en vacances? [En ce qui concerne votre famille (enfants, parents, frères et soeurs, cousins, neveux, etc.), pourriez-vous me dire comment et à quelle fréquence vous communiquez ".
VARIABLE LABELS mb172f "Allez-vous en vacances ou en week-end avec, ou chez certains membres de votre parenté? [En ce qui concerne votre famille (enfants, parents, frères et soeurs, cousins, neveux, etc.), pourriez-vous me dire comment et à quelle ".
VARIABLE LABELS tb173a "Recevez-vous chez vous des amis ou connaissances? [Voici maintenant la même question concernant les amis et les connaissances.]".
VARIABLE LABELS mb173b "Allez-vous chez des amis ou connaissances? [Voici maintenant la même question concernant les amis et les connaissances.]".
VARIABLE LABELS lb173c "Téléphonez-vous à des amis ou connaissances ou recevez-vous des téléphones de leur part? [Voici maintenant la même question concernant les amis et les connaissances.]".
VARIABLE LABELS mb173d "Écrivez-vous des lettres ou des cartes postales à des amis ou des connaissances ou en recevez-vous de leur part? [Voici maintenant la même question concernant les amis et les connaissances.]".
VARIABLE LABELS mb173e "Passez-vous des week-end ou des vacances avec des amis ou des connaissances? [Voici maintenant la même question concernant les amis et les connaissances.]".
VARIABLE LABELS b174 "Aimeriez-vous avoir plus d'amis ou de bons copains que vous en avez?".
VARIABLE LABELS sb175a "Bonjour, bonsoir [Quel type de relations entretenez-vous avec vos voisins?]".
VARIABLE LABELS sb175b "Conversations occasionnelles [Quel type de relations entretenez-vous avec vos voisins?]".
VARIABLE LABELS sb175c "Vous vous rendez de petits services [Quel type de relations entretenez-vous avec vos voisins?]".
VARIABLE LABELS sb175d "Vous vous rendez des visites [Quel type de relations entretenez-vous avec vos voisins?]".
VARIABLE LABELS sb175e "Vous faites des sorties ou des promenades avec l'un ou l'autre d'entre eux [Quel type de relations entretenez-vous avec vos voisins?]".
VARIABLE LABELS sb176aa "Clubs de randonnée, de marche [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176ba "Clubs de randonnée, de marche [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176ca "Clubs de randonnée, de marche".
VARIABLE LABELS sb176ab "Autres associations ou clubs sportifs [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bb "Autres associations ou clubs sportifs [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176cb "Autres associations ou clubs sportifs [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176ac "Groupe de contemporains [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bc "Groupe de contemporains [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176cc "Groupe de contemporains [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176ad "Hobbies (bricolage, ...) [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bd "Hobbies (bricolage, ...) [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176cd "Hobbies (bricolage, ...) [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176ae "Parti / association politique [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176be "Parti / association politique [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176ce "Parti / association politique [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176af "Associations professionnelles, syndicales [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bf "Associations professionnelles, syndicales [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176cf "Associations professionnelles, syndicales [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176ag "Associations patriotiques [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bg "Associations patriotiques [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176cg "Associations patriotiques [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176ah "Associations culturelles (musique, théâtre, chant, sociétés folkloriques, de carnaval) [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bh "Associations culturelles (musique, théâtre, chant, sociétés folkloriques, de carnaval) [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176ch "Associations culturelles (musique, théâtre, chant, sociétés folkloriques, de carnaval) [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176ai "Paroisse, associations religieuses ou paroissiales (y compris chant sacré) [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bi "Paroisse, associations religieuses ou paroissiales (y compris chant sacré) [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176ci "Paroisse, associations religieuses ou paroissiales (y compris chant sacré) [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176aj "Associations de bienfaisance, caritatives [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bj "Associations de bienfaisance, caritatives [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176cj "Associations de bienfaisance, caritatives [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176ak "Solidarité (Tiers Monde, Droits de l'homme) [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bk "Solidarité (Tiers Monde, Droits de l'homme) [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176ck "Solidarité (Tiers Monde, Droits de l'homme) [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176al "Associations de quartiers, d'habitants [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bl "Associations de quartiers, d'habitants [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176cl "Associations de quartiers, d'habitants [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb176am "Autre [De quelles associations faites vous partie?]".
VARIABLE LABELS sb176bm "Autre [Avez-vous des responsabilités dans cette association?]".
VARIABLE LABELS sb176cm "Autre [Est-ce une association plus spécialement destinée aux personnes âgées?]".
VARIABLE LABELS sb177 "En ce qui concerne les associations ou clubs. Au total, à combien de réunions participez-vous chaque mois en moyenne (sans tenir compte des périodes de vacances, de maladie)?".
VARIABLE LABELS b178 "Existe-t-il dans votre quartier/village une association ou un club plus spécialement destiné aux personnes âgées? (club d'aînés,...)".
VARIABLE LABELS b179 "Participez-vous aux réunions de cette association ou club du village/quartier? (à un/e au moins) [Au cas où une association ou un club existe dans votre quartier/village:]".
VARIABLE LABELS b180 "À quelle fréquence participez-vous aux réunions? [Si oui:]".
VARIABLE LABELS b181 "Y avez-vous des responsabilités (par exemple: membre du comité, président/e, caissier/caissière, responsable d'un groupe, d'une activité)? [Si oui:]".
VARIABLE LABELS sb182aa "Conjoint/e (partenaire) [Vous occupez-vous d'une personne de votre entourage qui a besoin d'aide ou de soins?]".
VARIABLE LABELS sb182ba "Conjoint/e (partenaire) [Habitez-vous avec cette personne?]".
VARIABLE LABELS sb182ca "Conjoint/e (partenaire) [Vous vous en occupez:]".
VARIABLE LABELS b182ab "Père, mère, beau-père, belle-mère [Vous occupez-vous d'une personne de votre entourage qui a besoin d'aide ou de soins?]".
VARIABLE LABELS b182bb "Père, mère, beau-père, belle-mère [Habitez-vous avec cette personne?]".
VARIABLE LABELS b182cb "Père, mère, beau-père, belle-mère [Vous vous en occupez:]".
VARIABLE LABELS sb182ac "Enfants, petits-enfants [Vous occupez-vous d'une personne de votre entourage qui a besoin d'aide ou de soins?]".
VARIABLE LABELS sb182bc "Enfants, petits-enfants [Habitez-vous avec cette personne?]".
VARIABLE LABELS sb182cc "Enfants, petits-enfants [Vous vous en occupez:]".
VARIABLE LABELS sb182ad "Autre parent [Vous occupez-vous d'une personne de votre entourage qui a besoin d'aide ou de soins?]".
VARIABLE LABELS sb182bd "Autre parent [Habitez-vous avec cette personne?]".
VARIABLE LABELS sb182cd "Autre parent [Vous vous en occupez:]".
VARIABLE LABELS sb182ae "Amis, connaissances [Vous occupez-vous d'une personne de votre entourage qui a besoin d'aide ou de soins?]".
VARIABLE LABELS sb182be "Amis, connaissances [Habitez-vous avec cette personne?]".
VARIABLE LABELS sb182ce "Amis, connaissances [Vous vous en occupez:]".
VARIABLE LABELS sb182af "Voisins [Vous occupez-vous d'une personne de votre entourage qui a besoin d'aide ou de soins?]".
VARIABLE LABELS sb182bf "Voisins [Habitez-vous avec cette personne?]".
VARIABLE LABELS sb182cf "Voisins [Vous vous en occupez:]".
VARIABLE LABELS sb182ag "Autres [Vous occupez-vous d'une personne de votre entourage qui a besoin d'aide ou de soins?]".
VARIABLE LABELS sb182bg "Autres [Habitez-vous avec cette personne?]".
VARIABLE LABELS sb182cg "Autres [Vous vous en occupez:]".
VARIABLE LABELS sb183aa "Membre d'un réseau de visiteurs d'hôpital, de pension, de prison [Activité sociale, d'entraide:]".
VARIABLE LABELS sb183ba "Membre d'un réseau de visiteurs d'hôpital, de pension, de prison [À quelle fréquence?]".
VARIABLE LABELS sb183ab "Transport de personnes âgées, handicapées [Activité sociale, d'entraide:]".
VARIABLE LABELS sb183bb "Transport de personnes âgées, handicapées [À quelle fréquence?]".
VARIABLE LABELS sb183ac "Collecte de fonds pour des oeuvres [Activité sociale, d'entraide:]".
VARIABLE LABELS sb183bc "Collecte de fonds pour des oeuvres [À quelle fréquence?]".
VARIABLE LABELS sb183ad "Autre [Activité sociale, d'entraide:]".
VARIABLE LABELS sb183bd "Autre [À quelle fréquence?]".
VARIABLE LABELS sb184 "Votez-vous?".
VARIABLE LABELS b185 "Si vous comparez avec ce que vous faisiez quand vous aviez 45 ans environ, diriez-vous qu'aujourd'hui vous allez voter:".
VARIABLE LABELS b186aa "Clubs de randonnée, de marche [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186ba "Clubs de randonnée, de marche [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186ab "Autres associations ou clubs sportifs [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bb "Autres associations ou clubs sportifs [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186ac "Groupe de contemporains [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bc "Groupe de contemporains [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186ad "Hobbies (bricolage, ...) [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bd "Hobbies (bricolage, ...) [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186ae "Parti / association politique [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186be "Parti / association politique [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186af "Associations professionnelles, syndicales [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bf "Associations professionnelles, syndicales [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186ag "Associations patriotiques [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bg "Associations patriotiques [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186ah "Associations culturelles (musique, théâtre, chant, sociétés folkloriques, de carnaval) [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bh "Associations culturelles (musique, théâtre, chant, sociétés folkloriques, de carnaval) [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186ai "Paroisse, associations religieuses ou paroissiales (y compris chant sacré) [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bi "Paroisse, associations religieuses ou paroissiales (y compris chant sacré) [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186aj "Associations de bienfaisance, caritatives [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bj "Associations de bienfaisance, caritatives [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186ak "Solidarité (Tiers Monde, Droits de l'homme) [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bk "Solidarité (Tiers Monde, Droits de l'homme) [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186al "Associations de quartiers, d'habitants [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bl "Associations de quartiers, d'habitants [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b186am "Autre [À l'époque de vos 45 ans... Étiez-vous membre actif ou passif des associations suivantes?]".
VARIABLE LABELS b186bm "Autre [À l'époque de vos 45 ans... Aviez-vous des responsabilités dans cette association?]".
VARIABLE LABELS b187a "Seul/e [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187b "Actif/ve, entreprenant/e [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187c "Ennuyé/e (vous vous ennuyez) [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187d "Confiant/e [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187e "Insomniaque (difficulté à dormir) [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187f "Serein/e, calme [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187g "Triste [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187h "Sociable [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187i "Anxieux/se [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187j "Autonome, indépendant/e [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187k "Méfiant/e, peureux/se [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187l "Sûr/e de vous [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS b187m "Fatigué/e [Par rapport à voici 5 ans, c'est-à-dire en 1989, diriez-vous que vous vous sentez plus, le même, ou moins:]".
VARIABLE LABELS tb188 "Quelle est votre confession (religion)?".
VARIABLE LABELS mb191 "Fils / Fille [Enfant 5]".
VARIABLE LABELS mb192a "Année de naissance [Enfant 5]".
VARIABLE LABELS sb192b "Si décédé/e, année de décès [Enfant 5]".
VARIABLE LABELS mb193 "Quel est son état civil, aujourd'hui ? [Enfant 5]".
VARIABLE LABELS mb194 "A-t-il/elle des enfants ? (nombre) [Enfant 5]".
VARIABLE LABELS sb195 "Combien de ses enfants ont moins de 15 ans ? [Enfant 5]".
VARIABLE LABELS mb196 "A-t-il/elle des petits-enfants (vos arrière-petits-enfants) ? (nombre) [Enfant 5]".
VARIABLE LABELS sb197 "Quelle est l'activité principale de votre fils/fille ? [Enfant 5]".
VARIABLE LABELS sb198 "Quelle est l'activité principale de son/sa conjoint/e ? [Enfant 5]".
VARIABLE LABELS sb199 "Votre enfant habite : [Enfant 5]".
VARIABLE LABELS sb200 "Par rapport à vous, votre enfant habite : [Enfant 5]".
VARIABLE LABELS sb201 "Combien de temps vous faut-il pour vous rendre chez votre enfant ? (selon le moyen le plus commode) [Enfant 5]".
VARIABLE LABELS mb202 "Fils / Fille [Enfant 6]".
VARIABLE LABELS mb203a "Année de naissance [Enfant 6]".
VARIABLE LABELS sb203b "Si décédé/e, année de décès [Enfant 6]".
VARIABLE LABELS mb204 "Quel est son état civil, aujourd'hui ? [Enfant 6]".
VARIABLE LABELS mb205 "A-t-il/elle des enfants ? (nombre) [Enfant 6]".
VARIABLE LABELS sb206 "Combien de ses enfants ont moins de 15 ans ? [Enfant 6]".
VARIABLE LABELS mb207 "A-t-il/elle des petits-enfants (vos arrière-petits-enfants) ? (nombre) [Enfant 6]".
VARIABLE LABELS sb208 "Quelle est l'activité principale de votre fils/fille ? [Enfant 6]".
VARIABLE LABELS sb209 "Quelle est l'activité principale de son/sa conjoint/e ? [Enfant 6]".
VARIABLE LABELS sb210 "Votre enfant habite : [Enfant 6]".
VARIABLE LABELS sb211 "Par rapport à vous, votre enfant habite : [Enfant 6]".
VARIABLE LABELS sb212 "Combien de temps vous faut-il pour vous rendre chez votre enfant ? (selon le moyen le plus commode) [Enfant 6]".
VARIABLE LABELS mb213 "Fils / Fille [Enfant 7]".
VARIABLE LABELS mb214a "Année de naissance [Enfant 7]".
VARIABLE LABELS sb214b "Si décédé/e, année de décès [Enfant 7]".
VARIABLE LABELS mb215 "Quel est son état civil, aujourd'hui ? [Enfant 7]".
VARIABLE LABELS mb216 "A-t-il/elle des enfants ? (nombre) [Enfant 7]".
VARIABLE LABELS sb217 "Combien de ses enfants ont moins de 15 ans ? [Enfant 7]".
VARIABLE LABELS mb218 "A-t-il/elle des petits-enfants (vos arrière-petits-enfants) ? (nombre) [Enfant 7]".
VARIABLE LABELS sb219 "Quelle est l'activité principale de votre fils/fille ? [Enfant 7]".
VARIABLE LABELS sb220 "Quelle est l'activité principale de son/sa conjoint/e ? [Enfant 7]".
VARIABLE LABELS sb221 "Votre enfant habite : [Enfant 7]".
VARIABLE LABELS sb222 "Par rapport à vous, votre enfant habite : [Enfant 7]".
VARIABLE LABELS sb223 "Combien de temps vous faut-il pour vous rendre chez votre enfant ? (selon le moyen le plus commode) [Enfant 7]".
VARIABLE LABELS mb224 "Fils / Fille [Enfant 8]".
VARIABLE LABELS mb225a "Année de naissance [Enfant 8]".
VARIABLE LABELS sb225b "Si décédé/e, année de décès [Enfant 8]".
VARIABLE LABELS mb226 "Quel est son état civil, aujourd'hui ? [Enfant 8]".
VARIABLE LABELS mb227 "A-t-il/elle des enfants ? (nombre) [Enfant 8]".
VARIABLE LABELS sb228 "Combien de ses enfants ont moins de 15 ans ? [Enfant 8]".
VARIABLE LABELS mb229 "A-t-il/elle des petits-enfants (vos arrière-petits-enfants) ? (nombre) [Enfant 8]".
VARIABLE LABELS sb230 "Quelle est l'activité principale de votre fils/fille ? [Enfant 8]".
VARIABLE LABELS sb231 "Quelle est l'activité principale de son/sa conjoint/e ? [Enfant 8]".
VARIABLE LABELS sb232 "Votre enfant habite : [Enfant 8]".
VARIABLE LABELS sb233 "Par rapport à vous, votre enfant habite : [Enfant 8]".
VARIABLE LABELS sb234 "Combien de temps vous faut-il pour vous rendre chez votre enfant ? (selon le moyen le plus commode) [Enfant 8]".
VARIABLE LABELS mv1 "Quelle profession exerçait alors votre père?".
VARIABLE LABELS tv2 "Quelle est la dernière école que vous avez fréquentée régulièrement?".
VARIABLE LABELS mv3a "Certificat de capacité (fin d'apprentissage) [Avez-vous l'un ou l'autre des diplômes suivants?]".
VARIABLE LABELS mv3b "Maîtrise fédérale [Avez-vous l'un ou l'autre des diplômes suivants?]".
VARIABLE LABELS mv3c "Certificat de maturité (baccalauréat, Abitur) [Avez-vous l'un ou l'autre des diplômes suivants?]".
VARIABLE LABELS mv3d "Diplôme école professionnelle supérieure (p. ex. ETS, école sociale...) [Avez-vous l'un ou l'autre des diplômes suivants?]".
VARIABLE LABELS mv3e "Licence ou diplôme universitaire [Avez-vous l'un ou l'autre des diplômes suivants?]".
VARIABLE LABELS mv4 "En quelle année avez-vous exercé votre premier emploi régulier? (les apprentis, indiquer le premier travail après l'apprentissage)".
VARIABLE LABELS mv5 "Quelle activité professionnelle exerciez-vous alors?".
VARIABLE LABELS v6 "À l'époque de vos 45 ans, où habitiez-vous?".
VARIABLE LABELS sv7 "Quelle langue parliez-vous alors le plus souvent chez vous, à la maison?".
VARIABLE LABELS v8 "À 45 ans environ, quelle était votre activité principale?".
VARIABLE LABELS v9 "Exerciez-vous votre profession (travail, emploi) à ... ?".
VARIABLE LABELS v10 "Exerciez-vous alors, à côté du foyer, des enfants, un travail régulier rétribué?".
VARIABLE LABELS v11 "À quelle fraction de temps? [Ego - Travail à côté du foyer]".
VARIABLE LABELS v12 "Avez-vous repris ultérieurement une activité professionnelle régulière? (pendant plus d'une année au moins)".
VARIABLE LABELS v13 "À quelle fraction de temps? [Ego - Repris activité professionnelle]".
VARIABLE LABELS v14 "À l'époque de vos 45 ans, quelle était l'activité principale de votre conjoint/e?".
VARIABLE LABELS v15 "Exerçait-il/elle sa profession (emploi, travail) à: [Conjoint‎/e]".
VARIABLE LABELS v16 "Est-ce qu'à côté du foyer, votre conjoint/e exerçait un travail régulier rétribué?".
VARIABLE LABELS v17 "À quelle fraction de temps? [Conjoint‎/e - Travail à côté du foyer]".
VARIABLE LABELS v18 "Est-ce que votre conjoint/e a repris plus tard une activité professionnelle régulière?".
VARIABLE LABELS v19 "À quelle fraction de temps? [Conjoint‎/e - Repris activité professionnelle]".
VARIABLE LABELS sv20 "Quelle est actuellement votre situation professionnelle (activité principale)?".
VARIABLE LABELS v21 "Est-ce qu'actuellement vous travaillez à:".
VARIABLE LABELS sv22 "Quelle était votre profession principale à l'âge de 60 ans environ?".
VARIABLE LABELS sv23 "À l'âge de 60 ans, exerciez-vous cette profession à:".
VARIABLE LABELS sv24 "À l'âge de 60 ans environ, exerciez-vous cette profession comme indépendant ou non?".
VARIABLE LABELS sv25 "À l'âge de 60 ans environ, dans quel secteur d'activité exerciez-vous cette profession?".
VARIABLE LABELS sv26 "À l'âge de 60 ans environ, dans votre emploi principal aviez-vous des subordonnés et si oui combien?".
VARIABLE LABELS v27a "De la variété des tâches à assumer [Comment jugez-vous le travail principal que vous exerciez à 60 ans? Du point de vue...]".
VARIABLE LABELS v27b "Des responsabilités à assumer [Comment jugez-vous le travail principal que vous exerciez à 60 ans? Du point de vue...]".
VARIABLE LABELS v27c "Des contacts avec vos collègues de travail [Comment jugez-vous le travail principal que vous exerciez à 60 ans? Du point de vue...]".
VARIABLE LABELS v27d "Du salaire/rémunération [Comment jugez-vous le travail principal que vous exerciez à 60 ans? Du point de vue...]".
VARIABLE LABELS v27e "De l'expérience qu'il demande (demandait) [Comment jugez-vous le travail principal que vous exerciez à 60 ans? Du point de vue...]".
VARIABLE LABELS v27f "Tout compte fait, comment est (était)-il en général? [Comment jugez-vous le travail principal que vous exerciez à 60 ans?]".
VARIABLE LABELS v28a "Il était pénible physiquement, demandait de gros travaux [Diriez-vous de ce travail que:]".
VARIABLE LABELS v28b "Les conditions de travail en général laissaient à désirer (bruit, poussière, lumière, odeurs, chaleurs...) [Diriez-vous de ce travail que:]".
VARIABLE LABELS v29 "À 60 ans, en plus de votre emploi principal, exerciez-vous une profession accessoire (dans l'agriculture, la viticulture, le tourisme, le commerce, par exemple)?".
VARIABLE LABELS v30 "Quelle était cette profession accessoire?".
VARIABLE LABELS v31 "Exercez-vous toujours actuellement cette activité?".
VARIABLE LABELS v32a "Votre situation professionnelle s'est-elle: [Entre l'âge de 45 ans et l'âge de 60 ans environ, beaucoup de choses ont pu changer dans votre vie professionnelle. Comment cela s'est-il passé pour vous?]".
VARIABLE LABELS v32b "Du point de vue de l'intérêt, votre travail est-il devenu: [Entre l'âge de 45 ans et l'âge de 60 ans environ, beaucoup de choses ont pu changer dans votre vie professionnelle. Comment cela s'est-il passé pour vous?]".
VARIABLE LABELS v32c "Du point de vue de l'effort physique, votre travail est-il devenu: [Entre l'âge de 45 ans et l'âge de 60 ans environ, beaucoup de choses ont pu changer dans votre vie professionnelle. Comment cela s'est-il passé pour vous?]".
VARIABLE LABELS v32d "Les conditions de l'emploi (sécurité de l'emploi, nombre de jours de congé, durée des vacances) se sont-elles: [Entre l'âge de 45 ans et l'âge de 60 ans environ, beaucoup de choses ont pu changer dans votre vie professionnelle.".
VARIABLE LABELS v32e "La rémunération de votre travail s'est-elle: [Entre l'âge de 45 ans et l'âge de 60 ans environ, beaucoup de choses ont pu changer dans votre vie professionnelle. Comment cela s'est-il passé pour vous?]".
VARIABLE LABELS mv33 "Si vous deviez porter un jugement global sur votre vie professionnelle, diriez-vous que vous êtes?".
VARIABLE LABELS v34 "Exercez-vous, à côté du foyer, des enfants, un travail régulier rétribué?".
VARIABLE LABELS v35 "À quelle fraction de temps? [Travail à côté du foyer]".
VARIABLE LABELS v36a "Dans votre profession, un âge officiel (légal) de la retraite différent de l'âge AVS est-il fixé?".
VARIABLE LABELS v36b "Âge légal de la retraite si pas AVS".
VARIABLE LABELS v37 "Vous-même, voulez-vous me rappelez si vous atteint l'âge légal de la retraite?".
VARIABLE LABELS mv38a "Avez-vous pris votre retraite à l'âge légal, une retraite anticipée ou une retraite repoussée?".
VARIABLE LABELS mv38b "Âge de la retraite".
VARIABLE LABELS mv39a "Nécessité financière [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail? S'agissait-il de:]".
VARIABLE LABELS mv39b "Intérêt pour le travail [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail? S'agissait-il de:]".
VARIABLE LABELS mv39c "Besoin de contact avec des collègues de travail [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail? S'agissait-il de:]".
VARIABLE LABELS mv39d "Désir d'une vie bien réglée [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail? S'agissait-il de:]".
VARIABLE LABELS mv39e "Autres raisons [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail? S'agissait-il de:]".
VARIABLE LABELS mv39f "Besoin de l'entreprise [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail? S'agissait-il de:]".
VARIABLE LABELS mv40a "J'ai été mis à la retraite anticipée du fait de problèmes de l'entreprise [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite?]".
VARIABLE LABELS mv40b "J'ai passé à la retraite suite à une période de chômage [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite?]".
VARIABLE LABELS mv40c "J'ai eu des problèmes de santé [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite?]".
VARIABLE LABELS mv40d "J'en avais assez de mon travail [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite?]".
VARIABLE LABELS mv40e "J'avais des moyens financiers suffisants pour pouvoir arrêter de travailler [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite?]".
VARIABLE LABELS mv40f "Pour profiter de la retraite avec mon/ma conjoint/e [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite?]".
VARIABLE LABELS mv40g "Parce que mon/ma conjoint/e avait des problèmes de santé [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite?]".
VARIABLE LABELS mv40h "Autres raisons [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite?]".
VARIABLE LABELS v41a "Changé d'entreprise [Entre l'âge de 60 ans et l'année qui a précédé votre retraite, est-ce que vous avez:]".
VARIABLE LABELS v41b "Changé de poste de travail dans la même entreprise [Entre l'âge de 60 ans et l'année qui a précédé votre retraite, est-ce que vous avez:]".
VARIABLE LABELS v41c "Diminué votre horaire de travail [Entre l'âge de 60 ans et l'année qui a précédé votre retraite, est-ce que vous avez:]".
VARIABLE LABELS v42 "Actuellement, exercez-vous une activité rémunérée (travail), même à temps très partiel ou occasionnellement?".
VARIABLE LABELS v43 "Voyez-vous encore certains de vos anciens collègues de travail?".
VARIABLE LABELS v44a "La compagnie ou les contacts humains [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite?]".
VARIABLE LABELS v44b "Le sentiment d'être utile [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite?]".
VARIABLE LABELS v44c "La considération, pouvoir dire son mot [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite?]".
VARIABLE LABELS v44d "L'intérêt que présentait votre travail [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite?]".
VARIABLE LABELS v44e "Les rentrées d'argent [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite?]".
VARIABLE LABELS v44f "Savoir ce qu'on va faire de la journée [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite?]".
VARIABLE LABELS v44g "D'autres choses [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite?]".
VARIABLE LABELS v45a "La vie paisible [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes?]".
VARIABLE LABELS v45b "Ne plus être lié, ne plus avoir de responsabilités [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes?]".
VARIABLE LABELS v45c "L'occupation personnelle [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes?]".
VARIABLE LABELS v45d "Ne plus devoir travailler [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes?]".
VARIABLE LABELS v45e "La pension [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes?]".
VARIABLE LABELS v45f "Faire ce qu'on a envie de faire [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes?]".
VARIABLE LABELS v45g "D'autres choses [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes?]".
VARIABLE LABELS v46 "Imaginons que vous ayez pu choisir en toute liberté le moment de prendre votre retraite (sans tenir compte des questions d'argent, de santé, etc.). Est-ce que vous auriez choisi de la prendre:".
VARIABLE LABELS v47a "Actuellement, est-ce que vous pensez que vous prendrez la retraite à l'âge légal, une retraite anticipée ou une retraite repoussée?".
VARIABLE LABELS v47b "Âge estimé pour la retraite".
VARIABLE LABELS v48a "Chômage et problèmes économiques [Les raisons suivantes interviennent-elles dans le fait que vous pensez prendre une retraite anticipée?]".
VARIABLE LABELS v48b "J'ai des problèmes de santé [Les raisons suivantes interviennent-elles dans le fait que vous pensez prendre une retraite anticipée?]".
VARIABLE LABELS v48c "J'en ai assez de mon travail [Les raisons suivantes interviennent-elles dans le fait que vous pensez prendre une retraite anticipée?]".
VARIABLE LABELS v48d "J'ai les moyens économiques (revenu, caisse de retraite) suffisants pour arrêter [Les raisons suivantes interviennent-elles dans le fait que vous pensez prendre une retraite anticipée?]".
VARIABLE LABELS v48e "Pour profiter de la retrait de mon/ma conjoint/e et vivre avec lui/elle [Les raisons suivantes interviennent-elles dans le fait que vous pensez prendre une retraite anticipée?]".
VARIABLE LABELS v48f "Parce que mon/ma conjoint/e a des problèmes de santé [Les raisons suivantes interviennent-elles dans le fait que vous pensez prendre une retraite anticipée?]".
VARIABLE LABELS v48g "Autres raisons [Les raisons suivantes interviennent-elles dans le fait que vous pensez prendre une retraite anticipée?]".
VARIABLE LABELS v49a "Nécessité financière [Lesquelles parmi les raisons suivantes vous poussent à envisager de repousser votre retraite?]".
VARIABLE LABELS v49b "Intérêt pour le travail [Lesquelles parmi les raisons suivantes vous poussent à envisager de repousser votre retraite?]".
VARIABLE LABELS v49c "Besoin de contact avec des collègues de travail [Lesquelles parmi les raisons suivantes vous poussent à envisager de repousser votre retraite?]".
VARIABLE LABELS v49d "Désir d'une vie bien réglée [Lesquelles parmi les raisons suivantes vous poussent à envisager de repousser votre retraite?]".
VARIABLE LABELS v49e "Parce que mon/ma conjoint/e travaillera encore [Lesquelles parmi les raisons suivantes vous poussent à envisager de repousser votre retraite?]".
VARIABLE LABELS v49f "Autres raisons [Lesquelles parmi les raisons suivantes vous poussent à envisager de repousser votre retraite?]".
VARIABLE LABELS v50a "Imaginons que vous puissiez choisir en toute liberté (sans tenir compte des questions d'argent, de santé, etc.) le moment de prendre votre retraite. Est-ce que vous prendriez votre retraite:".
VARIABLE LABELS v50b "Âge souhaité pour la retraite".
VARIABLE LABELS v51a "Changé d'entreprise [Entre l'âge de 60 ans et maintenant, est-ce que vous avez:]".
VARIABLE LABELS v51b "Changé de poste de travail dans la même entreprise [Entre l'âge de 60 ans et maintenant, est-ce que vous avez:]".
VARIABLE LABELS v51c "Diminué votre horaire de travail [Entre l'âge de 60 ans et maintenant, est-ce que vous avez:]".
VARIABLE LABELS v52a "Nécessité financière [Lesquelles parmi ces raisons vous poussent à poursuivre votre travail au-delà de l'âge légal?]".
VARIABLE LABELS v52b "Intérêt pour le travail [Lesquelles parmi ces raisons vous poussent à poursuivre votre travail au-delà de l'âge légal?]".
VARIABLE LABELS v52c "Besoin de contact avec des collègues de travail [Lesquelles parmi ces raisons vous poussent à poursuivre votre travail au-delà de l'âge légal?]".
VARIABLE LABELS v52d "Désir d'une vie bien réglée [Lesquelles parmi ces raisons vous poussent à poursuivre votre travail au-delà de l'âge légal?]".
VARIABLE LABELS v52e "Parce que mon/ma conjoint/e travaillera encore [Lesquelles parmi ces raisons vous poussent à poursuivre votre travail au-delà de l'âge légal?]".
VARIABLE LABELS v52f "Autres raisons [Lesquelles parmi ces raisons vous poussent à poursuivre votre travail au-delà de l'âge légal?]".
VARIABLE LABELS v53 "Dans combien de temps pensez-vous prendre votre retraite?".
VARIABLE LABELS v54a "Imaginons que vous puissiez choisir en toute liberté (sans tenir compte des questions d'argent, de santé, etc.) le moment où vous pouvez prendre votre retraite. Est-ce que:".
VARIABLE LABELS v54b "Âge souhaité pour la retraite".
VARIABLE LABELS v55 "Pensez-vous retrouver un emploi?".
VARIABLE LABELS v56a "Quand pourrez-vous bénéficier de la pension de retraite?".
VARIABLE LABELS v56b "Nombre d'années avant la retraite".
VARIABLE LABELS v57 "Votre chômage actuel affectera-t-il le montant de votre pension de retraite? (pas AVS mais caisse de retraite)".
VARIABLE LABELS v58a "Imaginons que vous puissiez choisir en toute liberté (sans tenir compte des questions d'argent, de santé, etc.) le moment de prendre la retraite, est-ce que vous choisiriez de la prendre:".
VARIABLE LABELS v58b "Âge souhaité pour la retraite".
VARIABLE LABELS sv59 "Quelle est actuellement l'activité principale de votre conjoint/e?".
VARIABLE LABELS sv60 "Quelle était sa profession à 60 ans?".
VARIABLE LABELS sv61 "Est-ce que votre conjoint/e exerçait sa profession à:".
VARIABLE LABELS sv62 "Est-ce que votre conjoint exerçait sa profession comme indépendant ou non?".
VARIABLE LABELS sv63 "Dans quel secteur d'activité, votre conjoint/e exerçait-il/elle son activité?".
VARIABLE LABELS sv64 "Est-ce que votre conjoint/e a des subordonnés, et si oui combien?".
VARIABLE LABELS sv65a "Votre conjoint/e a-t-il pris sa retraite...".
VARIABLE LABELS sv65b "Âge de la retraite conjoint/e".
VARIABLE LABELS v66a "Nécessité financière [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à poursuivre son travail? S'agissait-il de:]".
VARIABLE LABELS v66b "Intérêt pour le travail [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à poursuivre son travail? S'agissait-il de:]".
VARIABLE LABELS v66c "Besoin de contact avec des collègues de travail [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à poursuivre son travail? S'agissait-il de:]".
VARIABLE LABELS v66d "Désir d'une vie bien réglée [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à poursuivre son travail? S'agissait-il de:]".
VARIABLE LABELS v66e "Autres raisons [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à poursuivre son travail? S'agissait-il de:]".
VARIABLE LABELS v66f "Besoin de l'entreprise [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à poursuivre son travail? S'agissait-il de:]".
VARIABLE LABELS v67a "Il/elle a été mis à la retraite anticipée du fait de problèmes de l'entreprise [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à prendre une retraite anticipée?]".
VARIABLE LABELS v67b "Il/elle a passé à la retraite suite à une période de chômage [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à prendre une retraite anticipée?]".
VARIABLE LABELS v67c "Il/elle a eu des problèmes de santé [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à prendre une retraite anticipée?]".
VARIABLE LABELS v67d "Il/elle en avait assez de mon travail [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à prendre une retraite anticipée?]".
VARIABLE LABELS v67e "Il/elle avait des moyens financiers suffisants pour pouvoir arrêter de travailler [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à prendre une retraite anticipée?]".
VARIABLE LABELS v67f "Pour profiter de la retraite avec moi [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à prendre une retraite anticipée?]".
VARIABLE LABELS v67g "Parce que j'avais des problèmes de santé [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à prendre une retraite anticipée?]".
VARIABLE LABELS v67h "Autres raisons [Parmi les raisons suivantes, lesquelles ont poussé votre conjoint/e à prendre une retraite anticipée?]".
VARIABLE LABELS v68 "Admettons que votre conjoint/e ait pu choisir en toute liberté le moment de prendre sa retraite (sans tenir compte des questions d'argent, de santé, etc.). Est-ce qu'il l'aurait prise:".
VARIABLE LABELS v69 "Actuellement, votre conjoint/e exerce-t-il une activité rémunérée, même à temps très partiel ou occasionnellement?".
VARIABLE LABELS v70a "La vie paisible [Vous personnellement, dans les cinq première années qui ont suivi la retraite de votre conjoint/e, comment avez-vous apprécié les choses suivantes?]".
VARIABLE LABELS v70b "Avoir mon/ma conjoint/e à la maison [Vous personnellement, dans les cinq première années qui ont suivi la retraite de votre conjoint/e, comment avez-vous apprécié les choses suivantes?]".
VARIABLE LABELS v70c "La pension de retraite et l'AVS [Vous personnellement, dans les cinq première années qui ont suivi la retraite de votre conjoint/e, comment avez-vous apprécié les choses suivantes?]".
VARIABLE LABELS v70d "Pouvoir faire ensemble avec mon conjoint/e ce qu'on avait envie de faire [Vous personnellement, dans les cinq première années qui ont suivi la retraite de votre conjoint/e, comment avez-vous apprécié les choses suivantes?]".
VARIABLE LABELS v71 "Rappelez-moi, s'il vous plaît, si votre conjoint/e a déjà atteint l'âge légal de la retraite?".
VARIABLE LABELS v72a "Actuellement, est-ce que votre conjoint/e pense prendre la retraite à l'âge légal, une retraite anticipée ou une retraite repoussée?".
VARIABLE LABELS v72b "Âge estimé pour la retraite conjoint‎/e".
VARIABLE LABELS v73a "Chômage et problèmes économiques [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à penser prendre une retraite anticipée?]".
VARIABLE LABELS v73b "Il a des problèmes de santé [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à penser prendre une retraite anticipée?]".
VARIABLE LABELS v73c "Il en a assez de son travail [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à penser prendre une retraite anticipée?]".
VARIABLE LABELS v73d "Il a les moyens économiques (revenu, caisse de retraite) suffisants pour arrêter [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à penser prendre une retraite anticipée?]".
VARIABLE LABELS v73e "Pour profiter de ma retraite et vivre avec moi [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à penser prendre une retraite anticipée?]".
VARIABLE LABELS v73f "Parce que j'ai des problèmes de santé [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à penser prendre une retraite anticipée?]".
VARIABLE LABELS v73g "Autres raisons [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à penser prendre une retraite anticipée?]".
VARIABLE LABELS v74a "Nécessité financière [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à envisager de repousser sa retraite?]".
VARIABLE LABELS v74b "Intérêt pour le travail [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à envisager de repousser sa retraite?]".
VARIABLE LABELS v74c "Besoin de contact avec des collègues de travail [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à envisager de repousser sa retraite?]".
VARIABLE LABELS v74d "Désir d'une vie bien réglée [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à envisager de repousser sa retraite?]".
VARIABLE LABELS v74e "Parce que je travaillerai encore [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à envisager de repousser sa retraite?]".
VARIABLE LABELS v74f "Autres raisons [Parmi les raisons suivantes, lesquelles poussent votre conjoint/e à envisager de repousser sa retraite?]".
VARIABLE LABELS v75a "Imaginons que votre conjoint/e puisse choisir en toute liberté quand prendre sa retraite (sans tenir compte des questions d'argent, de santé, etc.). Est-ce que qu'il/elle la prendrait:".
VARIABLE LABELS v75b "Âge souhaité pour la retraite conjoint‎/e".
VARIABLE LABELS v76a "Nécessité financière [Lesquelles parmi ces raisons poussent votre conjoint/e à poursuivre son travail au-delà de l'âge légal?]".
VARIABLE LABELS v76b "Intérêt pour le travail [Lesquelles parmi ces raisons poussent votre conjoint/e à poursuivre son travail au-delà de l'âge légal?]".
VARIABLE LABELS v76c "Besoin de contact avec des collègues de travail [Lesquelles parmi ces raisons poussent votre conjoint/e à poursuivre son travail au-delà de l'âge légal?]".
VARIABLE LABELS v76d "Désir d'une vie bien réglée [Lesquelles parmi ces raisons poussent votre conjoint/e à poursuivre son travail au-delà de l'âge légal?]".
VARIABLE LABELS v76e "Parce que je travaille encore [Lesquelles parmi ces raisons poussent votre conjoint/e à poursuivre son travail au-delà de l'âge légal?]".
VARIABLE LABELS v76f "Autres raisons [Lesquelles parmi ces raisons poussent votre conjoint/e à poursuivre son travail au-delà de l'âge légal?]".
VARIABLE LABELS v77 "Dans combien de temps votre conjoint/e pense-t-il/elle prendre sa retraite?".
VARIABLE LABELS v78a "Imaginons que votre conjoint/e puisse choisir en toute liberté (sans tenir compte des questions d'argent, de santé, etc.) le moment où prendre sa retraite. Est-ce qu'il/elle:".
VARIABLE LABELS v78b "Âge souhaité pour la retraite conjoint‎/e".
VARIABLE LABELS v79 "Est-ce que votre conjoint/e exerce à côté du foyer, du ménage un travail régulier rétribué?".
VARIABLE LABELS v80 "À quelle fraction de temps?".
VARIABLE LABELS mv81 "À l'époque qui a précédé son décès, votre conjoint/e:".
VARIABLE LABELS mv82 "Depuis quelle année était-il/elle à l'AI?".
VARIABLE LABELS mv83 "S'agissait-il d'une invalidité totale ou partielle?".
VARIABLE LABELS mv84 "A-t-il/elle passé à l'AI à la suite:".
VARIABLE LABELS mv85 "Votre conjoint/e avait-il/elle".
VARIABLE LABELS v86a "Nécessité financière [Parmi les raisons suivantes, lesquelles lui ont fait repoussé sa retraite?]".
VARIABLE LABELS v86b "Intérêt pour le travail [Parmi les raisons suivantes, lesquelles lui ont fait repoussé sa retraite?]".
VARIABLE LABELS v86c "Besoin de contact avec des collègues de travail [Parmi les raisons suivantes, lesquelles lui ont fait repoussé sa retraite?]".
VARIABLE LABELS v86d "Désir d'une vie bien réglée [Parmi les raisons suivantes, lesquelles lui ont fait repoussé sa retraite?]".
VARIABLE LABELS v86e "Parce que je travaillais encore [Parmi les raisons suivantes, lesquelles lui ont fait repoussé sa retraite?]".
VARIABLE LABELS v86f "Autres raisons [Parmi les raisons suivantes, lesquelles lui ont fait repoussé sa retraite?]".
VARIABLE LABELS v86g "Besoin de l'entreprise [Parmi les raisons suivantes, lesquelles lui ont fait repoussé sa retraite?]".
VARIABLE LABELS v87a "Chômage et problèmes économiques [Parmi les raisons suivantes, lesquelles l'ont poussé à prendre une retraite anticipée?]".
VARIABLE LABELS v87b "Il/elle avait des problèmes de santé [Parmi les raisons suivantes, lesquelles l'ont poussé à prendre une retraite anticipée?]".
VARIABLE LABELS v87c "Il/elle en avait assez de son travail [Parmi les raisons suivantes, lesquelles l'ont poussé à prendre une retraite anticipée?]".
VARIABLE LABELS v87d "Il/elle avait les moyens économiques (revenu, caisse de retraite) suffisants pour arrêter [Parmi les raisons suivantes, lesquelles l'ont poussé à prendre une retraite anticipée?]".
VARIABLE LABELS v87e "Pour profiter de ma retraite et vivre avec moi [Parmi les raisons suivantes, lesquelles l'ont poussé à prendre une retraite anticipée?]".
VARIABLE LABELS v87f "Parce que j'avais des problèmes de santé [Parmi les raisons suivantes, lesquelles l'ont poussé à prendre une retraite anticipée?]".
VARIABLE LABELS v87g "Autres raisons [Parmi les raisons suivantes, lesquelles l'ont poussé à prendre une retraite anticipée?]".
VARIABLE LABELS mv88 "Est-ce que le décès de votre conjoint s'est produit subitement?".
VARIABLE LABELS mv89 "Est-ce que votre conjoint/e était gravement malade (accidenté) depuis longtemps?".
VARIABLE LABELS mv90 "Pendant sa période maladie, est-ce que votre conjoint/e était principalement à la maison ou à l'hôpital?".
VARIABLE LABELS mv91 "Pendant cette période, est-ce que sa maladie, en plus du souci, vous a causé beaucoup de travail, pris beaucoup de temps?".
VARIABLE LABELS mv92a "Enfant/s [Durant cette période, est-ce que vous avez bénéficié d'aide et de soutien:]".
VARIABLE LABELS uv92b "Père/mère [Durant cette période, est-ce que vous avez bénéficié d'aide et de soutien:]".
VARIABLE LABELS mv92c "Frère(s) / soeur(s) [Durant cette période, est-ce que vous avez bénéficié d'aide et de soutien:]".
VARIABLE LABELS mv92d "Un autre parent [Durant cette période, est-ce que vous avez bénéficié d'aide et de soutien:]".
VARIABLE LABELS mv92e "Ami(s) [Durant cette période, est-ce que vous avez bénéficié d'aide et de soutien:]".
VARIABLE LABELS mv92f "Voisin(s) [Durant cette période, est-ce que vous avez bénéficié d'aide et de soutien:]".
VARIABLE LABELS mv92g "De services sociaux ou d'associations religieuses [Durant cette période, est-ce que vous avez bénéficié d'aide et de soutien:]".
VARIABLE LABELS mv92h "Autres aides [Durant cette période, est-ce que vous avez bénéficié d'aide et de soutien:]".
VARIABLE LABELS mv93a "Les problèmes financiers [Lorsque vous vous êtes retrouvé‎/e seul‎/e après le décès de votre conjoint‎/e, les problèmes que je vais énumérer maintenant ont-ils été pour vous des problèmes très graves ‎/ des problèmes".
VARIABLE LABELS mv93b "Le manque de contacts sociaux [Lorsque vous vous êtes retrouvé‎/e seul‎/e après le décès de votre conjoint‎/e, les problèmes que je vais énumérer maintenant ont-ils été pour vous des problèmes très graves ‎/ des problèmes".
VARIABLE LABELS mv93c "La solitude, le fait d'être seul [Lorsque vous vous êtes retrouvé‎/e seul‎/e après le décès de votre conjoint‎/e, les problèmes que je vais énumérer maintenant ont-ils été pour vous des problèmes très graves ‎/ des problèmes".
VARIABLE LABELS mv93d "La difficulté de trouver du travail [Lorsque vous vous êtes retrouvé‎/e seul‎/e après le décès de votre conjoint‎/e, les problèmes que je vais énumérer maintenant ont-ils été pour vous des problèmes très graves ‎/ des".
VARIABLE LABELS mv93e "S'habituer à tout faire seul/e [Lorsque vous vous êtes retrouvé‎/e seul‎/e après le décès de votre conjoint‎/e, les problèmes que je vais énumérer maintenant ont-ils été pour vous des problèmes très graves ‎/ des problèmes".
VARIABLE LABELS mv93f "Prendre seul/e certaines responsabilités [Lorsque vous vous êtes retrouvé‎/e seul‎/e après le décès de votre conjoint‎/e, les problèmes que je vais énumérer maintenant ont-ils été pour vous des problèmes très graves ‎/ des".
VARIABLE LABELS mv93g "La vie n'avait plus de sens [Lorsque vous vous êtes retrouvé‎/e seul‎/e après le décès de votre conjoint‎/e, les problèmes que je vais énumérer maintenant ont-ils été pour vous des problèmes très graves ‎/ des problèmes".
VARIABLE LABELS mv93h "Autre [Lorsque vous vous êtes retrouvé‎/e seul‎/e après le décès de votre conjoint‎/e, les problèmes que je vais énumérer maintenant ont-ils été pour vous des problèmes très graves ‎/ des problèmes importants, mais pas très".
VARIABLE LABELS mv94 "Pour faire face à ces problèmes, avez-vous pu compter sur l'aide de quelqu'un?".
VARIABLE LABELS mv95a "Enfant/s [Avez-vous pu compter sur l'aide:]".
VARIABLE LABELS uv95b "Père/mère [Avez-vous pu compter sur l'aide:]".
VARIABLE LABELS mv95c "Frère(s) / soeur(s) [Avez-vous pu compter sur l'aide:]".
VARIABLE LABELS mv95d "Un autre parent [Avez-vous pu compter sur l'aide:]".
VARIABLE LABELS mv95e "Ami(s) [Avez-vous pu compter sur l'aide:]".
VARIABLE LABELS mv95f "Voisin(s) [Avez-vous pu compter sur l'aide:]".
VARIABLE LABELS mv95g "De services sociaux ou d'associations religieuses [Avez-vous pu compter sur l'aide:]".
VARIABLE LABELS mv95h "Autres aides [Avez-vous pu compter sur l'aide:]".
VARIABLE LABELS v96 "Avez-vous cherché de l'aide?".
VARIABLE LABELS v97 "Avez-vous entendu parler de cours de préparation à la retraite?".
VARIABLE LABELS v98 "Vous a-t-on invité (par l'intermédiaire de votre entreprise ou par d'autres voies) à suivre des cours de préparation à la retraite?".
VARIABLE LABELS v99 "Avez-vous participé à, ou participez-vous actuellement à, des cours de préparation à la retraite?".
VARIABLE LABELS v100 "S'agissait-il?".
VARIABLE LABELS mv101 "Dans notre société, si vous comparez les retraités avec les personnes actives professionnellement, pensez-vous que, dans l'ensemble, les retraités sont:".
VARIABLE LABELS mv102 "Et du point de vue de leurs conditions économiques, pensez-vous que, dans l'ensemble, les retraités vivent dans:".
VARIABLE LABELS v103a "L'occasion de profiter de la vie, de faire ce qu'on aime faire [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103b "Des difficultés matérielles [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103c "Le sentiment d'être inutile [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103d "La liberté, l'indépendance [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103e "Des problèmes de santé, ou la crainte de problèmes de santé [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103f "Une nouvelle étape de la vie [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103g "L'ennui [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103h "L'occasion de faire des choses nouvelles, qu'on n'a pas faites avant [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103i "L'occasion de s'occuper plus de ceux qu'on aime, de sa famille [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103j "En gros, la même chose qu'avant [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103k "La fin de la vie [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103l "L'occasion de connaître des gens nouveaux [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103m "La vieillesse [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103n "L'occasion de se rendre utile [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103o "La mise à l'écart [Pour vous, la retraite c'est....]".
VARIABLE LABELS v103p "L'âge d'or [Pour vous, la retraite c'est....]".
VARIABLE LABELS tv104aa "D'une maladie [Souffrez-vous actuellement:]".
VARIABLE LABELS sv104ca "Depuis quel âge environ? [Maladie]".
VARIABLE LABELS tv104ab "Des suites d'un accident [Souffrez-vous actuellement:]".
VARIABLE LABELS sv104cb "Depuis quel âge environ? [Accident]".
VARIABLE LABELS tv104ac "Des suites d'une opération [Souffrez-vous actuellement:]".
VARIABLE LABELS sv104cc "Depuis quel âge environ? [Opération]".
VARIABLE LABELS sv105aa "Médecin [Au cours de la dernière année, avez-vous été chez le/ou reçu la visite du:]".
VARIABLE LABELS sv105ba "Combien de fois environ? [Médecin]".
VARIABLE LABELS sv105ab "Dentiste [Au cours de la dernière année, avez-vous été chez le/ou reçu la visite du:]".
VARIABLE LABELS sv105bb "Combien de fois environ? [Dentiste]".
VARIABLE LABELS sv105ar "Nombre de visites chez le médecin (recodé)".
VARIABLE LABELS sv105br "Nombre de visites chez le dentiste (recodé)".
VARIABLE LABELS lv106 "Au cours de la dernière année (12 derniers mois), avez-vous été hospitalisé/e?".
VARIABLE LABELS sv107 "Combien de fois avez-vous été hospitalisé/e?".
VARIABLE LABELS sv107r "Nombre de séjours hospitaliers (recodé)".
VARIABLE LABELS sv108 "Actuellement, êtes-vous bien remis?".
VARIABLE LABELS sv109aa "Aide principale: conjoint‎/e [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ab "Aide principale: fille/belle-fille [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ac "Aide principale: fils/beau-fils [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS v109ad "Aide principale: père, mère, beaux-parents [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ae "Aide principale: soeur, frère [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109af "Aide principale: petits-enfants [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ag "Aide principale: ami/e [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ah "Aide principale: voisin/e [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ai "Aide principale: femme de ménage, personnel de maison [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109aj "Aide principale: bénévoles [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ba "Autres aides: conjoint‎/e [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109bb "Autres aides: fille/belle-fille [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109bc "Autres aides: fils/beau-fils [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS v109bd "Autres aides: père, mère, beaux-parents [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109be "Autres aides: soeur, frère [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109bf "Autres aides: petits-enfants [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109bg "Autres aides: ami/e [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109bh "Autres aides: voisin/e [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109bi "Autres aides: femme de ménage, personnel de maison [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109bj "Autres aides: bénévoles [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ca "Si l'aide habite avec la personne: conjoint‎/e [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109cb "Si l'aide habite avec la personne: fille/belle-fille [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109cc "Si l'aide habite avec la personne: fils/beau-fils [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS v109cd "Si l'aide habite avec la personne: père, mère, beaux-parents [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ce "Si l'aide habite avec la personne: soeur, frère [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109cf "Si l'aide habite avec la personne: petits-enfants [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109cg "Si l'aide habite avec la personne: ami/e [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ch "Si l'aide habite avec la personne: voisin/e [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109ci "Si l'aide habite avec la personne: femme de ménage, personnel de maison [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv109cj "Si l'aide habite avec la personne: bénévoles [Quand vous avez été hospitalisé/e, hormis le personnel hospitalier, qui s'est occupé de vous?]".
VARIABLE LABELS sv110a "Où avez-vous été hospitalisé/e? Premier séjour".
VARIABLE LABELS sv110b "Où avez-vous été hospitalisé/e? Deuxième séjour".
VARIABLE LABELS sv110c "Où avez-vous été hospitalisé/e? Troisième séjour".
VARIABLE LABELS mv111 "Au cours de ces trois derniers mois, avez-vous dû rester au lit quelques jours pour cause de maladie ou d'accident (plus d'un jour)?".
VARIABLE LABELS mv112 "Quand était-ce la dernière fois?".
VARIABLE LABELS mv113 "Combien de temps environ avez-vous dû rester au lit?".
VARIABLE LABELS sv114aa "Aide principale: conjoint‎/e [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ab "Aide principale: fille/belle-fille [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ac "Aide principale: fils/beau-fils [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS v114ad "Aide principale: père, mère, beaux-parents [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ae "Aide principale: soeur, frère [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114af "Aide principale: petits-enfants [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ag "Aide principale: ami/e [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ah "Aide principale: voisin/e [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ai "Aide principale: femme de ménage, personnel de maison [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114aj "Aide principale: bénévoles [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ak "Aide principale: professionnel de la santé [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ba "Autres aides: conjoint‎/e [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114bb "Autres aides: fille/belle-fille [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114bc "Autres aides: fils/beau-fils [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS v114bd "Autres aides: père, mère, beaux-parents [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114be "Autres aides: soeur, frère [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114bf "Autres aides: petits-enfants [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114bg "Autres aides: ami/e [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114bh "Autres aides: voisin/e [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114bi "Autres aides: femme de ménage, personnel de maison [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114bj "Autres aides: bénévoles [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114bk "Autres aides: professionnel de la santé [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ca "Si l'aide habite avec la personne: conjoint‎/e [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114cb "Si l'aide habite avec la personne: fille/belle-fille [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114cc "Si l'aide habite avec la personne: fils/beau-fils [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS v114cd "Si l'aide habite avec la personne: père, mère, beaux-parents [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ce "Si l'aide habite avec la personne: soeur, frère [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114cf "Si l'aide habite avec la personne: petits-enfants [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114cg "Si l'aide habite avec la personne: ami/e [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ch "Si l'aide habite avec la personne: voisin/e [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ci "Si l'aide habite avec la personne: femme de ménage, personnel de maison [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114cj "Si l'aide habite avec la personne: bénévoles [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114ck "Si l'aide habite avec la personne: professionnel de la santé [Quand vous avez dû garder le lit, qui s'est occupé de vous?]".
VARIABLE LABELS sv114d "Enquêteur: en synthèse [aide quand ego a dû garder le lit]".
VARIABLE LABELS mv115a "J'ai bon appétit [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115b "Je me sens un peu isolé/e, un peu seul/e même parmi des amis [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115c "J'ai de la peine à dormir la nuit [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115d "Je prends plaisir à ce que je fais [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115e "Je trouve le temps long [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115f "Je me sens triste [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115g "J'ai confiance en moi [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115h "J'ai des crises de larmes et envie de pleurer [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115i "Je me sens fatigué‎/e [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115j "Je me fais du souci pour ma santé [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115k "Je me sens irritable [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115l "J'ai confiance en l'avenir [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS mv115m "Je me sens anxieux, /anxieuse, angoissé/e [Voici une série d'affirmations. Pouvez-vous dire, s.v.p., si c'est votre cas en choisissant entre: toujours, souvent, rarement ou jamais:]".
VARIABLE LABELS tv116 "Dans la vie de tous les jours arrive-t-il que votre mémoire vous joue des tours?".
VARIABLE LABELS sv117 "Vous arrive-t-il de passer plusieurs heures, voire une journée entière, à ne rien faire, parce que vous n'arrivez pas à vous y mettre?".
VARIABLE LABELS sv118a "J'essaierais de penser à autre chose [Nous allons imaginer une situation (...) vous vous cassiez la jambe. (...) Comment réagiriez-vous?]".
VARIABLE LABELS sv118b "Je me dirais que ça ne vaut pas la peine de s'énerver pour ce qui s'est passé [Nous allons imaginer une situation (...) vous vous cassiez la jambe. (...) Comment réagiriez-vous?]".
VARIABLE LABELS sv118c "Je me rendrais compte que de toute façon moi-même je n'aurais rien pu changer [Nous allons imaginer une situation (...) vous vous cassiez la jambe. (...) Comment réagiriez-vous?]".
VARIABLE LABELS sv118d "Je me dirais que j'ai déjà fait face à des situations beaucoup plus difficiles [Nous allons imaginer une situation (...) vous vous cassiez la jambe. (...) Comment réagiriez-vous?]".
VARIABLE LABELS sv118e "J'essaierais de trouver quelqu'un qui me donne du courage [Nous allons imaginer une situation (...) vous vous cassiez la jambe. (...) Comment réagiriez-vous?]".
VARIABLE LABELS sv118f "Je m'imaginerais que les choses vont s'arranger d'elles-mêmes [Nous allons imaginer une situation (...) vous vous cassiez la jambe. (...) Comment réagiriez-vous?]".
VARIABLE LABELS sv118g "Je passerais en revue tout ce que je peux faire pour me rétablir le plus vite possible [Nous allons imaginer une situation (...) vous vous cassiez la jambe. (...) Comment réagiriez-vous?]".
VARIABLE LABELS sv118h "Je me dirais que ça pourrait être encore bien pire [Nous allons imaginer une situation (...) vous vous cassiez la jambe. (...) Comment réagiriez-vous?]".
VARIABLE LABELS sv118i "J'essaierais d'adapter ma vie aux circonstances [Nous allons imaginer une situation (...) vous vous cassiez la jambe. (...) Comment réagiriez-vous?]".
VARIABLE LABELS sv118j "Je préférerais souffrir en silence [Nous allons imaginer une situation (...) vous vous cassiez la jambe. (...) Comment réagiriez-vous?]".
VARIABLE LABELS v119a "Vos parents (père, mère) ou beaux-parents (père, mère de votre conjoint/e) [À quelle fréquence rencontrez-vous les membres de votre famille (sans compter ceux avec qui vous habitez)?]".
VARIABLE LABELS v119b "Vos frères et soeurs, beaux-frères, belles-soeurs [À quelle fréquence rencontrez-vous les membres de votre famille (sans compter ceux avec qui vous habitez)?]".
VARIABLE LABELS v119c "Vos enfants ou beaux-enfants [À quelle fréquence rencontrez-vous les membres de votre famille (sans compter ceux avec qui vous habitez)?]".
VARIABLE LABELS v119d "Vos petits-enfants / arrières petits-enfants [À quelle fréquence rencontrez-vous les membres de votre famille (sans compter ceux avec qui vous habitez)?]".
VARIABLE LABELS v119e "Vos neveux ou petits-neveux [À quelle fréquence rencontrez-vous les membres de votre famille (sans compter ceux avec qui vous habitez)?]".
VARIABLE LABELS v119f "Autre parent (cousin/e, tante, oncle) [À quelle fréquence rencontrez-vous les membres de votre famille (sans compter ceux avec qui vous habitez)?]".
VARIABLE LABELS sv120a "Faire le ménage chez un parent, lui apporter ou lui préparer des repas [Actuellement, vous arrive-t-il de rendre certains des services suivants à l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS sv120b "Faire les courses pour un parent [Actuellement, vous arrive-t-il de rendre certains des services suivants à l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS sv120c "Faire des réparations, bricoler, jardiner chez un parent [Actuellement, vous arrive-t-il de rendre certains des services suivants à l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS sv120d "Confectionner des vêtements ou d'autres objets pour un parent [Actuellement, vous arrive-t-il de rendre certains des services suivants à l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS sv120e "Emmener un parent plus âgé, ou handicapé, en promenade, au spectacle, au café /restaurant [Actuellement, vous arrive-t-il de rendre certains des services suivants à l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS sv120f "Aider un parent dans son travail professionnel [Actuellement, vous arrive-t-il de rendre certains des services suivants à l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS sv120g "Garder des enfants petits, leur faire faire les devoirs (chez vous ou chez eux) [Actuellement, vous arrive-t-il de rendre certains des services suivants à l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS sv120h "Emmener les enfants en promenade, au spectacle, au restaurant [Actuellement, vous arrive-t-il de rendre certains des services suivants à l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS v121a "Votre père ou mère, le père ou la mère de votre conjoint/e [Actuellement, quels sont le ou les deux parent(s) à qui vous rendez le plus de service (en dehors de ceux avec qui vous habitez)?]".
VARIABLE LABELS sv121b "Fils ou fille, beau-fils ou belle-fille [Actuellement, quels sont le ou les deux parent(s) à qui vous rendez le plus de service (en dehors de ceux avec qui vous habitez)?]".
VARIABLE LABELS sv121c "Petit-fils/fille [Actuellement, quels sont le ou les deux parent(s) à qui vous rendez le plus de service (en dehors de ceux avec qui vous habitez)?]".
VARIABLE LABELS sv121d "Frère/soeur [Actuellement, quels sont le ou les deux parent(s) à qui vous rendez le plus de service (en dehors de ceux avec qui vous habitez)?]".
VARIABLE LABELS sv121e "Cousin/e [Actuellement, quels sont le ou les deux parent(s) à qui vous rendez le plus de service (en dehors de ceux avec qui vous habitez)?]".
VARIABLE LABELS sv121f "Autre parent [Actuellement, quels sont le ou les deux parent(s) à qui vous rendez le plus de service (en dehors de ceux avec qui vous habitez)?]".
VARIABLE LABELS sv121g "Aucun [Actuellement, quels sont le ou les deux parent(s) à qui vous rendez le plus de service (en dehors de ceux avec qui vous habitez)?]".
VARIABLE LABELS sv122a "Préparer les repas principaux [Dans votre ménage (logement), accomplissez-vous toujours ou presque les tâches suivantes? (...)]".
VARIABLE LABELS sv122b "Faire les achats courants [Dans votre ménage (logement), accomplissez-vous toujours ou presque les tâches suivantes? (...)]".
VARIABLE LABELS sv122c "Faire les grands nettoyages [Dans votre ménage (logement), accomplissez-vous toujours ou presque les tâches suivantes? (...)]".
VARIABLE LABELS sv122d "Faire les lits [Dans votre ménage (logement), accomplissez-vous toujours ou presque les tâches suivantes? (...)]".
VARIABLE LABELS sv122e "Faire la vaisselle [Dans votre ménage (logement), accomplissez-vous toujours ou presque les tâches suivantes? (...)]".
VARIABLE LABELS sv122f "Faire la lessive [Dans votre ménage (logement), accomplissez-vous toujours ou presque les tâches suivantes? (...)]".
VARIABLE LABELS sv122g "Remplir la feuille d'impôt, la feuille d'assurance, etc. [Dans votre ménage (logement), accomplissez-vous toujours ou presque les tâches suivantes? (...)]".
VARIABLE LABELS sv123a "Faire le ménage chez vous [Actuellement, est-ce qu'il arrive que l'un ou l'autre des membres de votre famille (...) vous rendent des services, par exemple...]".
VARIABLE LABELS sv123b "Apporter ou préparer des repas chez vous [Actuellement, est-ce qu'il arrive que l'un ou l'autre des membres de votre famille (...) vous rendent des services, par exemple...]".
VARIABLE LABELS sv123c "Faire les courses pour vous [Actuellement, est-ce qu'il arrive que l'un ou l'autre des membres de votre famille (...) vous rendent des services, par exemple...]".
VARIABLE LABELS sv123d "Faire des réparations, bricoler, jardiner chez vous [Actuellement, est-ce qu'il arrive que l'un ou l'autre des membres de votre famille (...) vous rendent des services, par exemple...]".
VARIABLE LABELS sv123e "Vous aide à remplir la feuille d'impôt, la déclara- tion d'assurance, etc. [Actuellement, est-ce qu'il arrive que l'un ou l'autre des membres de votre famille (...) vous rendent des services, par exemple...]".
VARIABLE LABELS sv123f "Vous aide dans votre toilette (bain, coiffure, etc.) [Actuellement, est-ce qu'il arrive que l'un ou l'autre des membres de votre famille (...) vous rendent des services, par exemple...]".
VARIABLE LABELS sv124a "Qui trouve que l'on s'amuse bien avec vous [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes):]".
VARIABLE LABELS sv124b "Qui vous demande sérieusement votre avis [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes):]".
VARIABLE LABELS sv124c "Qui aime bien passer de longs moments avec vous [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes):]".
VARIABLE LABELS sv124d "Qui a pour vous une affection profonde [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes):]".
VARIABLE LABELS sv124e "Qui vous donne des marques de respect [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes):]".
VARIABLE LABELS sv124f "Qui compte un peu sur vous pour arranger les brouilles, les disputes [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes):]".
VARIABLE LABELS sv124g "Qui compte sur vous pour maintenir l'esprit de famille [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes):]".
VARIABLE LABELS sv124h "Qui aime se confier à vous [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes):]".
VARIABLE LABELS sv124i "Pour qui vous comptez (pour qui vous êtes important) [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes):]".
VARIABLE LABELS mv125a "Fils [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS mv125b "Fille [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS mv125c "Belle-fille [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS mv125d "Beau-fils [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS v125e "Père/mère (ou 2e père ou mère) [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS v125f "Beau-père/belle-mère (parents du conjoint) [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS mv125g "Petits-enfants, arrières petits-enfants [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS mv125h "Soeur [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS mv125i "Frère [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS mv125j "Cousins/cousines [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS mv125k "Neveux/nièces, petits-neveux/nièces [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS mv125l "Autre [Quels sont les deux membres de votre famille auxquels vous êtes le plus attaché (conjoint/e excepté/e)?]".
VARIABLE LABELS sv126a "Première personne [Où habitent-ils/elles?]".
VARIABLE LABELS sv126b "Seconde personne [Où habitent-ils/elles?]".
VARIABLE LABELS mv127a "Première personne [À quelle fréquence les voyez-vous ou avez-vous une conversation téléphonique avec eux?]".
VARIABLE LABELS mv127b "Seconde personne [À quelle fréquence les voyez-vous ou avez-vous une conversation téléphonique avec eux?]".
VARIABLE LABELS tv128 "À propos du téléphone, est-ce que vous utilisez le téléphone sans aide?".
VARIABLE LABELS sv129a "Faire le ménage chez eux, leur apporter ou préparer des repas chez eux [Actuellement, vous arrive-t-il de rendre des services à un/e ami/e, connaissance, voisin/e, par exemple:]".
VARIABLE LABELS sv129b "Faire les courses pour eux [Actuellement, vous arrive-t-il de rendre des services à un/e ami/e, connaissance, voisin/e, par exemple:]".
VARIABLE LABELS sv129c "Garder des enfants petits, leur faire faire les devoirs (chez vous ou chez eux) [Actuellement, vous arrive-t-il de rendre des services à un/e ami/e, connaissance, voisin/e, par exemple:]".
VARIABLE LABELS sv129d "Faire des réparations, bricoler, jardiner chez eux [Actuellement, vous arrive-t-il de rendre des services à un/e ami/e, connaissance, voisin/e, par exemple:]".
VARIABLE LABELS sv129e "Emmener les enfants en promenade, au spectacle, au restaurant [Actuellement, vous arrive-t-il de rendre des services à un/e ami/e, connaissance, voisin/e, par exemple:]".
VARIABLE LABELS sv129f "Emmener une personne âgée, ou handicapée, en promenade, au spectacle, au café/restaurant [Actuellement, vous arrive-t-il de rendre des services à un/e ami/e, connaissance, voisin/e, par exemple:]".
VARIABLE LABELS sv129g "Les aider dans leur travail professionnel [Actuellement, vous arrive-t-il de rendre des services à un/e ami/e, connaissance, voisin/e, par exemple:]".
VARIABLE LABELS sv130a "Faire le ménage chez vous".
VARIABLE LABELS sv130b "Apporter ou préparer des repas chez vous".
VARIABLE LABELS sv130c "Faire les courses pour vous".
VARIABLE LABELS sv130d "Faire des réparations, bricoler, jardiner chez vous".
VARIABLE LABELS sv130e "Vous aide à remplir la feuille d'impôt, la déclara- tion d'assurance, etc.".
VARIABLE LABELS sv130f "Vous aide dans votre toilette (bain, coiffure, etc.)".
VARIABLE LABELS sv131 "Parmi vos amis et connaissances (en dehors des membres de votre famille), y en a-t-il certains auxquels vous êtes très attaché/e?".
VARIABLE LABELS sv132a "Première personne [Pensez à la personne ou aux deux personnes à qui vous êtes le plus attaché/e. Où habitent-elles?]".
VARIABLE LABELS sv132b "Seconde personne [Pensez à la personne ou aux deux personnes à qui vous êtes le plus attaché/e. Où habitent-elles?]".
VARIABLE LABELS sv133a "Première personne [Ami‎/e. À quelle fréquence les voyez-vous ou avez-vous une conversation téléphonique avec eux?]".
VARIABLE LABELS sv133b "Seconde personne [Ami‎/e. À quelle fréquence les voyez-vous ou avez-vous une conversation téléphonique avec eux?]".
VARIABLE LABELS mv134 "Tout compte fait, si vous deviez porter un jugement global sur les rapports humains que vous avez eus ou créés au cours de votre vie, diriez-vous que vous êtes:".
VARIABLE LABELS tv135aa "Promenades à pied - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1a "Promenades à pied - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2a "Promenades à pied - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3a "Promenades à pied - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4a "Promenades à pied - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ca "Promenades à pied - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135da "Promenades à pied - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135ea "Promenades à pied - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ab "Jardinage - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1b "Jardinage - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2b "Jardinage - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3b "Jardinage - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4b "Jardinage - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cb "Jardinage - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135db "Jardinage - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135eb "Jardinage - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS tv135ac "Gymnastique - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1c "Gymnastique - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2c "Gymnastique - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3c "Gymnastique - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4c "Gymnastique - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cc "Gymnastique - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dc "Gymnastique - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135ec "Gymnastique - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ad "Sport - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1d "Sport - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2d "Sport - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3d "Sport - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4d "Sport - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cd "Sport - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dd "Sport - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135ed "Sport - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ae "Café, tea-room, restaurant - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1e "Café, tea-room, restaurant - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2e "Café, tea-room, restaurant - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3e "Café, tea-room, restaurant - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4e "Café, tea-room, restaurant - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ce "Café, tea-room, restaurant - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135de "Café, tea-room, restaurant - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135ee "Café, tea-room, restaurant - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135af "Cinéma, concert, théâtre - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1f "Cinéma, concert, théâtre - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2f "Cinéma, concert, théâtre - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3f "Cinéma, concert, théâtre - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4f "Cinéma, concert, théâtre - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cf "Cinéma, concert, théâtre - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135df "Cinéma, concert, théâtre - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135ef "Cinéma, concert, théâtre - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS tv135ag "Excursions de 1-2 jours - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1g "Excursions de 1-2 jours - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2g "Excursions de 1-2 jours - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3g "Excursions de 1-2 jours - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4g "Excursions de 1-2 jours - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cg "Excursions de 1-2 jours - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dg "Excursions de 1-2 jours - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135eg "Excursions de 1-2 jours - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ah "Voyage de 3-4 jours au moins - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1h "Voyage de 3-4 jours au moins - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2h "Voyage de 3-4 jours au moins - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3h "Voyage de 3-4 jours au moins - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4h "Voyage de 3-4 jours au moins - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ch "Voyage de 3-4 jours au moins - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dh "Voyage de 3-4 jours au moins - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135eh "Voyage de 3-4 jours au moins - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ai "Jouer d'un instrument de musique - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1i "Jouer d'un instrument de musique - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2i "Jouer d'un instrument de musique - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3i "Jouer d'un instrument de musique - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4i "Jouer d'un instrument de musique - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ci "Jouer d'un instrument de musique - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135di "Jouer d'un instrument de musique - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135ei "Jouer d'un instrument de musique - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135aj "Autres activités artistiques - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1j "Autres activités artistiques - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2j "Autres activités artistiques - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3j "Autres activités artistiques - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4j "Autres activités artistiques - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cj "Autres activités artistiques - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dj "Autres activités artistiques - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135ej "Autres activités artistiques - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ak "Cours, conférences - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1k "Cours, conférences - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2k "Cours, conférences - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3k "Cours, conférences - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4k "Cours, conférences - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ck "Cours, conférences - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dk "Cours, conférences - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135ek "Cours, conférences - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS tv135al "Jeux de société - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1l "Jeux de société - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2l "Jeux de société - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3l "Jeux de société - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4l "Jeux de société - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cl "Jeux de société - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dl "Jeux de société - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135el "Jeux de société - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS tv135am "Mots croisés, patiences, mots cachés - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1m "Mots croisés, patiences, mots cachés - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2m "Mots croisés, patiences, mots cachés - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3m "Mots croisés, patiences, mots cachés - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4m "Mots croisés, patiences, mots cachés - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cm "Mots croisés, patiences, mots cachés - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dm "Mots croisés, patiences, mots cachés - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135em "Mots croisés, patiences, mots cachés - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS tv135an "Travaux d'aiguille - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1n "Travaux d'aiguille - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2n "Travaux d'aiguille - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3n "Travaux d'aiguille - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4n "Travaux d'aiguille - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cn "Travaux d'aiguille - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dn "Travaux d'aiguille - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135en "Travaux d'aiguille - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS tv135ao "Autres travaux manuels (bricolage, réparations, menuiserie...) - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1o "Autres travaux manuels (bricolage, réparations, menuiserie...) - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2o "Autres travaux manuels (bricolage, réparations, menuiserie...) - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3o "Autres travaux manuels (bricolage, réparations, menuiserie...) - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4o "Autres travaux manuels (bricolage, réparations, menuiserie...) - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135co "Autres travaux manuels (bricolage, réparations, menuiserie...) - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135do "Autres travaux manuels (bricolage, réparations, menuiserie...) - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135eo "Autres travaux manuels (bricolage, réparations, menuiserie...) - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ap "Manifestations politiques, syndicales - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1p "Manifestations politiques, syndicales - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2p "Manifestations politiques, syndicales - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3p "Manifestations politiques, syndicales - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4p "Manifestations politiques, syndicales - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cp "Manifestations politiques, syndicales - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dp "Manifestations politiques, syndicales - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135ep "Manifestations politiques, syndicales - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135aq "Manifestations villageoises ou de quartier - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1q "Manifestations villageoises ou de quartier - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2q "Manifestations villageoises ou de quartier - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3q "Manifestations villageoises ou de quartier - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4q "Manifestations villageoises ou de quartier - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cq "Manifestations villageoises ou de quartier - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dq "Manifestations villageoises ou de quartier - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135eq "Manifestations villageoises ou de quartier - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS tv135ar "Prier - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1r "Prier - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2r "Prier - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3r "Prier - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4r "Prier - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cr "Prier - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dr "Prier - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135er "Prier - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS tv135as "Aller à l'église, temple - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1s "Aller à l'église, temple - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2s "Aller à l'église, temple - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3s "Aller à l'église, temple - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4s "Aller à l'église, temple - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135cs "Aller à l'église, temple - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135ds "Aller à l'église, temple - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135es "Aller à l'église, temple - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS tv135at "Écouter la messe ‎/ le culte à la radio ou à la TV - pratique aujourd'hui [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b1t "Écouter la messe ‎/ le culte à la radio ou à la TV - seul‎/e [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b2t "Écouter la messe ‎/ le culte à la radio ou à la TV - avec conjoint [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b3t "Écouter la messe ‎/ le culte à la radio ou à la TV - avec autre membre de la famille [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135b4t "Écouter la messe ‎/ le culte à la radio ou à la TV - avec amis, connaissances [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS sv135ct "Écouter la messe ‎/ le culte à la radio ou à la TV - dans clubs ou associations [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135dt "Écouter la messe ‎/ le culte à la radio ou à la TV - pratique à 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS v135et "Écouter la messe ‎/ le culte à la radio ou à la TV - comparaison aujourd'hui ‎/ 45 ans [Je vais énumérer maintenant une liste d'activités (...)]".
VARIABLE LABELS pv136 "Voici une échelle de revenus mensuels bruts [donner la carte]. Où situez- vous le revenu mensuel brut total de votre ménage?".
VARIABLE LABELS sv137 "Parmi les catégories suivantes [donner la carte], où vous situez-vous approximativement votre fortune?".
VARIABLE LABELS sv138a "Salaire ou gain d'un travail régulier".
VARIABLE LABELS sv138b "Salaire ou gain d'un travail occasionnel".
VARIABLE LABELS sv138c "Caisses de retraite (autre qu'AVS)".
VARIABLE LABELS sv138d "Intérêts d'épargne, de rentes, titres, assurance-vie, etc.".
VARIABLE LABELS sv138e "Revenu d'une location (appartement, immeuble, terrain)".
VARIABLE LABELS sv138f "Prestations complémentaires de l'AVS (mais pas l'AVS)".
VARIABLE LABELS sv138g "Allocation de l'assurance-chômage".
VARIABLE LABELS sv138h "Allocation de l'assurance-invalidité".
VARIABLE LABELS sv138i "Autres allocations cantonales ou communales".
VARIABLE LABELS sv138j "Une aide financière de la part d'organismes privés ou religieux".
VARIABLE LABELS sv138k "Une aide financière de la part de membres de votre famille (enfants par exemple)".
VARIABLE LABELS sv139 "Avec votre revenu actuel, avez-vous de la peine à 'nouer les deux bouts'?".
VARIABLE LABELS sv140 "Comment répartissez-vous les frais de ménage et du logement avec la personne (les personnes) qui vivent avec vous (en dehors du conjoint/e)?".
VARIABLE LABELS mv141a "Dans notre société où tout évolue si vite, l'expérience des personnes comme moi n'intéresse guère les autres [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS v141b "À condition de savoir passer la main, les personnes comme moi peuvent se rendre utiles [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141c "Pour les personnes comme moi, ce n'est guère important de faire attention à ses habits et à son apparence [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141d "Le monde évolue si vite que les personnes comme moi ne le comprennent plus (bien) [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141e "Pour réussir dans la vie, il faut avoir de la chance et aussi du piston [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141f "Les personnes comme moi ne doivent pas trop demander à leurs enfants car ceux-ci ont leur travail et leur vie à mener [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS v141g "Dans cette société, les personnes comme moi ont un rôle à jouer [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141h "Les personnes comme moi ont meilleur temps de rester chez elles, on y est plus tranquille et on n'a pas d'ennui [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141i "Sans Dieu, la vie n'a pas de sens [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141j "Dans notre société, on ne respecte plus guère les personnes comme moi [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141k "Grâce au progrès, la vie à mon âge est plus facile aujourd'hui qu'autrefois [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS v141l "Les personnes de mon âge doivent être solidaires des plus vieux [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141m "Les personnes comme moi n'ont rien à attendre de personne [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141n "Les enfants devraient se responsabiliser beaucoup plus de leurs parents quand ceux-ci sont de mon âge [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141o "À condition d'être assez discrètes, les personnes comme moi peuvent rendre beaucoup de services [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141p "Pour les personnes comme moi, aller voter est inutile [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141q "Les personnes de mon âge doivent être solidaires des plus jeunes [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141r "Personne ne s'intéresse vraiment aux gens comme moi [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS v141s "Il est urgent que les personnes comme moi s'organisent entre elles pour défendre leurs intérêts [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141t "Tout ce qui reste aux personnes comme moi c'est d'attendre la mort [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141u "Les parents laissent beaucoup trop faire leurs enfants [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141v "Pour des personnes comme moi, le mieux est de prendre la vie comme elle vient, au jour le jour [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141w "Les jeunes d'aujourd'hui ont la vie beaucoup trop facile [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS v141x "Dans cette société, les personnes comme moi sont mises à l'écart [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141y "Les personnes comme moi doivent avoir des projets, même modestes, et chercher à les réaliser [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141z "Les personnes comme moi doivent profiter de cette phase de vie pour faire le point sur leur existence [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141aa "La place de la femme est d'être à son foyer, dans son ménage [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS mv141ab "Pour réussir dans la vie, il faut travailler dur [Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS sv142 "Pensez-vous que, dans la vie, il y a des règles (lignes directrices) claires qui permettent de distinguer ce qui est bien de ce qui est mal et qui vous guident pour savoir ce qu'il faut faire?".
VARIABLE LABELS mv143aa "Les bonnes manières - Première qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ab "L'indépendance - Première qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ac "L'application au travail - Première qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ad "Le sens des responsabilités - Première qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ae "L'imagination - Première qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143af "La tolérance et respect des autres - Première qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ag "L'esprit des économies, ne pas gaspiller l'argent des autres - Première qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ah "La foi religieuse - Première qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ai "La générosité envers les autres et l'esprit de service (altruisme) - Première qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143aj "L'obéissance - Première qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ba "Les bonnes manières - Deuxième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143bb "L'indépendance - Deuxième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143bc "L'application au travail - Deuxième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143bd "Le sens des responsabilités - Deuxième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143be "L'imagination - Deuxième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143bf "La tolérance et respect des autres - Deuxième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143bg "L'esprit des économies, ne pas gaspiller l'argent des autres - Deuxième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143bh "La foi religieuse - Deuxième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143bi "La générosité envers les autres et l'esprit de service (altruisme) - Deuxième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143bj "L'obéissance - Deuxième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ca "Les bonnes manières - Troisième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143cb "L'indépendance - Troisième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143cc "L'application au travail - Troisième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143cd "Le sens des responsabilités - Troisième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ce "L'imagination - Troisième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143cf "La tolérance et respect des autres - Troisième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143cg "L'esprit des économies, ne pas gaspiller l'argent des autres - Troisième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ch "La foi religieuse - Troisième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143ci "La générosité envers les autres et l'esprit de service (altruisme) - Troisième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv143cj "L'obéissance - Troisième qualité [Voici une liste [donner la carte] de 10 qualités que les parents peuvent chercher à encourager chez leurs enfants.]".
VARIABLE LABELS mv144aa "Préserver la liberté d'expression - Première importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144ab "Le maintien de l'ordre et de la paix - Première importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144ac "Améliorer l'éducation, la formation des jeunes - Première importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144ad "La lutte contre le chômage et pour l'emploi - Première importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144ae "Lutter contre le crime et assurer la sécurité des personnes - Première importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144af "La protection de l'environnement et de la nature - Première importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144ag "La lutte contre la drogue - Première importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144ah "Le développement des transports publics - Première importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144ai "Maintien et amélioration de l'économie suisse - Première importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144aj "Le maintien et l'amélioration des assurances et prestations sociales (AVS, AI, assurance-maladie, etc.) - Première importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants".
VARIABLE LABELS mv144ba "Préserver la liberté d'expression - Deuxième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144bb "Le maintien de l'ordre et de la paix - Deuxième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144bc "Améliorer l'éducation, la formation des jeunes - Deuxième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144bd "La lutte contre le chômage et pour l'emploi - Deuxième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144be "Lutter contre le crime et assurer la sécurité des personnes - Deuxième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144bf "La protection de l'environnement et de la nature - Deuxième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144bg "La lutte contre la drogue - Deuxième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144bh "Le développement des transports publics - Deuxième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144bi "Maintien et amélioration de l'économie suisse - Deuxième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144bj "Le maintien et l'amélioration des assurances et prestations sociales (AVS, AI, assurance-maladie, etc.) - Deuxième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants".
VARIABLE LABELS mv144ca "Préserver la liberté d'expression - Troisième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144cb "Le maintien de l'ordre et de la paix - Troisième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144cc "Améliorer l'éducation, la formation des jeunes - Troisième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144cd "La lutte contre le chômage et pour l'emploi - Troisième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144ce "Lutter contre le crime et assurer la sécurité des personnes - Troisième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144cf "La protection de l'environnement et de la nature - Troisième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144cg "La lutte contre la drogue - Troisième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144ch "Le développement des transports publics - Troisième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144ci "Maintien et amélioration de l'économie suisse - Troisième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants, auxquels il faut donner la priorité?]".
VARIABLE LABELS mv144cj "Le maintien et l'amélioration des assurances et prestations sociales (AVS, AI, assurance-maladie, etc.) - Troisième importance [Parlons de notre pays, la Suisse. Quels sont aujourd'hui les problèmes les plus importants".
VARIABLE LABELS mv145 "Quand vous pensez à votre vie, estimez-vous que vous avez été libre de faire vos choix, d'orienter votre vie selon votre volonté, ou au contraire pensez-vous que votre vie s'est faite le plus souvent sans que vous ayez v (...)".
VARIABLE LABELS svf1 "La personne interviewée habite:".
VARIABLE LABELS svf2 "Où réside la personne interrogée?".
VARIABLE LABELS svf3 "À quel étage? (rez-de-chaussée = 0)".
VARIABLE LABELS svf4 "L'accès de l'habitation se fait-il:".
VARIABLE LABELS svf5 "Combien de logement(s) y a-t-il dans le bâtiment où habite la persone interviewée?".
VARIABLE LABELS mvf6a "Des problèmes de mémoire [D'après vous, la personne interviewée avait-elle:]".
VARIABLE LABELS mvf6b "Des difficultés d'audition [D'après vous, la personne interviewée avait-elle:]".
VARIABLE LABELS mvf6c "Des difficultés d'élocution [D'après vous, la personne interviewée avait-elle:]".
VARIABLE LABELS mvf7 "Etait-elle alitée, paralysée ou utilisant des moyens auxiliaires tels que béquilles, cadre, par exemple?".
VARIABLE LABELS mvf8 "Était-elle: [Attitude]".
VARIABLE LABELS mvf9 "Une autre (d'autres) personne(s) assistait(ent)-elle(s) à l'entretien?".
VARIABLE LABELS mvf9a "Conjoint/e [Si oui, laquelle (lesquelles)?]".
VARIABLE LABELS mvf9b "Enfant(s) [Si oui, laquelle (lesquelles)?]".
VARIABLE LABELS mvf9c "Petit(s)-enfant(s) [Si oui, laquelle (lesquelles)?]".
VARIABLE LABELS mvf9d "Autre(s) [Si oui, laquelle (lesquelles)?]".
VARIABLE LABELS mvf10 "Cette (ces) personne(s) est (sont)-elle(s) intervenue(s) pendant l'entretien?".
VARIABLE LABELS mvf11 "Ces interventions ont-elles, à votre avis, infléchi ou faussé les réponses?".
VARIABLE LABELS svfdate "Date de l'entretien: (jour mois)".
VARIABLE LABELS ix1a "Dans quelle langue vous exprimez-vous le plus souvent actuellement? [1ère langue]".
VARIABLE LABELS ix1b "Dans quelle langue vous exprimez-vous le plus souvent actuellement? [2e langue]".
VARIABLE LABELS i2b "Dans votre jeunesse, quelle langue parliez-vous à la maison? [2e langue]".
VARIABLE LABELS i10 "Avez-vous (actuellement) des enfants?".
VARIABLE LABELS i11 "Combien d'enfants avez-vous?".
VARIABLE LABELS i1_18 "Quelle est l'activité principale de votre fils/fille? [Enfant 1]".
VARIABLE LABELS i1_19 "Quelle est l'activité principale de son/sa conjoint/e? [Enfant 1]".
VARIABLE LABELS i1_21 "Par rapport à vous, votre enfant habite: [Enfant 1]".
VARIABLE LABELS i2_18 "Quelle est l'activité principale de votre fils/fille? [Enfant 2]".
VARIABLE LABELS i2_19 "Quelle est l'activité principale de son/sa conjoint/e? [Enfant 2]".
VARIABLE LABELS i2_21 "Par rapport à vous, votre enfant habite: [Enfant 2]".
VARIABLE LABELS i3_18 "Quelle est l'activité principale de votre fils/fille? [Enfant 3]".
VARIABLE LABELS i3_19 "Quelle est l'activité principale de son/sa conjoint/e? [Enfant 3]".
VARIABLE LABELS i3_21 "Par rapport à vous, votre enfant habite: [Enfant 3]".
VARIABLE LABELS i4_18 "Quelle est l'activité principale de votre fils/fille? [Enfant 4]".
VARIABLE LABELS i4_19 "Quelle est l'activité principale de son/sa conjoint/e? [Enfant 4]".
VARIABLE LABELS i4_21 "Par rapport à vous, votre enfant habite: [Enfant 4]".
VARIABLE LABELS i5_18 "Quelle est l'activité principale de votre fils/fille? [Enfant 5]".
VARIABLE LABELS i5_19 "Quelle est l'activité principale de son/sa conjoint/e? [Enfant 5]".
VARIABLE LABELS i5_21 "Par rapport à vous, votre enfant habite: [Enfant 5]".
VARIABLE LABELS i6_18 "Quelle est l'activité principale de votre fils/fille? [Enfant 6]".
VARIABLE LABELS i6_19 "Quelle est l'activité principale de son/sa conjoint/e? [Enfant 6]".
VARIABLE LABELS i6_21 "Par rapport à vous, votre enfant habite: [Enfant 6]".
VARIABLE LABELS i7_18 "Quelle est l'activité principale de votre fils/fille? [Enfant 7]".
VARIABLE LABELS i7_19 "Quelle est l'activité principale de son/sa conjoint/e? [Enfant 7]".
VARIABLE LABELS i7_21 "Par rapport à vous, votre enfant habite: [Enfant 7]".
VARIABLE LABELS i8_18 "Quelle est l'activité principale de votre fils/fille? [Enfant 8]".
VARIABLE LABELS i8_19 "Quelle est l'activité principale de son/sa conjoint/e? [Enfant 8]".
VARIABLE LABELS i8_21 "Par rapport à vous, votre enfant habite: [Enfant 8]".
VARIABLE LABELS ix2a "Avez-vous eu des enfants qui sont décédés?".
VARIABLE LABELS ix2b "Si oui: Combien? [Enfants décédés]".
VARIABLE LABELS i81 "Avez-vous (actuellement) un ou plusieurs frères et soeurs?".
VARIABLE LABELS i83a "La même institution que vous [Pouvez-vous nous dire, combien de frères et de soeurs habitent dans:]".
VARIABLE LABELS i83b "Le même quartier/village que vous (mais pas la même maison) [Pouvez-vous nous dire, combien de frères et de soeurs habitent dans:]".
VARIABLE LABELS i83c "La même commune que vous (mais pas le même quartier/village) [Pouvez-vous nous dire, combien de frères et de soeurs habitent dans:]".
VARIABLE LABELS i83d "Le même canton que vous (mais pas la même commune) [Pouvez-vous nous dire, combien de frères et de soeurs habitent dans:]".
VARIABLE LABELS i83e "Un autre canton [Pouvez-vous nous dire, combien de frères et de soeurs habitent dans:]".
VARIABLE LABELS i83f "Un autre pays [Pouvez-vous nous dire, combien de frères et de soeurs habitent dans:]".
VARIABLE LABELS i83g "Je ne sais pas [Pouvez-vous nous dire, combien de frères et de soeurs habitent dans:]".
VARIABLE LABELS i84 "Avez-vous (actuellement) un ou plusieurs neveux ou petits-neveux".
VARIABLE LABELS i91 "Depuis quand résidez-vous dans cette maison (institution)? (mois année)".
VARIABLE LABELS i91mois "Mois [Depuis quand résidez-vous dans cette maison (institution)?]".
VARIABLE LABELS i91annee "Année [Depuis quand résidez-vous dans cette maison (institution)?]".
VARIABLE LABELS i93 "Où habitiez-vous avant de venir vous installer ici?".
VARIABLE LABELS i95 "Dans cette institution, vous disposez:".
VARIABLE LABELS ix3a "Est-ce qu'un/e parent/e à vous réside également dans cette institution?".
VARIABLE LABELS ix3b "S'agit-il:".
VARIABLE LABELS i117d "Votre voiture, ou celle du conjoint (si a voiture) [Quand vous vous déplacez pour aller en ville ou dans un autre lieu, utilisez- vous:]".
VARIABLE LABELS ix4 "Avant l'âge AVS, quelle était votre activité principale?".
VARIABLE LABELS i22 "Quelle était votre profession principale?".
VARIABLE LABELS i24 "Vous exerciez votre activité professionnelle à titre de:".
VARIABLE LABELS i25 "Dans quel secteur d'activité exerciez-vous cette profession?".
VARIABLE LABELS i26 "Aviez-vous des subordonnés et si oui combien?".
VARIABLE LABELS ix5 "En ce qui concerne votre conjoint/e, quelle était son activité principale (avant qu'il/elle n'atteigne l'âge AVS ou avant son décès)?".
VARIABLE LABELS i60 "Quelle est ou était (avant la retraite ou le décès) la profession principale de votre conjoint/e?".
VARIABLE LABELS i62 "Votre conjoint exerçait (exerce) son activité professionnelle à titre de:".
VARIABLE LABELS i63 "Dans quel secteur d'activité exerçait-il/elle cette profession?".
VARIABLE LABELS i64 "Est-ce que votre conjoint/e avait des subordonnés, et si oui combien?".
VARIABLE LABELS i65 "Votre conjoint/e a-t-il pris sa retraite:".
VARIABLE LABELS i59 "Quelle est actuellement l'activité principale de votre conjoint/e?".
VARIABLE LABELS i136 "Voici une échelle de revenus mensuels bruts [donner la carte]. Où situez-vous votre revenu mensuel brut total?".
VARIABLE LABELS ix6a "Vous-même [Qui est-ce qui paie votre pension ici?]".
VARIABLE LABELS ix6b "Votre famille [Qui est-ce qui paie votre pension ici?]".
VARIABLE LABELS ix6c "Le Canton, l'État [Qui est-ce qui paie votre pension ici?]".
VARIABLE LABELS i98 "Est-ce que vous avez accès à un téléphone?".
VARIABLE LABELS i175a "Conversations occasionnelles [Quel type de relations entretenez-vous avec les autres pensionnaires de la maison?]".
VARIABLE LABELS i175b "Avec certains, vous vous rendez de petits services (entr'aide) [Quel type de relations entretenez-vous avec les autres pensionnaires de la maison?]".
VARIABLE LABELS i175c "Vous vous rendez des visites dans les chambres [Quel type de relations entretenez-vous avec les autres pensionnaires de la maison?]".
VARIABLE LABELS i175d "Vous faites des promenades avec certains d'entre eux [Quel type de relations entretenez-vous avec les autres pensionnaires de la maison?]".
VARIABLE LABELS i182aa "Conjoint/e (partenaire) [Actuellement, est-ce que vous vous occupez (au cours de ces trois derniers mois) d'une personne de votre entourage qui a besoin d'aide, d'être entourée?]".
VARIABLE LABELS i182ba "Conjoint/e (partenaire) [Habitez-vous avec cette personne?]".
VARIABLE LABELS i182ac "Enfants, petits-enfants [Actuellement, est-ce que vous vous occupez (au cours de ces trois derniers mois) d'une personne de votre entourage qui a besoin d'aide, d'être entourée?]".
VARIABLE LABELS i182bc "Enfants, petits-enfants [Habitez-vous avec cette personne?]".
VARIABLE LABELS i182ad "Autre parent [Actuellement, est-ce que vous vous occupez (au cours de ces trois derniers mois) d'une personne de votre entourage qui a besoin d'aide, d'être entourée?]".
VARIABLE LABELS i182bd "Autre parent [Habitez-vous avec cette personne?]".
VARIABLE LABELS i182ae "Amis, connaissances, voisins [Actuellement, est-ce que vous vous occupez (au cours de ces trois derniers mois) d'une personne de votre entourage qui a besoin d'aide, d'être entourée?]".
VARIABLE LABELS i182be "Amis, connaissances, voisins [Habitez-vous avec cette personne?]".
VARIABLE LABELS i182ag "Autres [Actuellement, est-ce que vous vous occupez (au cours de ces trois derniers mois) d'une personne de votre entourage qui a besoin d'aide, d'être entourée?]".
VARIABLE LABELS i182bg "Autres [Habitez-vous avec cette personne?]".
VARIABLE LABELS i125a "Conjoint [Quelles sont les deux personnes auxquels vous êtes le plus attaché?]".
VARIABLE LABELS i125l "Ami‎/e [Quelles sont les deux personnes auxquels vous êtes le plus attaché?]".
VARIABLE LABELS i135e "Fréquenter la cafétéria, le restaurant [Pouvez-vous me dire lesquelles d'entre elles vous faites actuellement et à quelle fréquence?]".
VARIABLE LABELS i135f "Participer aux animations [Pouvez-vous me dire lesquelles d'entre elles vous faites actuellement et à quelle fréquence?]".
VARIABLE LABELS i135j "Activités artistiques (peindre, jouer d'un instrument) [Pouvez-vous me dire lesquelles d'entre elles vous faites actuellement et à quelle fréquence?]".
VARIABLE LABELS rjpb "Sexe [Questions concernant le 'proxi' (répondant):]".
VARIABLE LABELS rjpc "Âge approximatif [Questions concernant le 'proxi' (répondant):]".
VARIABLE LABELS jpd "Membre du personnel de l'institution [Questions concernant le 'proxi' (répondant):]".
VARIABLE LABELS jpg "Connaît la personne âgée depuis: [Questions concernant le 'proxi' (répondant):]".
VARIABLE LABELS j9 "Quel est l'âge (approximatif) de votre conjoint/e?".
VARIABLE LABELS j12a "Sexe [Enfant 1 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j12b "Âge [Enfant 1 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j12c "Lieu d'habitat [Enfant 1 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j23a "Sexe [Enfant 2 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j23b "Âge [Enfant 2 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j23c "Lieu d'habitat [Enfant 2 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j34a "Sexe [Enfant 3 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j34b "Âge [Enfant 3 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j34c "Lieu d'habitat [Enfant 3 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j45a "Sexe [Enfant 4 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j45b "Âge [Enfant 4 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j45c "Lieu d'habitat [Enfant 4 - Pour les enfants qui viennent le plus souvent visiter leur parent, veuillez indiquer le sexe, l'âge et le lieu d'habitat]".
VARIABLE LABELS j26 "Avez-vous un ou plusieurs petits-enfants (arrière-petits enfants)?".
VARIABLE LABELS j115a "A bon appétit [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115b "Souffre de solitude [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115c "Souffre d'insomnie [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115d "Prend plaisir à ce qu'elle/il fait [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115e "Trouve le temps long [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115f "Se sent triste [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115g "A confiance en elle [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115h "A des crises de larmes, envie de pleurer [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115i "Se sent fatigué/e [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115j "Se fait du souci pour sa santé [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115k "Est irritable [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j115m "Est anxieux, /anxieuse, angoissé/e [Voici une série d'affirmations. Par exemple: 'a un bon appétit'. Pouvez-vous dire, s.v.p., si c'est son cas?]".
VARIABLE LABELS j172a "Recevez-vous, chez vous, les membres de votre famille?".
VARIABLE LABELS j172b "Recevez-vous, chez vous, des amis ou connaissances?".
VARIABLE LABELS dpd "Relation avec l'interviewé/e [Proxi]".
VARIABLE LABELS dpe "Par rapport à M./Mme ..., vous habitez: [Proxi]".
VARIABLE LABELS dpf "Vous connaissez Mme/M. ... depuis: [Proxi]".
VARIABLE LABELS dpg "Vous rendez visite à Mme/M ...: [Proxi]".
VARIABLE LABELS dph "Quelle est actuellement votre activité principale? [Proxi]".
VARIABLE LABELS dpi "État civil [Proxi]".
VARIABLE LABELS dpl "Date de l'entretien".
VARIABLE LABELS d1_12 "Sexe [Enfant 1]".
VARIABLE LABELS d1_13 "Âge [Enfant 1]".
VARIABLE LABELS d1_14 "Quel est son état civil, aujourd'hui? [Enfant 1]".
VARIABLE LABELS d1_15 "A-t-il/elle des enfants? Nombre: [Enfant 1]".
VARIABLE LABELS d1_18 "Quelle est son activité principale? [Enfant 1]".
VARIABLE LABELS d1_19 "Quelle est l'activité principale de son/sa conjoint/e? [Enfant 1]".
VARIABLE LABELS d1_21 "Par rapport à la personne, son enfant habite: [Enfant 1]".
VARIABLE LABELS d2_12 "Sexe [Enfant 2]".
VARIABLE LABELS d2_13 "Âge [Enfant 2]".
VARIABLE LABELS d2_14 "Quel est son état civil, aujourd'hui? [Enfant 2]".
VARIABLE LABELS d2_15 "A-t-il/elle des enfants? Nombre: [Enfant 2]".
VARIABLE LABELS d2_18 "Quelle est son activité principale? [Enfant 2]".
VARIABLE LABELS d2_19 "Quelle est l'activité principale de son/sa conjoint/e? [Enfant 2]".
VARIABLE LABELS d2_21 "Par rapport à la personne, son enfant habite: [Enfant 2]".
VARIABLE LABELS dx7 "Le 'proxi' est-il un des deux enfants cités ici?".
VARIABLE LABELS d26 "Avez-vous (la personne âgée, pas le proxi!) un ou des petits-enfants (arrière-petits enfants)?".
VARIABLE LABELS d91 "Depuis combien d'années habitez-vous dans cette maison/appartement?".
VARIABLE LABELS d93 "Où habitiez-vous avant de venir vous installer ici?".
VARIABLE LABELS d96 "Votre appartement est-il adapté à vos besoins et à votre santé?".
VARIABLE LABELS dx8a "Conjoint/e [Qui est-ce qui s'occupe de vous plus particulièrement (visites, téléphones, démarches administratives)?]".
VARIABLE LABELS dx8b "Enfant(s) [Qui est-ce qui s'occupe de vous plus particulièrement (visites, téléphones, démarches administratives)?]".
VARIABLE LABELS dx8c "Parent(s), beaux-parents [Qui est-ce qui s'occupe de vous plus particulièrement (visites, téléphones, démarches administratives)?]".
VARIABLE LABELS dx8d "Autre(s) membre(s) de la famille [Qui est-ce qui s'occupe de vous plus particulièrement (visites, téléphones, démarches administratives)?]".
VARIABLE LABELS dx8e "Ami(s), amie(s) [Qui est-ce qui s'occupe de vous plus particulièrement (visites, téléphones, démarches administratives)?]".
VARIABLE LABELS dx8f "Employé/e(s), infirmière, garde [Qui est-ce qui s'occupe de vous plus particulièrement (visites, téléphones, démarches administratives)?]".
VARIABLE LABELS dx8g "Autre [Qui est-ce qui s'occupe de vous plus particulièrement (visites, téléphones, démarches administratives)?]".
VARIABLE LABELS dx8h "Voisin [Qui est-ce qui s'occupe de vous plus particulièrement (visites, téléphones, démarches administratives)?]".
VARIABLE LABELS senfviv "Nombre d'enfants vivants".
VARIABLE LABELS sansfam "Personnes sans famille".
VARIABLE LABELS snfratri "Nombre de frères/soeurs vivant-e-s".
VARIABLE LABELS lgenfviv "Ego a au moins un enfant vivant".
VARIABLE LABELS lgfratri "Ego a au moins un frère/une soeur vivant‎/e".
VARIABLE LABELS ptsenfan "Ego a au moins un petit-fils ou une petite-fille".
VARIABLE LABELS frersoeu "Ego a au moins un frère ou une soeur".
VARIABLE LABELS parents "Ego a encore son père ou sa mère".
VARIABLE LABELS bparents "Ego a encore son beau-père ou sa belle-mère".
VARIABLE LABELS agemere "Âge de la mère".
VARIABLE LABELS agepere "Âge du père".
VARIABLE LABELS agebmere "Âge de la belle-mère".
VARIABLE LABELS agebpere "Âge du beau-père".
VARIABLE LABELS ageascd "Âge des ascendants directs".
VARIABLE LABELS ageascal "Âge des ascendants par alliance".
VARIABLE LABELS agmari1 "Âge au mariage".
VARIABLE LABELS agveuv1 "Âge au veuvage".
VARIABLE LABELS pondrep5 "Pondération".

*Définition des labels de valeurs

VALUE LABELS typeq
	1	"Questionnaire domicile apte < 80 ans"
	2	"Questionnaire domicile apte ≥ 80 ans"
	3	"Questionnaire institution apte"
	4	"Questionnaire institution non apte"
	5	"Questionnaire domicile non apte".
VALUE LABELS canton
	1	"Valais"
	2	"Genève".
VALUE LABELS sexe
	1	"Homme"
	2	"Femme".
VALUE LABELS clage
	1	"60-64 ans"
	2	"65-69 ans"
	3	"70-74 ans"
	4	"75-79 ans"
	5	"80-84 ans"
	6	"85 ans et +".
VALUE LABELS dateentr
	-2	"NR".
VALUE LABELS tb1
	-2	"NR".
VALUE LABELS tb2
	-2	"NR"
	1	"Français"
	2	"Allemand, suisse allemand"
	3	"Italien"
	4	"Espagnol"
	5	"Anglais"
	6	"Langues slaves"
	7	"Autre"
	8	"Patois".
VALUE LABELS nb3
	-9	"NV"
	-7	"INAP"
	-2	"NR"
	84	"Dällikon"
	102	"Weiach"
	114	"Fischenthal"
	121	"Wetzikon (ZH)"
	133	"Horgen"
	135	"Kilchberg (ZH)"
	141	"Thalwil"
	154	"Küsnacht (ZH)"
	191	"Dübendorf"
	230	"Winterthur"
	253	"Zürich"
	306	"Lyss"
	351	"Bern"
	355	"Köniz"
	356	"Muri bei Bern"
	371	"Biel‎/Bienne"
	418	"Oberburg"
	433	"Cortébert"
	438	"Orvin"
	443	"Saint-Imier"
	446	"Tramelan"
	556	"Zielebach"
	576	"Grindelwald"
	581	"Interlaken"
	619	"Oberdiessbach"
	662	"Ferenbalm"
	667	"Laupen"
	682	"Bévilard"
	687	"Corcelles (BE)"
	690	"Court"
	699	"Monible"
	700	"Moutier"
	703	"Reconvilier"
	723	"La Neuveville"
	784	"Innertkirchen"
	843	"Saanen"
	853	"Rüschegg"
	902	"Langnau im Emmental"
	905	"Rüderswil"
	929	"Hilterfingen"
	1003	"Escholzmatt"
	1061	"Luzern"
	1149	"Willisau Stadt"
	1301	"Einsiedeln"
	1402	"Engelberg"
	1509	"Stans"
	1618	"Mühlehorn"
	1709	"Unterägeri"
	1711	"Zug"
	2001	"Aumont"
	2011	"Cugy (FR)"
	2035	"Nuvilly"
	2046	"Vesin"
	2080	"Hennens"
	2096	"Romont (FR)"
	2102	"Ursy"
	2113	"Vuisternens-devant-Romont"
	2121	"Haut-Intyamon"
	2124	"Broc"
	2125	"Bulle"
	2135	"Gruyères"
	2147	"Pont-la-Ville"
	2175	"Belfaux"
	2184	"Corpataux"
	2196	"Fribourg"
	2210	"Montévraz"
	2217	"Ponthaux"
	2220	"Le Mouret"
	2254	"Courtepin"
	2265	"Kerzers"
	2293	"Düdingen"
	2296	"Heitenried"
	2299	"Plaffeien"
	2323	"Bossonnens"
	2325	"Châtel-Saint-Denis"
	2326	"Le Crêt"
	2336	"Semsales"
	2546	"Grenchen"
	2581	"Olten"
	2582	"Rickenbach (SO)"
	2601	"Solothurn"
	2701	"Basel"
	2802	"Allschwil"
	2825	"Füllinsdorf"
	2829	"Liestal"
	2888	"Langenbruck"
	2937	"Neuhausen am Rheinfall"
	2964	"Stein am Rhein"
	3001	"Herisau"
	3021	"Bühler"
	3111	"Oberegg"
	3203	"St. Gallen"
	3215	"Rorschach"
	3231	"Au (SG)"
	3233	"Berneck"
	3235	"Rheineck"
	3296	"Sargans"
	3392	"Kirchberg (SG)"
	3393	"Lütisburg"
	3406	"Mogelsberg"
	3561	"Poschiavo"
	3787	"St. Moritz"
	3832	"Grono"
	3901	"Chur"
	3952	"Jenins"
	4021	"Baden"
	4063	"Bremgarten (AG)"
	4082	"Wohlen (AG)"
	4172	"Münchwilen (AG)"
	4258	"Rheinfelden"
	4315	"Rekingen (AG)"
	4401	"Arbon"
	4405	"<none>"
	4461	"Amriswil"
	4545	"Diessenhofen"
	4641	"Altnau"
	4941	"Märstetten"
	5002	"Bellinzona"
	5040	"Ludiano"
	5192	"Lugano"
	5210	"Paradiso"
	5222	"Sessa"
	5401	"Aigle"
	5402	"Bex"
	5405	"Gryon"
	5406	"Lavey-Morcles"
	5408	"Noville"
	5409	"Ollon"
	5410	"Ormont-Dessous"
	5411	"Ormont-Dessus"
	5412	"Rennaz"
	5422	"Aubonne"
	5434	"Saint-George"
	5455	"Constantine"
	5456	"<none>"
	5477	"Cossonay"
	5484	"Gollion"
	5492	"Montricher"
	5495	"Penthalaz"
	5524	"Goumoens-la-Ville"
	5531	"Penthéréaz"
	5554	"Concise"
	5561	"Grandson"
	5568	"Sainte-Croix"
	5586	"Lausanne"
	5590	"Pully"
	5591	"Renens (VD)"
	5601	"Chexbres"
	5602	"Cully"
	5604	"Forel (Lavaux)"
	5605	"Grandvaux"
	5607	"Puidoux"
	5621	"Aclens"
	5633	"Echandens"
	5635	"Ecublens (VD)"
	5642	"Morges"
	5688	"Syens"
	5703	"Bassins"
	5711	"Commugny"
	5712	"Coppet"
	5713	"Crans-près-Céligny"
	5714	"Crassier"
	5718	"Genolier"
	5721	"Gland"
	5722	"Grens"
	5723	"Mies"
	5724	"Nyon"
	5725	"Prangins"
	5727	"Saint-Cergue"
	5743	"Arnex-sur-Orbe"
	5744	"Ballaigues"
	5745	"Baulmes"
	5750	"Les Clées"
	5754	"Juriens"
	5757	"Orbe"
	5764	"Vallorbe"
	5765	"Vaulion"
	5789	"Ferlens (VD)"
	5792	"Montpreveyres"
	5793	"Oron-la-Ville"
	5816	"Corcelles-près-Payerne"
	5841	"Château-d'Oex"
	5853	"Bursins"
	5854	"Burtigny"
	5857	"Gilly"
	5859	"Mont-sur-Rolle"
	5861	"Rolle"
	5881	"Blonay"
	5886	"Montreux"
	5889	"La Tour-de-Peilz"
	5890	"Vevey"
	5928	"Rovray"
	5934	"Valeyres-sous-Ursins"
	5938	"Yverdon-les-Bains"
	6002	"Brig-Glis"
	6006	"Mund"
	6007	"Naters"
	6008	"Ried-Brig"
	6009	"Simplon"
	6021	"Ardon"
	6022	"Chamoson"
	6023	"Conthey"
	6024	"Nendaz"
	6025	"Vétroz"
	6028	"-- code à corriger --"
	6031	"Bagnes"
	6032	"Bourg-Saint-Pierre"
	6034	"Orsières"
	6053	"Biel (VS)"
	6081	"Les Agettes"
	6082	"Ayent"
	6083	"Evolène"
	6084	"Hérémence"
	6085	"Mase"
	6086	"Nax"
	6087	"Saint-Martin (VS)"
	6088	"Vernamiège"
	6089	"Vex"
	6101	"Agarn"
	6105	"Erschmatt"
	6106	"Feschel"
	6110	"Leuk"
	6111	"Leukerbad"
	6113	"Salgesch"
	6116	"Varen"
	6133	"Fully"
	6134	"Isérables"
	6135	"Leytron"
	6136	"Martigny"
	6139	"Riddes"
	6140	"Saillon"
	6141	"Saxon"
	6142	"Trient"
	6151	"Champéry"
	6152	"Collombey-Muraz"
	6153	"Monthey"
	6155	"Saint-Gingolph"
	6156	"Troistorrents"
	6159	"Vouvry"
	6173	"Bitsch"
	6175	"Goppisberg"
	6179	"Mörel"
	6191	"Ausserberg"
	6195	"Ferden"
	6200	"Steg"
	6213	"Evionnaz"
	6217	"Saint-Maurice"
	6219	"Vernayaz"
	6231	"Ayer"
	6232	"Chalais"
	6233	"Chandolin"
	6234	"Chermignon"
	6235	"Chippis"
	6237	"Grimentz"
	6238	"Grône"
	6239	"Icogne"
	6240	"Lens"
	6241	"Miège"
	6242	"Mollens (VS)"
	6243	"Montana"
	6244	"Randogne"
	6245	"Saint-Jean"
	6246	"Saint-Léonard"
	6247	"Saint-Luc"
	6248	"Sierre"
	6249	"Venthône"
	6250	"Veyras"
	6251	"Vissoie"
	6261	"Arbaz"
	6263	"Grimisuat"
	6264	"Salins"
	6265	"Savièse"
	6266	"Sion"
	6267	"Veysonnaz"
	6285	"Grächen"
	6293	"Stalden (VS)"
	6297	"Visp"
	6402	"Bevaix"
	6407	"Corcelles-Cormondrèche"
	6408	"Cortaillod"
	6413	"Rochefort"
	6421	"La Chaux-de-Fonds"
	6423	"La Sagne"
	6431	"Les Brenets"
	6436	"Le Locle"
	6455	"Le Landeron"
	6458	"Neuchâtel"
	6459	"Saint-Blaise"
	6475	"Dombresson"
	6505	"Couvet"
	6506	"Fleurier"
	6511	"Les Verrières"
	6601	"Aire-la-Ville"
	6602	"Anières"
	6603	"Avully"
	6604	"Avusy"
	6606	"Bellevue"
	6607	"Bernex"
	6608	"Carouge (GE)"
	6609	"Cartigny"
	6610	"Céligny"
	6612	"Chêne-Bougeries"
	6613	"Chêne-Bourg"
	6614	"Choulex"
	6615	"Collex-Bossy"
	6616	"Collonge-Bellerive"
	6617	"Cologny"
	6618	"Confignon"
	6619	"Corsier (GE)"
	6620	"Dardagny"
	6621	"Genève"
	6622	"Genthod"
	6623	"Le Grand-Saconnex"
	6625	"Hermance"
	6626	"Jussy"
	6627	"Laconnex"
	6628	"Lancy"
	6629	"Meinier"
	6630	"Meyrin"
	6631	"Onex"
	6632	"Perly-Certoux"
	6633	"Plan-les-Ouates"
	6634	"Pregny-Chambésy"
	6635	"Presinge"
	6638	"Satigny"
	6639	"Soral"
	6640	"Thônex"
	6641	"Troinex"
	6642	"Vandoeuvres"
	6643	"Vernier"
	6644	"Versoix"
	6645	"Veyrier"
	6707	"Courfaivre"
	6708	"Courrendlin"
	6711	"Delémont"
	6727	"Vicques"
	6757	"Saignelégier"
	6775	"Bonfol"
	6782	"Cornol"
	6783	"Courchavon"
	6800	"Porrentruy"
	6804	"Saint-Ursanne"
	7101	"Albanie"
	7102	"Belgique"
	7103	"Bulgarie"
	7105	"Allemagne"
	7107	"France"
	7108	"Royaume-Uni"
	7111	"Italie"
	7112	"Yougoslavie"
	7115	"Pays-Bas"
	7117	"Autriche"
	7118	"Pologne"
	7119	"Portugal"
	7120	"Roumanie"
	7121	"Suède"
	7122	"Suisse"
	7123	"Espagne"
	7124	"Tchécoslovaquie"
	7127	"Hongrie"
	7129	"Grèce"
	7130	"Monaco"
	7140	"Russie"
	7141	"Ukraine"
	7150	"Ain, Haute-Savoie"
	7209	"Inde"
	7210	"Indonésie"
	7212	"Iran"
	7213	"Israël"
	7224	"Liban"
	7231	"Philippines"
	7236	"Thaïlande"
	7300	"Afrique"
	7301	"Egypte"
	7302	"Algérie"
	7312	"Djibouti"
	7326	"Libye"
	7330	"Maroc"
	7340	"Sénégal"
	7344	"Afrique du Sud"
	7351	"Tunisie"
	7401	"Argentine"
	7410	"Equateur"
	7418	"Canada"
	7420	"Cuba"
	7421	"Mexique"
	7425	"Pérou"
	7432	"Etats-Unis"
	7691	"Nouvelle-Calédonie"
	7801	"Canton de Zurich"
	7802	"Canton de Berne"
	7803	"Canton de Lucerne"
	7810	"Canton de Fribourg"
	7811	"Canton de Soleure"
	7812	"Canton de Bâle-Ville"
	7813	"Canton de Bâle-Campagne"
	7814	"Canton de Schaffhouse"
	7817	"Canton se Saint-Gall"
	7818	"Canton des Grisons"
	7819	"Canton d'Argovie"
	7820	"Canton de Thurgovie"
	7821	"Canton du Tessin"
	7822	"Canton de Vaud"
	7823	"Canton du Valais"
	7824	"Canton de Neuchâtel"
	7825	"Canton de Genève"
	7826	"Canton du Jura"
	8000	"Autre".
VALUE LABELS tb4
	-2	"NR"
	1	"Célibataire"
	2	"Marié/e, remarié‎/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS mb5
	-9	"NV"
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb6a
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb6b
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb7a
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb7b
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb8a
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb8b
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb9
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb10
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb11
	-7	"INAP".
VALUE LABELS mb12
	-7	"INAP"
	-2	"NR"
	1	"Fils"
	2	"Fille".
VALUE LABELS mb13a
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb13b
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb14
	-7	"INAP"
	-2	"NR"
	1	"Célibataire"
	2	"Marié‎/e, remarié/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS mb15
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b16
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb17
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb20
	-7	"INAP"
	-2	"NR"
	1	"Chez lui‎/elle"
	2	"Chez vous"
	3	"Dans une pension, home foyer"
	8	"Autre".
VALUE LABELS sb21
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS b22
	-7	"INAP"
	-2	"NR"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS mb23
	-7	"INAP"
	-2	"NR"
	1	"Fils"
	2	"Fille".
VALUE LABELS mb24a
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb24b
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb25
	-7	"INAP"
	-2	"NR"
	1	"Célibataire"
	2	"Marié‎/e, remarié/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS mb26
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b27
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb28
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb29
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb30
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb31
	-7	"INAP"
	-2	"NR"
	1	"Chez lui‎/elle"
	2	"Chez vous"
	3	"Dans une pension, home foyer"
	8	"Autre".
VALUE LABELS sb32
	-7	"INAP"
	-2	"NR"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS b33
	-7	"INAP"
	-2	"NR"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS mb34
	-7	"INAP"
	-2	"NR"
	1	"Fils"
	2	"Fille".
VALUE LABELS mb35a
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb35b
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb36
	-7	"INAP"
	-2	"NR"
	1	"Célibataire"
	2	"Marié‎/e, remarié/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS mb37
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b38
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb39
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb40
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb41
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb42
	-7	"INAP"
	-2	"NR"
	1	"Chez lui‎/elle"
	2	"Chez vous"
	3	"Dans une pension, home foyer"
	8	"Autre".
VALUE LABELS sb43
	-7	"INAP"
	-2	"NR"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS b44
	-7	"INAP"
	-2	"NR"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS mb45
	-7	"INAP"
	-2	"NR"
	1	"Fils"
	2	"Fille".
VALUE LABELS mb46a
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb46b
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb47
	-7	"INAP"
	-2	"NR"
	1	"Célibataire"
	2	"Marié‎/e, remarié/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS mb48
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b49
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb50
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb51
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb52
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb53
	-7	"INAP"
	-2	"NR"
	1	"Chez lui‎/elle"
	2	"Chez vous"
	3	"Dans une pension, home foyer"
	8	"Autre".
VALUE LABELS sb54
	-7	"INAP"
	-2	"NR"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS b55
	-7	"INAP"
	-2	"NR"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS b56a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b56b
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b56c
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b57
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Célibataire"
	2	"Mariée, remariée"
	3	"Divorcée, séparée"
	4	"Veuve".
VALUE LABELS b58
	-7	"INAP"
	-2	"NR"
	1	"Chez elle"
	2	"Chez vous"
	3	"Chez un autre enfant"
	4	"Dans un établissement du type: pension, home, foyer, hôpital"
	8	"Autre".
VALUE LABELS b59a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b59b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b59c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b59d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b59e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b59f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b60
	-7	"INAP"
	-2	"NR"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS b61
	-7	"INAP"
	-2	"NR"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS b62a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b62b
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b62c
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b63
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Célibataire"
	2	"Marié, remarié"
	3	"Divorcé, séparé"
	4	"Veuf".
VALUE LABELS b64
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Chez lui"
	2	"Chez vous"
	3	"Chez un autre enfant"
	4	"Dans un établissement du type: pension, home, foyer, hôpital"
	8	"Autre".
VALUE LABELS b65a
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Oui".
VALUE LABELS b65b
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Oui".
VALUE LABELS b65c
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Oui".
VALUE LABELS b65d
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Oui".
VALUE LABELS b65e
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Oui".
VALUE LABELS b65f
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Oui".
VALUE LABELS b66
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS b67
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS b68a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b68b
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b68c
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b69
	-7	"INAP"
	-2	"NR"
	-1	"Je ne sais pas"
	1	"Célibataire"
	2	"Mariée, remariée"
	3	"Divorcée, séparée"
	4	"Veuve".
VALUE LABELS b70
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Chez elle"
	2	"Chez vous"
	3	"Chez un autre enfant"
	4	"Dans un établissement du type: pension, home, foyer, hôpital"
	8	"Autre".
VALUE LABELS b71a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b71b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b71c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b71d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b71e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b71f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b72
	-7	"INAP"
	-2	"NR"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS b73
	-7	"INAP"
	-2	"NR"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS b74a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b74b
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b74c
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS b75
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Célibataire"
	2	"Marié, remarié"
	3	"Divorcé, séparé"
	4	"Veuf".
VALUE LABELS b76
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Chez lui"
	2	"Chez vous"
	3	"Chez un autre enfant"
	4	"Dans un établissement du type: pension, home, foyer, hôpital"
	8	"Autre".
VALUE LABELS b77a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b77b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b77c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b77d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b77e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b77f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS b78
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS b79
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS sb80
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb81a
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb81b
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb81ab
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb82
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb83a
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb83b
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb83c
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb83d
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb83e
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb83f
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb83g
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb84
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS pb85
	-7	"INAP"
	-2	"NR"
	1	"Chez vous (propriétaire)"
	2	"Chez vous (locataire)"
	3	"Chez un de vos enfants"
	4	"Chez un autre membre de votre famille"
	5	"Chez un/e ami‎/e"
	6	"Couvent"
	8	"Autre".
VALUE LABELS pb86
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS pb87
	-7	"INAP"
	-2	"NR".
VALUE LABELS pb88a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS pb88b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS q88c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS pb88d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS pb88e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS pb88f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS pb88g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb89
	-7	"INAP"
	-2	"NR"
	0	"Non, je ne le souhaite pas"
	1	"Oui, parfois je le souhaite"
	2	"Oui, j'aimerais beaucoup".
VALUE LABELS sb90
	-7	"INAP"
	-2	"NR"
	1	"Enfant(s)"
	2	"Parent(s)"
	3	"Pension, institution"
	4	"Compagnon"
	8	"Autres".
VALUE LABELS sb91
	-7	"INAP"
	-2	"NR"
	1	"Moins de 2 ans"
	2	"Entre 2 et 5 ans"
	3	"Plus de 5 ans"
	4	"Plus de 10 ans"
	5	"Plus de 20 ans"
	6	"Plus de 50 ans"
	7	"Toujours habité ici".
VALUE LABELS sb92
	-7	"INAP"
	-2	"NR"
	1	"Moins de 2 ans"
	2	"Entre 2 et 5 ans"
	3	"Plus de 5 ans"
	4	"Plus de 10 ans"
	5	"Plus de 20 ans"
	6	"Plus de 50 ans"
	7	"Toujours habité ici".
VALUE LABELS sb93
	-7	"INAP"
	-2	"NR"
	245	"Oberengstringen"
	351	"Bern"
	371	"Biel (BE)"
	412	"Kirchberg (BE)"
	443	"Saint-Imier"
	592	"Schwanden bei Brienz"
	2228	"Villars-sur-Glâne"
	2546	"Grenchen"
	2573	"Dulliken"
	4001	"Aarau"
	4021	"Baden"
	5192	"Lugano"
	5401	"Aigle"
	5409	"Ollon"
	5422	"Aubonne"
	5495	"Penthalaz"
	5586	"Lausanne"
	5589	"Prilly"
	5602	"Cully"
	5603	"Epesses"
	5627	"Chavannes-près-Renens"
	5635	"Ecublens (VD)"
	5637	"Lavigny"
	5642	"Morges"
	5711	"Commugny"
	5713	"Crans-près-Céligny"
	5717	"Founex"
	5721	"Gland"
	5723	"Mies"
	5724	"Nyon"
	5886	"Montreux"
	6007	"Naters"
	6021	"Ardon"
	6022	"Chamoson"
	6023	"Conthey"
	6024	"Nendaz"
	6081	"Les Agettes"
	6082	"Ayent"
	6086	"Nax"
	6089	"Vex"
	6132	"Charrat"
	6136	"Martigny"
	6217	"Saint-Maurice"
	6231	"Ayer"
	6235	"Chippis"
	6238	"Grône"
	6240	"Lens"
	6242	"Mollens (VS)"
	6243	"Montana"
	6245	"Saint-Jean"
	6248	"Sierre"
	6249	"Venthône"
	6251	"Vissoie"
	6264	"Salins"
	6266	"Sion"
	6454	"Hauterive"
	6455	"Le Landeron"
	6458	"Neuchâtel"
	6459	"Saint-Blaise"
	6510	"Travers"
	6602	"Anières"
	6604	"Avusy"
	6606	"Bellevue"
	6607	"Bernex"
	6608	"Carouge (GE)"
	6610	"Céligny"
	6612	"Chêne-Bougeries"
	6613	"Chêne-Bourg"
	6614	"Choulex"
	6615	"Collex-Bossy"
	6616	"Collonge-Bellerive"
	6618	"Confignon"
	6619	"Corsier (GE)"
	6621	"Genève"
	6623	"Le Grand-Saconnex"
	6625	"Hermance"
	6626	"Jussy"
	6627	"Laconnex"
	6628	"Lancy"
	6629	"Meinier"
	6630	"Meyrin"
	6631	"Onex"
	6632	"Perly-Certoux"
	6633	"Plan-les-Ouates"
	6634	"Pregny-Chambésy"
	6636	"Puplinge"
	6639	"Soral"
	6640	"Thônex"
	6641	"Troinex"
	6643	"Vernier"
	6644	"Versoix"
	6645	"Veyrier"
	6707	"Courfaivre"
	7105	"Allemagne"
	7107	"France"
	7111	"Italie"
	7115	"Pays-Bas"
	7118	"Pologne"
	7120	"Roumanie"
	7121	"Suède"
	7122	"Suisse"
	7125	"Turquie"
	7130	"Monaco"
	7150	"Ain, Haute-Savoie (France)"
	7230	"Pakistan"
	7401	"Argentine"
	7432	"États-Unis"
	7696	"Guadeloupe"
	7801	"Canton de Zurich"
	7802	"Canton de Berne"
	7821	"Canton du Tessin"
	7825	"Canton de Genève".
VALUE LABELS pb94a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb94b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb94c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb94d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb94e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb94f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb94g
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb94h
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb94i
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb94j
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb94k
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb95
	-7	"INAP"
	-2	"NR"
	1	"1 ou 1 et ½ pièce"
	2	"2 ou 2 et ½ pièces"
	3	"3 ou 3 et ½ pièces"
	4	"4 ou 4 et ½ pièces"
	5	"5 pièces et plus".
VALUE LABELS mb96a
	-7	"INAP"
	-2	"NR"
	1	"Pas du tout satisfaisant"
	2	"Pas très satisfaisant"
	3	"Plutôt satisfaisant"
	4	"Très satisfaisant".
VALUE LABELS mb96b
	-7	"INAP"
	-2	"NR"
	1	"Pas du tout satisfaisant"
	2	"Pas très satisfaisant"
	3	"Plutôt satisfaisant"
	4	"Très satisfaisant".
VALUE LABELS mb96c
	-7	"INAP"
	-2	"NR"
	1	"Pas du tout satisfaisant"
	2	"Pas très satisfaisant"
	3	"Plutôt satisfaisant"
	4	"Très satisfaisant".
VALUE LABELS mb96d
	-7	"INAP"
	-2	"NR"
	1	"Pas du tout satisfaisant"
	2	"Pas très satisfaisant"
	3	"Plutôt satisfaisant"
	4	"Très satisfaisant".
VALUE LABELS mb96e
	-7	"INAP"
	-2	"NR"
	1	"Pas du tout satisfaisant"
	2	"Pas très satisfaisant"
	3	"Plutôt satisfaisant"
	4	"Très satisfaisant".
VALUE LABELS mb96f
	-7	"INAP"
	-2	"NR"
	1	"Pas du tout satisfaisant"
	2	"Pas très satisfaisant"
	3	"Plutôt satisfaisant"
	4	"Très satisfaisant".
VALUE LABELS mb97a
	-7	"INAP"
	-2	"NR"
	1	"Je ne peux plus y aller"
	2	"Je n'y vais jamais"
	3	"Plus de 30 min."
	4	"Moins de 30 min."
	5	"Moins de 15 min."
	6	"Moins de 5 min.".
VALUE LABELS mb97b
	-7	"INAP"
	-2	"NR"
	1	"Je ne peux plus y aller"
	2	"Je n'y vais jamais"
	3	"Plus de 30 min."
	4	"Moins de 30 min."
	5	"Moins de 15 min."
	6	"Moins de 5 min.".
VALUE LABELS sb97c
	-7	"INAP"
	-2	"NR"
	1	"Je ne peux plus y aller"
	2	"Je n'y vais jamais"
	3	"Plus de 30 min."
	4	"Moins de 30 min."
	5	"Moins de 15 min."
	6	"Moins de 5 min.".
VALUE LABELS pb98a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS pb98b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS pb98c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS pb98d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS pb98e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS pb98f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS pb98g
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS pb98h
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb98i
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS pb98j
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb99
	-7	"INAP"
	-2	"NR"
	1	"Dans la même maison (chez des voisins)"
	2	"Dans la maison voisine"
	3	"Dans une cabine publique".
VALUE LABELS sb100a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb100b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb100c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb100d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb100e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb100f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb100g
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb100h
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb100i
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb100j
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb100k
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb101a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb101b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb101c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb101d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb101e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb101f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb101g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb101h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb101i
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb101j
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb101k
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, en partie"
	2	"Oui".
VALUE LABELS sb102a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb102b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb102c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb102d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS tb103
	-7	"INAP"
	-2	"NR"
	1	"Jamais"
	2	"Moins d'1 fois par semaine, rarement"
	3	"1 à 3 fois par semaine"
	4	"Tous les jours".
VALUE LABELS sb104
	-7	"INAP"
	-2	"NR"
	1	"Jamais"
	2	"Moins d'1 fois par semaine, rarement"
	3	"1 à 3 fois par semaine"
	4	"Tous les jours, moins d'1 heure"
	5	"Tous les jours, 1 à 2 heures"
	6	"Tous les jours, 2 à 3 heures"
	7	"Tous les jours, plus de 3 heures".
VALUE LABELS tb105
	-7	"INAP"
	-2	"NR"
	1	"Jamais"
	2	"Moins d'1 fois par semaine, rarement"
	3	"1 à 3 fois par semaine"
	4	"Tous les jours".
VALUE LABELS tb106
	-7	"INAP"
	-2	"NR"
	1	"Jamais"
	2	"Moins d'1 fois par semaine, rarement"
	3	"1 à 3 fois par semaine"
	4	"Tous les jours, moins d'1 heure"
	5	"Tous les jours, 1 heure ou plus".
VALUE LABELS mb107a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb107b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb107c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb107d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb107e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb107f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb108a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb108b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb108c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb108d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb108e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb108f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb109a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb109b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb109c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb109d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb109e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb109f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb109g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb109h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb110a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb110b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb110c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb110d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb110e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb110f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb110g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb110h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mb110i
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb111
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb112
	-7	"INAP"
	-2	"NR"
	1	"Jamais"
	2	"Pratiquement jamais"
	3	"Quelques fois par mois"
	4	"Une fois par semaine environ"
	5	"Quelques fois par semaine"
	6	"Tous les jours ou presque".
VALUE LABELS sb113
	-7	"INAP"
	-2	"NR"
	1	"Moi"
	2	"Mon‎/ma conjoint‎/e"
	3	"Les deux"
	8	"Autre personne".
VALUE LABELS sb113a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb113b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb113c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb113d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb114
	-7	"INAP"
	-2	"NR"
	0	"Non, je n'en ai jamais eu"
	1	"Oui".
VALUE LABELS b115
	-7	"INAP"
	-2	"NR".
VALUE LABELS b116a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b116b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b116c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b116d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS tb117a
	-2	"NR"
	0	"Non"
	1	"Oui, mais accompagné/e seulement"
	2	"Oui, seul‎/e ou accompagné/e".
VALUE LABELS tb117b
	-2	"NR"
	0	"Non"
	1	"Oui, mais accompagné/e seulement"
	2	"Oui, seul‎/e ou accompagné/e".
VALUE LABELS tb117c
	-2	"NR"
	0	"Non"
	1	"Oui, mais accompagné/e seulement"
	2	"Oui, seul‎/e ou accompagné/e".
VALUE LABELS tb117d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mb118a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mb118b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mb118c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mb119a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mb119b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mb119c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS b120
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b121
	-7	"INAP"
	-2	"NR".
VALUE LABELS b122
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b123
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb124
	-7	"INAP"
	-2	"NR"
	1	"Mauvais"
	2	"Plutôt mauvais"
	3	"Satisfaisant"
	4	"Bon"
	5	"Très bon".
VALUE LABELS b126
	-7	"INAP"
	-2	"NR"
	0	"Non, jamais"
	1	"Oui, c'est arrivé".
VALUE LABELS b127
	-7	"INAP"
	-2	"NR".
VALUE LABELS b128
	-7	"INAP"
	-2	"NR".
VALUE LABELS b129
	-7	"INAP"
	-2	"NR"
	1	"Accident"
	2	"Maladie"
	3	"Accident et maladie"
	8	"Autre cause".
VALUE LABELS b130
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, avec gênes occasionnelles ou mineures"
	3	"Oui, totalement".
VALUE LABELS b131
	-7	"INAP"
	-2	"NR"
	1	"Non, pas du tout"
	2	"Non, presque pas"
	3	"Oui, durant plusieurs mois"
	4	"Oui, durant plusieurs années"
	5	"Oui, jusqu'à aujourd'hui".
VALUE LABELS tb132aa
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS tb132ab
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS tb132ac
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS tb132ad
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS tb132ae
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS tb132af
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS tb132ag
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS tb132ah
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS tb132ai
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS tb132aj
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS tb132ak
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, un peu"
	3	"Non, pas du tout".
VALUE LABELS sb132ba
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS sb132bb
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS sb132bc
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS sb132bd
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS sb132be
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS sb132bf
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS sb132bg
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS sb132bh
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS sb132bi
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS sb132bj
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS sb132bk
	-8	"INAP (NON sur trouble)"
	-7	"INAP"
	-2	"NR"
	1	"Oui beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS tb133
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS tb134
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté majeure".
VALUE LABELS tb135
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, un peu"
	3	"Oui, beaucoup".
VALUE LABELS tb136
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS tb137
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté majeure".
VALUE LABELS tb138
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté majeure".
VALUE LABELS tb139
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, un peu"
	3	"Oui, beaucoup".
VALUE LABELS tb140
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS tb141
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb142
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS tb143
	-7	"INAP"
	-2	"NR".
VALUE LABELS nbmedic
	-7	"INAP"
	-2	"NR".
VALUE LABELS mb144
	-7	"INAP"
	-2	"NR"
	1	"Je ne peux pas prendre mes médicaments seul‎/e"
	2	"Avec une certaine aide (pour le dosage, pour ne pas oublier)"
	3	"Seul‎/e, sans aide".
VALUE LABELS sb146
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb147a
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb147b
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb147c
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb147d
	-7	"INAP"
	-2	"NR".
VALUE LABELS sb148
	-7	"INAP"
	-2	"NR"
	1	"Jamais, abstinent"
	2	"Plus rarement"
	3	"Une ou deux fois par semaine"
	4	"Plusieurs fois par semaine"
	5	"Une fois par jour"
	6	"Deux fois par jour (lors des repas)"
	7	"Trois fois ou plus par jour".
VALUE LABELS tb149
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté".
VALUE LABELS tb150
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté".
VALUE LABELS tb151
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté".
VALUE LABELS tb152
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté".
VALUE LABELS tb153
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté".
VALUE LABELS tb154
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté".
VALUE LABELS tb155
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté".
VALUE LABELS tb156
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté".
VALUE LABELS tb157
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté".
VALUE LABELS tb158
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, mais avec difficulté"
	3	"Oui, sans difficulté".
VALUE LABELS tb159
	-7	"INAP"
	-2	"NR"
	1	"Non, jamais"
	2	"Non, car je porte une sonde"
	3	"Oui, quelque fois"
	4	"Oui, fréquemment".
VALUE LABELS sb160
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb161a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb161b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb161c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb161d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb161e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb161f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb161g
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb161h
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb161i
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb161j
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb162a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb162b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb162c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb162d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb162e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb162f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb162g
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mb163
	-7	"INAP"
	-2	"NR"
	1	"Mauvais"
	2	"Plutôt mauvais"
	3	"Satisfaisant"
	4	"Bon"
	5	"Très bon".
VALUE LABELS mb164
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Moins bon"
	2	"Équivalent"
	3	"Meilleur".
VALUE LABELS mb165
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Resté le même"
	2	"Déterioré"
	3	"Amélioré".
VALUE LABELS mb166
	-7	"INAP"
	-2	"NR"
	1	"Mauvais"
	2	"Plutôt mauvais"
	3	"Satisfaisant"
	4	"Bon"
	5	"Très bon".
VALUE LABELS mb167
	-7	"INAP"
	-2	"NR"
	1	"Moins bon"
	2	"À peu près le même"
	3	"Meilleur".
VALUE LABELS sb168a
	-2	"NR"
	1	"Oui".
VALUE LABELS sb168b
	-2	"NR"
	1	"Oui".
VALUE LABELS pb169aa
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb169ab
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb169ac
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb169ad
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb169ae
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb169af
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb169ag
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb169ah
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS pb169ba
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS pb169bb
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS pb169bc
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS pb169bd
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS pb169be
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS pb169bf
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS pb169bg
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS pb169bh
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS sb170aa
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb170ab
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb170ac
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb170ad
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb170ae
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb170af
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb170ag
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb170ah
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sb170ba
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS sb170bb
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS sb170bc
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS sb170bd
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS sb170be
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS sb170bf
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS sb170bg
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS sb170bh
	-7	"INAP"
	-2	"NR"
	1	"Moins souvent"
	2	"Tous les mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS mb171a
	-7	"INAP"
	-2	"NR"
	1	"D'accord"
	2	"En partie d'accord"
	3	"Pas d'accord".
VALUE LABELS mb171b
	-7	"INAP"
	-2	"NR"
	1	"D'accord"
	2	"En partie d'accord"
	3	"Pas d'accord".
VALUE LABELS mb171c
	-7	"INAP"
	-2	"NR"
	1	"D'accord"
	2	"En partie d'accord"
	3	"Pas d'accord".
VALUE LABELS mb171d
	-7	"INAP"
	-2	"NR"
	1	"D'accord"
	2	"En partie d'accord"
	3	"Pas d'accord".
VALUE LABELS mb171e
	-7	"INAP"
	-2	"NR"
	1	"D'accord"
	2	"En partie d'accord"
	3	"Pas d'accord".
VALUE LABELS mb171f
	-7	"INAP"
	-2	"NR"
	1	"D'accord"
	2	"En partie d'accord"
	3	"Pas d'accord".
VALUE LABELS tb172a
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS mb172b
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS lb172c
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS mb172d
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS mb172e
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS mb172f
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS tb173a
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS mb173b
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS lb173c
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS mb173d
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS mb173e
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS b174
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb175a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb175b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb175c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb175d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb175e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176aa
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ba
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ca
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ab
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bb
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176cb
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ac
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bc
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176cc
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ad
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bd
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176cd
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ae
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176be
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ce
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176af
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bf
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176cf
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ag
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bg
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176cg
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ah
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bh
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ch
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ai
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bi
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ci
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176aj
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bj
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176cj
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ak
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bk
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176ck
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176al
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bl
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176cl
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176am
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176bm
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb176cm
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sb177
	-7	"INAP"
	-2	"NR"
	1	"Moins d'1 réunion par mois"
	2	"1 réunion environ par mois"
	3	"2 à 4 réunions par mois"
	4	"5 réunions ou plus par mois".
VALUE LABELS b178
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	0	"Non, à ma connaissance il n'y en a pas"
	1	"Oui, il y en a au moins 1".
VALUE LABELS b179
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b180
	-7	"INAP"
	-2	"NR"
	1	"1 fois par an"
	2	"Quelques fois par an"
	3	"Au moins une fois par mois".
VALUE LABELS b181
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182aa
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182ba
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182ca
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS b182ab
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b182bb
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b182cb
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS sb182ac
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182bc
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182cc
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS sb182ad
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182bd
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182cd
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS sb182ae
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182be
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182ce
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS sb182af
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182bf
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182cf
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS sb182ag
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182bg
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb182cg
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS sb183aa
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb183ba
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS sb183ab
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb183bb
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS sb183ac
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb183bc
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS sb183ad
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sb183bd
	-7	"INAP"
	-2	"NR"
	1	"Occasionnellement"
	2	"Une fois par mois ou plus"
	3	"Une fois par semaine ou plus"
	4	"Tous les jours".
VALUE LABELS sb184
	-7	"INAP"
	-2	"NR"
	1	"Jamais"
	2	"Très rarement"
	3	"Environ une fois par an"
	4	"Environ une fois sur deux"
	5	"Chaque fois ou presque".
VALUE LABELS b185
	-7	"INAP"
	-2	"NR"
	1	"À 45 ans environ, je n'avais pas le droit de voter"
	2	"Je ne vais plus voter"
	3	"Moins qu'avant"
	4	"La même chose"
	5	"Plus régulièrement".
VALUE LABELS b186aa
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186ba
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186ab
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bb
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186ac
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bc
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186ad
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bd
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186ae
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186be
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186af
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bf
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186ag
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bg
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186ah
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bh
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186ai
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bi
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186aj
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bj
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186ak
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bk
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186al
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bl
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186am
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b186bm
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS b187a
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187b
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187c
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187d
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187e
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187f
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187g
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187h
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187i
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187j
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187k
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187l
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS b187m
	-7	"INAP"
	-2	"NR"
	1	"Moins"
	2	"Égal"
	3	"Plus".
VALUE LABELS tb188
	-7	"INAP"
	-2	"NR"
	0	"Sans religion"
	1	"Catholique"
	2	"Protestant"
	3	"Israélite"
	4	"Musulman"
	8	"Autre".
VALUE LABELS mb191
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS mb192a
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb192b
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS mb193
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Célibataire"
	2	"Marié‎/e, remarié/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS mb194
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb195
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS mb196
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb197
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb198
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb199
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Chez lui‎/elle"
	2	"Chez vous"
	3	"Dans une pension, home foyer"
	8	"Autre".
VALUE LABELS sb200
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS sb201
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS mb202
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS mb203a
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb203b
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS mb204
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Célibataire"
	2	"Marié‎/e, remarié/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS mb205
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb206
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS mb207
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb208
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb209
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb210
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Chez lui‎/elle"
	2	"Chez vous"
	3	"Dans une pension, home foyer"
	8	"Autre".
VALUE LABELS sb211
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS sb212
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS mb213
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS mb214a
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb214b
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS mb215
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Célibataire"
	2	"Marié‎/e, remarié/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS mb216
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb217
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS mb218
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb219
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb220
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb221
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Chez lui‎/elle"
	2	"Chez vous"
	3	"Dans une pension, home foyer"
	8	"Autre".
VALUE LABELS sb222
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS sb223
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS mb224
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS mb225a
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb225b
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS mb226
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Célibataire"
	2	"Marié‎/e, remarié/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS mb227
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb228
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS mb229
	-7	"INAP"
	-2	"NR"
	-1	"NSP".
VALUE LABELS sb230
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb231
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS sb232
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Chez lui‎/elle"
	2	"Chez vous"
	3	"Dans une pension, home foyer"
	8	"Autre".
VALUE LABELS sb233
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Même appartement‎/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement‎/villa séparé‎/e"
	3	"Même quartier‎/village (mais pas le même ensemble)"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS sb234
	-7	"INAP"
	-2	"NR"
	-1	"NSP"
	1	"Moins d'½ heure"
	2	"½ à 1 heure"
	3	"1 à 2 heures"
	4	"2 à 5 heures"
	5	"Plus de 5 heures".
VALUE LABELS mv1
	-2	"NR"
	10	"Agriculteurs exploitants"
	11	"Agriculteurs exploitants sur petite exploitation"
	12	"Agriculteurs exploitants sur moyenne exploitation"
	13	"Agriculteurs exploitants sur grande exploitation"
	20	"Artisans, commerçants et chefs d'entreprise"
	21	"Artisans"
	22	"Commerçants et assimilés"
	23	"Chefs d'entreprise de 10 salariés ou plus"
	30	"Cadres et professions intellectuelles supérieures"
	31	"Professions libérales"
	33	"Cadres de la fonction publique"
	34	"Professeurs, professions scientifiques"
	35	"Professions de l'information, des arts et des spectacles"
	37	"Cadres administratifs et commerciaux d'entreprise"
	38	"Ingénieurs et cadres techniques d'entreprise"
	40	"Professions intermédiaires"
	42	"Instituteurs et assimilés"
	43	"Professions intermédiaires de la santé et du travail social"
	44	"Clergé, religieux"
	45	"Professions intermédiaires administratives de la fonction publique"
	46	"Professions intermédiaires administratives et commerciales d'entreprise"
	47	"Techniciens (sauf techniciens tertiaires)"
	48	"Contremaîtres, agents de maîtrise (maîtrise administrative exclue)"
	50	"Employés"
	52	"Employés civils et agents de service de la fonction publique"
	53	"Policiers et militaires"
	54	"Employés administratifs d'entreprise"
	55	"Employés de commerce"
	56	"Personnels des services directs aux particuliers"
	60	"Ouvriers"
	62	"Ouvriers qualifiés de type industriel"
	63	"Ouvriers qualifiés de type artisanal"
	64	"Chauffeurs"
	65	"Ouvriers qualifiés de la manutention, du magasinage et des transports"
	67	"Ouvriers non qualifiés de type industriel"
	68	"Ouvriers non qualifiés de type artisanal"
	69	"Ouvriers agricoles et assimilés"
	70	"Population inactive"
	71	"Anciens agriculteurs"
	72	"Anciens artisans, commerçants, chefs d'entreprise"
	80	"Autres".
VALUE LABELS tv2
	-2	"NR"
	1	"Primaire"
	2	"Secondaire inférieur (cycle d'orientation)"
	3	"Professionnelle (pour apprentis)"
	4	"Secondaire supérieure (collège, école de commerce, gymnase, etc.)"
	5	"École technique supérieur"
	6	"École professionnelle supérieure (ex: infirmières, assistants sociaux, maîtres d'école, etc.)"
	7	"Université, école polytechnique fédérale".
VALUE LABELS mv3a
	-2	"NR"
	1	"Oui".
VALUE LABELS mv3b
	-2	"NR"
	1	"Oui".
VALUE LABELS mv3c
	-2	"NR"
	1	"Oui".
VALUE LABELS mv3d
	-2	"NR"
	1	"Oui".
VALUE LABELS mv3e
	-2	"NR"
	1	"Oui".
VALUE LABELS mv4
	-2	"NR".
VALUE LABELS mv5
	-2	"NR"
	10	"Agriculteurs exploitants"
	11	"Agriculteurs exploitants sur petite exploitation"
	12	"Agriculteurs exploitants sur moyenne exploitation"
	13	"Agriculteurs exploitants sur grande exploitation"
	20	"Artisans, commerçants et chefs d'entreprise"
	21	"Artisans"
	22	"Commerçants et assimilés"
	23	"Chefs d'entreprise de 10 salariés ou plus"
	30	"Cadres et professions intellectuelles supérieures"
	31	"Professions libérales"
	33	"Cadres de la fonction publique"
	34	"Professeurs, professions scientifiques"
	35	"Professions de l'information, des arts et des spectacles"
	37	"Cadres administratifs et commerciaux d'entreprise"
	38	"Ingénieurs et cadres techniques d'entreprise"
	40	"Professions intermédiaires"
	42	"Instituteurs et assimilés"
	43	"Professions intermédiaires de la santé et du travail social"
	44	"Clergé, religieux"
	45	"Professions intermédiaires administratives de la fonction publique"
	46	"Professions intermédiaires administratives et commerciales d'entreprise"
	47	"Techniciens (sauf techniciens tertiaires)"
	48	"Contremaîtres, agents de maîtrise (maîtrise administrative exclue)"
	50	"Employés"
	52	"Employés civils et agents de service de la fonction publique"
	53	"Policiers et militaires"
	54	"Employés administratifs d'entreprise"
	55	"Employés de commerce"
	56	"Personnels des services directs aux particuliers"
	60	"Ouvriers"
	62	"Ouvriers qualifiés de type industriel"
	63	"Ouvriers qualifiés de type artisanal"
	64	"Chauffeurs"
	65	"Ouvriers qualifiés de la manutention, du magasinage et des transports"
	67	"Ouvriers non qualifiés de type industriel"
	68	"Ouvriers non qualifiés de type artisanal"
	69	"Ouvriers agricoles et assimilés"
	70	"Population inactive"
	71	"Anciens agriculteurs"
	72	"Anciens artisans, commerçants, chefs d'entreprise"
	80	"Autres".
VALUE LABELS v6
	-9	"NV"
	-7	"INAP"
	-2	"NR"
	84	"Dällikon"
	102	"Weiach"
	114	"Fischenthal"
	121	"Wetzikon (ZH)"
	133	"Horgen"
	135	"Kilchberg (ZH)"
	141	"Thalwil"
	154	"Küsnacht (ZH)"
	191	"Dübendorf"
	230	"Winterthur"
	253	"Zürich"
	306	"Lyss"
	351	"Bern"
	355	"Köniz"
	356	"Muri bei Bern"
	371	"Biel‎/Bienne"
	418	"Oberburg"
	433	"Cortébert"
	438	"Orvin"
	443	"Saint-Imier"
	446	"Tramelan"
	556	"Zielebach"
	576	"Grindelwald"
	581	"Interlaken"
	619	"Oberdiessbach"
	662	"Ferenbalm"
	667	"Laupen"
	682	"Bévilard"
	687	"Corcelles (BE)"
	690	"Court"
	699	"Monible"
	700	"Moutier"
	703	"Reconvilier"
	723	"La Neuveville"
	784	"Innertkirchen"
	843	"Saanen"
	853	"Rüschegg"
	902	"Langnau im Emmental"
	905	"Rüderswil"
	929	"Hilterfingen"
	1003	"Escholzmatt"
	1061	"Luzern"
	1149	"Willisau Stadt"
	1301	"Einsiedeln"
	1402	"Engelberg"
	1509	"Stans"
	1618	"Mühlehorn"
	1709	"Unterägeri"
	1711	"Zug"
	2001	"Aumont"
	2011	"Cugy (FR)"
	2035	"Nuvilly"
	2046	"Vesin"
	2080	"Hennens"
	2096	"Romont (FR)"
	2102	"Ursy"
	2113	"Vuisternens-devant-Romont"
	2121	"Haut-Intyamon"
	2124	"Broc"
	2125	"Bulle"
	2135	"Gruyères"
	2147	"Pont-la-Ville"
	2175	"Belfaux"
	2184	"Corpataux"
	2196	"Fribourg"
	2210	"Montévraz"
	2217	"Ponthaux"
	2220	"Le Mouret"
	2254	"Courtepin"
	2265	"Kerzers"
	2293	"Düdingen"
	2296	"Heitenried"
	2299	"Plaffeien"
	2323	"Bossonnens"
	2325	"Châtel-Saint-Denis"
	2326	"Le Crêt"
	2336	"Semsales"
	2546	"Grenchen"
	2581	"Olten"
	2582	"Rickenbach (SO)"
	2601	"Solothurn"
	2701	"Basel"
	2802	"Allschwil"
	2825	"Füllinsdorf"
	2829	"Liestal"
	2888	"Langenbruck"
	2937	"Neuhausen am Rheinfall"
	2964	"Stein am Rhein"
	3001	"Herisau"
	3021	"Bühler"
	3111	"Oberegg"
	3203	"St. Gallen"
	3215	"Rorschach"
	3231	"Au (SG)"
	3233	"Berneck"
	3235	"Rheineck"
	3296	"Sargans"
	3392	"Kirchberg (SG)"
	3393	"Lütisburg"
	3406	"Mogelsberg"
	3561	"Poschiavo"
	3787	"St. Moritz"
	3832	"Grono"
	3901	"Chur"
	3952	"Jenins"
	4021	"Baden"
	4063	"Bremgarten (AG)"
	4082	"Wohlen (AG)"
	4172	"Münchwilen (AG)"
	4258	"Rheinfelden"
	4315	"Rekingen (AG)"
	4401	"Arbon"
	4405	"<none>"
	4461	"Amriswil"
	4545	"Diessenhofen"
	4641	"Altnau"
	4941	"Märstetten"
	5002	"Bellinzona"
	5040	"Ludiano"
	5192	"Lugano"
	5210	"Paradiso"
	5222	"Sessa"
	5401	"Aigle"
	5402	"Bex"
	5405	"Gryon"
	5406	"Lavey-Morcles"
	5408	"Noville"
	5409	"Ollon"
	5410	"Ormont-Dessous"
	5411	"Ormont-Dessus"
	5412	"Rennaz"
	5422	"Aubonne"
	5434	"Saint-George"
	5455	"Constantine"
	5456	"<none>"
	5477	"Cossonay"
	5484	"Gollion"
	5492	"Montricher"
	5495	"Penthalaz"
	5524	"Goumoens-la-Ville"
	5531	"Penthéréaz"
	5554	"Concise"
	5561	"Grandson"
	5568	"Sainte-Croix"
	5586	"Lausanne"
	5590	"Pully"
	5591	"Renens (VD)"
	5601	"Chexbres"
	5602	"Cully"
	5604	"Forel (Lavaux)"
	5605	"Grandvaux"
	5607	"Puidoux"
	5621	"Aclens"
	5633	"Echandens"
	5635	"Ecublens (VD)"
	5642	"Morges"
	5688	"Syens"
	5703	"Bassins"
	5711	"Commugny"
	5712	"Coppet"
	5713	"Crans-près-Céligny"
	5714	"Crassier"
	5718	"Genolier"
	5721	"Gland"
	5722	"Grens"
	5723	"Mies"
	5724	"Nyon"
	5725	"Prangins"
	5727	"Saint-Cergue"
	5743	"Arnex-sur-Orbe"
	5744	"Ballaigues"
	5745	"Baulmes"
	5750	"Les Clées"
	5754	"Juriens"
	5757	"Orbe"
	5764	"Vallorbe"
	5765	"Vaulion"
	5789	"Ferlens (VD)"
	5792	"Montpreveyres"
	5793	"Oron-la-Ville"
	5816	"Corcelles-près-Payerne"
	5841	"Château-d'Oex"
	5853	"Bursins"
	5854	"Burtigny"
	5857	"Gilly"
	5859	"Mont-sur-Rolle"
	5861	"Rolle"
	5881	"Blonay"
	5886	"Montreux"
	5889	"La Tour-de-Peilz"
	5890	"Vevey"
	5928	"Rovray"
	5934	"Valeyres-sous-Ursins"
	5938	"Yverdon-les-Bains"
	6002	"Brig-Glis"
	6006	"Mund"
	6007	"Naters"
	6008	"Ried-Brig"
	6009	"Simplon"
	6021	"Ardon"
	6022	"Chamoson"
	6023	"Conthey"
	6024	"Nendaz"
	6025	"Vétroz"
	6028	"-- code à corriger --"
	6031	"Bagnes"
	6032	"Bourg-Saint-Pierre"
	6034	"Orsières"
	6053	"Biel (VS)"
	6081	"Les Agettes"
	6082	"Ayent"
	6083	"Evolène"
	6084	"Hérémence"
	6085	"Mase"
	6086	"Nax"
	6087	"Saint-Martin (VS)"
	6088	"Vernamiège"
	6089	"Vex"
	6101	"Agarn"
	6105	"Erschmatt"
	6106	"Feschel"
	6110	"Leuk"
	6111	"Leukerbad"
	6113	"Salgesch"
	6116	"Varen"
	6133	"Fully"
	6134	"Isérables"
	6135	"Leytron"
	6136	"Martigny"
	6139	"Riddes"
	6140	"Saillon"
	6141	"Saxon"
	6142	"Trient"
	6151	"Champéry"
	6152	"Collombey-Muraz"
	6153	"Monthey"
	6155	"Saint-Gingolph"
	6156	"Troistorrents"
	6159	"Vouvry"
	6173	"Bitsch"
	6175	"Goppisberg"
	6179	"Mörel"
	6191	"Ausserberg"
	6195	"Ferden"
	6200	"Steg"
	6213	"Evionnaz"
	6217	"Saint-Maurice"
	6219	"Vernayaz"
	6231	"Ayer"
	6232	"Chalais"
	6233	"Chandolin"
	6234	"Chermignon"
	6235	"Chippis"
	6237	"Grimentz"
	6238	"Grône"
	6239	"Icogne"
	6240	"Lens"
	6241	"Miège"
	6242	"Mollens (VS)"
	6243	"Montana"
	6244	"Randogne"
	6245	"Saint-Jean"
	6246	"Saint-Léonard"
	6247	"Saint-Luc"
	6248	"Sierre"
	6249	"Venthône"
	6250	"Veyras"
	6251	"Vissoie"
	6261	"Arbaz"
	6263	"Grimisuat"
	6264	"Salins"
	6265	"Savièse"
	6266	"Sion"
	6267	"Veysonnaz"
	6285	"Grächen"
	6293	"Stalden (VS)"
	6297	"Visp"
	6402	"Bevaix"
	6407	"Corcelles-Cormondrèche"
	6408	"Cortaillod"
	6413	"Rochefort"
	6421	"La Chaux-de-Fonds"
	6423	"La Sagne"
	6431	"Les Brenets"
	6436	"Le Locle"
	6455	"Le Landeron"
	6458	"Neuchâtel"
	6459	"Saint-Blaise"
	6475	"Dombresson"
	6505	"Couvet"
	6506	"Fleurier"
	6511	"Les Verrières"
	6601	"Aire-la-Ville"
	6602	"Anières"
	6603	"Avully"
	6604	"Avusy"
	6606	"Bellevue"
	6607	"Bernex"
	6608	"Carouge (GE)"
	6609	"Cartigny"
	6610	"Céligny"
	6612	"Chêne-Bougeries"
	6613	"Chêne-Bourg"
	6614	"Choulex"
	6615	"Collex-Bossy"
	6616	"Collonge-Bellerive"
	6617	"Cologny"
	6618	"Confignon"
	6619	"Corsier (GE)"
	6620	"Dardagny"
	6621	"Genève"
	6622	"Genthod"
	6623	"Le Grand-Saconnex"
	6625	"Hermance"
	6626	"Jussy"
	6627	"Laconnex"
	6628	"Lancy"
	6629	"Meinier"
	6630	"Meyrin"
	6631	"Onex"
	6632	"Perly-Certoux"
	6633	"Plan-les-Ouates"
	6634	"Pregny-Chambésy"
	6635	"Presinge"
	6638	"Satigny"
	6639	"Soral"
	6640	"Thônex"
	6641	"Troinex"
	6642	"Vandoeuvres"
	6643	"Vernier"
	6644	"Versoix"
	6645	"Veyrier"
	6707	"Courfaivre"
	6708	"Courrendlin"
	6711	"Delémont"
	6727	"Vicques"
	6757	"Saignelégier"
	6775	"Bonfol"
	6782	"Cornol"
	6783	"Courchavon"
	6800	"Porrentruy"
	6804	"Saint-Ursanne"
	7101	"Albanie"
	7102	"Belgique"
	7103	"Bulgarie"
	7105	"Allemagne"
	7107	"France"
	7108	"Royaume-Uni"
	7111	"Italie"
	7112	"Yougoslavie"
	7115	"Pays-Bas"
	7117	"Autriche"
	7118	"Pologne"
	7119	"Portugal"
	7120	"Roumanie"
	7121	"Suède"
	7122	"Suisse"
	7123	"Espagne"
	7124	"Tchécoslovaquie"
	7127	"Hongrie"
	7129	"Grèce"
	7130	"Monaco"
	7140	"Russie"
	7141	"Ukraine"
	7150	"Ain, Haute-Savoie"
	7209	"Inde"
	7210	"Indonésie"
	7212	"Iran"
	7213	"Israël"
	7224	"Liban"
	7231	"Philippines"
	7236	"Thaïlande"
	7300	"Afrique"
	7301	"Egypte"
	7302	"Algérie"
	7312	"Djibouti"
	7326	"Libye"
	7330	"Maroc"
	7340	"Sénégal"
	7344	"Afrique du Sud"
	7351	"Tunisie"
	7401	"Argentine"
	7410	"Equateur"
	7418	"Canada"
	7420	"Cuba"
	7421	"Mexique"
	7425	"Pérou"
	7432	"Etats-Unis"
	7691	"Nouvelle-Calédonie"
	7801	"Canton de Zurich"
	7802	"Canton de Berne"
	7803	"Canton de Lucerne"
	7810	"Canton de Fribourg"
	7811	"Canton de Soleure"
	7812	"Canton de Bâle-Ville"
	7813	"Canton de Bâle-Campagne"
	7814	"Canton de Schaffhouse"
	7817	"Canton se Saint-Gall"
	7818	"Canton des Grisons"
	7819	"Canton d'Argovie"
	7820	"Canton de Thurgovie"
	7821	"Canton du Tessin"
	7822	"Canton de Vaud"
	7823	"Canton du Valais"
	7824	"Canton de Neuchâtel"
	7825	"Canton de Genève"
	7826	"Canton du Jura"
	8000	"Autre".
VALUE LABELS sv7
	-2	"NR"
	1	"Français"
	2	"Allemand, suisse-allemand"
	3	"Italien"
	4	"Espagnol"
	5	"Anglais"
	6	"Langues slaves"
	7	"Patois"
	8	"Autre".
VALUE LABELS v8
	-7	"INAP"
	-2	"NR"
	1	"Exerçait un emploi (métier, profession)"
	2	"S'occupait du foyer, des enfants"
	3	"Autre".
VALUE LABELS v9
	-7	"INAP"
	-2	"NR"
	1	"Plein temps"
	2	"Trois quarts temps environ"
	3	"Mi-temps environ".
VALUE LABELS v10
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v11
	-7	"INAP"
	-2	"NR"
	1	"Mi-temps environ"
	2	"Quart temps environ"
	3	"Quelques heures".
VALUE LABELS v12
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v13
	-7	"INAP"
	-2	"NR"
	1	"Plein-temps"
	2	"Trois quarts temps environ"
	3	"Mi-temps environ"
	4	"Quart temps environ"
	5	"Quelques heures".
VALUE LABELS v14
	-7	"INAP"
	-2	"NR"
	1	"Exerçait un emploi (métier, profession)"
	2	"S'occupait du foyer, des enfants"
	3	"Problèmes de santé ‎(maladie, AI...)"
	4	"Autre (formation, retraite...)".
VALUE LABELS v15
	-7	"INAP"
	-2	"NR"
	1	"Plein temps"
	2	"Trois quarts temps environ"
	3	"Mi-temps environ".
VALUE LABELS v16
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v17
	-7	"INAP"
	-2	"NR"
	1	"Mi-temps environ"
	2	"Quart temps environ"
	3	"Quelques heures".
VALUE LABELS v18
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v19
	-7	"INAP"
	-2	"NR"
	1	"Plein-temps"
	2	"Trois quarts temps environ"
	3	"Mi-temps environ"
	4	"Quart temps environ"
	5	"Quelques heures".
VALUE LABELS sv20
	-7	"INAP"
	-2	"NR"
	1	"Retraité‎/e"
	2	"Exerce une profession (emploi, travail)"
	3	"Au chômage"
	4	"Personne au foyer ('ménager‎/ère')"
	5	"Assurance invalidité".
VALUE LABELS v21
	-7	"INAP"
	-2	"NR"
	1	"Plein temps"
	2	"Trois quarts temps environ"
	3	"Mi-temps environ".
VALUE LABELS sv22
	-7	"INAP"
	-2	"NR"
	10	"Agriculteurs exploitants"
	11	"Agriculteurs exploitants sur petite exploitation"
	12	"Agriculteurs exploitants sur moyenne exploitation"
	13	"Agriculteurs exploitants sur grande exploitation"
	20	"Artisans, commerçants et chefs d'entreprise"
	21	"Artisans"
	22	"Commerçants et assimilés"
	23	"Chefs d'entreprise de 10 salariés ou plus"
	30	"Cadres et professions intellectuelles supérieures"
	31	"Professions libérales"
	33	"Cadres de la fonction publique"
	34	"Professeurs, professions scientifiques"
	35	"Professions de l'information, des arts et des spectacles"
	37	"Cadres administratifs et commerciaux d'entreprise"
	38	"Ingénieurs et cadres techniques d'entreprise"
	40	"Professions intermédiaires"
	42	"Instituteurs et assimilés"
	43	"Professions intermédiaires de la santé et du travail social"
	44	"Clergé, religieux"
	45	"Professions intermédiaires administratives de la fonction publique"
	46	"Professions intermédiaires administratives et commerciales d'entreprise"
	47	"Techniciens (sauf techniciens tertiaires)"
	48	"Contremaîtres, agents de maîtrise (maîtrise administrative exclue)"
	50	"Employés"
	52	"Employés civils et agents de service de la fonction publique"
	53	"Policiers et militaires"
	54	"Employés administratifs d'entreprise"
	55	"Employés de commerce"
	56	"Personnels des services directs aux particuliers"
	60	"Ouvriers"
	62	"Ouvriers qualifiés de type industriel"
	63	"Ouvriers qualifiés de type artisanal"
	64	"Chauffeurs"
	65	"Ouvriers qualifiés de la manutention, du magasinage et des transports"
	67	"Ouvriers non qualifiés de type industriel"
	68	"Ouvriers non qualifiés de type artisanal"
	69	"Ouvriers agricoles et assimilés"
	70	"Population inactive"
	71	"Anciens agriculteurs"
	72	"Anciens artisans, commerçants, chefs d'entreprise"
	80	"Autres".
VALUE LABELS sv23
	-7	"INAP"
	-2	"NR"
	1	"Plein temps"
	2	"Trois quarts temps environ"
	3	"Mi-temps environ".
VALUE LABELS sv24
	-7	"INAP"
	-2	"NR"
	1	"Indépendant"
	2	"Salarié dans une entreprise privée"
	3	"Salarié à l'État ou dans une entreprise de l'État".
VALUE LABELS sv25
	-7	"INAP"
	-2	"NR"
	1	"Dans l'agriculture"
	2	"Dans la production et la construction"
	3	"Dans les services aux entreprises et la distribution"
	4	"Dans les services sociaux et personnels".
VALUE LABELS sv26
	-7	"INAP"
	-2	"NR"
	1	"Travaillait seul ou aucun subordonné"
	2	"Moins de 5"
	3	"De 6 à 20"
	4	"De 21 à 50"
	5	"Plus de 50".
VALUE LABELS v27a
	-7	"INAP"
	-2	"NR"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS v27b
	-7	"INAP"
	-2	"NR"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS v27c
	-7	"INAP"
	-2	"NR"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS v27d
	-7	"INAP"
	-2	"NR"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS v27e
	-7	"INAP"
	-2	"NR"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS v27f
	-7	"INAP"
	-2	"NR"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS v28a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v28b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v29
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, occasionnellement"
	3	"Oui, en saison"
	4	"Oui, toute l'année".
VALUE LABELS v30
	-7	"INAP"
	-2	"NR"
	10	"Agriculteurs exploitants"
	11	"Agriculteurs exploitants sur petite exploitation"
	12	"Agriculteurs exploitants sur moyenne exploitation"
	13	"Agriculteurs exploitants sur grande exploitation"
	20	"Artisans, commerçants et chefs d'entreprise"
	21	"Artisans"
	22	"Commerçants et assimilés"
	23	"Chefs d'entreprise de 10 salariés ou plus"
	30	"Cadres et professions intellectuelles supérieures"
	31	"Professions libérales"
	33	"Cadres de la fonction publique"
	34	"Professeurs, professions scientifiques"
	35	"Professions de l'information, des arts et des spectacles"
	37	"Cadres administratifs et commerciaux d'entreprise"
	38	"Ingénieurs et cadres techniques d'entreprise"
	40	"Professions intermédiaires"
	42	"Instituteurs et assimilés"
	43	"Professions intermédiaires de la santé et du travail social"
	44	"Clergé, religieux"
	45	"Professions intermédiaires administratives de la fonction publique"
	46	"Professions intermédiaires administratives et commerciales d'entreprise"
	47	"Techniciens (sauf techniciens tertiaires)"
	48	"Contremaîtres, agents de maîtrise (maîtrise administrative exclue)"
	50	"Employés"
	52	"Employés civils et agents de service de la fonction publique"
	53	"Policiers et militaires"
	54	"Employés administratifs d'entreprise"
	55	"Employés de commerce"
	56	"Personnels des services directs aux particuliers"
	60	"Ouvriers"
	62	"Ouvriers qualifiés de type industriel"
	63	"Ouvriers qualifiés de type artisanal"
	64	"Chauffeurs"
	65	"Ouvriers qualifiés de la manutention, du magasinage et des transports"
	67	"Ouvriers non qualifiés de type industriel"
	68	"Ouvriers non qualifiés de type artisanal"
	69	"Ouvriers agricoles et assimilés"
	70	"Population inactive"
	71	"Anciens agriculteurs"
	72	"Anciens artisans, commerçants, chefs d'entreprise"
	80	"Autres".
VALUE LABELS v31
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v32a
	-7	"INAP"
	-2	"NR"
	1	"Améliorée"
	2	"Restée la même"
	3	"Détériorée".
VALUE LABELS v32b
	-7	"INAP"
	-2	"NR"
	1	"Plus varié"
	2	"Resté le même"
	3	"Plus monotone".
VALUE LABELS v32c
	-7	"INAP"
	-2	"NR"
	1	"Moins pénible"
	2	"Resté le même"
	3	"Plus pénible".
VALUE LABELS v32d
	-7	"INAP"
	-2	"NR"
	1	"Améliorée"
	2	"Restée la même"
	3	"Détériorée".
VALUE LABELS v32e
	-7	"INAP"
	-2	"NR"
	1	"Améliorée"
	2	"Restée la même"
	3	"Détériorée".
VALUE LABELS mv33
	-2	"NR"
	1	"Très satisfait"
	2	"Assez satisfait"
	3	"Plutôt pas satisfait"
	4	"Pas du tout satisfait".
VALUE LABELS v34
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v35
	-7	"INAP"
	-2	"NR"
	1	"Mi-temps environ"
	2	"Quart temps environ"
	3	"Quelques heures".
VALUE LABELS v36a
	-2	"NR"
	1	"Oui, l'âge de la retraite est de ... ans"
	2	"Non, l'âge légal est l'âge AVS"
	3	"Non, il n'y a pas d'âge officiel".
VALUE LABELS v36b
	-7	"INAP"
	-2	"NR".
VALUE LABELS v37
	-7	"INAP"
	-2	"NR"
	0	"Non, pas encore"
	1	"Oui, je l'ai atteint et dépassé".
VALUE LABELS mv38a
	-7	"INAP"
	-2	"NR"
	1	"Âge légal"
	2	"Retraite repoussée"
	3	"Retraite anticipée".
VALUE LABELS mv38b
	-7	"INAP"
	-2	"NR".
VALUE LABELS mv39a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv39b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv39c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv39d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv39e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv39f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv40a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv40b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv40c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv40d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv40e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv40f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv40g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv40h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v41a
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui"
	3	"Non pertinent".
VALUE LABELS v41b
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui"
	3	"Non pertinent".
VALUE LABELS v41c
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui"
	3	"Non pertinent".
VALUE LABELS v42
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, à mi-temps environ"
	3	"Oui, à quart temps environ"
	4	"Oui, quelques heures par semaine"
	5	"Oui, en saison ou à l'occasion".
VALUE LABELS v43
	-7	"INAP"
	-2	"NR"
	1	"Oui, régulièrement"
	2	"Oui, à l'occasion"
	3	"Par hasard"
	4	"Non, pas du tout".
VALUE LABELS v44a
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v44b
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v44c
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v44d
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v44e
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v44f
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v44g
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v45a
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v45b
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v45c
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v45d
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v45e
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v45f
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v45g
	-7	"INAP"
	-2	"NR"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS v46
	-7	"INAP"
	-2	"NR"
	1	"Plus tôt que vous ne l'avez fait"
	2	"Au même moment que vous l'avez fait"
	3	"Plus tard que vous l'avez fait"
	4	"Je continuerais encore à travailler aujoud'hui".
VALUE LABELS v47a
	-7	"INAP"
	-2	"NR"
	1	"Âge légal"
	2	"Retraite repoussée"
	3	"Retraite anticipée"
	4	"Je ne sais pas".
VALUE LABELS v47b
	-7	"INAP"
	-2	"NR".
VALUE LABELS v48a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v48b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v48c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v48d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v48e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v48f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v48g
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v49a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v49b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v49c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v49d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v49e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v49f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v50a
	-7	"INAP"
	-2	"NR"
	1	"À l'âge normal (AVS‎ / légal)"
	2	"Tout de suite"
	3	"Avant l'âge légal"
	4	"Après l'âge légal"
	5	"Le plus tard possible (jamais)".
VALUE LABELS v50b
	-7	"INAP"
	-2	"NR".
VALUE LABELS v51a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v51b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v51c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v52a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v52b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v52c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v52d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v52e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v52f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v53
	-7	"INAP"
	-2	"NR"
	1	"Dans moins d'un an"
	2	"D'ici 1 à 3 ans"
	3	"D'ici 4 à 5 ans"
	4	"D'ici plus de 5 ans"
	9	"Ne sait pas".
VALUE LABELS v54a
	-7	"INAP"
	-2	"NR"
	1	"Vous prendriez la retraite comme vous prévoyiez de le faire"
	2	"Vous la prendriez -ou l'auriez prise plus tôt"
	3	"Vous la prendriez plus tard (le plus tard possible)".
VALUE LABELS v54b
	-7	"INAP"
	-2	"NR".
VALUE LABELS v55
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Je l'espère, mais j'ai peu de chances"
	3	"Oui, j'ai de bonnes chances".
VALUE LABELS v56a
	-7	"INAP"
	-2	"NR"
	1	"À l'âge normal (AVS ‎/ légal)"
	2	"Avant l'âge normal"
	3	"Je n'aurai pas d'autres retraites que l'AVS".
VALUE LABELS v56b
	-7	"INAP"
	-2	"NR".
VALUE LABELS v57
	-7	"INAP"
	-2	"NR"
	1	"Oui, beaucoup"
	2	"Oui, assez"
	3	"Non, pour ainsi dire pas"
	4	"Je n'avais pas de caisse de retraite"
	9	"Je ne sais pas".
VALUE LABELS v58a
	-7	"INAP"
	-2	"NR"
	1	"À l'âge légal (AVS)"
	2	"Tout de suite"
	3	"Avant l'âge légal"
	4	"Après l'âge légal"
	5	"Le plus tard possible (jamais)".
VALUE LABELS v58b
	-7	"INAP"
	-2	"NR".
VALUE LABELS sv59
	-7	"INAP"
	-2	"NR"
	1	"Retraité‎/e"
	2	"Exerce une profession (emploi, travail)"
	3	"Au chômage"
	4	"Personne au foyer (ménager‎/ère)"
	5	"À l'assurance invalidité".
VALUE LABELS sv60
	-7	"INAP"
	-2	"NR"
	10	"Agriculteurs exploitants"
	11	"Agriculteurs exploitants sur petite exploitation"
	12	"Agriculteurs exploitants sur moyenne exploitation"
	13	"Agriculteurs exploitants sur grande exploitation"
	20	"Artisans, commerçants et chefs d'entreprise"
	21	"Artisans"
	22	"Commerçants et assimilés"
	23	"Chefs d'entreprise de 10 salariés ou plus"
	30	"Cadres et professions intellectuelles supérieures"
	31	"Professions libérales"
	33	"Cadres de la fonction publique"
	34	"Professeurs, professions scientifiques"
	35	"Professions de l'information, des arts et des spectacles"
	37	"Cadres administratifs et commerciaux d'entreprise"
	38	"Ingénieurs et cadres techniques d'entreprise"
	40	"Professions intermédiaires"
	42	"Instituteurs et assimilés"
	43	"Professions intermédiaires de la santé et du travail social"
	44	"Clergé, religieux"
	45	"Professions intermédiaires administratives de la fonction publique"
	46	"Professions intermédiaires administratives et commerciales d'entreprise"
	47	"Techniciens (sauf techniciens tertiaires)"
	48	"Contremaîtres, agents de maîtrise (maîtrise administrative exclue)"
	50	"Employés"
	52	"Employés civils et agents de service de la fonction publique"
	53	"Policiers et militaires"
	54	"Employés administratifs d'entreprise"
	55	"Employés de commerce"
	56	"Personnels des services directs aux particuliers"
	60	"Ouvriers"
	62	"Ouvriers qualifiés de type industriel"
	63	"Ouvriers qualifiés de type artisanal"
	64	"Chauffeurs"
	65	"Ouvriers qualifiés de la manutention, du magasinage et des transports"
	67	"Ouvriers non qualifiés de type industriel"
	68	"Ouvriers non qualifiés de type artisanal"
	69	"Ouvriers agricoles et assimilés"
	70	"Population inactive"
	71	"Anciens agriculteurs"
	72	"Anciens artisans, commerçants, chefs d'entreprise"
	80	"Autres".
VALUE LABELS sv61
	-7	"INAP"
	-2	"NR"
	1	"Plein temps"
	2	"Trois quarts temps environ"
	3	"Mi-temps temps environ".
VALUE LABELS sv62
	-7	"INAP"
	-2	"NR"
	1	"Indépendant"
	2	"Salarié dans une entreprise privée"
	3	"Salarié à l'État ou dans une entreprise de l'État".
VALUE LABELS sv63
	-7	"INAP"
	-2	"NR"
	1	"L'agriculture"
	2	"La production et construction"
	3	"Les services aux entreprises et distribution"
	4	"Les services sociaux et personnels".
VALUE LABELS sv64
	-7	"INAP"
	-2	"NR"
	1	"Travaillait seul ou aucun subordonné"
	2	"Moins de 5"
	3	"De 6 à 20"
	4	"De 21 à 50"
	5	"Plus de 50".
VALUE LABELS sv65a
	-7	"INAP"
	-2	"NR"
	1	"Âge légal"
	2	"Retraite repoussée"
	3	"Retraite anticipée".
VALUE LABELS sv65b
	-7	"INAP"
	-2	"NR".
VALUE LABELS v66a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v66b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v66c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v66d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v66e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v66f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v67a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v67b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v67c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v67d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v67e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v67f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v67g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v67h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v68
	-7	"INAP"
	-2	"NR"
	1	"Plus tôt qu'il ne l'a fait"
	2	"Au même moment qu'il l'a fait"
	3	"Plus tard qu'il l'a fait"
	4	"Il continuerait encore à travailler aujourd'hui".
VALUE LABELS v69
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, à mi-temps environ"
	3	"Oui, à quart temps environ"
	4	"Oui, quelques heures par semaine"
	5	"Oui, à l'occasion".
VALUE LABELS v70a
	-7	"INAP"
	-2	"NR"
	1	"Positivement"
	2	"Plutôt positivement"
	3	"Plutôt négativement"
	4	"Négativement".
VALUE LABELS v70b
	-7	"INAP"
	-2	"NR"
	1	"Positivement"
	2	"Plutôt positivement"
	3	"Plutôt négativement"
	4	"Négativement".
VALUE LABELS v70c
	-7	"INAP"
	-2	"NR"
	1	"Positivement"
	2	"Plutôt positivement"
	3	"Plutôt négativement"
	4	"Négativement".
VALUE LABELS v70d
	-7	"INAP"
	-2	"NR"
	1	"Positivement"
	2	"Plutôt positivement"
	3	"Plutôt négativement"
	4	"Négativement".
VALUE LABELS v71
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v72a
	-7	"INAP"
	-2	"NR"
	1	"Âge légal"
	2	"Retraite repoussée"
	3	"Retraite anticipée"
	4	"Il ne sait pas".
VALUE LABELS v72b
	-7	"INAP"
	-2	"NR".
VALUE LABELS v73a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v73b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v73c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v73d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v73e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v73f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v73g
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v74a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v74b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v74c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v74d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v74e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v74f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v75a
	-7	"INAP"
	-2	"NR"
	1	"À l'âge normal (AVS ‎/ légal)"
	2	"Tout de suite"
	3	"Avant l'âge légal"
	4	"Après âge normal"
	5	"Le plus tard possible (jamais)".
VALUE LABELS v75b
	-7	"INAP"
	-2	"NR".
VALUE LABELS v76a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v76b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v76c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v76d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v76e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v76f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v77
	-7	"INAP"
	-2	"NR"
	1	"Dans moins d'un an"
	2	"D'ici 1 à 3 ans"
	3	"D'ici 4 à 5 ans"
	4	"D'ici plus de 5 ans"
	8	"Il/elle ne sait pas"
	9	"Je ne sais pas".
VALUE LABELS v78a
	-7	"INAP"
	-2	"NR"
	1	"Prendrait la retraite comme il prévoit de le faire"
	2	"Il la prendrait – ou l'aurait prise – plus tôt"
	3	"Il la prendrait plus tard (le plus tard possible)".
VALUE LABELS v78b
	-7	"INAP"
	-2	"NR".
VALUE LABELS v79
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v80
	-7	"INAP"
	-2	"NR"
	1	"Mi-temps environ"
	2	"Quart temps environ"
	3	"Quelques heures".
VALUE LABELS mv81
	-7	"INAP"
	-2	"NR"
	1	"Exerçait une profession à plein temps"
	2	"Exerçait une profession à  temps partiel"
	3	"Était etraité"
	4	"Était à l'AI (assurance invalidité)"
	5	"S'occupait du oyer".
VALUE LABELS mv82
	-7	"INAP"
	-2	"NR".
VALUE LABELS mv83
	-7	"INAP"
	-2	"NR"
	1	"Totale"
	2	"Partielle".
VALUE LABELS mv84
	-7	"INAP"
	-2	"NR"
	1	"D'un accident"
	2	"D'une maladie".
VALUE LABELS mv85
	-7	"INAP"
	-2	"NR"
	1	"Repoussé l'âge de la retraite"
	2	"Pris sa retraite à l'âge légal"
	3	"Pris sa retraite plus tôt"
	4	"Autres cas".
VALUE LABELS v86a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v86b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v86c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v86d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v86e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v86f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v86g
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v87a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v87b
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v87c
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v87d
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v87e
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v87f
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS v87g
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv88
	-7	"INAP"
	-2	"NR"
	1	"Décès subit, inattendu (suite à un accident, une courte maladie)"
	2	"Décès attendu, précédé d'une longue maladie".
VALUE LABELS mv89
	-7	"INAP"
	-2	"NR"
	1	"Pas du tout; tout au plus une maladie de courte durée"
	2	"Malade depuis 2-3 mois"
	3	"Malade depuis 4-6 mois"
	4	"Malade depuis plus de 6 mois".
VALUE LABELS mv90
	-7	"INAP"
	-2	"NR"
	1	"Principalement à la maison"
	2	"Principalement à l'hôpital"
	3	"Les deux en alternance".
VALUE LABELS mv91
	-7	"INAP"
	-2	"NR"
	0	"Non, pas beaucoup de travail"
	1	"Oui, beaucoup de travail".
VALUE LABELS mv92a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS uv92b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv92c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv92d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv92e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv92f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv92g
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv92h
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv93a
	-7	"INAP"
	-2	"NR"
	1	"Très grave"
	2	"Important"
	3	"Mineur"
	4	"Pas un problème".
VALUE LABELS mv93b
	-7	"INAP"
	-2	"NR"
	1	"Très grave"
	2	"Important"
	3	"Mineur"
	4	"Pas un problème".
VALUE LABELS mv93c
	-7	"INAP"
	-2	"NR"
	1	"Très grave"
	2	"Important"
	3	"Mineur"
	4	"Pas un problème".
VALUE LABELS mv93d
	-7	"INAP"
	-2	"NR"
	1	"Très grave"
	2	"Important"
	3	"Mineur"
	4	"Pas un problème".
VALUE LABELS mv93e
	-7	"INAP"
	-2	"NR"
	1	"Très grave"
	2	"Important"
	3	"Mineur"
	4	"Pas un problème".
VALUE LABELS mv93f
	-7	"INAP"
	-2	"NR"
	1	"Très grave"
	2	"Important"
	3	"Mineur"
	4	"Pas un problème".
VALUE LABELS mv93g
	-7	"INAP"
	-2	"NR"
	1	"Très grave"
	2	"Important"
	3	"Mineur"
	4	"Pas un problème".
VALUE LABELS mv93h
	-7	"INAP"
	-2	"NR"
	1	"Très grave"
	2	"Important"
	3	"Mineur"
	4	"Pas un problème".
VALUE LABELS mv94
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mv95a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS uv95b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv95c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv95d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv95e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv95f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv95g
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv95h
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v96
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v97
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v98
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v99
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v100
	-7	"INAP"
	-2	"NR"
	1	"De quelques conférences"
	2	"De cours ou séminaires intensifs (semaines complètes ou cours étalés sur plusieurs semaines)".
VALUE LABELS mv101
	-7	"INAP"
	-2	"NR"
	1	"Mieux considérés que les personnes actives"
	2	"Moins bien considérés"
	3	"La même chose".
VALUE LABELS mv102
	-7	"INAP"
	-3	"NV"
	-2	"NR"
	1	"De meilleures conditions que les personnes actives"
	2	"De moins bonnes conditions"
	3	"Des conditions sensiblement égales".
VALUE LABELS v103a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103i
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103j
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103k
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103l
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103m
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103n
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103o
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v103p
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS tv104aa
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sv104ca
	-7	"INAP"
	-2	"NR".
VALUE LABELS tv104ab
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sv104cb
	-7	"INAP"
	-2	"NR".
VALUE LABELS tv104ac
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sv104cc
	-7	"INAP"
	-2	"NR".
VALUE LABELS sv105aa
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sv105ba
	-7	"INAP"
	-2	"NR".
VALUE LABELS sv105ab
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS sv105bb
	-7	"INAP"
	-2	"NR".
VALUE LABELS sv105ar
	-7	"INAP"
	-2	"NR".
VALUE LABELS sv105br
	-7	"INAP"
	-2	"NR".
VALUE LABELS lv106
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv107
	-7	"INAP"
	-2	"NR".
VALUE LABELS sv107r
	-7	"INAP"
	-2	"NR"
	1	"1 fois"
	2	"2 fois ou plus".
VALUE LABELS sv108
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, à peu près"
	2	"Oui, tout à fait".
VALUE LABELS sv109aa
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ab
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ac
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v109ad
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ae
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109af
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ag
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ah
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ai
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109aj
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ba
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109bb
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109bc
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v109bd
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109be
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109bf
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109bg
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109bh
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109bi
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109bj
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ca
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109cb
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109cc
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v109cd
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ce
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109cf
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109cg
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ch
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109ci
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv109cj
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv110a
	-7	"INAP"
	-2	"NR"
	1	"Hôpital cantonal de Genève (Genève)"
	2	"Hôpital de gériatrie (Thônex)"
	3	"Clinique La Colline (Genève)"
	4	"Clinique ophtalmologique (Genève)"
	5	"Clinique Vert-Pré (Chêne-Bougeries)"
	6	"Clinique de Joli-Mont (Genève)"
	7	"Hôpital de la Tour (Meyrin)"
	8	"Hôpital Beau-Séjour (Genève)"
	9	"Clinique Générale Beaulieu (Genève)"
	10	"Centre de Soins Continus (Collonge-Bellerive)"
	11	"Clinique des Grangettes (Chêne-Bougeries)"
	12	"Clinique de l'Arve (Carouge)"
	13	"Clinique La Lignière (Gland)"
	40	"Hôpital de Sion (Sion)"
	41	"Clinique genevoise de Montana (Montana)"
	42	"Hôpital gériatrique de Gravelone (Sion)"
	43	"Hôpital régional de Sierre (Sierre)"
	44	"Hôpital de Champsec (Sion)"
	45	"Clinique de Valère (Sion)"
	46	"Hôpital régional de Martigny (Martigny)"
	47	"Hôpital de district Sion, Hérens, Conthey (Sion)"
	48	"Sierre (Sierre)"
	49	"Sana Val (Montana)"
	50	"Centre valaisan de pneumologie (Montana)"
	51	"Ancien Hôpital de Sierre (Sierre)"
	52	"Hôpital d'arrondissement de Sierre (Sierre)"
	53	"Clinique de Loèche-les-Bains (Reumaclinic, Leukerbad)"
	98	"Étranger"
	99	"Autre canton".
VALUE LABELS sv110b
	-7	"INAP"
	-2	"NR"
	1	"Hôpital cantonal de Genève (Genève)"
	2	"Hôpital de gériatrie (Thônex)"
	3	"Clinique La Colline (Genève)"
	4	"Clinique ophtalmologique (Genève)"
	5	"Clinique Vert-Pré (Chêne-Bougeries)"
	6	"Clinique de Joli-Mont (Genève)"
	7	"Hôpital de la Tour (Meyrin)"
	8	"Hôpital Beau-Séjour (Genève)"
	9	"Clinique Générale Beaulieu(Genève)"
	10	"Centre de Soins Continus (CESCO, Collonge-Bellerive)"
	11	"Clinique des Grangettes (Chêne-Bougeries)"
	12	"Clinique de l'Arve (Carouge)"
	40	"Hôpital de Sion (Sion)"
	41	"Clinique genevoise d'altitude (Montana)"
	42	"Hôpital gériatrique de Gravelone (Sion)"
	43	"Hôpital régional de Sierre (Sierre)"
	44	"Hôpital de Champsec (Sion)"
	45	"Clinique de Valère (Sion)"
	46	"Hôpital régional de Martigny (Martigny)"
	47	"Hôpital de district Sion, Hérens, Conthey (Sion)"
	48	"Hôpital Anoudim (Sierre)"
	49	"Sana Val (Montana)"
	50	"Centre valaisan de pneumologie (Montana)"
	51	"Ancien Hôpital de Sierre (Sierre)"
	52	"Hôpital d'arrondissement (Sierre)"
	53	"Clinique de Loèche-les-Bains (Reumaclinic, Leukerbad)"
	80	"Inconnu"
	98	"Étranger"
	99	"Autre canton".
VALUE LABELS sv110c
	-7	"INAP"
	-2	"NR"
	1	"Hôpital cantonal de Genève (Genève)"
	2	"Hôpital de gériatrie (Thônex)"
	3	"Clinique La Colline (Genève)"
	4	"Clinique ophtalmologique (Genève)"
	5	"Clinique Vert-Pré (Chêne-Bougeries)"
	6	"Clinique de Joli-Mont (Genève)"
	7	"Hôpital de la Tour (Meyrin)"
	8	"Hôpital Beau-Séjour (Genève)"
	9	"Clinique Générale Beaulieu(Genève)"
	10	"Centre de Soins Continus (CESCO, Collonge-Bellerive)"
	11	"Clinique des Grangettes (Chêne-Bougeries)"
	12	"Clinique de l'Arve (Carouge)"
	40	"Hôpital de Sion (Sion)"
	41	"Clinique genevoise d'altitude (Montana)"
	42	"Hôpital gériatrique de Gravelone (Sion)"
	43	"Hôpital régional de Sierre (Sierre)"
	44	"Hôpital de Champsec (Sion)"
	45	"Clinique de Valère (Sion)"
	46	"Hôpital régional de Martigny (Martigny)"
	47	"Hôpital de district Sion, Hérens, Conthey (Sion)"
	48	"Hôpital Anoudim (Sierre)"
	49	"Sana Val (Montana)"
	50	"Centre valaisan de pneumologie (Montana)"
	51	"Ancien Hôpital de Sierre (Sierre)"
	52	"Hôpital d'arrondissement (Sierre)"
	53	"Clinique de Loèche-les-Bains (Reumaclinic, Leukerbad)"
	80	"Inconnu"
	98	"Étranger"
	99	"Autre canton".
VALUE LABELS mv111
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui"
	2	"Reste toujours au lit".
VALUE LABELS mv112
	-7	"INAP"
	-2	"NR"
	1	"Dans les 4 dernières semaines"
	2	"Il y a 1 mois ou plus".
VALUE LABELS mv113
	-7	"INAP"
	-2	"NR"
	1	"Moins d'une semaine"
	2	"Une semaine ou plus".
VALUE LABELS sv114aa
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ab
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ac
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v114ad
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ae
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114af
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ag
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ah
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ai
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114aj
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ak
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ba
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114bb
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114bc
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v114bd
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114be
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114bf
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114bg
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114bh
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114bi
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114bj
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114bk
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ca
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114cb
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114cc
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v114cd
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ce
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114cf
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114cg
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ch
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ci
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114cj
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114ck
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv114d
	-7	"INAP"
	-2	"NR"
	1	"Une ou plusieurs personnes se sont occupées de l'interviewé‎/e"
	2	"Personne, 'je n'ai pas eu besoin d'aide'"
	3	"Personne, 'personne ne s'occupe de moi'".
VALUE LABELS mv115a
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115b
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115c
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115d
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115e
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115f
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115g
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115h
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115i
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115j
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115k
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115l
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS mv115m
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS tv116
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv117
	-7	"INAP"
	-2	"NR"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv118a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv118b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv118c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv118d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv118e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv118f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv118g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv118h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv118i
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv118j
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v119a
	-7	"INAP"
	-2	"NR"
	1	"Jamais ou moins d'une fois par an"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS v119b
	-7	"INAP"
	-2	"NR"
	1	"Jamais ou moins d'une fois par an"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS v119c
	-7	"INAP"
	-2	"NR"
	1	"Jamais ou moins d'une fois par an"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS v119d
	-7	"INAP"
	-2	"NR"
	1	"Jamais ou moins d'une fois par an"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS v119e
	-7	"INAP"
	-2	"NR"
	1	"Jamais ou moins d'une fois par an"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS v119f
	-7	"INAP"
	1	"Jamais ou moins d'une fois par an"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS sv120a
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv120b
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv120c
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv120d
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv120e
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv120f
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv120g
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv120h
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS v121a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv121b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv121c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv121d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv121e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv121f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv121g
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv122a
	-2	"NR"
	1	"Toujours vous"
	2	"Partagé entre vous et autre personne"
	3	"Toujours autren personne"
	4	"Donné à l'extérieur".
VALUE LABELS sv122b
	-2	"NR"
	1	"Toujours vous"
	2	"Partagé entre vous et autre personne"
	3	"Toujours autren personne"
	4	"Donné à l'extérieur".
VALUE LABELS sv122c
	-2	"NR"
	1	"Toujours vous"
	2	"Partagé entre vous et autre personne"
	3	"Toujours autren personne"
	4	"Donné à l'extérieur".
VALUE LABELS sv122d
	-2	"NR"
	1	"Toujours vous"
	2	"Partagé entre vous et autre personne"
	3	"Toujours autren personne"
	4	"Donné à l'extérieur".
VALUE LABELS sv122e
	-2	"NR"
	1	"Toujours vous"
	2	"Partagé entre vous et autre personne"
	3	"Toujours autren personne"
	4	"Donné à l'extérieur".
VALUE LABELS sv122f
	-2	"NR"
	1	"Toujours vous"
	2	"Partagé entre vous et autre personne"
	3	"Toujours autren personne"
	4	"Donné à l'extérieur".
VALUE LABELS sv122g
	-2	"NR"
	1	"Toujours vous"
	2	"Partagé entre vous et autre personne"
	3	"Toujours autren personne"
	4	"Donné à l'extérieur".
VALUE LABELS sv123a
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv123b
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv123c
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv123d
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv123e
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv123f
	-7	"INAP"
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv124a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv124b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv124c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv124d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv124e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv124f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv124g
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv124h
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv124i
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv125a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv125b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv125c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv125d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v125e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS v125f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv125g
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv125h
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv125i
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv125j
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv125k
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS mv125l
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS sv126a
	-7	"INAP"
	-2	"NR"
	1	"Même appartement, villa"
	2	"Même bâtiment, ensemble résidentiel (mais autre appartement)"
	3	"Même quartier, village (mais pas même ensemble)"
	4	"Même commune (mais pas même quartier/village)"
	5	"Même canton (mais pas même commune)"
	6	"Autre canton"
	7	"À l'étranger".
VALUE LABELS sv126b
	-7	"INAP"
	-2	"NR"
	1	"Même appartement, villa"
	2	"Même bâtiment, ensemble résidentiel (mais autre appartement)"
	3	"Même quartier, village (mais pas même ensemble)"
	4	"Même commune (mais pas même quartier/village)"
	5	"Même canton (mais pas même commune)"
	6	"Autre canton"
	7	"À l'étranger".
VALUE LABELS mv127a
	-7	"INAP"
	-2	"NR"
	1	"Tous les jours"
	2	"Deux ou trois fois par semaine"
	3	"Chaque semaine"
	4	"Chaque quinzaine"
	5	"Chaque mois"
	6	"Quelques fois dans l'année"
	7	"Une fois par an environ"
	8	"Plus rarement".
VALUE LABELS mv127b
	-7	"INAP"
	-2	"NR"
	1	"Tous les jours"
	2	"Deux ou trois fois par semaine"
	3	"Chaque semaine"
	4	"Chaque quinzaine"
	5	"Chaque mois"
	6	"Quelques fois dans l'année"
	7	"Une fois par an environ"
	8	"Plus rarement".
VALUE LABELS tv128
	-7	"INAP"
	-2	"NR"
	1	"Oui, pas de problème, ni pour téléphoner, ni pour trouver les numéros"
	2	"Oui, utilise le téléphone sans problème, mais a besoin d'aide pour chercher les numéros qu'il/elle ne connaît pas"
	3	"Non, répond seul/e au téléphone, mas n'appelle pas sans aide"
	4	"Non, n'utilise pas, ou pas seul/e le téléphone".
VALUE LABELS sv129a
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv129b
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv129c
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv129d
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv129e
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv129f
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv129g
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv130a
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv130b
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv130c
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv130d
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv130e
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv130f
	-2	"NR"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS sv131
	-7	"INAP"
	-2	"NR"
	0	"Non, personne"
	1	"Oui, une"
	2	"Oui, deux"
	3	"Oui, trois ou plus".
VALUE LABELS sv132a
	-7	"INAP"
	-2	"NR"
	1	"Même appartement, villa"
	2	"Même bâtiment, ensemble résidentiel (mais autre appartement)"
	3	"Même quartier, village (mais pas même ensemble)"
	4	"Même commune (mais pas même quartier/village)"
	5	"Même canton (mais pas même commune)"
	6	"Autre canton"
	7	"À l'étranger".
VALUE LABELS sv132b
	-7	"INAP"
	-2	"NR"
	1	"Même appartement, villa"
	2	"Même bâtiment, ensemble résidentiel (mais autre appartement)"
	3	"Même quartier, village (mais pas même ensemble)"
	4	"Même commune (mais pas même quartier/village)"
	5	"Même canton (mais pas même commune)"
	6	"Autre canton"
	7	"À l'étranger".
VALUE LABELS sv133a
	-7	"INAP"
	-2	"NR"
	1	"Tous les jours"
	2	"Deux ou trois fois par semaine"
	3	"Chaque semaine"
	4	"Chaque quinzaine"
	5	"Chaque mois"
	6	"Quelques fois dans l'année"
	7	"Une fois par an environ"
	8	"Plus rarement".
VALUE LABELS sv133b
	-7	"INAP"
	-2	"NR"
	1	"Tous les jours"
	2	"Deux ou trois fois par semaine"
	3	"Chaque semaine"
	4	"Chaque quinzaine"
	5	"Chaque mois"
	6	"Quelques fois dans l'année"
	7	"Une fois par an environ"
	8	"Plus rarement".
VALUE LABELS mv134
	-2	"NR"
	1	"Très satisfait"
	2	"Assez satisfait"
	3	"Plutôt pas satisfait"
	4	"Pas satisfait du tout".
VALUE LABELS tv135aa
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135ca
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135da
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135ea
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS sv135ab
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cb
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135db
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135eb
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS tv135ac
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cc
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dc
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135ec
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS sv135ad
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cd
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dd
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135ed
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS sv135ae
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135ce
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135de
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135ee
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS sv135af
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cf
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135df
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135ef
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS tv135ag
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cg
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dg
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135eg
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS sv135ah
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135ch
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dh
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135eh
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS sv135ai
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1i
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2i
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3i
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4i
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135ci
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135di
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135ei
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS sv135aj
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1j
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2j
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3j
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4j
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cj
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dj
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135ej
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS sv135ak
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1k
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2k
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3k
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4k
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135ck
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dk
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135ek
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS tv135al
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1l
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2l
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3l
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4l
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cl
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dl
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135el
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS tv135am
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1m
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2m
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3m
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4m
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cm
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dm
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135em
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS tv135an
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1n
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2n
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3n
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4n
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cn
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dn
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135en
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS tv135ao
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1o
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2o
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3o
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4o
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135co
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135do
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135eo
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS sv135ap
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1p
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2p
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3p
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4p
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cp
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dp
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135ep
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS sv135aq
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1q
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2q
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3q
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4q
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cq
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dq
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135eq
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS tv135ar
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1r
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2r
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3r
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4r
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cr
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dr
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135er
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS tv135as
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1s
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2s
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3s
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4s
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135cs
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135ds
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135es
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS tv135at
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS sv135b1t
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b2t
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b3t
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135b4t
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv135ct
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135dt
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS v135et
	-7	"INAP"
	-2	"NR"
	1	"Plus (+) actuel qu'à 45 ans"
	2	"Égal (=) actuel qu'à 45 ans"
	3	"Moins (-) actuel qu'à 45 ans".
VALUE LABELS pv136
	-7	"INAP"
	-2	"NR"
	1	"Moins de 2000 Frs"
	2	"De 2000 à moins de 3000 Frs"
	3	"De 3000 à moins de 4000 Frs"
	4	"De 4000 à moins de 6000 Frs"
	5	"De 6000 à moins de 8000 Frs"
	6	"De 8000 à moins de 10'000 Frs"
	7	"10'000 Frs et plus".
VALUE LABELS sv137
	-7	"INAP"
	-2	"NR"
	1	"Rien ou presque rien"
	2	"Moins de 60'000 Frs"
	3	"Entre 60'000 et 150'000 Frs"
	4	"Entre 150'000 et 500'000 Frs"
	5	"Plus de 500'000 Frs".
VALUE LABELS sv138a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv138b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv138c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv138d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv138e
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv138f
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv138g
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv138h
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv138i
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv138j
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv138k
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS sv139
	-7	"INAP"
	-2	"NR"
	1	"Non, pas du tout"
	2	"Non, il faut se contenter de ce qu'on a"
	3	"C'est parfois difficile, mais dans l'ensemble ça va"
	4	"Oui, un peu de peine"
	5	"Oui, beaucoup de peine".
VALUE LABELS sv140
	-7	"INAP"
	-2	"NR"
	1	"Plutôt moi qui paie une pension"
	2	"Plutôt eux qui paient pour moi"
	3	"Budgets séparés"
	8	"Autre".
VALUE LABELS mv141a
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS v141b
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141c
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141d
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141e
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141f
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS v141g
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141h
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141i
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141j
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141k
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS v141l
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141m
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141n
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141o
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141p
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141q
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141r
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS v141s
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141t
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141u
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141v
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141w
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS v141x
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141y
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141z
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141aa
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS mv141ab
	-7	"INAP"
	-2	"NR"
	1	"D'accord".
VALUE LABELS sv142
	-7	"INAP"
	-2	"NR"
	-1	"Ne sait pas (d'accord ni avec l'un, ni avec l'autre)"
	1	"Oui, il y a des règles claires qui vous guident en toutes circonstances"
	2	"Non, il n'y a pas de règles claires, cela dépend des circonstances".
VALUE LABELS mv143aa
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ab
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ac
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ad
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ae
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143af
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ag
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ah
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ai
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143aj
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ba
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143bb
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143bc
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143bd
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143be
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143bf
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143bg
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143bh
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143bi
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143bj
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ca
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143cb
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143cc
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143cd
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ce
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143cf
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143cg
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ch
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143ci
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv143cj
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144aa
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ab
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ac
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ad
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ae
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144af
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ag
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ah
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ai
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144aj
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ba
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144bb
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144bc
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144bd
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144be
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144bf
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144bg
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144bh
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144bi
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144bj
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ca
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144cb
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144cc
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144cd
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ce
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144cf
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144cg
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ch
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144ci
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv144cj
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS mv145
	-7	"INAP"
	-2	"NR"
	1	"Pas du tout libre"
	2	"Plutôt pas libre"
	3	"Plutôt libre"
	4	"Tout à fait libre".
VALUE LABELS svf1
	-2	"NR"
	1	"Un logement privé normal (appartement,villa, etc.)"
	2	"Un logement à encadrement médico-social (appartement,studio)"
	3	"Une pension"
	4	"Une institution (pour malades, ou grands vieillards)"
	5	"Hôpital".
VALUE LABELS svf2
	-2	"NR"
	1	"Ferme isolée"
	2	"Hameau"
	3	"Village"
	4	"Ville".
VALUE LABELS svf3
	-2	"NR".
VALUE LABELS svf4
	-2	"NR"
	1	"Rez, aucune marche"
	2	"Rez, une ou plusieurs marches"
	3	"Étage, par un escalier"
	4	"Étage, par un ascenseur".
VALUE LABELS svf5
	-2	"NR"
	1	"Un seul logement"
	2	"Deux logements"
	3	"De 3 à 8 logements"
	4	"9 et plus logements".
VALUE LABELS mvf6a
	-2	"NR"
	1	"Oui".
VALUE LABELS mvf6b
	-2	"NR"
	1	"Oui".
VALUE LABELS mvf6c
	-2	"NR"
	1	"Oui".
VALUE LABELS mvf7
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mvf8
	-2	"NR"
	1	"Coopérative"
	2	"Indifférente"
	3	"Non-coopérative".
VALUE LABELS mvf9
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS mvf9a
	-2	"NR"
	1	"Oui".
VALUE LABELS mvf9b
	-2	"NR"
	1	"Oui".
VALUE LABELS mvf9c
	-2	"NR"
	1	"Oui".
VALUE LABELS mvf9d
	-2	"NR"
	1	"Oui".
VALUE LABELS mvf10
	-2	"NR"
	0	"Non"
	1	"Oui, parfois"
	2	"Oui, très souvent".
VALUE LABELS mvf11
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS svfdate
	-7	"INAP"
	-2	"NR".
VALUE LABELS ix1a
	-7	"INAP"
	-2	"NR"
	1	"Français"
	2	"Allemand, suisse-allemand"
	3	"Italien"
	4	"Espagnol"
	5	"Anglais"
	6	"Langues slaves"
	7	"Patois"
	8	"Autres".
VALUE LABELS ix1b
	-7	"INAP"
	-2	"NR"
	1	"Français"
	2	"Allemand, suisse-allemand"
	3	"Italien"
	4	"Espagnol"
	5	"Anglais"
	6	"Langues slaves"
	7	"Patois"
	8	"Autres".
VALUE LABELS i2b
	-7	"INAP"
	-2	"NR"
	1	"Français"
	2	"Allemand, suisse-allemand"
	3	"Italien"
	4	"Espagnol"
	5	"Anglais"
	6	"Langues slaves"
	7	"Patois"
	8	"Autres".
VALUE LABELS i10
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i11
	-7	"INAP"
	-2	"NR".
VALUE LABELS i1_18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i1_19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i1_21
	-7	"INAP"
	-2	"NR"
	3	"Même quartier‎/village"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays"
	8	"Autre".
VALUE LABELS i2_18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i2_19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i2_21
	-7	"INAP"
	-2	"NR"
	3	"Même quartier‎/village"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays"
	8	"Autre".
VALUE LABELS i3_18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i3_19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i3_21
	-7	"INAP"
	-2	"NR"
	3	"Même quartier‎/village"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays"
	8	"Autre".
VALUE LABELS i4_18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i4_19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i4_21
	-7	"INAP"
	-2	"NR"
	3	"Même quartier‎/village"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays"
	8	"Autre".
VALUE LABELS i5_18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i5_19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i5_21
	-7	"INAP"
	-2	"NR"
	3	"Même quartier‎/village"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays"
	8	"Autre".
VALUE LABELS i6_18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i6_19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i6_21
	-7	"INAP"
	-2	"NR"
	3	"Même quartier‎/village"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays"
	8	"Autre".
VALUE LABELS i7_18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i7_19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i7_21
	-7	"INAP"
	-2	"NR"
	3	"Même quartier‎/village"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays"
	8	"Autre".
VALUE LABELS i8_18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i8_19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"S'occupe principalement de son foyer"
	8	"Autre".
VALUE LABELS i8_21
	-7	"INAP"
	-2	"NR"
	3	"Même quartier‎/village"
	4	"Même commune (mais pas le même quartier‎/village)"
	5	"Même canton (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays"
	8	"Autre".
VALUE LABELS ix2a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS ix2b
	-7	"INAP"
	-2	"NR".
VALUE LABELS i81
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i83a
	-7	"INAP"
	-2	"NR".
VALUE LABELS i83b
	-7	"INAP"
	-2	"NR".
VALUE LABELS i83c
	-7	"INAP"
	-2	"NR".
VALUE LABELS i83d
	-7	"INAP"
	-2	"NR".
VALUE LABELS i83e
	-7	"INAP"
	-2	"NR".
VALUE LABELS i83f
	-7	"INAP"
	-2	"NR".
VALUE LABELS i83g
	-7	"INAP"
	-2	"NR".
VALUE LABELS i84
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i91
	-7	"INAP"
	-2	"NR".
VALUE LABELS i91mois
	-7	"INAP"
	-2	"NR"
	1	"Janvier"
	2	"Février"
	3	"Mars"
	4	"Avril"
	5	"Mai"
	6	"Juin"
	7	"Juillet"
	8	"Août"
	9	"Septembre"
	10	"Octobre"
	11	"Novembre"
	12	"Décembre".
VALUE LABELS i91annee
	-7	"INAP"
	-2	"NR".
VALUE LABELS i93
	-9	"NV"
	-7	"INAP"
	-2	"NR"
	84	"Dällikon"
	102	"Weiach"
	114	"Fischenthal"
	121	"Wetzikon (ZH)"
	133	"Horgen"
	135	"Kilchberg (ZH)"
	141	"Thalwil"
	154	"Küsnacht (ZH)"
	191	"Dübendorf"
	230	"Winterthur"
	253	"Zürich"
	306	"Lyss"
	351	"Bern"
	355	"Köniz"
	356	"Muri bei Bern"
	371	"Biel‎/Bienne"
	418	"Oberburg"
	433	"Cortébert"
	438	"Orvin"
	443	"Saint-Imier"
	446	"Tramelan"
	556	"Zielebach"
	576	"Grindelwald"
	581	"Interlaken"
	619	"Oberdiessbach"
	662	"Ferenbalm"
	667	"Laupen"
	682	"Bévilard"
	687	"Corcelles (BE)"
	690	"Court"
	699	"Monible"
	700	"Moutier"
	703	"Reconvilier"
	723	"La Neuveville"
	784	"Innertkirchen"
	843	"Saanen"
	853	"Rüschegg"
	902	"Langnau im Emmental"
	905	"Rüderswil"
	929	"Hilterfingen"
	1003	"Escholzmatt"
	1061	"Luzern"
	1149	"Willisau Stadt"
	1301	"Einsiedeln"
	1402	"Engelberg"
	1509	"Stans"
	1618	"Mühlehorn"
	1709	"Unterägeri"
	1711	"Zug"
	2001	"Aumont"
	2011	"Cugy (FR)"
	2035	"Nuvilly"
	2046	"Vesin"
	2080	"Hennens"
	2096	"Romont (FR)"
	2102	"Ursy"
	2113	"Vuisternens-devant-Romont"
	2121	"Haut-Intyamon"
	2124	"Broc"
	2125	"Bulle"
	2135	"Gruyères"
	2147	"Pont-la-Ville"
	2175	"Belfaux"
	2184	"Corpataux"
	2196	"Fribourg"
	2210	"Montévraz"
	2217	"Ponthaux"
	2220	"Le Mouret"
	2254	"Courtepin"
	2265	"Kerzers"
	2293	"Düdingen"
	2296	"Heitenried"
	2299	"Plaffeien"
	2323	"Bossonnens"
	2325	"Châtel-Saint-Denis"
	2326	"Le Crêt"
	2336	"Semsales"
	2546	"Grenchen"
	2581	"Olten"
	2582	"Rickenbach (SO)"
	2601	"Solothurn"
	2701	"Basel"
	2802	"Allschwil"
	2825	"Füllinsdorf"
	2829	"Liestal"
	2888	"Langenbruck"
	2937	"Neuhausen am Rheinfall"
	2964	"Stein am Rhein"
	3001	"Herisau"
	3021	"Bühler"
	3111	"Oberegg"
	3203	"St. Gallen"
	3215	"Rorschach"
	3231	"Au (SG)"
	3233	"Berneck"
	3235	"Rheineck"
	3296	"Sargans"
	3392	"Kirchberg (SG)"
	3393	"Lütisburg"
	3406	"Mogelsberg"
	3561	"Poschiavo"
	3787	"St. Moritz"
	3832	"Grono"
	3901	"Chur"
	3952	"Jenins"
	4021	"Baden"
	4063	"Bremgarten (AG)"
	4082	"Wohlen (AG)"
	4172	"Münchwilen (AG)"
	4258	"Rheinfelden"
	4315	"Rekingen (AG)"
	4401	"Arbon"
	4405	"<none>"
	4461	"Amriswil"
	4545	"Diessenhofen"
	4641	"Altnau"
	4941	"Märstetten"
	5002	"Bellinzona"
	5040	"Ludiano"
	5192	"Lugano"
	5210	"Paradiso"
	5222	"Sessa"
	5401	"Aigle"
	5402	"Bex"
	5405	"Gryon"
	5406	"Lavey-Morcles"
	5408	"Noville"
	5409	"Ollon"
	5410	"Ormont-Dessous"
	5411	"Ormont-Dessus"
	5412	"Rennaz"
	5422	"Aubonne"
	5434	"Saint-George"
	5455	"Constantine"
	5456	"<none>"
	5477	"Cossonay"
	5484	"Gollion"
	5492	"Montricher"
	5495	"Penthalaz"
	5524	"Goumoens-la-Ville"
	5531	"Penthéréaz"
	5554	"Concise"
	5561	"Grandson"
	5568	"Sainte-Croix"
	5586	"Lausanne"
	5590	"Pully"
	5591	"Renens (VD)"
	5601	"Chexbres"
	5602	"Cully"
	5604	"Forel (Lavaux)"
	5605	"Grandvaux"
	5607	"Puidoux"
	5621	"Aclens"
	5633	"Echandens"
	5635	"Ecublens (VD)"
	5642	"Morges"
	5688	"Syens"
	5703	"Bassins"
	5711	"Commugny"
	5712	"Coppet"
	5713	"Crans-près-Céligny"
	5714	"Crassier"
	5718	"Genolier"
	5721	"Gland"
	5722	"Grens"
	5723	"Mies"
	5724	"Nyon"
	5725	"Prangins"
	5727	"Saint-Cergue"
	5743	"Arnex-sur-Orbe"
	5744	"Ballaigues"
	5745	"Baulmes"
	5750	"Les Clées"
	5754	"Juriens"
	5757	"Orbe"
	5764	"Vallorbe"
	5765	"Vaulion"
	5789	"Ferlens (VD)"
	5792	"Montpreveyres"
	5793	"Oron-la-Ville"
	5816	"Corcelles-près-Payerne"
	5841	"Château-d'Oex"
	5853	"Bursins"
	5854	"Burtigny"
	5857	"Gilly"
	5859	"Mont-sur-Rolle"
	5861	"Rolle"
	5881	"Blonay"
	5886	"Montreux"
	5889	"La Tour-de-Peilz"
	5890	"Vevey"
	5928	"Rovray"
	5934	"Valeyres-sous-Ursins"
	5938	"Yverdon-les-Bains"
	6002	"Brig-Glis"
	6006	"Mund"
	6007	"Naters"
	6008	"Ried-Brig"
	6009	"Simplon"
	6021	"Ardon"
	6022	"Chamoson"
	6023	"Conthey"
	6024	"Nendaz"
	6025	"Vétroz"
	6028	"-- code à corriger --"
	6031	"Bagnes"
	6032	"Bourg-Saint-Pierre"
	6034	"Orsières"
	6053	"Biel (VS)"
	6081	"Les Agettes"
	6082	"Ayent"
	6083	"Evolène"
	6084	"Hérémence"
	6085	"Mase"
	6086	"Nax"
	6087	"Saint-Martin (VS)"
	6088	"Vernamiège"
	6089	"Vex"
	6101	"Agarn"
	6105	"Erschmatt"
	6106	"Feschel"
	6110	"Leuk"
	6111	"Leukerbad"
	6113	"Salgesch"
	6116	"Varen"
	6133	"Fully"
	6134	"Isérables"
	6135	"Leytron"
	6136	"Martigny"
	6139	"Riddes"
	6140	"Saillon"
	6141	"Saxon"
	6142	"Trient"
	6151	"Champéry"
	6152	"Collombey-Muraz"
	6153	"Monthey"
	6155	"Saint-Gingolph"
	6156	"Troistorrents"
	6159	"Vouvry"
	6173	"Bitsch"
	6175	"Goppisberg"
	6179	"Mörel"
	6191	"Ausserberg"
	6195	"Ferden"
	6200	"Steg"
	6213	"Evionnaz"
	6217	"Saint-Maurice"
	6219	"Vernayaz"
	6231	"Ayer"
	6232	"Chalais"
	6233	"Chandolin"
	6234	"Chermignon"
	6235	"Chippis"
	6237	"Grimentz"
	6238	"Grône"
	6239	"Icogne"
	6240	"Lens"
	6241	"Miège"
	6242	"Mollens (VS)"
	6243	"Montana"
	6244	"Randogne"
	6245	"Saint-Jean"
	6246	"Saint-Léonard"
	6247	"Saint-Luc"
	6248	"Sierre"
	6249	"Venthône"
	6250	"Veyras"
	6251	"Vissoie"
	6261	"Arbaz"
	6263	"Grimisuat"
	6264	"Salins"
	6265	"Savièse"
	6266	"Sion"
	6267	"Veysonnaz"
	6285	"Grächen"
	6293	"Stalden (VS)"
	6297	"Visp"
	6402	"Bevaix"
	6407	"Corcelles-Cormondrèche"
	6408	"Cortaillod"
	6413	"Rochefort"
	6421	"La Chaux-de-Fonds"
	6423	"La Sagne"
	6431	"Les Brenets"
	6436	"Le Locle"
	6455	"Le Landeron"
	6458	"Neuchâtel"
	6459	"Saint-Blaise"
	6475	"Dombresson"
	6505	"Couvet"
	6506	"Fleurier"
	6511	"Les Verrières"
	6601	"Aire-la-Ville"
	6602	"Anières"
	6603	"Avully"
	6604	"Avusy"
	6606	"Bellevue"
	6607	"Bernex"
	6608	"Carouge (GE)"
	6609	"Cartigny"
	6610	"Céligny"
	6612	"Chêne-Bougeries"
	6613	"Chêne-Bourg"
	6614	"Choulex"
	6615	"Collex-Bossy"
	6616	"Collonge-Bellerive"
	6617	"Cologny"
	6618	"Confignon"
	6619	"Corsier (GE)"
	6620	"Dardagny"
	6621	"Genève"
	6622	"Genthod"
	6623	"Le Grand-Saconnex"
	6625	"Hermance"
	6626	"Jussy"
	6627	"Laconnex"
	6628	"Lancy"
	6629	"Meinier"
	6630	"Meyrin"
	6631	"Onex"
	6632	"Perly-Certoux"
	6633	"Plan-les-Ouates"
	6634	"Pregny-Chambésy"
	6635	"Presinge"
	6638	"Satigny"
	6639	"Soral"
	6640	"Thônex"
	6641	"Troinex"
	6642	"Vandoeuvres"
	6643	"Vernier"
	6644	"Versoix"
	6645	"Veyrier"
	6707	"Courfaivre"
	6708	"Courrendlin"
	6711	"Delémont"
	6727	"Vicques"
	6757	"Saignelégier"
	6775	"Bonfol"
	6782	"Cornol"
	6783	"Courchavon"
	6800	"Porrentruy"
	6804	"Saint-Ursanne"
	7101	"Albanie"
	7102	"Belgique"
	7103	"Bulgarie"
	7105	"Allemagne"
	7107	"France"
	7108	"Royaume-Uni"
	7111	"Italie"
	7112	"Yougoslavie"
	7115	"Pays-Bas"
	7117	"Autriche"
	7118	"Pologne"
	7119	"Portugal"
	7120	"Roumanie"
	7121	"Suède"
	7122	"Suisse"
	7123	"Espagne"
	7124	"Tchécoslovaquie"
	7127	"Hongrie"
	7129	"Grèce"
	7130	"Monaco"
	7140	"Russie"
	7141	"Ukraine"
	7150	"Ain, Haute-Savoie"
	7209	"Inde"
	7210	"Indonésie"
	7212	"Iran"
	7213	"Israël"
	7224	"Liban"
	7231	"Philippines"
	7236	"Thaïlande"
	7300	"Afrique"
	7301	"Egypte"
	7302	"Algérie"
	7312	"Djibouti"
	7326	"Libye"
	7330	"Maroc"
	7340	"Sénégal"
	7344	"Afrique du Sud"
	7351	"Tunisie"
	7401	"Argentine"
	7410	"Equateur"
	7418	"Canada"
	7420	"Cuba"
	7421	"Mexique"
	7425	"Pérou"
	7432	"Etats-Unis"
	7691	"Nouvelle-Calédonie"
	7801	"Canton de Zurich"
	7802	"Canton de Berne"
	7803	"Canton de Lucerne"
	7810	"Canton de Fribourg"
	7811	"Canton de Soleure"
	7812	"Canton de Bâle-Ville"
	7813	"Canton de Bâle-Campagne"
	7814	"Canton de Schaffhouse"
	7817	"Canton se Saint-Gall"
	7818	"Canton des Grisons"
	7819	"Canton d'Argovie"
	7820	"Canton de Thurgovie"
	7821	"Canton du Tessin"
	7822	"Canton de Vaud"
	7823	"Canton du Valais"
	7824	"Canton de Neuchâtel"
	7825	"Canton de Genève"
	7826	"Canton du Jura"
	8000	"Autre".
VALUE LABELS i95
	-7	"INAP"
	-2	"NR"
	1	"D'un studio"
	2	"D'une chambre pour vous seul/e"
	3	"D'une chambre partagée avec une autre personne"
	4	"D'une chambre partagée avec deux autres personnes"
	5	"D'une chambre partagée avec trois ou plus autres personnes".
VALUE LABELS ix3a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS ix3b
	-7	"INAP"
	-2	"NR"
	1	"Conjoint/e"
	2	"Soeur/frère"
	3	"Autre parent".
VALUE LABELS i117d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, mais accompagné‎/e seulement"
	2	"Oui, seul‎/e ou accompagné/e".
VALUE LABELS ix4
	-7	"INAP"
	-2	"NR"
	2	"Exerçait une profession (emploi, travail)"
	4	"Personne au foyer (ménager‎/ère)"
	5	"À l'assurance invalidité"
	8	"Autre".
VALUE LABELS i22
	-7	"INAP"
	-2	"NR"
	10	"Agriculteurs exploitants"
	11	"Agriculteurs exploitants sur petite exploitation"
	12	"Agriculteurs exploitants sur moyenne exploitation"
	13	"Agriculteurs exploitants sur grande exploitation"
	20	"Artisans, commerçants et chefs d'entreprise"
	21	"Artisans"
	22	"Commerçants et assimilés"
	23	"Chefs d'entreprise de 10 salariés ou plus"
	30	"Cadres et professions intellectuelles supérieures"
	31	"Professions libérales"
	33	"Cadres de la fonction publique"
	34	"Professeurs, professions scientifiques"
	35	"Professions de l'information, des arts et des spectacles"
	37	"Cadres administratifs et commerciaux d'entreprise"
	38	"Ingénieurs et cadres techniques d'entreprise"
	40	"Professions intermédiaires"
	42	"Instituteurs et assimilés"
	43	"Professions intermédiaires de la santé et du travail social"
	44	"Clergé, religieux"
	45	"Professions intermédiaires administratives de la fonction publique"
	46	"Professions intermédiaires administratives et commerciales d'entreprise"
	47	"Techniciens (sauf techniciens tertiaires)"
	48	"Contremaîtres, agents de maîtrise (maîtrise administrative exclue)"
	50	"Employés"
	52	"Employés civils et agents de service de la fonction publique"
	53	"Policiers et militaires"
	54	"Employés administratifs d'entreprise"
	55	"Employés de commerce"
	56	"Personnels des services directs aux particuliers"
	60	"Ouvriers"
	62	"Ouvriers qualifiés de type industriel"
	63	"Ouvriers qualifiés de type artisanal"
	64	"Chauffeurs"
	65	"Ouvriers qualifiés de la manutention, du magasinage et des transports"
	67	"Ouvriers non qualifiés de type industriel"
	68	"Ouvriers non qualifiés de type artisanal"
	69	"Ouvriers agricoles et assimilés"
	70	"Population inactive"
	71	"Anciens agriculteurs"
	72	"Anciens artisans, commerçants, chefs d'entreprise"
	80	"Autres".
VALUE LABELS i24
	-7	"INAP"
	-2	"NR"
	1	"Indépendant"
	2	"Salarié dans une entreprise privée"
	3	"Salarié à l'État ou dans une entreprise de l'État".
VALUE LABELS i25
	-7	"INAP"
	-2	"NR"
	1	"Dans l'agriculture"
	2	"Dans la production et la construction"
	3	"Dans les services aux entreprises et la distribution"
	4	"Dans les services sociaux et personnels".
VALUE LABELS i26
	-7	"INAP"
	-2	"NR"
	1	"Travaillait seul ou aucun subordonné"
	2	"Moins de cinq"
	3	"De 6 à 20"
	4	"De 21 à 50"
	5	"Plus de 50".
VALUE LABELS ix5
	-7	"INAP"
	-2	"NR"
	2	"Exerçait une profession (emploi, travail)"
	4	"Personne au foyer (ménager‎/ère)"
	5	"À l'assurance invalidité"
	8	"Autre".
VALUE LABELS i60
	-7	"INAP"
	-2	"NR"
	10	"Agriculteurs exploitants"
	11	"Agriculteurs exploitants sur petite exploitation"
	12	"Agriculteurs exploitants sur moyenne exploitation"
	13	"Agriculteurs exploitants sur grande exploitation"
	20	"Artisans, commerçants et chefs d'entreprise"
	21	"Artisans"
	22	"Commerçants et assimilés"
	23	"Chefs d'entreprise de 10 salariés ou plus"
	30	"Cadres et professions intellectuelles supérieures"
	31	"Professions libérales"
	33	"Cadres de la fonction publique"
	34	"Professeurs, professions scientifiques"
	35	"Professions de l'information, des arts et des spectacles"
	37	"Cadres administratifs et commerciaux d'entreprise"
	38	"Ingénieurs et cadres techniques d'entreprise"
	40	"Professions intermédiaires"
	42	"Instituteurs et assimilés"
	43	"Professions intermédiaires de la santé et du travail social"
	44	"Clergé, religieux"
	45	"Professions intermédiaires administratives de la fonction publique"
	46	"Professions intermédiaires administratives et commerciales d'entreprise"
	47	"Techniciens (sauf techniciens tertiaires)"
	48	"Contremaîtres, agents de maîtrise (maîtrise administrative exclue)"
	50	"Employés"
	52	"Employés civils et agents de service de la fonction publique"
	53	"Policiers et militaires"
	54	"Employés administratifs d'entreprise"
	55	"Employés de commerce"
	56	"Personnels des services directs aux particuliers"
	60	"Ouvriers"
	62	"Ouvriers qualifiés de type industriel"
	63	"Ouvriers qualifiés de type artisanal"
	64	"Chauffeurs"
	65	"Ouvriers qualifiés de la manutention, du magasinage et des transports"
	67	"Ouvriers non qualifiés de type industriel"
	68	"Ouvriers non qualifiés de type artisanal"
	69	"Ouvriers agricoles et assimilés"
	70	"Population inactive"
	71	"Anciens agriculteurs"
	72	"Anciens artisans, commerçants, chefs d'entreprise"
	80	"Autres".
VALUE LABELS i62
	-7	"INAP"
	-2	"NR"
	1	"Indépendant"
	2	"Salarié dans une entreprise privée"
	3	"Salarié à l'État ou dans une entreprise de l'État".
VALUE LABELS i63
	-7	"INAP"
	-2	"NR"
	1	"Dans l'agriculture"
	2	"Dans la production et la construction"
	3	"Dans les services aux entreprises et la distribution"
	4	"Dans les services sociaux et personnels".
VALUE LABELS i64
	-7	"INAP"
	-2	"NR"
	1	"Travaillait seul ou aucun subordonné"
	2	"Moins de cinq"
	3	"De 6 à 20"
	4	"De 21 à 50"
	5	"Plus de 50".
VALUE LABELS i65
	-7	"INAP"
	-2	"NR"
	1	"Âge normal"
	2	"Retraite repoussée"
	3	"Retraite anticipée"
	4	"Conjoint/e décédé/e avant la retraite".
VALUE LABELS i59
	-7	"INAP"
	-2	"NR"
	1	"Retraité‎/e"
	2	"Exerce une profession (emploi, travail)"
	4	"Personne au foyer (ménager‎/ère)"
	5	"Autre".
VALUE LABELS i136
	-7	"INAP"
	-2	"NR"
	1	"Moins de 2000 Frs"
	2	"De 2000 à moins de 3000 Frs"
	3	"De 3000 à moins de 4000 Frs"
	4	"De 4000 à moins de 6000 Frs"
	5	"De 6000 à moins de 8000 Frs"
	6	"De 8000 à moins de 10'000 Frs"
	7	"10'000 Frs et plus".
VALUE LABELS ix6a
	-7	"INAP"
	-2	"NR"
	1	"Tout"
	2	"En partie"
	3	"Non".
VALUE LABELS ix6b
	-7	"INAP"
	-2	"NR"
	1	"Tout"
	2	"En partie"
	3	"Non".
VALUE LABELS ix6c
	-7	"INAP"
	-2	"NR"
	1	"Tout"
	2	"En partie"
	3	"Non".
VALUE LABELS i98
	-7	"INAP"
	-2	"NR"
	0	"Il n'y a pas de téléphone accessible"
	1	"Je ne peux pas accéder à un téléphone"
	2	"Je n'accède que difficilement à un téléphone"
	3	"J'ai accès à un téléphone".
VALUE LABELS i175a
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i175b
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i175c
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i175d
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i182aa
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i182ba
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i182ac
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i182bc
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i182ad
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i182bd
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i182ae
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i182be
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i182ag
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i182bg
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS i125a
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS i125l
	-7	"INAP"
	-2	"NR"
	1	"Oui".
VALUE LABELS i135e
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS i135f
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS i135j
	-7	"INAP"
	-2	"NR"
	1	"Non"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS rjpb
	-7	"INAP"
	-2	"NR"
	1	"Femme"
	2	"Homme".
VALUE LABELS rjpc
	-7	"INAP"
	-2	"NR".
VALUE LABELS jpd
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS jpg
	-7	"INAP"
	-2	"NR"
	1	"Moins de 3 mois"
	2	"3-6 mois"
	3	"De 6 mois à 1 an"
	4	"Plus d'un an".
VALUE LABELS j9
	-7	"INAP"
	-2	"NR".
VALUE LABELS j12a
	-7	"INAP"
	-2	"NR"
	1	"Féminin"
	2	"Masculin".
VALUE LABELS j12b
	-7	"INAP"
	-2	"NR".
VALUE LABELS j12c
	-9	"NV"
	-7	"INAP"
	-2	"NR"
	84	"Dällikon"
	102	"Weiach"
	114	"Fischenthal"
	121	"Wetzikon (ZH)"
	133	"Horgen"
	135	"Kilchberg (ZH)"
	141	"Thalwil"
	154	"Küsnacht (ZH)"
	191	"Dübendorf"
	230	"Winterthur"
	253	"Zürich"
	306	"Lyss"
	351	"Bern"
	355	"Köniz"
	356	"Muri bei Bern"
	371	"Biel‎/Bienne"
	418	"Oberburg"
	433	"Cortébert"
	438	"Orvin"
	443	"Saint-Imier"
	446	"Tramelan"
	556	"Zielebach"
	576	"Grindelwald"
	581	"Interlaken"
	619	"Oberdiessbach"
	662	"Ferenbalm"
	667	"Laupen"
	682	"Bévilard"
	687	"Corcelles (BE)"
	690	"Court"
	699	"Monible"
	700	"Moutier"
	703	"Reconvilier"
	723	"La Neuveville"
	784	"Innertkirchen"
	843	"Saanen"
	853	"Rüschegg"
	902	"Langnau im Emmental"
	905	"Rüderswil"
	929	"Hilterfingen"
	1003	"Escholzmatt"
	1061	"Luzern"
	1149	"Willisau Stadt"
	1301	"Einsiedeln"
	1402	"Engelberg"
	1509	"Stans"
	1618	"Mühlehorn"
	1709	"Unterägeri"
	1711	"Zug"
	2001	"Aumont"
	2011	"Cugy (FR)"
	2035	"Nuvilly"
	2046	"Vesin"
	2080	"Hennens"
	2096	"Romont (FR)"
	2102	"Ursy"
	2113	"Vuisternens-devant-Romont"
	2121	"Haut-Intyamon"
	2124	"Broc"
	2125	"Bulle"
	2135	"Gruyères"
	2147	"Pont-la-Ville"
	2175	"Belfaux"
	2184	"Corpataux"
	2196	"Fribourg"
	2210	"Montévraz"
	2217	"Ponthaux"
	2220	"Le Mouret"
	2254	"Courtepin"
	2265	"Kerzers"
	2293	"Düdingen"
	2296	"Heitenried"
	2299	"Plaffeien"
	2323	"Bossonnens"
	2325	"Châtel-Saint-Denis"
	2326	"Le Crêt"
	2336	"Semsales"
	2546	"Grenchen"
	2581	"Olten"
	2582	"Rickenbach (SO)"
	2601	"Solothurn"
	2701	"Basel"
	2802	"Allschwil"
	2825	"Füllinsdorf"
	2829	"Liestal"
	2888	"Langenbruck"
	2937	"Neuhausen am Rheinfall"
	2964	"Stein am Rhein"
	3001	"Herisau"
	3021	"Bühler"
	3111	"Oberegg"
	3203	"St. Gallen"
	3215	"Rorschach"
	3231	"Au (SG)"
	3233	"Berneck"
	3235	"Rheineck"
	3296	"Sargans"
	3392	"Kirchberg (SG)"
	3393	"Lütisburg"
	3406	"Mogelsberg"
	3561	"Poschiavo"
	3787	"St. Moritz"
	3832	"Grono"
	3901	"Chur"
	3952	"Jenins"
	4021	"Baden"
	4063	"Bremgarten (AG)"
	4082	"Wohlen (AG)"
	4172	"Münchwilen (AG)"
	4258	"Rheinfelden"
	4315	"Rekingen (AG)"
	4401	"Arbon"
	4405	"<none>"
	4461	"Amriswil"
	4545	"Diessenhofen"
	4641	"Altnau"
	4941	"Märstetten"
	5002	"Bellinzona"
	5040	"Ludiano"
	5192	"Lugano"
	5210	"Paradiso"
	5222	"Sessa"
	5401	"Aigle"
	5402	"Bex"
	5405	"Gryon"
	5406	"Lavey-Morcles"
	5408	"Noville"
	5409	"Ollon"
	5410	"Ormont-Dessous"
	5411	"Ormont-Dessus"
	5412	"Rennaz"
	5422	"Aubonne"
	5434	"Saint-George"
	5455	"Constantine"
	5456	"<none>"
	5477	"Cossonay"
	5484	"Gollion"
	5492	"Montricher"
	5495	"Penthalaz"
	5524	"Goumoens-la-Ville"
	5531	"Penthéréaz"
	5554	"Concise"
	5561	"Grandson"
	5568	"Sainte-Croix"
	5586	"Lausanne"
	5590	"Pully"
	5591	"Renens (VD)"
	5601	"Chexbres"
	5602	"Cully"
	5604	"Forel (Lavaux)"
	5605	"Grandvaux"
	5607	"Puidoux"
	5621	"Aclens"
	5633	"Echandens"
	5635	"Ecublens (VD)"
	5642	"Morges"
	5688	"Syens"
	5703	"Bassins"
	5711	"Commugny"
	5712	"Coppet"
	5713	"Crans-près-Céligny"
	5714	"Crassier"
	5718	"Genolier"
	5721	"Gland"
	5722	"Grens"
	5723	"Mies"
	5724	"Nyon"
	5725	"Prangins"
	5727	"Saint-Cergue"
	5743	"Arnex-sur-Orbe"
	5744	"Ballaigues"
	5745	"Baulmes"
	5750	"Les Clées"
	5754	"Juriens"
	5757	"Orbe"
	5764	"Vallorbe"
	5765	"Vaulion"
	5789	"Ferlens (VD)"
	5792	"Montpreveyres"
	5793	"Oron-la-Ville"
	5816	"Corcelles-près-Payerne"
	5841	"Château-d'Oex"
	5853	"Bursins"
	5854	"Burtigny"
	5857	"Gilly"
	5859	"Mont-sur-Rolle"
	5861	"Rolle"
	5881	"Blonay"
	5886	"Montreux"
	5889	"La Tour-de-Peilz"
	5890	"Vevey"
	5928	"Rovray"
	5934	"Valeyres-sous-Ursins"
	5938	"Yverdon-les-Bains"
	6002	"Brig-Glis"
	6006	"Mund"
	6007	"Naters"
	6008	"Ried-Brig"
	6009	"Simplon"
	6021	"Ardon"
	6022	"Chamoson"
	6023	"Conthey"
	6024	"Nendaz"
	6025	"Vétroz"
	6028	"-- code à corriger --"
	6031	"Bagnes"
	6032	"Bourg-Saint-Pierre"
	6034	"Orsières"
	6053	"Biel (VS)"
	6081	"Les Agettes"
	6082	"Ayent"
	6083	"Evolène"
	6084	"Hérémence"
	6085	"Mase"
	6086	"Nax"
	6087	"Saint-Martin (VS)"
	6088	"Vernamiège"
	6089	"Vex"
	6101	"Agarn"
	6105	"Erschmatt"
	6106	"Feschel"
	6110	"Leuk"
	6111	"Leukerbad"
	6113	"Salgesch"
	6116	"Varen"
	6133	"Fully"
	6134	"Isérables"
	6135	"Leytron"
	6136	"Martigny"
	6139	"Riddes"
	6140	"Saillon"
	6141	"Saxon"
	6142	"Trient"
	6151	"Champéry"
	6152	"Collombey-Muraz"
	6153	"Monthey"
	6155	"Saint-Gingolph"
	6156	"Troistorrents"
	6159	"Vouvry"
	6173	"Bitsch"
	6175	"Goppisberg"
	6179	"Mörel"
	6191	"Ausserberg"
	6195	"Ferden"
	6200	"Steg"
	6213	"Evionnaz"
	6217	"Saint-Maurice"
	6219	"Vernayaz"
	6231	"Ayer"
	6232	"Chalais"
	6233	"Chandolin"
	6234	"Chermignon"
	6235	"Chippis"
	6237	"Grimentz"
	6238	"Grône"
	6239	"Icogne"
	6240	"Lens"
	6241	"Miège"
	6242	"Mollens (VS)"
	6243	"Montana"
	6244	"Randogne"
	6245	"Saint-Jean"
	6246	"Saint-Léonard"
	6247	"Saint-Luc"
	6248	"Sierre"
	6249	"Venthône"
	6250	"Veyras"
	6251	"Vissoie"
	6261	"Arbaz"
	6263	"Grimisuat"
	6264	"Salins"
	6265	"Savièse"
	6266	"Sion"
	6267	"Veysonnaz"
	6285	"Grächen"
	6293	"Stalden (VS)"
	6297	"Visp"
	6402	"Bevaix"
	6407	"Corcelles-Cormondrèche"
	6408	"Cortaillod"
	6413	"Rochefort"
	6421	"La Chaux-de-Fonds"
	6423	"La Sagne"
	6431	"Les Brenets"
	6436	"Le Locle"
	6455	"Le Landeron"
	6458	"Neuchâtel"
	6459	"Saint-Blaise"
	6475	"Dombresson"
	6505	"Couvet"
	6506	"Fleurier"
	6511	"Les Verrières"
	6601	"Aire-la-Ville"
	6602	"Anières"
	6603	"Avully"
	6604	"Avusy"
	6606	"Bellevue"
	6607	"Bernex"
	6608	"Carouge (GE)"
	6609	"Cartigny"
	6610	"Céligny"
	6612	"Chêne-Bougeries"
	6613	"Chêne-Bourg"
	6614	"Choulex"
	6615	"Collex-Bossy"
	6616	"Collonge-Bellerive"
	6617	"Cologny"
	6618	"Confignon"
	6619	"Corsier (GE)"
	6620	"Dardagny"
	6621	"Genève"
	6622	"Genthod"
	6623	"Le Grand-Saconnex"
	6625	"Hermance"
	6626	"Jussy"
	6627	"Laconnex"
	6628	"Lancy"
	6629	"Meinier"
	6630	"Meyrin"
	6631	"Onex"
	6632	"Perly-Certoux"
	6633	"Plan-les-Ouates"
	6634	"Pregny-Chambésy"
	6635	"Presinge"
	6638	"Satigny"
	6639	"Soral"
	6640	"Thônex"
	6641	"Troinex"
	6642	"Vandoeuvres"
	6643	"Vernier"
	6644	"Versoix"
	6645	"Veyrier"
	6707	"Courfaivre"
	6708	"Courrendlin"
	6711	"Delémont"
	6727	"Vicques"
	6757	"Saignelégier"
	6775	"Bonfol"
	6782	"Cornol"
	6783	"Courchavon"
	6800	"Porrentruy"
	6804	"Saint-Ursanne"
	7101	"Albanie"
	7102	"Belgique"
	7103	"Bulgarie"
	7105	"Allemagne"
	7107	"France"
	7108	"Royaume-Uni"
	7111	"Italie"
	7112	"Yougoslavie"
	7115	"Pays-Bas"
	7117	"Autriche"
	7118	"Pologne"
	7119	"Portugal"
	7120	"Roumanie"
	7121	"Suède"
	7122	"Suisse"
	7123	"Espagne"
	7124	"Tchécoslovaquie"
	7127	"Hongrie"
	7129	"Grèce"
	7130	"Monaco"
	7140	"Russie"
	7141	"Ukraine"
	7150	"Ain, Haute-Savoie"
	7209	"Inde"
	7210	"Indonésie"
	7212	"Iran"
	7213	"Israël"
	7224	"Liban"
	7231	"Philippines"
	7236	"Thaïlande"
	7300	"Afrique"
	7301	"Egypte"
	7302	"Algérie"
	7312	"Djibouti"
	7326	"Libye"
	7330	"Maroc"
	7340	"Sénégal"
	7344	"Afrique du Sud"
	7351	"Tunisie"
	7401	"Argentine"
	7410	"Equateur"
	7418	"Canada"
	7420	"Cuba"
	7421	"Mexique"
	7425	"Pérou"
	7432	"Etats-Unis"
	7691	"Nouvelle-Calédonie"
	7801	"Canton de Zurich"
	7802	"Canton de Berne"
	7803	"Canton de Lucerne"
	7810	"Canton de Fribourg"
	7811	"Canton de Soleure"
	7812	"Canton de Bâle-Ville"
	7813	"Canton de Bâle-Campagne"
	7814	"Canton de Schaffhouse"
	7817	"Canton se Saint-Gall"
	7818	"Canton des Grisons"
	7819	"Canton d'Argovie"
	7820	"Canton de Thurgovie"
	7821	"Canton du Tessin"
	7822	"Canton de Vaud"
	7823	"Canton du Valais"
	7824	"Canton de Neuchâtel"
	7825	"Canton de Genève"
	7826	"Canton du Jura"
	8000	"Autre".
VALUE LABELS j23a
	-7	"INAP"
	-2	"NR"
	1	"Féminin"
	2	"Masculin".
VALUE LABELS j23b
	-7	"INAP"
	-2	"NR".
VALUE LABELS j23c
	-9	"NV"
	-7	"INAP"
	-2	"NR"
	84	"Dällikon"
	102	"Weiach"
	114	"Fischenthal"
	121	"Wetzikon (ZH)"
	133	"Horgen"
	135	"Kilchberg (ZH)"
	141	"Thalwil"
	154	"Küsnacht (ZH)"
	191	"Dübendorf"
	230	"Winterthur"
	253	"Zürich"
	306	"Lyss"
	351	"Bern"
	355	"Köniz"
	356	"Muri bei Bern"
	371	"Biel‎/Bienne"
	418	"Oberburg"
	433	"Cortébert"
	438	"Orvin"
	443	"Saint-Imier"
	446	"Tramelan"
	556	"Zielebach"
	576	"Grindelwald"
	581	"Interlaken"
	619	"Oberdiessbach"
	662	"Ferenbalm"
	667	"Laupen"
	682	"Bévilard"
	687	"Corcelles (BE)"
	690	"Court"
	699	"Monible"
	700	"Moutier"
	703	"Reconvilier"
	723	"La Neuveville"
	784	"Innertkirchen"
	843	"Saanen"
	853	"Rüschegg"
	902	"Langnau im Emmental"
	905	"Rüderswil"
	929	"Hilterfingen"
	1003	"Escholzmatt"
	1061	"Luzern"
	1149	"Willisau Stadt"
	1301	"Einsiedeln"
	1402	"Engelberg"
	1509	"Stans"
	1618	"Mühlehorn"
	1709	"Unterägeri"
	1711	"Zug"
	2001	"Aumont"
	2011	"Cugy (FR)"
	2035	"Nuvilly"
	2046	"Vesin"
	2080	"Hennens"
	2096	"Romont (FR)"
	2102	"Ursy"
	2113	"Vuisternens-devant-Romont"
	2121	"Haut-Intyamon"
	2124	"Broc"
	2125	"Bulle"
	2135	"Gruyères"
	2147	"Pont-la-Ville"
	2175	"Belfaux"
	2184	"Corpataux"
	2196	"Fribourg"
	2210	"Montévraz"
	2217	"Ponthaux"
	2220	"Le Mouret"
	2254	"Courtepin"
	2265	"Kerzers"
	2293	"Düdingen"
	2296	"Heitenried"
	2299	"Plaffeien"
	2323	"Bossonnens"
	2325	"Châtel-Saint-Denis"
	2326	"Le Crêt"
	2336	"Semsales"
	2546	"Grenchen"
	2581	"Olten"
	2582	"Rickenbach (SO)"
	2601	"Solothurn"
	2701	"Basel"
	2802	"Allschwil"
	2825	"Füllinsdorf"
	2829	"Liestal"
	2888	"Langenbruck"
	2937	"Neuhausen am Rheinfall"
	2964	"Stein am Rhein"
	3001	"Herisau"
	3021	"Bühler"
	3111	"Oberegg"
	3203	"St. Gallen"
	3215	"Rorschach"
	3231	"Au (SG)"
	3233	"Berneck"
	3235	"Rheineck"
	3296	"Sargans"
	3392	"Kirchberg (SG)"
	3393	"Lütisburg"
	3406	"Mogelsberg"
	3561	"Poschiavo"
	3787	"St. Moritz"
	3832	"Grono"
	3901	"Chur"
	3952	"Jenins"
	4021	"Baden"
	4063	"Bremgarten (AG)"
	4082	"Wohlen (AG)"
	4172	"Münchwilen (AG)"
	4258	"Rheinfelden"
	4315	"Rekingen (AG)"
	4401	"Arbon"
	4405	"<none>"
	4461	"Amriswil"
	4545	"Diessenhofen"
	4641	"Altnau"
	4941	"Märstetten"
	5002	"Bellinzona"
	5040	"Ludiano"
	5192	"Lugano"
	5210	"Paradiso"
	5222	"Sessa"
	5401	"Aigle"
	5402	"Bex"
	5405	"Gryon"
	5406	"Lavey-Morcles"
	5408	"Noville"
	5409	"Ollon"
	5410	"Ormont-Dessous"
	5411	"Ormont-Dessus"
	5412	"Rennaz"
	5422	"Aubonne"
	5434	"Saint-George"
	5455	"Constantine"
	5456	"<none>"
	5477	"Cossonay"
	5484	"Gollion"
	5492	"Montricher"
	5495	"Penthalaz"
	5524	"Goumoens-la-Ville"
	5531	"Penthéréaz"
	5554	"Concise"
	5561	"Grandson"
	5568	"Sainte-Croix"
	5586	"Lausanne"
	5590	"Pully"
	5591	"Renens (VD)"
	5601	"Chexbres"
	5602	"Cully"
	5604	"Forel (Lavaux)"
	5605	"Grandvaux"
	5607	"Puidoux"
	5621	"Aclens"
	5633	"Echandens"
	5635	"Ecublens (VD)"
	5642	"Morges"
	5688	"Syens"
	5703	"Bassins"
	5711	"Commugny"
	5712	"Coppet"
	5713	"Crans-près-Céligny"
	5714	"Crassier"
	5718	"Genolier"
	5721	"Gland"
	5722	"Grens"
	5723	"Mies"
	5724	"Nyon"
	5725	"Prangins"
	5727	"Saint-Cergue"
	5743	"Arnex-sur-Orbe"
	5744	"Ballaigues"
	5745	"Baulmes"
	5750	"Les Clées"
	5754	"Juriens"
	5757	"Orbe"
	5764	"Vallorbe"
	5765	"Vaulion"
	5789	"Ferlens (VD)"
	5792	"Montpreveyres"
	5793	"Oron-la-Ville"
	5816	"Corcelles-près-Payerne"
	5841	"Château-d'Oex"
	5853	"Bursins"
	5854	"Burtigny"
	5857	"Gilly"
	5859	"Mont-sur-Rolle"
	5861	"Rolle"
	5881	"Blonay"
	5886	"Montreux"
	5889	"La Tour-de-Peilz"
	5890	"Vevey"
	5928	"Rovray"
	5934	"Valeyres-sous-Ursins"
	5938	"Yverdon-les-Bains"
	6002	"Brig-Glis"
	6006	"Mund"
	6007	"Naters"
	6008	"Ried-Brig"
	6009	"Simplon"
	6021	"Ardon"
	6022	"Chamoson"
	6023	"Conthey"
	6024	"Nendaz"
	6025	"Vétroz"
	6028	"-- code à corriger --"
	6031	"Bagnes"
	6032	"Bourg-Saint-Pierre"
	6034	"Orsières"
	6053	"Biel (VS)"
	6081	"Les Agettes"
	6082	"Ayent"
	6083	"Evolène"
	6084	"Hérémence"
	6085	"Mase"
	6086	"Nax"
	6087	"Saint-Martin (VS)"
	6088	"Vernamiège"
	6089	"Vex"
	6101	"Agarn"
	6105	"Erschmatt"
	6106	"Feschel"
	6110	"Leuk"
	6111	"Leukerbad"
	6113	"Salgesch"
	6116	"Varen"
	6133	"Fully"
	6134	"Isérables"
	6135	"Leytron"
	6136	"Martigny"
	6139	"Riddes"
	6140	"Saillon"
	6141	"Saxon"
	6142	"Trient"
	6151	"Champéry"
	6152	"Collombey-Muraz"
	6153	"Monthey"
	6155	"Saint-Gingolph"
	6156	"Troistorrents"
	6159	"Vouvry"
	6173	"Bitsch"
	6175	"Goppisberg"
	6179	"Mörel"
	6191	"Ausserberg"
	6195	"Ferden"
	6200	"Steg"
	6213	"Evionnaz"
	6217	"Saint-Maurice"
	6219	"Vernayaz"
	6231	"Ayer"
	6232	"Chalais"
	6233	"Chandolin"
	6234	"Chermignon"
	6235	"Chippis"
	6237	"Grimentz"
	6238	"Grône"
	6239	"Icogne"
	6240	"Lens"
	6241	"Miège"
	6242	"Mollens (VS)"
	6243	"Montana"
	6244	"Randogne"
	6245	"Saint-Jean"
	6246	"Saint-Léonard"
	6247	"Saint-Luc"
	6248	"Sierre"
	6249	"Venthône"
	6250	"Veyras"
	6251	"Vissoie"
	6261	"Arbaz"
	6263	"Grimisuat"
	6264	"Salins"
	6265	"Savièse"
	6266	"Sion"
	6267	"Veysonnaz"
	6285	"Grächen"
	6293	"Stalden (VS)"
	6297	"Visp"
	6402	"Bevaix"
	6407	"Corcelles-Cormondrèche"
	6408	"Cortaillod"
	6413	"Rochefort"
	6421	"La Chaux-de-Fonds"
	6423	"La Sagne"
	6431	"Les Brenets"
	6436	"Le Locle"
	6455	"Le Landeron"
	6458	"Neuchâtel"
	6459	"Saint-Blaise"
	6475	"Dombresson"
	6505	"Couvet"
	6506	"Fleurier"
	6511	"Les Verrières"
	6601	"Aire-la-Ville"
	6602	"Anières"
	6603	"Avully"
	6604	"Avusy"
	6606	"Bellevue"
	6607	"Bernex"
	6608	"Carouge (GE)"
	6609	"Cartigny"
	6610	"Céligny"
	6612	"Chêne-Bougeries"
	6613	"Chêne-Bourg"
	6614	"Choulex"
	6615	"Collex-Bossy"
	6616	"Collonge-Bellerive"
	6617	"Cologny"
	6618	"Confignon"
	6619	"Corsier (GE)"
	6620	"Dardagny"
	6621	"Genève"
	6622	"Genthod"
	6623	"Le Grand-Saconnex"
	6625	"Hermance"
	6626	"Jussy"
	6627	"Laconnex"
	6628	"Lancy"
	6629	"Meinier"
	6630	"Meyrin"
	6631	"Onex"
	6632	"Perly-Certoux"
	6633	"Plan-les-Ouates"
	6634	"Pregny-Chambésy"
	6635	"Presinge"
	6638	"Satigny"
	6639	"Soral"
	6640	"Thônex"
	6641	"Troinex"
	6642	"Vandoeuvres"
	6643	"Vernier"
	6644	"Versoix"
	6645	"Veyrier"
	6707	"Courfaivre"
	6708	"Courrendlin"
	6711	"Delémont"
	6727	"Vicques"
	6757	"Saignelégier"
	6775	"Bonfol"
	6782	"Cornol"
	6783	"Courchavon"
	6800	"Porrentruy"
	6804	"Saint-Ursanne"
	7101	"Albanie"
	7102	"Belgique"
	7103	"Bulgarie"
	7105	"Allemagne"
	7107	"France"
	7108	"Royaume-Uni"
	7111	"Italie"
	7112	"Yougoslavie"
	7115	"Pays-Bas"
	7117	"Autriche"
	7118	"Pologne"
	7119	"Portugal"
	7120	"Roumanie"
	7121	"Suède"
	7122	"Suisse"
	7123	"Espagne"
	7124	"Tchécoslovaquie"
	7127	"Hongrie"
	7129	"Grèce"
	7130	"Monaco"
	7140	"Russie"
	7141	"Ukraine"
	7150	"Ain, Haute-Savoie"
	7209	"Inde"
	7210	"Indonésie"
	7212	"Iran"
	7213	"Israël"
	7224	"Liban"
	7231	"Philippines"
	7236	"Thaïlande"
	7300	"Afrique"
	7301	"Egypte"
	7302	"Algérie"
	7312	"Djibouti"
	7326	"Libye"
	7330	"Maroc"
	7340	"Sénégal"
	7344	"Afrique du Sud"
	7351	"Tunisie"
	7401	"Argentine"
	7410	"Equateur"
	7418	"Canada"
	7420	"Cuba"
	7421	"Mexique"
	7425	"Pérou"
	7432	"Etats-Unis"
	7691	"Nouvelle-Calédonie"
	7801	"Canton de Zurich"
	7802	"Canton de Berne"
	7803	"Canton de Lucerne"
	7810	"Canton de Fribourg"
	7811	"Canton de Soleure"
	7812	"Canton de Bâle-Ville"
	7813	"Canton de Bâle-Campagne"
	7814	"Canton de Schaffhouse"
	7817	"Canton se Saint-Gall"
	7818	"Canton des Grisons"
	7819	"Canton d'Argovie"
	7820	"Canton de Thurgovie"
	7821	"Canton du Tessin"
	7822	"Canton de Vaud"
	7823	"Canton du Valais"
	7824	"Canton de Neuchâtel"
	7825	"Canton de Genève"
	7826	"Canton du Jura"
	8000	"Autre".
VALUE LABELS j34a
	-7	"INAP"
	-2	"NR"
	1	"Féminin"
	2	"Masculin".
VALUE LABELS j34b
	-7	"INAP"
	-2	"NR".
VALUE LABELS j34c
	-9	"NV"
	-7	"INAP"
	-2	"NR"
	84	"Dällikon"
	102	"Weiach"
	114	"Fischenthal"
	121	"Wetzikon (ZH)"
	133	"Horgen"
	135	"Kilchberg (ZH)"
	141	"Thalwil"
	154	"Küsnacht (ZH)"
	191	"Dübendorf"
	230	"Winterthur"
	253	"Zürich"
	306	"Lyss"
	351	"Bern"
	355	"Köniz"
	356	"Muri bei Bern"
	371	"Biel‎/Bienne"
	418	"Oberburg"
	433	"Cortébert"
	438	"Orvin"
	443	"Saint-Imier"
	446	"Tramelan"
	556	"Zielebach"
	576	"Grindelwald"
	581	"Interlaken"
	619	"Oberdiessbach"
	662	"Ferenbalm"
	667	"Laupen"
	682	"Bévilard"
	687	"Corcelles (BE)"
	690	"Court"
	699	"Monible"
	700	"Moutier"
	703	"Reconvilier"
	723	"La Neuveville"
	784	"Innertkirchen"
	843	"Saanen"
	853	"Rüschegg"
	902	"Langnau im Emmental"
	905	"Rüderswil"
	929	"Hilterfingen"
	1003	"Escholzmatt"
	1061	"Luzern"
	1149	"Willisau Stadt"
	1301	"Einsiedeln"
	1402	"Engelberg"
	1509	"Stans"
	1618	"Mühlehorn"
	1709	"Unterägeri"
	1711	"Zug"
	2001	"Aumont"
	2011	"Cugy (FR)"
	2035	"Nuvilly"
	2046	"Vesin"
	2080	"Hennens"
	2096	"Romont (FR)"
	2102	"Ursy"
	2113	"Vuisternens-devant-Romont"
	2121	"Haut-Intyamon"
	2124	"Broc"
	2125	"Bulle"
	2135	"Gruyères"
	2147	"Pont-la-Ville"
	2175	"Belfaux"
	2184	"Corpataux"
	2196	"Fribourg"
	2210	"Montévraz"
	2217	"Ponthaux"
	2220	"Le Mouret"
	2254	"Courtepin"
	2265	"Kerzers"
	2293	"Düdingen"
	2296	"Heitenried"
	2299	"Plaffeien"
	2323	"Bossonnens"
	2325	"Châtel-Saint-Denis"
	2326	"Le Crêt"
	2336	"Semsales"
	2546	"Grenchen"
	2581	"Olten"
	2582	"Rickenbach (SO)"
	2601	"Solothurn"
	2701	"Basel"
	2802	"Allschwil"
	2825	"Füllinsdorf"
	2829	"Liestal"
	2888	"Langenbruck"
	2937	"Neuhausen am Rheinfall"
	2964	"Stein am Rhein"
	3001	"Herisau"
	3021	"Bühler"
	3111	"Oberegg"
	3203	"St. Gallen"
	3215	"Rorschach"
	3231	"Au (SG)"
	3233	"Berneck"
	3235	"Rheineck"
	3296	"Sargans"
	3392	"Kirchberg (SG)"
	3393	"Lütisburg"
	3406	"Mogelsberg"
	3561	"Poschiavo"
	3787	"St. Moritz"
	3832	"Grono"
	3901	"Chur"
	3952	"Jenins"
	4021	"Baden"
	4063	"Bremgarten (AG)"
	4082	"Wohlen (AG)"
	4172	"Münchwilen (AG)"
	4258	"Rheinfelden"
	4315	"Rekingen (AG)"
	4401	"Arbon"
	4405	"<none>"
	4461	"Amriswil"
	4545	"Diessenhofen"
	4641	"Altnau"
	4941	"Märstetten"
	5002	"Bellinzona"
	5040	"Ludiano"
	5192	"Lugano"
	5210	"Paradiso"
	5222	"Sessa"
	5401	"Aigle"
	5402	"Bex"
	5405	"Gryon"
	5406	"Lavey-Morcles"
	5408	"Noville"
	5409	"Ollon"
	5410	"Ormont-Dessous"
	5411	"Ormont-Dessus"
	5412	"Rennaz"
	5422	"Aubonne"
	5434	"Saint-George"
	5455	"Constantine"
	5456	"<none>"
	5477	"Cossonay"
	5484	"Gollion"
	5492	"Montricher"
	5495	"Penthalaz"
	5524	"Goumoens-la-Ville"
	5531	"Penthéréaz"
	5554	"Concise"
	5561	"Grandson"
	5568	"Sainte-Croix"
	5586	"Lausanne"
	5590	"Pully"
	5591	"Renens (VD)"
	5601	"Chexbres"
	5602	"Cully"
	5604	"Forel (Lavaux)"
	5605	"Grandvaux"
	5607	"Puidoux"
	5621	"Aclens"
	5633	"Echandens"
	5635	"Ecublens (VD)"
	5642	"Morges"
	5688	"Syens"
	5703	"Bassins"
	5711	"Commugny"
	5712	"Coppet"
	5713	"Crans-près-Céligny"
	5714	"Crassier"
	5718	"Genolier"
	5721	"Gland"
	5722	"Grens"
	5723	"Mies"
	5724	"Nyon"
	5725	"Prangins"
	5727	"Saint-Cergue"
	5743	"Arnex-sur-Orbe"
	5744	"Ballaigues"
	5745	"Baulmes"
	5750	"Les Clées"
	5754	"Juriens"
	5757	"Orbe"
	5764	"Vallorbe"
	5765	"Vaulion"
	5789	"Ferlens (VD)"
	5792	"Montpreveyres"
	5793	"Oron-la-Ville"
	5816	"Corcelles-près-Payerne"
	5841	"Château-d'Oex"
	5853	"Bursins"
	5854	"Burtigny"
	5857	"Gilly"
	5859	"Mont-sur-Rolle"
	5861	"Rolle"
	5881	"Blonay"
	5886	"Montreux"
	5889	"La Tour-de-Peilz"
	5890	"Vevey"
	5928	"Rovray"
	5934	"Valeyres-sous-Ursins"
	5938	"Yverdon-les-Bains"
	6002	"Brig-Glis"
	6006	"Mund"
	6007	"Naters"
	6008	"Ried-Brig"
	6009	"Simplon"
	6021	"Ardon"
	6022	"Chamoson"
	6023	"Conthey"
	6024	"Nendaz"
	6025	"Vétroz"
	6028	"-- code à corriger --"
	6031	"Bagnes"
	6032	"Bourg-Saint-Pierre"
	6034	"Orsières"
	6053	"Biel (VS)"
	6081	"Les Agettes"
	6082	"Ayent"
	6083	"Evolène"
	6084	"Hérémence"
	6085	"Mase"
	6086	"Nax"
	6087	"Saint-Martin (VS)"
	6088	"Vernamiège"
	6089	"Vex"
	6101	"Agarn"
	6105	"Erschmatt"
	6106	"Feschel"
	6110	"Leuk"
	6111	"Leukerbad"
	6113	"Salgesch"
	6116	"Varen"
	6133	"Fully"
	6134	"Isérables"
	6135	"Leytron"
	6136	"Martigny"
	6139	"Riddes"
	6140	"Saillon"
	6141	"Saxon"
	6142	"Trient"
	6151	"Champéry"
	6152	"Collombey-Muraz"
	6153	"Monthey"
	6155	"Saint-Gingolph"
	6156	"Troistorrents"
	6159	"Vouvry"
	6173	"Bitsch"
	6175	"Goppisberg"
	6179	"Mörel"
	6191	"Ausserberg"
	6195	"Ferden"
	6200	"Steg"
	6213	"Evionnaz"
	6217	"Saint-Maurice"
	6219	"Vernayaz"
	6231	"Ayer"
	6232	"Chalais"
	6233	"Chandolin"
	6234	"Chermignon"
	6235	"Chippis"
	6237	"Grimentz"
	6238	"Grône"
	6239	"Icogne"
	6240	"Lens"
	6241	"Miège"
	6242	"Mollens (VS)"
	6243	"Montana"
	6244	"Randogne"
	6245	"Saint-Jean"
	6246	"Saint-Léonard"
	6247	"Saint-Luc"
	6248	"Sierre"
	6249	"Venthône"
	6250	"Veyras"
	6251	"Vissoie"
	6261	"Arbaz"
	6263	"Grimisuat"
	6264	"Salins"
	6265	"Savièse"
	6266	"Sion"
	6267	"Veysonnaz"
	6285	"Grächen"
	6293	"Stalden (VS)"
	6297	"Visp"
	6402	"Bevaix"
	6407	"Corcelles-Cormondrèche"
	6408	"Cortaillod"
	6413	"Rochefort"
	6421	"La Chaux-de-Fonds"
	6423	"La Sagne"
	6431	"Les Brenets"
	6436	"Le Locle"
	6455	"Le Landeron"
	6458	"Neuchâtel"
	6459	"Saint-Blaise"
	6475	"Dombresson"
	6505	"Couvet"
	6506	"Fleurier"
	6511	"Les Verrières"
	6601	"Aire-la-Ville"
	6602	"Anières"
	6603	"Avully"
	6604	"Avusy"
	6606	"Bellevue"
	6607	"Bernex"
	6608	"Carouge (GE)"
	6609	"Cartigny"
	6610	"Céligny"
	6612	"Chêne-Bougeries"
	6613	"Chêne-Bourg"
	6614	"Choulex"
	6615	"Collex-Bossy"
	6616	"Collonge-Bellerive"
	6617	"Cologny"
	6618	"Confignon"
	6619	"Corsier (GE)"
	6620	"Dardagny"
	6621	"Genève"
	6622	"Genthod"
	6623	"Le Grand-Saconnex"
	6625	"Hermance"
	6626	"Jussy"
	6627	"Laconnex"
	6628	"Lancy"
	6629	"Meinier"
	6630	"Meyrin"
	6631	"Onex"
	6632	"Perly-Certoux"
	6633	"Plan-les-Ouates"
	6634	"Pregny-Chambésy"
	6635	"Presinge"
	6638	"Satigny"
	6639	"Soral"
	6640	"Thônex"
	6641	"Troinex"
	6642	"Vandoeuvres"
	6643	"Vernier"
	6644	"Versoix"
	6645	"Veyrier"
	6707	"Courfaivre"
	6708	"Courrendlin"
	6711	"Delémont"
	6727	"Vicques"
	6757	"Saignelégier"
	6775	"Bonfol"
	6782	"Cornol"
	6783	"Courchavon"
	6800	"Porrentruy"
	6804	"Saint-Ursanne"
	7101	"Albanie"
	7102	"Belgique"
	7103	"Bulgarie"
	7105	"Allemagne"
	7107	"France"
	7108	"Royaume-Uni"
	7111	"Italie"
	7112	"Yougoslavie"
	7115	"Pays-Bas"
	7117	"Autriche"
	7118	"Pologne"
	7119	"Portugal"
	7120	"Roumanie"
	7121	"Suède"
	7122	"Suisse"
	7123	"Espagne"
	7124	"Tchécoslovaquie"
	7127	"Hongrie"
	7129	"Grèce"
	7130	"Monaco"
	7140	"Russie"
	7141	"Ukraine"
	7150	"Ain, Haute-Savoie"
	7209	"Inde"
	7210	"Indonésie"
	7212	"Iran"
	7213	"Israël"
	7224	"Liban"
	7231	"Philippines"
	7236	"Thaïlande"
	7300	"Afrique"
	7301	"Egypte"
	7302	"Algérie"
	7312	"Djibouti"
	7326	"Libye"
	7330	"Maroc"
	7340	"Sénégal"
	7344	"Afrique du Sud"
	7351	"Tunisie"
	7401	"Argentine"
	7410	"Equateur"
	7418	"Canada"
	7420	"Cuba"
	7421	"Mexique"
	7425	"Pérou"
	7432	"Etats-Unis"
	7691	"Nouvelle-Calédonie"
	7801	"Canton de Zurich"
	7802	"Canton de Berne"
	7803	"Canton de Lucerne"
	7810	"Canton de Fribourg"
	7811	"Canton de Soleure"
	7812	"Canton de Bâle-Ville"
	7813	"Canton de Bâle-Campagne"
	7814	"Canton de Schaffhouse"
	7817	"Canton se Saint-Gall"
	7818	"Canton des Grisons"
	7819	"Canton d'Argovie"
	7820	"Canton de Thurgovie"
	7821	"Canton du Tessin"
	7822	"Canton de Vaud"
	7823	"Canton du Valais"
	7824	"Canton de Neuchâtel"
	7825	"Canton de Genève"
	7826	"Canton du Jura"
	8000	"Autre".
VALUE LABELS j45a
	-7	"INAP"
	-2	"NR"
	1	"Féminin"
	2	"Masculin".
VALUE LABELS j45b
	-7	"INAP"
	-2	"NR".
VALUE LABELS j45c
	-9	"NV"
	-7	"INAP"
	-2	"NR"
	84	"Dällikon"
	102	"Weiach"
	114	"Fischenthal"
	121	"Wetzikon (ZH)"
	133	"Horgen"
	135	"Kilchberg (ZH)"
	141	"Thalwil"
	154	"Küsnacht (ZH)"
	191	"Dübendorf"
	230	"Winterthur"
	253	"Zürich"
	306	"Lyss"
	351	"Bern"
	355	"Köniz"
	356	"Muri bei Bern"
	371	"Biel‎/Bienne"
	418	"Oberburg"
	433	"Cortébert"
	438	"Orvin"
	443	"Saint-Imier"
	446	"Tramelan"
	556	"Zielebach"
	576	"Grindelwald"
	581	"Interlaken"
	619	"Oberdiessbach"
	662	"Ferenbalm"
	667	"Laupen"
	682	"Bévilard"
	687	"Corcelles (BE)"
	690	"Court"
	699	"Monible"
	700	"Moutier"
	703	"Reconvilier"
	723	"La Neuveville"
	784	"Innertkirchen"
	843	"Saanen"
	853	"Rüschegg"
	902	"Langnau im Emmental"
	905	"Rüderswil"
	929	"Hilterfingen"
	1003	"Escholzmatt"
	1061	"Luzern"
	1149	"Willisau Stadt"
	1301	"Einsiedeln"
	1402	"Engelberg"
	1509	"Stans"
	1618	"Mühlehorn"
	1709	"Unterägeri"
	1711	"Zug"
	2001	"Aumont"
	2011	"Cugy (FR)"
	2035	"Nuvilly"
	2046	"Vesin"
	2080	"Hennens"
	2096	"Romont (FR)"
	2102	"Ursy"
	2113	"Vuisternens-devant-Romont"
	2121	"Haut-Intyamon"
	2124	"Broc"
	2125	"Bulle"
	2135	"Gruyères"
	2147	"Pont-la-Ville"
	2175	"Belfaux"
	2184	"Corpataux"
	2196	"Fribourg"
	2210	"Montévraz"
	2217	"Ponthaux"
	2220	"Le Mouret"
	2254	"Courtepin"
	2265	"Kerzers"
	2293	"Düdingen"
	2296	"Heitenried"
	2299	"Plaffeien"
	2323	"Bossonnens"
	2325	"Châtel-Saint-Denis"
	2326	"Le Crêt"
	2336	"Semsales"
	2546	"Grenchen"
	2581	"Olten"
	2582	"Rickenbach (SO)"
	2601	"Solothurn"
	2701	"Basel"
	2802	"Allschwil"
	2825	"Füllinsdorf"
	2829	"Liestal"
	2888	"Langenbruck"
	2937	"Neuhausen am Rheinfall"
	2964	"Stein am Rhein"
	3001	"Herisau"
	3021	"Bühler"
	3111	"Oberegg"
	3203	"St. Gallen"
	3215	"Rorschach"
	3231	"Au (SG)"
	3233	"Berneck"
	3235	"Rheineck"
	3296	"Sargans"
	3392	"Kirchberg (SG)"
	3393	"Lütisburg"
	3406	"Mogelsberg"
	3561	"Poschiavo"
	3787	"St. Moritz"
	3832	"Grono"
	3901	"Chur"
	3952	"Jenins"
	4021	"Baden"
	4063	"Bremgarten (AG)"
	4082	"Wohlen (AG)"
	4172	"Münchwilen (AG)"
	4258	"Rheinfelden"
	4315	"Rekingen (AG)"
	4401	"Arbon"
	4405	"<none>"
	4461	"Amriswil"
	4545	"Diessenhofen"
	4641	"Altnau"
	4941	"Märstetten"
	5002	"Bellinzona"
	5040	"Ludiano"
	5192	"Lugano"
	5210	"Paradiso"
	5222	"Sessa"
	5401	"Aigle"
	5402	"Bex"
	5405	"Gryon"
	5406	"Lavey-Morcles"
	5408	"Noville"
	5409	"Ollon"
	5410	"Ormont-Dessous"
	5411	"Ormont-Dessus"
	5412	"Rennaz"
	5422	"Aubonne"
	5434	"Saint-George"
	5455	"Constantine"
	5456	"<none>"
	5477	"Cossonay"
	5484	"Gollion"
	5492	"Montricher"
	5495	"Penthalaz"
	5524	"Goumoens-la-Ville"
	5531	"Penthéréaz"
	5554	"Concise"
	5561	"Grandson"
	5568	"Sainte-Croix"
	5586	"Lausanne"
	5590	"Pully"
	5591	"Renens (VD)"
	5601	"Chexbres"
	5602	"Cully"
	5604	"Forel (Lavaux)"
	5605	"Grandvaux"
	5607	"Puidoux"
	5621	"Aclens"
	5633	"Echandens"
	5635	"Ecublens (VD)"
	5642	"Morges"
	5688	"Syens"
	5703	"Bassins"
	5711	"Commugny"
	5712	"Coppet"
	5713	"Crans-près-Céligny"
	5714	"Crassier"
	5718	"Genolier"
	5721	"Gland"
	5722	"Grens"
	5723	"Mies"
	5724	"Nyon"
	5725	"Prangins"
	5727	"Saint-Cergue"
	5743	"Arnex-sur-Orbe"
	5744	"Ballaigues"
	5745	"Baulmes"
	5750	"Les Clées"
	5754	"Juriens"
	5757	"Orbe"
	5764	"Vallorbe"
	5765	"Vaulion"
	5789	"Ferlens (VD)"
	5792	"Montpreveyres"
	5793	"Oron-la-Ville"
	5816	"Corcelles-près-Payerne"
	5841	"Château-d'Oex"
	5853	"Bursins"
	5854	"Burtigny"
	5857	"Gilly"
	5859	"Mont-sur-Rolle"
	5861	"Rolle"
	5881	"Blonay"
	5886	"Montreux"
	5889	"La Tour-de-Peilz"
	5890	"Vevey"
	5928	"Rovray"
	5934	"Valeyres-sous-Ursins"
	5938	"Yverdon-les-Bains"
	6002	"Brig-Glis"
	6006	"Mund"
	6007	"Naters"
	6008	"Ried-Brig"
	6009	"Simplon"
	6021	"Ardon"
	6022	"Chamoson"
	6023	"Conthey"
	6024	"Nendaz"
	6025	"Vétroz"
	6028	"-- code à corriger --"
	6031	"Bagnes"
	6032	"Bourg-Saint-Pierre"
	6034	"Orsières"
	6053	"Biel (VS)"
	6081	"Les Agettes"
	6082	"Ayent"
	6083	"Evolène"
	6084	"Hérémence"
	6085	"Mase"
	6086	"Nax"
	6087	"Saint-Martin (VS)"
	6088	"Vernamiège"
	6089	"Vex"
	6101	"Agarn"
	6105	"Erschmatt"
	6106	"Feschel"
	6110	"Leuk"
	6111	"Leukerbad"
	6113	"Salgesch"
	6116	"Varen"
	6133	"Fully"
	6134	"Isérables"
	6135	"Leytron"
	6136	"Martigny"
	6139	"Riddes"
	6140	"Saillon"
	6141	"Saxon"
	6142	"Trient"
	6151	"Champéry"
	6152	"Collombey-Muraz"
	6153	"Monthey"
	6155	"Saint-Gingolph"
	6156	"Troistorrents"
	6159	"Vouvry"
	6173	"Bitsch"
	6175	"Goppisberg"
	6179	"Mörel"
	6191	"Ausserberg"
	6195	"Ferden"
	6200	"Steg"
	6213	"Evionnaz"
	6217	"Saint-Maurice"
	6219	"Vernayaz"
	6231	"Ayer"
	6232	"Chalais"
	6233	"Chandolin"
	6234	"Chermignon"
	6235	"Chippis"
	6237	"Grimentz"
	6238	"Grône"
	6239	"Icogne"
	6240	"Lens"
	6241	"Miège"
	6242	"Mollens (VS)"
	6243	"Montana"
	6244	"Randogne"
	6245	"Saint-Jean"
	6246	"Saint-Léonard"
	6247	"Saint-Luc"
	6248	"Sierre"
	6249	"Venthône"
	6250	"Veyras"
	6251	"Vissoie"
	6261	"Arbaz"
	6263	"Grimisuat"
	6264	"Salins"
	6265	"Savièse"
	6266	"Sion"
	6267	"Veysonnaz"
	6285	"Grächen"
	6293	"Stalden (VS)"
	6297	"Visp"
	6402	"Bevaix"
	6407	"Corcelles-Cormondrèche"
	6408	"Cortaillod"
	6413	"Rochefort"
	6421	"La Chaux-de-Fonds"
	6423	"La Sagne"
	6431	"Les Brenets"
	6436	"Le Locle"
	6455	"Le Landeron"
	6458	"Neuchâtel"
	6459	"Saint-Blaise"
	6475	"Dombresson"
	6505	"Couvet"
	6506	"Fleurier"
	6511	"Les Verrières"
	6601	"Aire-la-Ville"
	6602	"Anières"
	6603	"Avully"
	6604	"Avusy"
	6606	"Bellevue"
	6607	"Bernex"
	6608	"Carouge (GE)"
	6609	"Cartigny"
	6610	"Céligny"
	6612	"Chêne-Bougeries"
	6613	"Chêne-Bourg"
	6614	"Choulex"
	6615	"Collex-Bossy"
	6616	"Collonge-Bellerive"
	6617	"Cologny"
	6618	"Confignon"
	6619	"Corsier (GE)"
	6620	"Dardagny"
	6621	"Genève"
	6622	"Genthod"
	6623	"Le Grand-Saconnex"
	6625	"Hermance"
	6626	"Jussy"
	6627	"Laconnex"
	6628	"Lancy"
	6629	"Meinier"
	6630	"Meyrin"
	6631	"Onex"
	6632	"Perly-Certoux"
	6633	"Plan-les-Ouates"
	6634	"Pregny-Chambésy"
	6635	"Presinge"
	6638	"Satigny"
	6639	"Soral"
	6640	"Thônex"
	6641	"Troinex"
	6642	"Vandoeuvres"
	6643	"Vernier"
	6644	"Versoix"
	6645	"Veyrier"
	6707	"Courfaivre"
	6708	"Courrendlin"
	6711	"Delémont"
	6727	"Vicques"
	6757	"Saignelégier"
	6775	"Bonfol"
	6782	"Cornol"
	6783	"Courchavon"
	6800	"Porrentruy"
	6804	"Saint-Ursanne"
	7101	"Albanie"
	7102	"Belgique"
	7103	"Bulgarie"
	7105	"Allemagne"
	7107	"France"
	7108	"Royaume-Uni"
	7111	"Italie"
	7112	"Yougoslavie"
	7115	"Pays-Bas"
	7117	"Autriche"
	7118	"Pologne"
	7119	"Portugal"
	7120	"Roumanie"
	7121	"Suède"
	7122	"Suisse"
	7123	"Espagne"
	7124	"Tchécoslovaquie"
	7127	"Hongrie"
	7129	"Grèce"
	7130	"Monaco"
	7140	"Russie"
	7141	"Ukraine"
	7150	"Ain, Haute-Savoie"
	7209	"Inde"
	7210	"Indonésie"
	7212	"Iran"
	7213	"Israël"
	7224	"Liban"
	7231	"Philippines"
	7236	"Thaïlande"
	7300	"Afrique"
	7301	"Egypte"
	7302	"Algérie"
	7312	"Djibouti"
	7326	"Libye"
	7330	"Maroc"
	7340	"Sénégal"
	7344	"Afrique du Sud"
	7351	"Tunisie"
	7401	"Argentine"
	7410	"Equateur"
	7418	"Canada"
	7420	"Cuba"
	7421	"Mexique"
	7425	"Pérou"
	7432	"Etats-Unis"
	7691	"Nouvelle-Calédonie"
	7801	"Canton de Zurich"
	7802	"Canton de Berne"
	7803	"Canton de Lucerne"
	7810	"Canton de Fribourg"
	7811	"Canton de Soleure"
	7812	"Canton de Bâle-Ville"
	7813	"Canton de Bâle-Campagne"
	7814	"Canton de Schaffhouse"
	7817	"Canton se Saint-Gall"
	7818	"Canton des Grisons"
	7819	"Canton d'Argovie"
	7820	"Canton de Thurgovie"
	7821	"Canton du Tessin"
	7822	"Canton de Vaud"
	7823	"Canton du Valais"
	7824	"Canton de Neuchâtel"
	7825	"Canton de Genève"
	7826	"Canton du Jura"
	8000	"Autre".
VALUE LABELS j26
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS j115a
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115b
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115c
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115d
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115e
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115f
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115g
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115h
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115i
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115j
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115k
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j115m
	-7	"INAP"
	-2	"NR"
	2	"Toujours, souvent"
	3	"Rarement, jamais".
VALUE LABELS j172a
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS j172b
	-7	"INAP"
	-2	"NR"
	1	"Non ou presque jamais"
	2	"Oui, au moins une fois par an"
	3	"Oui, au moins une fois par mois"
	4	"Oui, au moins une fois par semaine"
	5	"Oui, tous les jours ou presque".
VALUE LABELS dpd
	-7	"INAP"
	-2	"NR"
	1	"Conjoint/e"
	2	"Enfant"
	3	"Frère/soeur"
	4	"Autre membre de la famille"
	5	"Ami/e"
	6	"Voisin/e"
	7	"Employé, garde, infirmière, autre professionnel"
	8	"Autre".
VALUE LABELS dpe
	-7	"INAP"
	-2	"NR"
	1	"Même appartement/villa"
	2	"Même bâtiment/ensemble résidentiel mais pas même appartement"
	3	"Même quartier/village mais pas même bâtiment"
	4	"Même commune (mais pas même quartier)"
	5	"Même canton (mais pas même commune)"
	6	"Autre".
VALUE LABELS dpf
	-7	"INAP"
	-2	"NR"
	1	"Moin de 3 mois"
	2	"3-6 mois"
	3	"6 mois - un an"
	4	"Plus d'un an"
	5	"Plus de trois ans"
	6	"Plus de 10 ans".
VALUE LABELS dpg
	-7	"INAP"
	-2	"NR"
	1	"Tous les jours ou presque"
	2	"Un jour sur deux"
	3	"Au moins une fois par semaine"
	4	"Au moins une fois tous les 15 jours"
	5	"Moins".
VALUE LABELS dph
	-7	"INAP"
	-2	"NR"
	1	"Exerce une profession"
	2	"S'occupe de son foyer"
	3	"Retraité/e"
	4	"Autre".
VALUE LABELS dpi
	-7	"INAP"
	-2	"NR"
	1	"Célibataire"
	2	"Marié/e"
	3	"Divorcé/e, séparé/e"
	4	"Veuve‎/veuf".
VALUE LABELS dpl
	-7	"INAP"
	-2	"NR".
VALUE LABELS d1_12
	-7	"INAP"
	-2	"NR"
	1	"Fils"
	2	"Fille".
VALUE LABELS d1_13
	-7	"INAP"
	-2	"NR".
VALUE LABELS d1_14
	-7	"INAP"
	-2	"NR"
	1	"Célibataire"
	2	"Marié‎/e, remarié‎/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS d1_15
	-7	"INAP"
	-2	"NR".
VALUE LABELS d1_18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS d1_19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS d1_21
	-7	"INAP"
	-2	"NR"
	1	"Même appartement/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement/villa séparé/e"
	3	"Même quartier/village, (mais pas le même ensemble résidentiel)"
	4	"Même commune, (mais pas le même quartier/village)"
	5	"Même canton, (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS d2_12
	-7	"INAP"
	-2	"NR"
	1	"Fils"
	2	"Fille".
VALUE LABELS d2_13
	-7	"INAP"
	-2	"NR".
VALUE LABELS d2_14
	-7	"INAP"
	-2	"NR"
	1	"Célibataire"
	2	"Marié‎/e, remarié‎/e"
	3	"Divorcé‎/e, séparé‎/e"
	4	"Veuf, veuve".
VALUE LABELS d2_15
	-7	"INAP"
	-2	"NR".
VALUE LABELS d2_18
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS d2_19
	-7	"INAP"
	-2	"NR"
	1	"Activité professionnelle rémunérée"
	2	"Actuellement au chômage"
	3	"S'occupe principalement de son foyer"
	4	"Études"
	8	"Autre".
VALUE LABELS d2_21
	-7	"INAP"
	-2	"NR"
	1	"Même appartement/villa que vous"
	2	"Même bâtiment ou ensemble résidentiel mais appartement/villa séparé/e"
	3	"Même quartier/village, (mais pas le même ensemble résidentiel)"
	4	"Même commune, (mais pas le même quartier/village)"
	5	"Même canton, (mais pas la même commune)"
	6	"Autre canton"
	7	"Autre pays".
VALUE LABELS dx7
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui, enfant 1"
	2	"Oui, enfant 2".
VALUE LABELS d26
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS d91
	-7	"INAP"
	-2	"NR"
	1	"Moins de 2 ans"
	2	"Entre 2 et 5 ans"
	3	"Plus de 5 ans"
	4	"Plus de 10 ans".
VALUE LABELS d93
	-9	"NV"
	-7	"INAP"
	-2	"NR"
	84	"Dällikon"
	102	"Weiach"
	114	"Fischenthal"
	121	"Wetzikon (ZH)"
	133	"Horgen"
	135	"Kilchberg (ZH)"
	141	"Thalwil"
	154	"Küsnacht (ZH)"
	191	"Dübendorf"
	230	"Winterthur"
	253	"Zürich"
	306	"Lyss"
	351	"Bern"
	355	"Köniz"
	356	"Muri bei Bern"
	371	"Biel‎/Bienne"
	418	"Oberburg"
	433	"Cortébert"
	438	"Orvin"
	443	"Saint-Imier"
	446	"Tramelan"
	556	"Zielebach"
	576	"Grindelwald"
	581	"Interlaken"
	619	"Oberdiessbach"
	662	"Ferenbalm"
	667	"Laupen"
	682	"Bévilard"
	687	"Corcelles (BE)"
	690	"Court"
	699	"Monible"
	700	"Moutier"
	703	"Reconvilier"
	723	"La Neuveville"
	784	"Innertkirchen"
	843	"Saanen"
	853	"Rüschegg"
	902	"Langnau im Emmental"
	905	"Rüderswil"
	929	"Hilterfingen"
	1003	"Escholzmatt"
	1061	"Luzern"
	1149	"Willisau Stadt"
	1301	"Einsiedeln"
	1402	"Engelberg"
	1509	"Stans"
	1618	"Mühlehorn"
	1709	"Unterägeri"
	1711	"Zug"
	2001	"Aumont"
	2011	"Cugy (FR)"
	2035	"Nuvilly"
	2046	"Vesin"
	2080	"Hennens"
	2096	"Romont (FR)"
	2102	"Ursy"
	2113	"Vuisternens-devant-Romont"
	2121	"Haut-Intyamon"
	2124	"Broc"
	2125	"Bulle"
	2135	"Gruyères"
	2147	"Pont-la-Ville"
	2175	"Belfaux"
	2184	"Corpataux"
	2196	"Fribourg"
	2210	"Montévraz"
	2217	"Ponthaux"
	2220	"Le Mouret"
	2254	"Courtepin"
	2265	"Kerzers"
	2293	"Düdingen"
	2296	"Heitenried"
	2299	"Plaffeien"
	2323	"Bossonnens"
	2325	"Châtel-Saint-Denis"
	2326	"Le Crêt"
	2336	"Semsales"
	2546	"Grenchen"
	2581	"Olten"
	2582	"Rickenbach (SO)"
	2601	"Solothurn"
	2701	"Basel"
	2802	"Allschwil"
	2825	"Füllinsdorf"
	2829	"Liestal"
	2888	"Langenbruck"
	2937	"Neuhausen am Rheinfall"
	2964	"Stein am Rhein"
	3001	"Herisau"
	3021	"Bühler"
	3111	"Oberegg"
	3203	"St. Gallen"
	3215	"Rorschach"
	3231	"Au (SG)"
	3233	"Berneck"
	3235	"Rheineck"
	3296	"Sargans"
	3392	"Kirchberg (SG)"
	3393	"Lütisburg"
	3406	"Mogelsberg"
	3561	"Poschiavo"
	3787	"St. Moritz"
	3832	"Grono"
	3901	"Chur"
	3952	"Jenins"
	4021	"Baden"
	4063	"Bremgarten (AG)"
	4082	"Wohlen (AG)"
	4172	"Münchwilen (AG)"
	4258	"Rheinfelden"
	4315	"Rekingen (AG)"
	4401	"Arbon"
	4405	"<none>"
	4461	"Amriswil"
	4545	"Diessenhofen"
	4641	"Altnau"
	4941	"Märstetten"
	5002	"Bellinzona"
	5040	"Ludiano"
	5192	"Lugano"
	5210	"Paradiso"
	5222	"Sessa"
	5401	"Aigle"
	5402	"Bex"
	5405	"Gryon"
	5406	"Lavey-Morcles"
	5408	"Noville"
	5409	"Ollon"
	5410	"Ormont-Dessous"
	5411	"Ormont-Dessus"
	5412	"Rennaz"
	5422	"Aubonne"
	5434	"Saint-George"
	5455	"Constantine"
	5456	"<none>"
	5477	"Cossonay"
	5484	"Gollion"
	5492	"Montricher"
	5495	"Penthalaz"
	5524	"Goumoens-la-Ville"
	5531	"Penthéréaz"
	5554	"Concise"
	5561	"Grandson"
	5568	"Sainte-Croix"
	5586	"Lausanne"
	5590	"Pully"
	5591	"Renens (VD)"
	5601	"Chexbres"
	5602	"Cully"
	5604	"Forel (Lavaux)"
	5605	"Grandvaux"
	5607	"Puidoux"
	5621	"Aclens"
	5633	"Echandens"
	5635	"Ecublens (VD)"
	5642	"Morges"
	5688	"Syens"
	5703	"Bassins"
	5711	"Commugny"
	5712	"Coppet"
	5713	"Crans-près-Céligny"
	5714	"Crassier"
	5718	"Genolier"
	5721	"Gland"
	5722	"Grens"
	5723	"Mies"
	5724	"Nyon"
	5725	"Prangins"
	5727	"Saint-Cergue"
	5743	"Arnex-sur-Orbe"
	5744	"Ballaigues"
	5745	"Baulmes"
	5750	"Les Clées"
	5754	"Juriens"
	5757	"Orbe"
	5764	"Vallorbe"
	5765	"Vaulion"
	5789	"Ferlens (VD)"
	5792	"Montpreveyres"
	5793	"Oron-la-Ville"
	5816	"Corcelles-près-Payerne"
	5841	"Château-d'Oex"
	5853	"Bursins"
	5854	"Burtigny"
	5857	"Gilly"
	5859	"Mont-sur-Rolle"
	5861	"Rolle"
	5881	"Blonay"
	5886	"Montreux"
	5889	"La Tour-de-Peilz"
	5890	"Vevey"
	5928	"Rovray"
	5934	"Valeyres-sous-Ursins"
	5938	"Yverdon-les-Bains"
	6002	"Brig-Glis"
	6006	"Mund"
	6007	"Naters"
	6008	"Ried-Brig"
	6009	"Simplon"
	6021	"Ardon"
	6022	"Chamoson"
	6023	"Conthey"
	6024	"Nendaz"
	6025	"Vétroz"
	6028	"-- code à corriger --"
	6031	"Bagnes"
	6032	"Bourg-Saint-Pierre"
	6034	"Orsières"
	6053	"Biel (VS)"
	6081	"Les Agettes"
	6082	"Ayent"
	6083	"Evolène"
	6084	"Hérémence"
	6085	"Mase"
	6086	"Nax"
	6087	"Saint-Martin (VS)"
	6088	"Vernamiège"
	6089	"Vex"
	6101	"Agarn"
	6105	"Erschmatt"
	6106	"Feschel"
	6110	"Leuk"
	6111	"Leukerbad"
	6113	"Salgesch"
	6116	"Varen"
	6133	"Fully"
	6134	"Isérables"
	6135	"Leytron"
	6136	"Martigny"
	6139	"Riddes"
	6140	"Saillon"
	6141	"Saxon"
	6142	"Trient"
	6151	"Champéry"
	6152	"Collombey-Muraz"
	6153	"Monthey"
	6155	"Saint-Gingolph"
	6156	"Troistorrents"
	6159	"Vouvry"
	6173	"Bitsch"
	6175	"Goppisberg"
	6179	"Mörel"
	6191	"Ausserberg"
	6195	"Ferden"
	6200	"Steg"
	6213	"Evionnaz"
	6217	"Saint-Maurice"
	6219	"Vernayaz"
	6231	"Ayer"
	6232	"Chalais"
	6233	"Chandolin"
	6234	"Chermignon"
	6235	"Chippis"
	6237	"Grimentz"
	6238	"Grône"
	6239	"Icogne"
	6240	"Lens"
	6241	"Miège"
	6242	"Mollens (VS)"
	6243	"Montana"
	6244	"Randogne"
	6245	"Saint-Jean"
	6246	"Saint-Léonard"
	6247	"Saint-Luc"
	6248	"Sierre"
	6249	"Venthône"
	6250	"Veyras"
	6251	"Vissoie"
	6261	"Arbaz"
	6263	"Grimisuat"
	6264	"Salins"
	6265	"Savièse"
	6266	"Sion"
	6267	"Veysonnaz"
	6285	"Grächen"
	6293	"Stalden (VS)"
	6297	"Visp"
	6402	"Bevaix"
	6407	"Corcelles-Cormondrèche"
	6408	"Cortaillod"
	6413	"Rochefort"
	6421	"La Chaux-de-Fonds"
	6423	"La Sagne"
	6431	"Les Brenets"
	6436	"Le Locle"
	6455	"Le Landeron"
	6458	"Neuchâtel"
	6459	"Saint-Blaise"
	6475	"Dombresson"
	6505	"Couvet"
	6506	"Fleurier"
	6511	"Les Verrières"
	6601	"Aire-la-Ville"
	6602	"Anières"
	6603	"Avully"
	6604	"Avusy"
	6606	"Bellevue"
	6607	"Bernex"
	6608	"Carouge (GE)"
	6609	"Cartigny"
	6610	"Céligny"
	6612	"Chêne-Bougeries"
	6613	"Chêne-Bourg"
	6614	"Choulex"
	6615	"Collex-Bossy"
	6616	"Collonge-Bellerive"
	6617	"Cologny"
	6618	"Confignon"
	6619	"Corsier (GE)"
	6620	"Dardagny"
	6621	"Genève"
	6622	"Genthod"
	6623	"Le Grand-Saconnex"
	6625	"Hermance"
	6626	"Jussy"
	6627	"Laconnex"
	6628	"Lancy"
	6629	"Meinier"
	6630	"Meyrin"
	6631	"Onex"
	6632	"Perly-Certoux"
	6633	"Plan-les-Ouates"
	6634	"Pregny-Chambésy"
	6635	"Presinge"
	6638	"Satigny"
	6639	"Soral"
	6640	"Thônex"
	6641	"Troinex"
	6642	"Vandoeuvres"
	6643	"Vernier"
	6644	"Versoix"
	6645	"Veyrier"
	6707	"Courfaivre"
	6708	"Courrendlin"
	6711	"Delémont"
	6727	"Vicques"
	6757	"Saignelégier"
	6775	"Bonfol"
	6782	"Cornol"
	6783	"Courchavon"
	6800	"Porrentruy"
	6804	"Saint-Ursanne"
	7101	"Albanie"
	7102	"Belgique"
	7103	"Bulgarie"
	7105	"Allemagne"
	7107	"France"
	7108	"Royaume-Uni"
	7111	"Italie"
	7112	"Yougoslavie"
	7115	"Pays-Bas"
	7117	"Autriche"
	7118	"Pologne"
	7119	"Portugal"
	7120	"Roumanie"
	7121	"Suède"
	7122	"Suisse"
	7123	"Espagne"
	7124	"Tchécoslovaquie"
	7127	"Hongrie"
	7129	"Grèce"
	7130	"Monaco"
	7140	"Russie"
	7141	"Ukraine"
	7150	"Ain, Haute-Savoie"
	7209	"Inde"
	7210	"Indonésie"
	7212	"Iran"
	7213	"Israël"
	7224	"Liban"
	7231	"Philippines"
	7236	"Thaïlande"
	7300	"Afrique"
	7301	"Egypte"
	7302	"Algérie"
	7312	"Djibouti"
	7326	"Libye"
	7330	"Maroc"
	7340	"Sénégal"
	7344	"Afrique du Sud"
	7351	"Tunisie"
	7401	"Argentine"
	7410	"Equateur"
	7418	"Canada"
	7420	"Cuba"
	7421	"Mexique"
	7425	"Pérou"
	7432	"Etats-Unis"
	7691	"Nouvelle-Calédonie"
	7801	"Canton de Zurich"
	7802	"Canton de Berne"
	7803	"Canton de Lucerne"
	7810	"Canton de Fribourg"
	7811	"Canton de Soleure"
	7812	"Canton de Bâle-Ville"
	7813	"Canton de Bâle-Campagne"
	7814	"Canton de Schaffhouse"
	7817	"Canton se Saint-Gall"
	7818	"Canton des Grisons"
	7819	"Canton d'Argovie"
	7820	"Canton de Thurgovie"
	7821	"Canton du Tessin"
	7822	"Canton de Vaud"
	7823	"Canton du Valais"
	7824	"Canton de Neuchâtel"
	7825	"Canton de Genève"
	7826	"Canton du Jura"
	8000	"Autre".
VALUE LABELS d96
	-7	"INAP"
	-2	"NR"
	1	"Oui"
	2	"Oui, en partie"
	3	"Non".
VALUE LABELS dx8a
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS dx8b
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS dx8c
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS dx8d
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS dx8e
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS dx8f
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS dx8g
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS dx8h
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS senfviv
	-7	"INAP".
VALUE LABELS sansfam
	-2	"NR"
	1	"Sans famille".
VALUE LABELS snfratri
	-7	"INAP".
VALUE LABELS lgenfviv
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS lgfratri
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS ptsenfan
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS frersoeu
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS parents
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS bparents
	-7	"INAP"
	0	"Non"
	1	"Oui".
VALUE LABELS agemere
	-7	"INAP"
	-2	"NR".
VALUE LABELS agepere
	-7	"INAP"
	-2	"NR".
VALUE LABELS agebmere
	-7	"INAP"
	-2	"NR".
VALUE LABELS agebpere
	-7	"INAP"
	-2	"NR".
VALUE LABELS ageascd
	-7	"INAP"
	-2	"NR".
VALUE LABELS ageascal
	-7	"INAP"
	-2	"NR".
VALUE LABELS agmari1
	-9	"NV"
	-7	"INAP"
	-2	"NR".
VALUE LABELS agveuv1
	-7	"INAP"
	-2	"NR".
VALUE LABELS pondrep5
	-7	"INAP"
	0	"Non"
	1	"Oui".

* Définition de la mesure des variables

VARIABLE LEVEL icode(NOMINAL).
VARIABLE LEVEL typeq(NOMINAL).
VARIABLE LEVEL canton(NOMINAL).
VARIABLE LEVEL sexe(NOMINAL).
VARIABLE LEVEL clage(ORDINAL).
VARIABLE LEVEL dateentr(NOMINAL).
VARIABLE LEVEL tb1(ORDINAL).
VARIABLE LEVEL tb2(NOMINAL).
VARIABLE LEVEL nb3(NOMINAL).
VARIABLE LEVEL tb4(NOMINAL).
VARIABLE LEVEL mb5(ORDINAL).
VARIABLE LEVEL mb6a(ORDINAL).
VARIABLE LEVEL mb6b(ORDINAL).
VARIABLE LEVEL mb7a(ORDINAL).
VARIABLE LEVEL mb7b(ORDINAL).
VARIABLE LEVEL mb8a(ORDINAL).
VARIABLE LEVEL mb8b(ORDINAL).
VARIABLE LEVEL mb9(ORDINAL).
VARIABLE LEVEL sb10(NOMINAL).
VARIABLE LEVEL sb11(SCALE).
VARIABLE LEVEL mb12(NOMINAL).
VARIABLE LEVEL mb13a(ORDINAL).
VARIABLE LEVEL sb13b(ORDINAL).
VARIABLE LEVEL mb14(NOMINAL).
VARIABLE LEVEL mb15(SCALE).
VARIABLE LEVEL b16(SCALE).
VARIABLE LEVEL mb17(SCALE).
VARIABLE LEVEL sb18(NOMINAL).
VARIABLE LEVEL sb19(NOMINAL).
VARIABLE LEVEL sb20(NOMINAL).
VARIABLE LEVEL sb21(ORDINAL).
VARIABLE LEVEL b22(ORDINAL).
VARIABLE LEVEL mb23(NOMINAL).
VARIABLE LEVEL mb24a(ORDINAL).
VARIABLE LEVEL sb24b(ORDINAL).
VARIABLE LEVEL mb25(NOMINAL).
VARIABLE LEVEL mb26(SCALE).
VARIABLE LEVEL b27(SCALE).
VARIABLE LEVEL mb28(SCALE).
VARIABLE LEVEL sb29(NOMINAL).
VARIABLE LEVEL sb30(NOMINAL).
VARIABLE LEVEL sb31(NOMINAL).
VARIABLE LEVEL sb32(ORDINAL).
VARIABLE LEVEL b33(ORDINAL).
VARIABLE LEVEL mb34(NOMINAL).
VARIABLE LEVEL mb35a(ORDINAL).
VARIABLE LEVEL sb35b(ORDINAL).
VARIABLE LEVEL mb36(NOMINAL).
VARIABLE LEVEL mb37(SCALE).
VARIABLE LEVEL b38(SCALE).
VARIABLE LEVEL mb39(SCALE).
VARIABLE LEVEL sb40(NOMINAL).
VARIABLE LEVEL sb41(NOMINAL).
VARIABLE LEVEL sb42(NOMINAL).
VARIABLE LEVEL sb43(ORDINAL).
VARIABLE LEVEL b44(ORDINAL).
VARIABLE LEVEL mb45(NOMINAL).
VARIABLE LEVEL mb46a(ORDINAL).
VARIABLE LEVEL sb46b(ORDINAL).
VARIABLE LEVEL mb47(NOMINAL).
VARIABLE LEVEL mb48(SCALE).
VARIABLE LEVEL b49(SCALE).
VARIABLE LEVEL mb50(SCALE).
VARIABLE LEVEL sb51(NOMINAL).
VARIABLE LEVEL sb52(NOMINAL).
VARIABLE LEVEL sb53(NOMINAL).
VARIABLE LEVEL sb54(ORDINAL).
VARIABLE LEVEL b55(ORDINAL).
VARIABLE LEVEL b56a(NOMINAL).
VARIABLE LEVEL b56b(ORDINAL).
VARIABLE LEVEL b56c(ORDINAL).
VARIABLE LEVEL b57(NOMINAL).
VARIABLE LEVEL b58(NOMINAL).
VARIABLE LEVEL b59a(NOMINAL).
VARIABLE LEVEL b59b(NOMINAL).
VARIABLE LEVEL b59c(NOMINAL).
VARIABLE LEVEL b59d(NOMINAL).
VARIABLE LEVEL b59e(NOMINAL).
VARIABLE LEVEL b59f(NOMINAL).
VARIABLE LEVEL b60(ORDINAL).
VARIABLE LEVEL b61(ORDINAL).
VARIABLE LEVEL b62a(NOMINAL).
VARIABLE LEVEL b62b(ORDINAL).
VARIABLE LEVEL b62c(ORDINAL).
VARIABLE LEVEL b63(NOMINAL).
VARIABLE LEVEL b64(NOMINAL).
VARIABLE LEVEL b65a(NOMINAL).
VARIABLE LEVEL b65b(NOMINAL).
VARIABLE LEVEL b65c(NOMINAL).
VARIABLE LEVEL b65d(NOMINAL).
VARIABLE LEVEL b65e(NOMINAL).
VARIABLE LEVEL b65f(NOMINAL).
VARIABLE LEVEL b66(ORDINAL).
VARIABLE LEVEL b67(ORDINAL).
VARIABLE LEVEL b68a(NOMINAL).
VARIABLE LEVEL b68b(ORDINAL).
VARIABLE LEVEL b68c(ORDINAL).
VARIABLE LEVEL b69(NOMINAL).
VARIABLE LEVEL b70(NOMINAL).
VARIABLE LEVEL b71a(NOMINAL).
VARIABLE LEVEL b71b(NOMINAL).
VARIABLE LEVEL b71c(NOMINAL).
VARIABLE LEVEL b71d(NOMINAL).
VARIABLE LEVEL b71e(NOMINAL).
VARIABLE LEVEL b71f(NOMINAL).
VARIABLE LEVEL b72(ORDINAL).
VARIABLE LEVEL b73(ORDINAL).
VARIABLE LEVEL b74a(NOMINAL).
VARIABLE LEVEL b74b(ORDINAL).
VARIABLE LEVEL b74c(ORDINAL).
VARIABLE LEVEL b75(NOMINAL).
VARIABLE LEVEL b76(NOMINAL).
VARIABLE LEVEL b77a(NOMINAL).
VARIABLE LEVEL b77b(NOMINAL).
VARIABLE LEVEL b77c(NOMINAL).
VARIABLE LEVEL b77d(NOMINAL).
VARIABLE LEVEL b77e(NOMINAL).
VARIABLE LEVEL b77f(NOMINAL).
VARIABLE LEVEL b78(ORDINAL).
VARIABLE LEVEL b79(ORDINAL).
VARIABLE LEVEL sb80(SCALE).
VARIABLE LEVEL sb81a(SCALE).
VARIABLE LEVEL sb81b(SCALE).
VARIABLE LEVEL sb81ab(SCALE).
VARIABLE LEVEL sb82(ORDINAL).
VARIABLE LEVEL sb83a(SCALE).
VARIABLE LEVEL mb83b(SCALE).
VARIABLE LEVEL mb83c(SCALE).
VARIABLE LEVEL mb83d(SCALE).
VARIABLE LEVEL mb83e(SCALE).
VARIABLE LEVEL mb83f(SCALE).
VARIABLE LEVEL mb83g(SCALE).
VARIABLE LEVEL sb84(NOMINAL).
VARIABLE LEVEL pb85(NOMINAL).
VARIABLE LEVEL pb86(NOMINAL).
VARIABLE LEVEL pb87(SCALE).
VARIABLE LEVEL pb88a(NOMINAL).
VARIABLE LEVEL pb88b(NOMINAL).
VARIABLE LEVEL q88c(NOMINAL).
VARIABLE LEVEL pb88d(NOMINAL).
VARIABLE LEVEL pb88e(NOMINAL).
VARIABLE LEVEL pb88f(NOMINAL).
VARIABLE LEVEL pb88g(NOMINAL).
VARIABLE LEVEL sb89(NOMINAL).
VARIABLE LEVEL sb90(NOMINAL).
VARIABLE LEVEL sb91(ORDINAL).
VARIABLE LEVEL sb92(ORDINAL).
VARIABLE LEVEL sb93(NOMINAL).
VARIABLE LEVEL pb94a(NOMINAL).
VARIABLE LEVEL pb94b(NOMINAL).
VARIABLE LEVEL pb94c(NOMINAL).
VARIABLE LEVEL pb94d(NOMINAL).
VARIABLE LEVEL pb94e(NOMINAL).
VARIABLE LEVEL pb94f(NOMINAL).
VARIABLE LEVEL pb94g(NOMINAL).
VARIABLE LEVEL pb94h(NOMINAL).
VARIABLE LEVEL pb94i(NOMINAL).
VARIABLE LEVEL pb94j(NOMINAL).
VARIABLE LEVEL sb94k(NOMINAL).
VARIABLE LEVEL pb95(ORDINAL).
VARIABLE LEVEL mb96a(ORDINAL).
VARIABLE LEVEL mb96b(ORDINAL).
VARIABLE LEVEL mb96c(ORDINAL).
VARIABLE LEVEL mb96d(ORDINAL).
VARIABLE LEVEL mb96e(ORDINAL).
VARIABLE LEVEL mb96f(ORDINAL).
VARIABLE LEVEL mb97a(ORDINAL).
VARIABLE LEVEL mb97b(ORDINAL).
VARIABLE LEVEL sb97c(ORDINAL).
VARIABLE LEVEL pb98a(NOMINAL).
VARIABLE LEVEL pb98b(NOMINAL).
VARIABLE LEVEL pb98c(NOMINAL).
VARIABLE LEVEL pb98d(NOMINAL).
VARIABLE LEVEL pb98e(NOMINAL).
VARIABLE LEVEL pb98f(NOMINAL).
VARIABLE LEVEL pb98g(NOMINAL).
VARIABLE LEVEL pb98h(NOMINAL).
VARIABLE LEVEL sb98i(NOMINAL).
VARIABLE LEVEL pb98j(NOMINAL).
VARIABLE LEVEL sb99(NOMINAL).
VARIABLE LEVEL sb100a(NOMINAL).
VARIABLE LEVEL sb100b(NOMINAL).
VARIABLE LEVEL sb100c(NOMINAL).
VARIABLE LEVEL sb100d(NOMINAL).
VARIABLE LEVEL sb100e(NOMINAL).
VARIABLE LEVEL sb100f(NOMINAL).
VARIABLE LEVEL sb100g(NOMINAL).
VARIABLE LEVEL sb100h(NOMINAL).
VARIABLE LEVEL sb100i(NOMINAL).
VARIABLE LEVEL sb100j(NOMINAL).
VARIABLE LEVEL sb100k(NOMINAL).
VARIABLE LEVEL sb101a(NOMINAL).
VARIABLE LEVEL sb101b(NOMINAL).
VARIABLE LEVEL sb101c(NOMINAL).
VARIABLE LEVEL sb101d(NOMINAL).
VARIABLE LEVEL sb101e(NOMINAL).
VARIABLE LEVEL sb101f(NOMINAL).
VARIABLE LEVEL sb101g(NOMINAL).
VARIABLE LEVEL sb101h(NOMINAL).
VARIABLE LEVEL sb101i(NOMINAL).
VARIABLE LEVEL sb101j(NOMINAL).
VARIABLE LEVEL sb101k(NOMINAL).
VARIABLE LEVEL sb102a(NOMINAL).
VARIABLE LEVEL sb102b(NOMINAL).
VARIABLE LEVEL sb102c(NOMINAL).
VARIABLE LEVEL sb102d(NOMINAL).
VARIABLE LEVEL tb103(ORDINAL).
VARIABLE LEVEL sb104(ORDINAL).
VARIABLE LEVEL tb105(ORDINAL).
VARIABLE LEVEL tb106(ORDINAL).
VARIABLE LEVEL mb107a(NOMINAL).
VARIABLE LEVEL mb107b(NOMINAL).
VARIABLE LEVEL mb107c(NOMINAL).
VARIABLE LEVEL mb107d(NOMINAL).
VARIABLE LEVEL mb107e(NOMINAL).
VARIABLE LEVEL mb107f(NOMINAL).
VARIABLE LEVEL mb108a(NOMINAL).
VARIABLE LEVEL mb108b(NOMINAL).
VARIABLE LEVEL mb108c(NOMINAL).
VARIABLE LEVEL mb108d(NOMINAL).
VARIABLE LEVEL mb108e(NOMINAL).
VARIABLE LEVEL mb108f(NOMINAL).
VARIABLE LEVEL mb109a(NOMINAL).
VARIABLE LEVEL mb109b(NOMINAL).
VARIABLE LEVEL mb109c(NOMINAL).
VARIABLE LEVEL mb109d(NOMINAL).
VARIABLE LEVEL mb109e(NOMINAL).
VARIABLE LEVEL mb109f(NOMINAL).
VARIABLE LEVEL mb109g(NOMINAL).
VARIABLE LEVEL mb109h(NOMINAL).
VARIABLE LEVEL mb110a(NOMINAL).
VARIABLE LEVEL mb110b(NOMINAL).
VARIABLE LEVEL mb110c(NOMINAL).
VARIABLE LEVEL mb110d(NOMINAL).
VARIABLE LEVEL mb110e(NOMINAL).
VARIABLE LEVEL mb110f(NOMINAL).
VARIABLE LEVEL mb110g(NOMINAL).
VARIABLE LEVEL mb110h(NOMINAL).
VARIABLE LEVEL mb110i(NOMINAL).
VARIABLE LEVEL sb111(NOMINAL).
VARIABLE LEVEL sb112(ORDINAL).
VARIABLE LEVEL sb113(NOMINAL).
VARIABLE LEVEL sb113a(NOMINAL).
VARIABLE LEVEL sb113b(NOMINAL).
VARIABLE LEVEL sb113c(NOMINAL).
VARIABLE LEVEL sb113d(NOMINAL).
VARIABLE LEVEL sb114(NOMINAL).
VARIABLE LEVEL b115(ORDINAL).
VARIABLE LEVEL b116a(NOMINAL).
VARIABLE LEVEL b116b(NOMINAL).
VARIABLE LEVEL b116c(NOMINAL).
VARIABLE LEVEL b116d(NOMINAL).
VARIABLE LEVEL tb117a(ORDINAL).
VARIABLE LEVEL tb117b(ORDINAL).
VARIABLE LEVEL tb117c(ORDINAL).
VARIABLE LEVEL tb117d(NOMINAL).
VARIABLE LEVEL mb118a(NOMINAL).
VARIABLE LEVEL mb118b(NOMINAL).
VARIABLE LEVEL mb118c(NOMINAL).
VARIABLE LEVEL mb119a(NOMINAL).
VARIABLE LEVEL mb119b(NOMINAL).
VARIABLE LEVEL mb119c(NOMINAL).
VARIABLE LEVEL b120(NOMINAL).
VARIABLE LEVEL b121(ORDINAL).
VARIABLE LEVEL b122(NOMINAL).
VARIABLE LEVEL b123(ORDINAL).
VARIABLE LEVEL mb124(NOMINAL).
VARIABLE LEVEL b126(NOMINAL).
VARIABLE LEVEL b127(SCALE).
VARIABLE LEVEL b128(ORDINAL).
VARIABLE LEVEL b129(NOMINAL).
VARIABLE LEVEL b130(ORDINAL).
VARIABLE LEVEL b131(ORDINAL).
VARIABLE LEVEL tb132aa(ORDINAL).
VARIABLE LEVEL tb132ab(ORDINAL).
VARIABLE LEVEL tb132ac(ORDINAL).
VARIABLE LEVEL tb132ad(ORDINAL).
VARIABLE LEVEL tb132ae(ORDINAL).
VARIABLE LEVEL tb132af(ORDINAL).
VARIABLE LEVEL tb132ag(ORDINAL).
VARIABLE LEVEL tb132ah(ORDINAL).
VARIABLE LEVEL tb132ai(ORDINAL).
VARIABLE LEVEL tb132aj(ORDINAL).
VARIABLE LEVEL tb132ak(ORDINAL).
VARIABLE LEVEL sb132ba(ORDINAL).
VARIABLE LEVEL sb132bb(ORDINAL).
VARIABLE LEVEL sb132bc(ORDINAL).
VARIABLE LEVEL sb132bd(ORDINAL).
VARIABLE LEVEL sb132be(ORDINAL).
VARIABLE LEVEL sb132bf(ORDINAL).
VARIABLE LEVEL sb132bg(ORDINAL).
VARIABLE LEVEL sb132bh(ORDINAL).
VARIABLE LEVEL sb132bi(ORDINAL).
VARIABLE LEVEL sb132bj(ORDINAL).
VARIABLE LEVEL sb132bk(ORDINAL).
VARIABLE LEVEL tb133(NOMINAL).
VARIABLE LEVEL tb134(ORDINAL).
VARIABLE LEVEL tb135(ORDINAL).
VARIABLE LEVEL tb136(NOMINAL).
VARIABLE LEVEL tb137(ORDINAL).
VARIABLE LEVEL tb138(ORDINAL).
VARIABLE LEVEL tb139(ORDINAL).
VARIABLE LEVEL tb140(NOMINAL).
VARIABLE LEVEL tb141(NOMINAL).
VARIABLE LEVEL sb142(NOMINAL).
VARIABLE LEVEL tb143(SCALE).
VARIABLE LEVEL nbmedic(ORDINAL).
VARIABLE LEVEL mb144(ORDINAL).
VARIABLE LEVEL sb146(NOMINAL).
VARIABLE LEVEL sb147a(SCALE).
VARIABLE LEVEL sb147b(SCALE).
VARIABLE LEVEL sb147c(SCALE).
VARIABLE LEVEL sb147d(SCALE).
VARIABLE LEVEL sb148(NOMINAL).
VARIABLE LEVEL tb149(NOMINAL).
VARIABLE LEVEL tb150(NOMINAL).
VARIABLE LEVEL tb151(NOMINAL).
VARIABLE LEVEL tb152(NOMINAL).
VARIABLE LEVEL tb153(NOMINAL).
VARIABLE LEVEL tb154(NOMINAL).
VARIABLE LEVEL tb155(NOMINAL).
VARIABLE LEVEL tb156(NOMINAL).
VARIABLE LEVEL tb157(NOMINAL).
VARIABLE LEVEL tb158(NOMINAL).
VARIABLE LEVEL tb159(NOMINAL).
VARIABLE LEVEL sb160(NOMINAL).
VARIABLE LEVEL sb161a(NOMINAL).
VARIABLE LEVEL sb161b(NOMINAL).
VARIABLE LEVEL sb161c(NOMINAL).
VARIABLE LEVEL sb161d(NOMINAL).
VARIABLE LEVEL sb161e(NOMINAL).
VARIABLE LEVEL sb161f(NOMINAL).
VARIABLE LEVEL sb161g(NOMINAL).
VARIABLE LEVEL sb161h(NOMINAL).
VARIABLE LEVEL sb161i(NOMINAL).
VARIABLE LEVEL sb161j(NOMINAL).
VARIABLE LEVEL sb162a(NOMINAL).
VARIABLE LEVEL sb162b(NOMINAL).
VARIABLE LEVEL sb162c(NOMINAL).
VARIABLE LEVEL sb162d(NOMINAL).
VARIABLE LEVEL sb162e(NOMINAL).
VARIABLE LEVEL sb162f(NOMINAL).
VARIABLE LEVEL sb162g(NOMINAL).
VARIABLE LEVEL mb163(ORDINAL).
VARIABLE LEVEL mb164(ORDINAL).
VARIABLE LEVEL mb165(NOMINAL).
VARIABLE LEVEL mb166(ORDINAL).
VARIABLE LEVEL mb167(ORDINAL).
VARIABLE LEVEL sb168a(NOMINAL).
VARIABLE LEVEL sb168b(NOMINAL).
VARIABLE LEVEL pb169aa(NOMINAL).
VARIABLE LEVEL pb169ab(NOMINAL).
VARIABLE LEVEL pb169ac(NOMINAL).
VARIABLE LEVEL pb169ad(NOMINAL).
VARIABLE LEVEL pb169ae(NOMINAL).
VARIABLE LEVEL pb169af(NOMINAL).
VARIABLE LEVEL pb169ag(NOMINAL).
VARIABLE LEVEL pb169ah(NOMINAL).
VARIABLE LEVEL pb169ba(ORDINAL).
VARIABLE LEVEL pb169bb(ORDINAL).
VARIABLE LEVEL pb169bc(ORDINAL).
VARIABLE LEVEL pb169bd(ORDINAL).
VARIABLE LEVEL pb169be(ORDINAL).
VARIABLE LEVEL pb169bf(ORDINAL).
VARIABLE LEVEL pb169bg(ORDINAL).
VARIABLE LEVEL pb169bh(ORDINAL).
VARIABLE LEVEL sb170aa(NOMINAL).
VARIABLE LEVEL sb170ab(NOMINAL).
VARIABLE LEVEL sb170ac(NOMINAL).
VARIABLE LEVEL sb170ad(NOMINAL).
VARIABLE LEVEL sb170ae(NOMINAL).
VARIABLE LEVEL sb170af(NOMINAL).
VARIABLE LEVEL sb170ag(NOMINAL).
VARIABLE LEVEL sb170ah(NOMINAL).
VARIABLE LEVEL sb170ba(ORDINAL).
VARIABLE LEVEL sb170bb(ORDINAL).
VARIABLE LEVEL sb170bc(ORDINAL).
VARIABLE LEVEL sb170bd(ORDINAL).
VARIABLE LEVEL sb170be(ORDINAL).
VARIABLE LEVEL sb170bf(ORDINAL).
VARIABLE LEVEL sb170bg(ORDINAL).
VARIABLE LEVEL sb170bh(ORDINAL).
VARIABLE LEVEL mb171a(ORDINAL).
VARIABLE LEVEL mb171b(ORDINAL).
VARIABLE LEVEL mb171c(ORDINAL).
VARIABLE LEVEL mb171d(ORDINAL).
VARIABLE LEVEL mb171e(ORDINAL).
VARIABLE LEVEL mb171f(ORDINAL).
VARIABLE LEVEL tb172a(ORDINAL).
VARIABLE LEVEL mb172b(ORDINAL).
VARIABLE LEVEL lb172c(ORDINAL).
VARIABLE LEVEL mb172d(ORDINAL).
VARIABLE LEVEL mb172e(ORDINAL).
VARIABLE LEVEL mb172f(ORDINAL).
VARIABLE LEVEL tb173a(ORDINAL).
VARIABLE LEVEL mb173b(ORDINAL).
VARIABLE LEVEL lb173c(ORDINAL).
VARIABLE LEVEL mb173d(ORDINAL).
VARIABLE LEVEL mb173e(ORDINAL).
VARIABLE LEVEL b174(NOMINAL).
VARIABLE LEVEL sb175a(NOMINAL).
VARIABLE LEVEL sb175b(NOMINAL).
VARIABLE LEVEL sb175c(NOMINAL).
VARIABLE LEVEL sb175d(NOMINAL).
VARIABLE LEVEL sb175e(NOMINAL).
VARIABLE LEVEL sb176aa(NOMINAL).
VARIABLE LEVEL sb176ba(NOMINAL).
VARIABLE LEVEL sb176ca(NOMINAL).
VARIABLE LEVEL sb176ab(NOMINAL).
VARIABLE LEVEL sb176bb(NOMINAL).
VARIABLE LEVEL sb176cb(NOMINAL).
VARIABLE LEVEL sb176ac(NOMINAL).
VARIABLE LEVEL sb176bc(NOMINAL).
VARIABLE LEVEL sb176cc(NOMINAL).
VARIABLE LEVEL sb176ad(NOMINAL).
VARIABLE LEVEL sb176bd(NOMINAL).
VARIABLE LEVEL sb176cd(NOMINAL).
VARIABLE LEVEL sb176ae(NOMINAL).
VARIABLE LEVEL sb176be(NOMINAL).
VARIABLE LEVEL sb176ce(NOMINAL).
VARIABLE LEVEL sb176af(NOMINAL).
VARIABLE LEVEL sb176bf(NOMINAL).
VARIABLE LEVEL sb176cf(NOMINAL).
VARIABLE LEVEL sb176ag(NOMINAL).
VARIABLE LEVEL sb176bg(NOMINAL).
VARIABLE LEVEL sb176cg(NOMINAL).
VARIABLE LEVEL sb176ah(NOMINAL).
VARIABLE LEVEL sb176bh(NOMINAL).
VARIABLE LEVEL sb176ch(NOMINAL).
VARIABLE LEVEL sb176ai(NOMINAL).
VARIABLE LEVEL sb176bi(NOMINAL).
VARIABLE LEVEL sb176ci(NOMINAL).
VARIABLE LEVEL sb176aj(NOMINAL).
VARIABLE LEVEL sb176bj(NOMINAL).
VARIABLE LEVEL sb176cj(NOMINAL).
VARIABLE LEVEL sb176ak(NOMINAL).
VARIABLE LEVEL sb176bk(NOMINAL).
VARIABLE LEVEL sb176ck(NOMINAL).
VARIABLE LEVEL sb176al(NOMINAL).
VARIABLE LEVEL sb176bl(NOMINAL).
VARIABLE LEVEL sb176cl(NOMINAL).
VARIABLE LEVEL sb176am(NOMINAL).
VARIABLE LEVEL sb176bm(NOMINAL).
VARIABLE LEVEL sb176cm(NOMINAL).
VARIABLE LEVEL sb177(ORDINAL).
VARIABLE LEVEL b178(NOMINAL).
VARIABLE LEVEL b179(NOMINAL).
VARIABLE LEVEL b180(ORDINAL).
VARIABLE LEVEL b181(NOMINAL).
VARIABLE LEVEL sb182aa(NOMINAL).
VARIABLE LEVEL sb182ba(NOMINAL).
VARIABLE LEVEL sb182ca(ORDINAL).
VARIABLE LEVEL b182ab(NOMINAL).
VARIABLE LEVEL b182bb(NOMINAL).
VARIABLE LEVEL b182cb(ORDINAL).
VARIABLE LEVEL sb182ac(NOMINAL).
VARIABLE LEVEL sb182bc(NOMINAL).
VARIABLE LEVEL sb182cc(ORDINAL).
VARIABLE LEVEL sb182ad(NOMINAL).
VARIABLE LEVEL sb182bd(NOMINAL).
VARIABLE LEVEL sb182cd(ORDINAL).
VARIABLE LEVEL sb182ae(NOMINAL).
VARIABLE LEVEL sb182be(NOMINAL).
VARIABLE LEVEL sb182ce(ORDINAL).
VARIABLE LEVEL sb182af(NOMINAL).
VARIABLE LEVEL sb182bf(NOMINAL).
VARIABLE LEVEL sb182cf(ORDINAL).
VARIABLE LEVEL sb182ag(NOMINAL).
VARIABLE LEVEL sb182bg(NOMINAL).
VARIABLE LEVEL sb182cg(ORDINAL).
VARIABLE LEVEL sb183aa(NOMINAL).
VARIABLE LEVEL sb183ba(ORDINAL).
VARIABLE LEVEL sb183ab(NOMINAL).
VARIABLE LEVEL sb183bb(ORDINAL).
VARIABLE LEVEL sb183ac(NOMINAL).
VARIABLE LEVEL sb183bc(ORDINAL).
VARIABLE LEVEL sb183ad(NOMINAL).
VARIABLE LEVEL sb183bd(ORDINAL).
VARIABLE LEVEL sb184(ORDINAL).
VARIABLE LEVEL b185(ORDINAL).
VARIABLE LEVEL b186aa(NOMINAL).
VARIABLE LEVEL b186ba(NOMINAL).
VARIABLE LEVEL b186ab(NOMINAL).
VARIABLE LEVEL b186bb(NOMINAL).
VARIABLE LEVEL b186ac(NOMINAL).
VARIABLE LEVEL b186bc(NOMINAL).
VARIABLE LEVEL b186ad(NOMINAL).
VARIABLE LEVEL b186bd(NOMINAL).
VARIABLE LEVEL b186ae(NOMINAL).
VARIABLE LEVEL b186be(NOMINAL).
VARIABLE LEVEL b186af(NOMINAL).
VARIABLE LEVEL b186bf(NOMINAL).
VARIABLE LEVEL b186ag(NOMINAL).
VARIABLE LEVEL b186bg(NOMINAL).
VARIABLE LEVEL b186ah(NOMINAL).
VARIABLE LEVEL b186bh(NOMINAL).
VARIABLE LEVEL b186ai(NOMINAL).
VARIABLE LEVEL b186bi(NOMINAL).
VARIABLE LEVEL b186aj(NOMINAL).
VARIABLE LEVEL b186bj(NOMINAL).
VARIABLE LEVEL b186ak(NOMINAL).
VARIABLE LEVEL b186bk(NOMINAL).
VARIABLE LEVEL b186al(NOMINAL).
VARIABLE LEVEL b186bl(NOMINAL).
VARIABLE LEVEL b186am(NOMINAL).
VARIABLE LEVEL b186bm(NOMINAL).
VARIABLE LEVEL b187a(ORDINAL).
VARIABLE LEVEL b187b(ORDINAL).
VARIABLE LEVEL b187c(ORDINAL).
VARIABLE LEVEL b187d(ORDINAL).
VARIABLE LEVEL b187e(ORDINAL).
VARIABLE LEVEL b187f(ORDINAL).
VARIABLE LEVEL b187g(ORDINAL).
VARIABLE LEVEL b187h(ORDINAL).
VARIABLE LEVEL b187i(ORDINAL).
VARIABLE LEVEL b187j(ORDINAL).
VARIABLE LEVEL b187k(ORDINAL).
VARIABLE LEVEL b187l(ORDINAL).
VARIABLE LEVEL b187m(ORDINAL).
VARIABLE LEVEL tb188(NOMINAL).
VARIABLE LEVEL mb191(NOMINAL).
VARIABLE LEVEL mb192a(ORDINAL).
VARIABLE LEVEL sb192b(ORDINAL).
VARIABLE LEVEL mb193(NOMINAL).
VARIABLE LEVEL mb194(SCALE).
VARIABLE LEVEL sb195(SCALE).
VARIABLE LEVEL mb196(SCALE).
VARIABLE LEVEL sb197(NOMINAL).
VARIABLE LEVEL sb198(NOMINAL).
VARIABLE LEVEL sb199(NOMINAL).
VARIABLE LEVEL sb200(ORDINAL).
VARIABLE LEVEL sb201(ORDINAL).
VARIABLE LEVEL mb202(NOMINAL).
VARIABLE LEVEL mb203a(ORDINAL).
VARIABLE LEVEL sb203b(ORDINAL).
VARIABLE LEVEL mb204(NOMINAL).
VARIABLE LEVEL mb205(SCALE).
VARIABLE LEVEL sb206(SCALE).
VARIABLE LEVEL mb207(SCALE).
VARIABLE LEVEL sb208(NOMINAL).
VARIABLE LEVEL sb209(NOMINAL).
VARIABLE LEVEL sb210(NOMINAL).
VARIABLE LEVEL sb211(ORDINAL).
VARIABLE LEVEL sb212(ORDINAL).
VARIABLE LEVEL mb213(NOMINAL).
VARIABLE LEVEL mb214a(ORDINAL).
VARIABLE LEVEL sb214b(ORDINAL).
VARIABLE LEVEL mb215(NOMINAL).
VARIABLE LEVEL mb216(SCALE).
VARIABLE LEVEL sb217(SCALE).
VARIABLE LEVEL mb218(SCALE).
VARIABLE LEVEL sb219(NOMINAL).
VARIABLE LEVEL sb220(NOMINAL).
VARIABLE LEVEL sb221(NOMINAL).
VARIABLE LEVEL sb222(ORDINAL).
VARIABLE LEVEL sb223(ORDINAL).
VARIABLE LEVEL mb224(NOMINAL).
VARIABLE LEVEL mb225a(ORDINAL).
VARIABLE LEVEL sb225b(ORDINAL).
VARIABLE LEVEL mb226(NOMINAL).
VARIABLE LEVEL mb227(SCALE).
VARIABLE LEVEL sb228(SCALE).
VARIABLE LEVEL mb229(SCALE).
VARIABLE LEVEL sb230(NOMINAL).
VARIABLE LEVEL sb231(NOMINAL).
VARIABLE LEVEL sb232(NOMINAL).
VARIABLE LEVEL sb233(ORDINAL).
VARIABLE LEVEL sb234(ORDINAL).
VARIABLE LEVEL mv1(NOMINAL).
VARIABLE LEVEL tv2(ORDINAL).
VARIABLE LEVEL mv3a(NOMINAL).
VARIABLE LEVEL mv3b(NOMINAL).
VARIABLE LEVEL mv3c(NOMINAL).
VARIABLE LEVEL mv3d(NOMINAL).
VARIABLE LEVEL mv3e(NOMINAL).
VARIABLE LEVEL mv4(SCALE).
VARIABLE LEVEL mv5(NOMINAL).
VARIABLE LEVEL v6(NOMINAL).
VARIABLE LEVEL sv7(NOMINAL).
VARIABLE LEVEL v8(NOMINAL).
VARIABLE LEVEL v9(ORDINAL).
VARIABLE LEVEL v10(NOMINAL).
VARIABLE LEVEL v11(ORDINAL).
VARIABLE LEVEL v12(NOMINAL).
VARIABLE LEVEL v13(ORDINAL).
VARIABLE LEVEL v14(NOMINAL).
VARIABLE LEVEL v15(NOMINAL).
VARIABLE LEVEL v16(NOMINAL).
VARIABLE LEVEL v17(ORDINAL).
VARIABLE LEVEL v18(NOMINAL).
VARIABLE LEVEL v19(ORDINAL).
VARIABLE LEVEL sv20(NOMINAL).
VARIABLE LEVEL v21(ORDINAL).
VARIABLE LEVEL sv22(NOMINAL).
VARIABLE LEVEL sv23(ORDINAL).
VARIABLE LEVEL sv24(NOMINAL).
VARIABLE LEVEL sv25(NOMINAL).
VARIABLE LEVEL sv26(ORDINAL).
VARIABLE LEVEL v27a(ORDINAL).
VARIABLE LEVEL v27b(ORDINAL).
VARIABLE LEVEL v27c(ORDINAL).
VARIABLE LEVEL v27d(ORDINAL).
VARIABLE LEVEL v27e(ORDINAL).
VARIABLE LEVEL v27f(ORDINAL).
VARIABLE LEVEL v28a(NOMINAL).
VARIABLE LEVEL v28b(NOMINAL).
VARIABLE LEVEL v29(ORDINAL).
VARIABLE LEVEL v30(NOMINAL).
VARIABLE LEVEL v31(NOMINAL).
VARIABLE LEVEL v32a(ORDINAL).
VARIABLE LEVEL v32b(ORDINAL).
VARIABLE LEVEL v32c(ORDINAL).
VARIABLE LEVEL v32d(ORDINAL).
VARIABLE LEVEL v32e(ORDINAL).
VARIABLE LEVEL mv33(ORDINAL).
VARIABLE LEVEL v34(NOMINAL).
VARIABLE LEVEL v35(ORDINAL).
VARIABLE LEVEL v36a(NOMINAL).
VARIABLE LEVEL v36b(SCALE).
VARIABLE LEVEL v37(NOMINAL).
VARIABLE LEVEL mv38a(NOMINAL).
VARIABLE LEVEL mv38b(SCALE).
VARIABLE LEVEL mv39a(NOMINAL).
VARIABLE LEVEL mv39b(NOMINAL).
VARIABLE LEVEL mv39c(NOMINAL).
VARIABLE LEVEL mv39d(NOMINAL).
VARIABLE LEVEL mv39e(NOMINAL).
VARIABLE LEVEL mv39f(NOMINAL).
VARIABLE LEVEL mv40a(NOMINAL).
VARIABLE LEVEL mv40b(NOMINAL).
VARIABLE LEVEL mv40c(NOMINAL).
VARIABLE LEVEL mv40d(NOMINAL).
VARIABLE LEVEL mv40e(NOMINAL).
VARIABLE LEVEL mv40f(NOMINAL).
VARIABLE LEVEL mv40g(NOMINAL).
VARIABLE LEVEL mv40h(NOMINAL).
VARIABLE LEVEL v41a(NOMINAL).
VARIABLE LEVEL v41b(NOMINAL).
VARIABLE LEVEL v41c(NOMINAL).
VARIABLE LEVEL v42(NOMINAL).
VARIABLE LEVEL v43(ORDINAL).
VARIABLE LEVEL v44a(ORDINAL).
VARIABLE LEVEL v44b(ORDINAL).
VARIABLE LEVEL v44c(ORDINAL).
VARIABLE LEVEL v44d(ORDINAL).
VARIABLE LEVEL v44e(ORDINAL).
VARIABLE LEVEL v44f(ORDINAL).
VARIABLE LEVEL v44g(ORDINAL).
VARIABLE LEVEL v45a(ORDINAL).
VARIABLE LEVEL v45b(ORDINAL).
VARIABLE LEVEL v45c(ORDINAL).
VARIABLE LEVEL v45d(ORDINAL).
VARIABLE LEVEL v45e(ORDINAL).
VARIABLE LEVEL v45f(ORDINAL).
VARIABLE LEVEL v45g(ORDINAL).
VARIABLE LEVEL v46(ORDINAL).
VARIABLE LEVEL v47a(NOMINAL).
VARIABLE LEVEL v47b(SCALE).
VARIABLE LEVEL v48a(NOMINAL).
VARIABLE LEVEL v48b(NOMINAL).
VARIABLE LEVEL v48c(NOMINAL).
VARIABLE LEVEL v48d(NOMINAL).
VARIABLE LEVEL v48e(NOMINAL).
VARIABLE LEVEL v48f(NOMINAL).
VARIABLE LEVEL v48g(NOMINAL).
VARIABLE LEVEL v49a(NOMINAL).
VARIABLE LEVEL v49b(NOMINAL).
VARIABLE LEVEL v49c(NOMINAL).
VARIABLE LEVEL v49d(NOMINAL).
VARIABLE LEVEL v49e(NOMINAL).
VARIABLE LEVEL v49f(NOMINAL).
VARIABLE LEVEL v50a(NOMINAL).
VARIABLE LEVEL v50b(SCALE).
VARIABLE LEVEL v51a(NOMINAL).
VARIABLE LEVEL v51b(NOMINAL).
VARIABLE LEVEL v51c(NOMINAL).
VARIABLE LEVEL v52a(NOMINAL).
VARIABLE LEVEL v52b(NOMINAL).
VARIABLE LEVEL v52c(NOMINAL).
VARIABLE LEVEL v52d(NOMINAL).
VARIABLE LEVEL v52e(NOMINAL).
VARIABLE LEVEL v52f(NOMINAL).
VARIABLE LEVEL v53(NOMINAL).
VARIABLE LEVEL v54a(NOMINAL).
VARIABLE LEVEL v54b(SCALE).
VARIABLE LEVEL v55(NOMINAL).
VARIABLE LEVEL v56a(NOMINAL).
VARIABLE LEVEL v56b(NOMINAL).
VARIABLE LEVEL v57(NOMINAL).
VARIABLE LEVEL v58a(NOMINAL).
VARIABLE LEVEL v58b(SCALE).
VARIABLE LEVEL sv59(NOMINAL).
VARIABLE LEVEL sv60(NOMINAL).
VARIABLE LEVEL sv61(NOMINAL).
VARIABLE LEVEL sv62(NOMINAL).
VARIABLE LEVEL sv63(NOMINAL).
VARIABLE LEVEL sv64(ORDINAL).
VARIABLE LEVEL sv65a(NOMINAL).
VARIABLE LEVEL sv65b(SCALE).
VARIABLE LEVEL v66a(NOMINAL).
VARIABLE LEVEL v66b(NOMINAL).
VARIABLE LEVEL v66c(NOMINAL).
VARIABLE LEVEL v66d(NOMINAL).
VARIABLE LEVEL v66e(NOMINAL).
VARIABLE LEVEL v66f(NOMINAL).
VARIABLE LEVEL v67a(NOMINAL).
VARIABLE LEVEL v67b(NOMINAL).
VARIABLE LEVEL v67c(NOMINAL).
VARIABLE LEVEL v67d(NOMINAL).
VARIABLE LEVEL v67e(NOMINAL).
VARIABLE LEVEL v67f(NOMINAL).
VARIABLE LEVEL v67g(NOMINAL).
VARIABLE LEVEL v67h(NOMINAL).
VARIABLE LEVEL v68(NOMINAL).
VARIABLE LEVEL v69(NOMINAL).
VARIABLE LEVEL v70a(NOMINAL).
VARIABLE LEVEL v70b(NOMINAL).
VARIABLE LEVEL v70c(NOMINAL).
VARIABLE LEVEL v70d(NOMINAL).
VARIABLE LEVEL v71(NOMINAL).
VARIABLE LEVEL v72a(NOMINAL).
VARIABLE LEVEL v72b(SCALE).
VARIABLE LEVEL v73a(NOMINAL).
VARIABLE LEVEL v73b(NOMINAL).
VARIABLE LEVEL v73c(NOMINAL).
VARIABLE LEVEL v73d(NOMINAL).
VARIABLE LEVEL v73e(NOMINAL).
VARIABLE LEVEL v73f(NOMINAL).
VARIABLE LEVEL v73g(NOMINAL).
VARIABLE LEVEL v74a(NOMINAL).
VARIABLE LEVEL v74b(NOMINAL).
VARIABLE LEVEL v74c(NOMINAL).
VARIABLE LEVEL v74d(NOMINAL).
VARIABLE LEVEL v74e(NOMINAL).
VARIABLE LEVEL v74f(NOMINAL).
VARIABLE LEVEL v75a(NOMINAL).
VARIABLE LEVEL v75b(SCALE).
VARIABLE LEVEL v76a(NOMINAL).
VARIABLE LEVEL v76b(NOMINAL).
VARIABLE LEVEL v76c(NOMINAL).
VARIABLE LEVEL v76d(NOMINAL).
VARIABLE LEVEL v76e(NOMINAL).
VARIABLE LEVEL v76f(NOMINAL).
VARIABLE LEVEL v77(NOMINAL).
VARIABLE LEVEL v78a(NOMINAL).
VARIABLE LEVEL v78b(SCALE).
VARIABLE LEVEL v79(NOMINAL).
VARIABLE LEVEL v80(ORDINAL).
VARIABLE LEVEL mv81(NOMINAL).
VARIABLE LEVEL mv82(ORDINAL).
VARIABLE LEVEL mv83(NOMINAL).
VARIABLE LEVEL mv84(NOMINAL).
VARIABLE LEVEL mv85(NOMINAL).
VARIABLE LEVEL v86a(NOMINAL).
VARIABLE LEVEL v86b(NOMINAL).
VARIABLE LEVEL v86c(NOMINAL).
VARIABLE LEVEL v86d(NOMINAL).
VARIABLE LEVEL v86e(NOMINAL).
VARIABLE LEVEL v86f(NOMINAL).
VARIABLE LEVEL v86g(NOMINAL).
VARIABLE LEVEL v87a(NOMINAL).
VARIABLE LEVEL v87b(NOMINAL).
VARIABLE LEVEL v87c(NOMINAL).
VARIABLE LEVEL v87d(NOMINAL).
VARIABLE LEVEL v87e(NOMINAL).
VARIABLE LEVEL v87f(NOMINAL).
VARIABLE LEVEL v87g(NOMINAL).
VARIABLE LEVEL mv88(NOMINAL).
VARIABLE LEVEL mv89(NOMINAL).
VARIABLE LEVEL mv90(NOMINAL).
VARIABLE LEVEL mv91(NOMINAL).
VARIABLE LEVEL mv92a(NOMINAL).
VARIABLE LEVEL uv92b(NOMINAL).
VARIABLE LEVEL mv92c(NOMINAL).
VARIABLE LEVEL mv92d(NOMINAL).
VARIABLE LEVEL mv92e(NOMINAL).
VARIABLE LEVEL mv92f(NOMINAL).
VARIABLE LEVEL mv92g(NOMINAL).
VARIABLE LEVEL mv92h(NOMINAL).
VARIABLE LEVEL mv93a(NOMINAL).
VARIABLE LEVEL mv93b(NOMINAL).
VARIABLE LEVEL mv93c(NOMINAL).
VARIABLE LEVEL mv93d(NOMINAL).
VARIABLE LEVEL mv93e(NOMINAL).
VARIABLE LEVEL mv93f(NOMINAL).
VARIABLE LEVEL mv93g(NOMINAL).
VARIABLE LEVEL mv93h(NOMINAL).
VARIABLE LEVEL mv94(NOMINAL).
VARIABLE LEVEL mv95a(NOMINAL).
VARIABLE LEVEL uv95b(NOMINAL).
VARIABLE LEVEL mv95c(NOMINAL).
VARIABLE LEVEL mv95d(NOMINAL).
VARIABLE LEVEL mv95e(NOMINAL).
VARIABLE LEVEL mv95f(NOMINAL).
VARIABLE LEVEL mv95g(NOMINAL).
VARIABLE LEVEL mv95h(NOMINAL).
VARIABLE LEVEL v96(NOMINAL).
VARIABLE LEVEL v97(NOMINAL).
VARIABLE LEVEL v98(NOMINAL).
VARIABLE LEVEL v99(NOMINAL).
VARIABLE LEVEL v100(NOMINAL).
VARIABLE LEVEL mv101(NOMINAL).
VARIABLE LEVEL mv102(NOMINAL).
VARIABLE LEVEL v103a(NOMINAL).
VARIABLE LEVEL v103b(NOMINAL).
VARIABLE LEVEL v103c(NOMINAL).
VARIABLE LEVEL v103d(NOMINAL).
VARIABLE LEVEL v103e(NOMINAL).
VARIABLE LEVEL v103f(NOMINAL).
VARIABLE LEVEL v103g(NOMINAL).
VARIABLE LEVEL v103h(NOMINAL).
VARIABLE LEVEL v103i(NOMINAL).
VARIABLE LEVEL v103j(NOMINAL).
VARIABLE LEVEL v103k(NOMINAL).
VARIABLE LEVEL v103l(NOMINAL).
VARIABLE LEVEL v103m(NOMINAL).
VARIABLE LEVEL v103n(NOMINAL).
VARIABLE LEVEL v103o(NOMINAL).
VARIABLE LEVEL v103p(NOMINAL).
VARIABLE LEVEL tv104aa(NOMINAL).
VARIABLE LEVEL sv104ca(SCALE).
VARIABLE LEVEL tv104ab(NOMINAL).
VARIABLE LEVEL sv104cb(SCALE).
VARIABLE LEVEL tv104ac(NOMINAL).
VARIABLE LEVEL sv104cc(SCALE).
VARIABLE LEVEL sv105aa(NOMINAL).
VARIABLE LEVEL sv105ba(SCALE).
VARIABLE LEVEL sv105ab(NOMINAL).
VARIABLE LEVEL sv105bb(SCALE).
VARIABLE LEVEL sv105ar(SCALE).
VARIABLE LEVEL sv105br(SCALE).
VARIABLE LEVEL lv106(NOMINAL).
VARIABLE LEVEL sv107(SCALE).
VARIABLE LEVEL sv107r(NOMINAL).
VARIABLE LEVEL sv108(NOMINAL).
VARIABLE LEVEL sv109aa(NOMINAL).
VARIABLE LEVEL sv109ab(NOMINAL).
VARIABLE LEVEL sv109ac(NOMINAL).
VARIABLE LEVEL v109ad(NOMINAL).
VARIABLE LEVEL sv109ae(NOMINAL).
VARIABLE LEVEL sv109af(NOMINAL).
VARIABLE LEVEL sv109ag(NOMINAL).
VARIABLE LEVEL sv109ah(NOMINAL).
VARIABLE LEVEL sv109ai(NOMINAL).
VARIABLE LEVEL sv109aj(NOMINAL).
VARIABLE LEVEL sv109ba(NOMINAL).
VARIABLE LEVEL sv109bb(NOMINAL).
VARIABLE LEVEL sv109bc(NOMINAL).
VARIABLE LEVEL v109bd(NOMINAL).
VARIABLE LEVEL sv109be(NOMINAL).
VARIABLE LEVEL sv109bf(NOMINAL).
VARIABLE LEVEL sv109bg(NOMINAL).
VARIABLE LEVEL sv109bh(NOMINAL).
VARIABLE LEVEL sv109bi(NOMINAL).
VARIABLE LEVEL sv109bj(NOMINAL).
VARIABLE LEVEL sv109ca(NOMINAL).
VARIABLE LEVEL sv109cb(NOMINAL).
VARIABLE LEVEL sv109cc(NOMINAL).
VARIABLE LEVEL v109cd(NOMINAL).
VARIABLE LEVEL sv109ce(NOMINAL).
VARIABLE LEVEL sv109cf(NOMINAL).
VARIABLE LEVEL sv109cg(NOMINAL).
VARIABLE LEVEL sv109ch(NOMINAL).
VARIABLE LEVEL sv109ci(NOMINAL).
VARIABLE LEVEL sv109cj(NOMINAL).
VARIABLE LEVEL sv110a(NOMINAL).
VARIABLE LEVEL sv110b(NOMINAL).
VARIABLE LEVEL sv110c(NOMINAL).
VARIABLE LEVEL mv111(NOMINAL).
VARIABLE LEVEL mv112(NOMINAL).
VARIABLE LEVEL mv113(NOMINAL).
VARIABLE LEVEL sv114aa(NOMINAL).
VARIABLE LEVEL sv114ab(NOMINAL).
VARIABLE LEVEL sv114ac(NOMINAL).
VARIABLE LEVEL v114ad(NOMINAL).
VARIABLE LEVEL sv114ae(NOMINAL).
VARIABLE LEVEL sv114af(NOMINAL).
VARIABLE LEVEL sv114ag(NOMINAL).
VARIABLE LEVEL sv114ah(NOMINAL).
VARIABLE LEVEL sv114ai(NOMINAL).
VARIABLE LEVEL sv114aj(NOMINAL).
VARIABLE LEVEL sv114ak(NOMINAL).
VARIABLE LEVEL sv114ba(NOMINAL).
VARIABLE LEVEL sv114bb(NOMINAL).
VARIABLE LEVEL sv114bc(NOMINAL).
VARIABLE LEVEL v114bd(NOMINAL).
VARIABLE LEVEL sv114be(NOMINAL).
VARIABLE LEVEL sv114bf(NOMINAL).
VARIABLE LEVEL sv114bg(NOMINAL).
VARIABLE LEVEL sv114bh(NOMINAL).
VARIABLE LEVEL sv114bi(NOMINAL).
VARIABLE LEVEL sv114bj(NOMINAL).
VARIABLE LEVEL sv114bk(NOMINAL).
VARIABLE LEVEL sv114ca(NOMINAL).
VARIABLE LEVEL sv114cb(NOMINAL).
VARIABLE LEVEL sv114cc(NOMINAL).
VARIABLE LEVEL v114cd(NOMINAL).
VARIABLE LEVEL sv114ce(NOMINAL).
VARIABLE LEVEL sv114cf(NOMINAL).
VARIABLE LEVEL sv114cg(NOMINAL).
VARIABLE LEVEL sv114ch(NOMINAL).
VARIABLE LEVEL sv114ci(NOMINAL).
VARIABLE LEVEL sv114cj(NOMINAL).
VARIABLE LEVEL sv114ck(NOMINAL).
VARIABLE LEVEL sv114d(NOMINAL).
VARIABLE LEVEL mv115a(ORDINAL).
VARIABLE LEVEL mv115b(ORDINAL).
VARIABLE LEVEL mv115c(ORDINAL).
VARIABLE LEVEL mv115d(ORDINAL).
VARIABLE LEVEL mv115e(ORDINAL).
VARIABLE LEVEL mv115f(ORDINAL).
VARIABLE LEVEL mv115g(ORDINAL).
VARIABLE LEVEL mv115h(ORDINAL).
VARIABLE LEVEL mv115i(ORDINAL).
VARIABLE LEVEL mv115j(ORDINAL).
VARIABLE LEVEL mv115k(ORDINAL).
VARIABLE LEVEL mv115l(ORDINAL).
VARIABLE LEVEL mv115m(ORDINAL).
VARIABLE LEVEL tv116(ORDINAL).
VARIABLE LEVEL sv117(ORDINAL).
VARIABLE LEVEL sv118a(NOMINAL).
VARIABLE LEVEL sv118b(NOMINAL).
VARIABLE LEVEL sv118c(NOMINAL).
VARIABLE LEVEL sv118d(NOMINAL).
VARIABLE LEVEL sv118e(NOMINAL).
VARIABLE LEVEL sv118f(NOMINAL).
VARIABLE LEVEL sv118g(NOMINAL).
VARIABLE LEVEL sv118h(NOMINAL).
VARIABLE LEVEL sv118i(NOMINAL).
VARIABLE LEVEL sv118j(NOMINAL).
VARIABLE LEVEL v119a(ORDINAL).
VARIABLE LEVEL v119b(ORDINAL).
VARIABLE LEVEL v119c(ORDINAL).
VARIABLE LEVEL v119d(ORDINAL).
VARIABLE LEVEL v119e(ORDINAL).
VARIABLE LEVEL v119f(ORDINAL).
VARIABLE LEVEL sv120a(ORDINAL).
VARIABLE LEVEL sv120b(ORDINAL).
VARIABLE LEVEL sv120c(ORDINAL).
VARIABLE LEVEL sv120d(ORDINAL).
VARIABLE LEVEL sv120e(ORDINAL).
VARIABLE LEVEL sv120f(ORDINAL).
VARIABLE LEVEL sv120g(ORDINAL).
VARIABLE LEVEL sv120h(ORDINAL).
VARIABLE LEVEL v121a(NOMINAL).
VARIABLE LEVEL sv121b(NOMINAL).
VARIABLE LEVEL sv121c(NOMINAL).
VARIABLE LEVEL sv121d(NOMINAL).
VARIABLE LEVEL sv121e(NOMINAL).
VARIABLE LEVEL sv121f(NOMINAL).
VARIABLE LEVEL sv121g(NOMINAL).
VARIABLE LEVEL sv122a(NOMINAL).
VARIABLE LEVEL sv122b(NOMINAL).
VARIABLE LEVEL sv122c(NOMINAL).
VARIABLE LEVEL sv122d(NOMINAL).
VARIABLE LEVEL sv122e(NOMINAL).
VARIABLE LEVEL sv122f(NOMINAL).
VARIABLE LEVEL sv122g(NOMINAL).
VARIABLE LEVEL sv123a(ORDINAL).
VARIABLE LEVEL sv123b(ORDINAL).
VARIABLE LEVEL sv123c(ORDINAL).
VARIABLE LEVEL sv123d(ORDINAL).
VARIABLE LEVEL sv123e(ORDINAL).
VARIABLE LEVEL sv123f(ORDINAL).
VARIABLE LEVEL sv124a(NOMINAL).
VARIABLE LEVEL sv124b(NOMINAL).
VARIABLE LEVEL sv124c(NOMINAL).
VARIABLE LEVEL sv124d(NOMINAL).
VARIABLE LEVEL sv124e(NOMINAL).
VARIABLE LEVEL sv124f(NOMINAL).
VARIABLE LEVEL sv124g(NOMINAL).
VARIABLE LEVEL sv124h(NOMINAL).
VARIABLE LEVEL sv124i(NOMINAL).
VARIABLE LEVEL mv125a(NOMINAL).
VARIABLE LEVEL mv125b(NOMINAL).
VARIABLE LEVEL mv125c(NOMINAL).
VARIABLE LEVEL mv125d(NOMINAL).
VARIABLE LEVEL v125e(NOMINAL).
VARIABLE LEVEL v125f(NOMINAL).
VARIABLE LEVEL mv125g(NOMINAL).
VARIABLE LEVEL mv125h(NOMINAL).
VARIABLE LEVEL mv125i(NOMINAL).
VARIABLE LEVEL mv125j(NOMINAL).
VARIABLE LEVEL mv125k(NOMINAL).
VARIABLE LEVEL mv125l(NOMINAL).
VARIABLE LEVEL sv126a(ORDINAL).
VARIABLE LEVEL sv126b(ORDINAL).
VARIABLE LEVEL mv127a(ORDINAL).
VARIABLE LEVEL mv127b(ORDINAL).
VARIABLE LEVEL tv128(NOMINAL).
VARIABLE LEVEL sv129a(ORDINAL).
VARIABLE LEVEL sv129b(ORDINAL).
VARIABLE LEVEL sv129c(ORDINAL).
VARIABLE LEVEL sv129d(ORDINAL).
VARIABLE LEVEL sv129e(ORDINAL).
VARIABLE LEVEL sv129f(ORDINAL).
VARIABLE LEVEL sv129g(ORDINAL).
VARIABLE LEVEL sv130a(ORDINAL).
VARIABLE LEVEL sv130b(ORDINAL).
VARIABLE LEVEL sv130c(ORDINAL).
VARIABLE LEVEL sv130d(ORDINAL).
VARIABLE LEVEL sv130e(ORDINAL).
VARIABLE LEVEL sv130f(ORDINAL).
VARIABLE LEVEL sv131(ORDINAL).
VARIABLE LEVEL sv132a(ORDINAL).
VARIABLE LEVEL sv132b(ORDINAL).
VARIABLE LEVEL sv133a(ORDINAL).
VARIABLE LEVEL sv133b(ORDINAL).
VARIABLE LEVEL mv134(ORDINAL).
VARIABLE LEVEL tv135aa(ORDINAL).
VARIABLE LEVEL sv135b1a(NOMINAL).
VARIABLE LEVEL sv135b2a(NOMINAL).
VARIABLE LEVEL sv135b3a(NOMINAL).
VARIABLE LEVEL sv135b4a(NOMINAL).
VARIABLE LEVEL sv135ca(NOMINAL).
VARIABLE LEVEL v135da(NOMINAL).
VARIABLE LEVEL v135ea(ORDINAL).
VARIABLE LEVEL sv135ab(ORDINAL).
VARIABLE LEVEL sv135b1b(NOMINAL).
VARIABLE LEVEL sv135b2b(NOMINAL).
VARIABLE LEVEL sv135b3b(NOMINAL).
VARIABLE LEVEL sv135b4b(NOMINAL).
VARIABLE LEVEL sv135cb(NOMINAL).
VARIABLE LEVEL v135db(NOMINAL).
VARIABLE LEVEL v135eb(ORDINAL).
VARIABLE LEVEL tv135ac(ORDINAL).
VARIABLE LEVEL sv135b1c(NOMINAL).
VARIABLE LEVEL sv135b2c(NOMINAL).
VARIABLE LEVEL sv135b3c(NOMINAL).
VARIABLE LEVEL sv135b4c(NOMINAL).
VARIABLE LEVEL sv135cc(NOMINAL).
VARIABLE LEVEL v135dc(NOMINAL).
VARIABLE LEVEL v135ec(ORDINAL).
VARIABLE LEVEL sv135ad(ORDINAL).
VARIABLE LEVEL sv135b1d(NOMINAL).
VARIABLE LEVEL sv135b2d(NOMINAL).
VARIABLE LEVEL sv135b3d(NOMINAL).
VARIABLE LEVEL sv135b4d(NOMINAL).
VARIABLE LEVEL sv135cd(NOMINAL).
VARIABLE LEVEL v135dd(NOMINAL).
VARIABLE LEVEL v135ed(ORDINAL).
VARIABLE LEVEL sv135ae(ORDINAL).
VARIABLE LEVEL sv135b1e(NOMINAL).
VARIABLE LEVEL sv135b2e(NOMINAL).
VARIABLE LEVEL sv135b3e(NOMINAL).
VARIABLE LEVEL sv135b4e(NOMINAL).
VARIABLE LEVEL sv135ce(NOMINAL).
VARIABLE LEVEL v135de(NOMINAL).
VARIABLE LEVEL v135ee(ORDINAL).
VARIABLE LEVEL sv135af(ORDINAL).
VARIABLE LEVEL sv135b1f(NOMINAL).
VARIABLE LEVEL sv135b2f(NOMINAL).
VARIABLE LEVEL sv135b3f(NOMINAL).
VARIABLE LEVEL sv135b4f(NOMINAL).
VARIABLE LEVEL sv135cf(NOMINAL).
VARIABLE LEVEL v135df(NOMINAL).
VARIABLE LEVEL v135ef(ORDINAL).
VARIABLE LEVEL tv135ag(ORDINAL).
VARIABLE LEVEL sv135b1g(NOMINAL).
VARIABLE LEVEL sv135b2g(NOMINAL).
VARIABLE LEVEL sv135b3g(NOMINAL).
VARIABLE LEVEL sv135b4g(NOMINAL).
VARIABLE LEVEL sv135cg(NOMINAL).
VARIABLE LEVEL v135dg(NOMINAL).
VARIABLE LEVEL v135eg(ORDINAL).
VARIABLE LEVEL sv135ah(ORDINAL).
VARIABLE LEVEL sv135b1h(NOMINAL).
VARIABLE LEVEL sv135b2h(NOMINAL).
VARIABLE LEVEL sv135b3h(NOMINAL).
VARIABLE LEVEL sv135b4h(NOMINAL).
VARIABLE LEVEL sv135ch(NOMINAL).
VARIABLE LEVEL v135dh(NOMINAL).
VARIABLE LEVEL v135eh(ORDINAL).
VARIABLE LEVEL sv135ai(ORDINAL).
VARIABLE LEVEL sv135b1i(NOMINAL).
VARIABLE LEVEL sv135b2i(NOMINAL).
VARIABLE LEVEL sv135b3i(NOMINAL).
VARIABLE LEVEL sv135b4i(NOMINAL).
VARIABLE LEVEL sv135ci(NOMINAL).
VARIABLE LEVEL v135di(NOMINAL).
VARIABLE LEVEL v135ei(ORDINAL).
VARIABLE LEVEL sv135aj(ORDINAL).
VARIABLE LEVEL sv135b1j(NOMINAL).
VARIABLE LEVEL sv135b2j(NOMINAL).
VARIABLE LEVEL sv135b3j(NOMINAL).
VARIABLE LEVEL sv135b4j(NOMINAL).
VARIABLE LEVEL sv135cj(NOMINAL).
VARIABLE LEVEL v135dj(NOMINAL).
VARIABLE LEVEL v135ej(ORDINAL).
VARIABLE LEVEL sv135ak(ORDINAL).
VARIABLE LEVEL sv135b1k(NOMINAL).
VARIABLE LEVEL sv135b2k(NOMINAL).
VARIABLE LEVEL sv135b3k(NOMINAL).
VARIABLE LEVEL sv135b4k(NOMINAL).
VARIABLE LEVEL sv135ck(NOMINAL).
VARIABLE LEVEL v135dk(NOMINAL).
VARIABLE LEVEL v135ek(ORDINAL).
VARIABLE LEVEL tv135al(ORDINAL).
VARIABLE LEVEL sv135b1l(NOMINAL).
VARIABLE LEVEL sv135b2l(NOMINAL).
VARIABLE LEVEL sv135b3l(NOMINAL).
VARIABLE LEVEL sv135b4l(NOMINAL).
VARIABLE LEVEL sv135cl(NOMINAL).
VARIABLE LEVEL v135dl(NOMINAL).
VARIABLE LEVEL v135el(ORDINAL).
VARIABLE LEVEL tv135am(ORDINAL).
VARIABLE LEVEL sv135b1m(NOMINAL).
VARIABLE LEVEL sv135b2m(NOMINAL).
VARIABLE LEVEL sv135b3m(NOMINAL).
VARIABLE LEVEL sv135b4m(NOMINAL).
VARIABLE LEVEL sv135cm(NOMINAL).
VARIABLE LEVEL v135dm(NOMINAL).
VARIABLE LEVEL v135em(ORDINAL).
VARIABLE LEVEL tv135an(ORDINAL).
VARIABLE LEVEL sv135b1n(NOMINAL).
VARIABLE LEVEL sv135b2n(NOMINAL).
VARIABLE LEVEL sv135b3n(NOMINAL).
VARIABLE LEVEL sv135b4n(NOMINAL).
VARIABLE LEVEL sv135cn(NOMINAL).
VARIABLE LEVEL v135dn(NOMINAL).
VARIABLE LEVEL v135en(ORDINAL).
VARIABLE LEVEL tv135ao(ORDINAL).
VARIABLE LEVEL sv135b1o(NOMINAL).
VARIABLE LEVEL sv135b2o(NOMINAL).
VARIABLE LEVEL sv135b3o(NOMINAL).
VARIABLE LEVEL sv135b4o(NOMINAL).
VARIABLE LEVEL sv135co(NOMINAL).
VARIABLE LEVEL v135do(NOMINAL).
VARIABLE LEVEL v135eo(ORDINAL).
VARIABLE LEVEL sv135ap(ORDINAL).
VARIABLE LEVEL sv135b1p(NOMINAL).
VARIABLE LEVEL sv135b2p(NOMINAL).
VARIABLE LEVEL sv135b3p(NOMINAL).
VARIABLE LEVEL sv135b4p(NOMINAL).
VARIABLE LEVEL sv135cp(NOMINAL).
VARIABLE LEVEL v135dp(NOMINAL).
VARIABLE LEVEL v135ep(ORDINAL).
VARIABLE LEVEL sv135aq(ORDINAL).
VARIABLE LEVEL sv135b1q(NOMINAL).
VARIABLE LEVEL sv135b2q(NOMINAL).
VARIABLE LEVEL sv135b3q(NOMINAL).
VARIABLE LEVEL sv135b4q(NOMINAL).
VARIABLE LEVEL sv135cq(NOMINAL).
VARIABLE LEVEL v135dq(NOMINAL).
VARIABLE LEVEL v135eq(ORDINAL).
VARIABLE LEVEL tv135ar(ORDINAL).
VARIABLE LEVEL sv135b1r(NOMINAL).
VARIABLE LEVEL sv135b2r(NOMINAL).
VARIABLE LEVEL sv135b3r(NOMINAL).
VARIABLE LEVEL sv135b4r(NOMINAL).
VARIABLE LEVEL sv135cr(NOMINAL).
VARIABLE LEVEL v135dr(NOMINAL).
VARIABLE LEVEL v135er(ORDINAL).
VARIABLE LEVEL tv135as(ORDINAL).
VARIABLE LEVEL sv135b1s(NOMINAL).
VARIABLE LEVEL sv135b2s(NOMINAL).
VARIABLE LEVEL sv135b3s(NOMINAL).
VARIABLE LEVEL sv135b4s(NOMINAL).
VARIABLE LEVEL sv135cs(NOMINAL).
VARIABLE LEVEL v135ds(NOMINAL).
VARIABLE LEVEL v135es(ORDINAL).
VARIABLE LEVEL tv135at(ORDINAL).
VARIABLE LEVEL sv135b1t(NOMINAL).
VARIABLE LEVEL sv135b2t(NOMINAL).
VARIABLE LEVEL sv135b3t(NOMINAL).
VARIABLE LEVEL sv135b4t(NOMINAL).
VARIABLE LEVEL sv135ct(NOMINAL).
VARIABLE LEVEL v135dt(NOMINAL).
VARIABLE LEVEL v135et(ORDINAL).
VARIABLE LEVEL pv136(ORDINAL).
VARIABLE LEVEL sv137(ORDINAL).
VARIABLE LEVEL sv138a(NOMINAL).
VARIABLE LEVEL sv138b(NOMINAL).
VARIABLE LEVEL sv138c(NOMINAL).
VARIABLE LEVEL sv138d(NOMINAL).
VARIABLE LEVEL sv138e(NOMINAL).
VARIABLE LEVEL sv138f(NOMINAL).
VARIABLE LEVEL sv138g(NOMINAL).
VARIABLE LEVEL sv138h(NOMINAL).
VARIABLE LEVEL sv138i(NOMINAL).
VARIABLE LEVEL sv138j(NOMINAL).
VARIABLE LEVEL sv138k(NOMINAL).
VARIABLE LEVEL sv139(ORDINAL).
VARIABLE LEVEL sv140(NOMINAL).
VARIABLE LEVEL mv141a(NOMINAL).
VARIABLE LEVEL v141b(NOMINAL).
VARIABLE LEVEL mv141c(NOMINAL).
VARIABLE LEVEL mv141d(NOMINAL).
VARIABLE LEVEL mv141e(NOMINAL).
VARIABLE LEVEL mv141f(NOMINAL).
VARIABLE LEVEL v141g(NOMINAL).
VARIABLE LEVEL mv141h(NOMINAL).
VARIABLE LEVEL mv141i(NOMINAL).
VARIABLE LEVEL mv141j(NOMINAL).
VARIABLE LEVEL mv141k(NOMINAL).
VARIABLE LEVEL v141l(NOMINAL).
VARIABLE LEVEL mv141m(NOMINAL).
VARIABLE LEVEL mv141n(NOMINAL).
VARIABLE LEVEL mv141o(NOMINAL).
VARIABLE LEVEL mv141p(NOMINAL).
VARIABLE LEVEL mv141q(NOMINAL).
VARIABLE LEVEL mv141r(NOMINAL).
VARIABLE LEVEL v141s(NOMINAL).
VARIABLE LEVEL mv141t(NOMINAL).
VARIABLE LEVEL mv141u(NOMINAL).
VARIABLE LEVEL mv141v(NOMINAL).
VARIABLE LEVEL mv141w(NOMINAL).
VARIABLE LEVEL v141x(NOMINAL).
VARIABLE LEVEL mv141y(NOMINAL).
VARIABLE LEVEL mv141z(NOMINAL).
VARIABLE LEVEL mv141aa(NOMINAL).
VARIABLE LEVEL mv141ab(NOMINAL).
VARIABLE LEVEL sv142(NOMINAL).
VARIABLE LEVEL mv143aa(NOMINAL).
VARIABLE LEVEL mv143ab(NOMINAL).
VARIABLE LEVEL mv143ac(NOMINAL).
VARIABLE LEVEL mv143ad(NOMINAL).
VARIABLE LEVEL mv143ae(NOMINAL).
VARIABLE LEVEL mv143af(NOMINAL).
VARIABLE LEVEL mv143ag(NOMINAL).
VARIABLE LEVEL mv143ah(NOMINAL).
VARIABLE LEVEL mv143ai(NOMINAL).
VARIABLE LEVEL mv143aj(NOMINAL).
VARIABLE LEVEL mv143ba(NOMINAL).
VARIABLE LEVEL mv143bb(NOMINAL).
VARIABLE LEVEL mv143bc(NOMINAL).
VARIABLE LEVEL mv143bd(NOMINAL).
VARIABLE LEVEL mv143be(NOMINAL).
VARIABLE LEVEL mv143bf(NOMINAL).
VARIABLE LEVEL mv143bg(NOMINAL).
VARIABLE LEVEL mv143bh(NOMINAL).
VARIABLE LEVEL mv143bi(NOMINAL).
VARIABLE LEVEL mv143bj(NOMINAL).
VARIABLE LEVEL mv143ca(NOMINAL).
VARIABLE LEVEL mv143cb(NOMINAL).
VARIABLE LEVEL mv143cc(NOMINAL).
VARIABLE LEVEL mv143cd(NOMINAL).
VARIABLE LEVEL mv143ce(NOMINAL).
VARIABLE LEVEL mv143cf(NOMINAL).
VARIABLE LEVEL mv143cg(NOMINAL).
VARIABLE LEVEL mv143ch(NOMINAL).
VARIABLE LEVEL mv143ci(NOMINAL).
VARIABLE LEVEL mv143cj(NOMINAL).
VARIABLE LEVEL mv144aa(NOMINAL).
VARIABLE LEVEL mv144ab(NOMINAL).
VARIABLE LEVEL mv144ac(NOMINAL).
VARIABLE LEVEL mv144ad(NOMINAL).
VARIABLE LEVEL mv144ae(NOMINAL).
VARIABLE LEVEL mv144af(NOMINAL).
VARIABLE LEVEL mv144ag(NOMINAL).
VARIABLE LEVEL mv144ah(NOMINAL).
VARIABLE LEVEL mv144ai(NOMINAL).
VARIABLE LEVEL mv144aj(NOMINAL).
VARIABLE LEVEL mv144ba(NOMINAL).
VARIABLE LEVEL mv144bb(NOMINAL).
VARIABLE LEVEL mv144bc(NOMINAL).
VARIABLE LEVEL mv144bd(NOMINAL).
VARIABLE LEVEL mv144be(NOMINAL).
VARIABLE LEVEL mv144bf(NOMINAL).
VARIABLE LEVEL mv144bg(NOMINAL).
VARIABLE LEVEL mv144bh(NOMINAL).
VARIABLE LEVEL mv144bi(NOMINAL).
VARIABLE LEVEL mv144bj(NOMINAL).
VARIABLE LEVEL mv144ca(NOMINAL).
VARIABLE LEVEL mv144cb(NOMINAL).
VARIABLE LEVEL mv144cc(NOMINAL).
VARIABLE LEVEL mv144cd(NOMINAL).
VARIABLE LEVEL mv144ce(NOMINAL).
VARIABLE LEVEL mv144cf(NOMINAL).
VARIABLE LEVEL mv144cg(NOMINAL).
VARIABLE LEVEL mv144ch(NOMINAL).
VARIABLE LEVEL mv144ci(NOMINAL).
VARIABLE LEVEL mv144cj(NOMINAL).
VARIABLE LEVEL mv145(NOMINAL).
VARIABLE LEVEL svf1(NOMINAL).
VARIABLE LEVEL svf2(NOMINAL).
VARIABLE LEVEL svf3(NOMINAL).
VARIABLE LEVEL svf4(NOMINAL).
VARIABLE LEVEL svf5(NOMINAL).
VARIABLE LEVEL mvf6a(NOMINAL).
VARIABLE LEVEL mvf6b(NOMINAL).
VARIABLE LEVEL mvf6c(NOMINAL).
VARIABLE LEVEL mvf7(NOMINAL).
VARIABLE LEVEL mvf8(NOMINAL).
VARIABLE LEVEL mvf9(NOMINAL).
VARIABLE LEVEL mvf9a(NOMINAL).
VARIABLE LEVEL mvf9b(NOMINAL).
VARIABLE LEVEL mvf9c(NOMINAL).
VARIABLE LEVEL mvf9d(NOMINAL).
VARIABLE LEVEL mvf10(NOMINAL).
VARIABLE LEVEL mvf11(NOMINAL).
VARIABLE LEVEL svfdate(NOMINAL).
VARIABLE LEVEL ix1a(NOMINAL).
VARIABLE LEVEL ix1b(NOMINAL).
VARIABLE LEVEL i2b(NOMINAL).
VARIABLE LEVEL i10(NOMINAL).
VARIABLE LEVEL i11(SCALE).
VARIABLE LEVEL i1_18(NOMINAL).
VARIABLE LEVEL i1_19(NOMINAL).
VARIABLE LEVEL i1_21(NOMINAL).
VARIABLE LEVEL i2_18(NOMINAL).
VARIABLE LEVEL i2_19(NOMINAL).
VARIABLE LEVEL i2_21(NOMINAL).
VARIABLE LEVEL i3_18(NOMINAL).
VARIABLE LEVEL i3_19(NOMINAL).
VARIABLE LEVEL i3_21(NOMINAL).
VARIABLE LEVEL i4_18(NOMINAL).
VARIABLE LEVEL i4_19(NOMINAL).
VARIABLE LEVEL i4_21(NOMINAL).
VARIABLE LEVEL i5_18(NOMINAL).
VARIABLE LEVEL i5_19(NOMINAL).
VARIABLE LEVEL i5_21(NOMINAL).
VARIABLE LEVEL i6_18(NOMINAL).
VARIABLE LEVEL i6_19(NOMINAL).
VARIABLE LEVEL i6_21(NOMINAL).
VARIABLE LEVEL i7_18(NOMINAL).
VARIABLE LEVEL i7_19(NOMINAL).
VARIABLE LEVEL i7_21(NOMINAL).
VARIABLE LEVEL i8_18(NOMINAL).
VARIABLE LEVEL i8_19(NOMINAL).
VARIABLE LEVEL i8_21(NOMINAL).
VARIABLE LEVEL ix2a(NOMINAL).
VARIABLE LEVEL ix2b(SCALE).
VARIABLE LEVEL i81(NOMINAL).
VARIABLE LEVEL i83a(SCALE).
VARIABLE LEVEL i83b(SCALE).
VARIABLE LEVEL i83c(SCALE).
VARIABLE LEVEL i83d(SCALE).
VARIABLE LEVEL i83e(SCALE).
VARIABLE LEVEL i83f(SCALE).
VARIABLE LEVEL i83g(SCALE).
VARIABLE LEVEL i84(NOMINAL).
VARIABLE LEVEL i91(NOMINAL).
VARIABLE LEVEL i91mois(ORDINAL).
VARIABLE LEVEL i91annee(ORDINAL).
VARIABLE LEVEL i93(NOMINAL).
VARIABLE LEVEL i95(NOMINAL).
VARIABLE LEVEL ix3a(NOMINAL).
VARIABLE LEVEL ix3b(NOMINAL).
VARIABLE LEVEL i117d(NOMINAL).
VARIABLE LEVEL ix4(NOMINAL).
VARIABLE LEVEL i22(NOMINAL).
VARIABLE LEVEL i24(NOMINAL).
VARIABLE LEVEL i25(NOMINAL).
VARIABLE LEVEL i26(ORDINAL).
VARIABLE LEVEL ix5(NOMINAL).
VARIABLE LEVEL i60(NOMINAL).
VARIABLE LEVEL i62(NOMINAL).
VARIABLE LEVEL i63(NOMINAL).
VARIABLE LEVEL i64(NOMINAL).
VARIABLE LEVEL i65(NOMINAL).
VARIABLE LEVEL i59(NOMINAL).
VARIABLE LEVEL i136(NOMINAL).
VARIABLE LEVEL ix6a(NOMINAL).
VARIABLE LEVEL ix6b(NOMINAL).
VARIABLE LEVEL ix6c(NOMINAL).
VARIABLE LEVEL i98(NOMINAL).
VARIABLE LEVEL i175a(NOMINAL).
VARIABLE LEVEL i175b(NOMINAL).
VARIABLE LEVEL i175c(NOMINAL).
VARIABLE LEVEL i175d(NOMINAL).
VARIABLE LEVEL i182aa(NOMINAL).
VARIABLE LEVEL i182ba(NOMINAL).
VARIABLE LEVEL i182ac(NOMINAL).
VARIABLE LEVEL i182bc(NOMINAL).
VARIABLE LEVEL i182ad(NOMINAL).
VARIABLE LEVEL i182bd(NOMINAL).
VARIABLE LEVEL i182ae(NOMINAL).
VARIABLE LEVEL i182be(NOMINAL).
VARIABLE LEVEL i182ag(NOMINAL).
VARIABLE LEVEL i182bg(NOMINAL).
VARIABLE LEVEL i125a(NOMINAL).
VARIABLE LEVEL i125l(NOMINAL).
VARIABLE LEVEL i135e(NOMINAL).
VARIABLE LEVEL i135f(NOMINAL).
VARIABLE LEVEL i135j(NOMINAL).
VARIABLE LEVEL rjpb(NOMINAL).
VARIABLE LEVEL rjpc(SCALE).
VARIABLE LEVEL jpd(NOMINAL).
VARIABLE LEVEL jpg(NOMINAL).
VARIABLE LEVEL j9(SCALE).
VARIABLE LEVEL j12a(NOMINAL).
VARIABLE LEVEL j12b(SCALE).
VARIABLE LEVEL j12c(NOMINAL).
VARIABLE LEVEL j23a(NOMINAL).
VARIABLE LEVEL j23b(SCALE).
VARIABLE LEVEL j23c(NOMINAL).
VARIABLE LEVEL j34a(NOMINAL).
VARIABLE LEVEL j34b(SCALE).
VARIABLE LEVEL j34c(NOMINAL).
VARIABLE LEVEL j45a(NOMINAL).
VARIABLE LEVEL j45b(SCALE).
VARIABLE LEVEL j45c(NOMINAL).
VARIABLE LEVEL j26(NOMINAL).
VARIABLE LEVEL j115a(NOMINAL).
VARIABLE LEVEL j115b(NOMINAL).
VARIABLE LEVEL j115c(NOMINAL).
VARIABLE LEVEL j115d(NOMINAL).
VARIABLE LEVEL j115e(NOMINAL).
VARIABLE LEVEL j115f(NOMINAL).
VARIABLE LEVEL j115g(NOMINAL).
VARIABLE LEVEL j115h(NOMINAL).
VARIABLE LEVEL j115i(NOMINAL).
VARIABLE LEVEL j115j(NOMINAL).
VARIABLE LEVEL j115k(NOMINAL).
VARIABLE LEVEL j115m(NOMINAL).
VARIABLE LEVEL j172a(NOMINAL).
VARIABLE LEVEL j172b(NOMINAL).
VARIABLE LEVEL dpd(NOMINAL).
VARIABLE LEVEL dpe(NOMINAL).
VARIABLE LEVEL dpf(NOMINAL).
VARIABLE LEVEL dpg(NOMINAL).
VARIABLE LEVEL dph(NOMINAL).
VARIABLE LEVEL dpi(NOMINAL).
VARIABLE LEVEL dpl(NOMINAL).
VARIABLE LEVEL d1_12(NOMINAL).
VARIABLE LEVEL d1_13(SCALE).
VARIABLE LEVEL d1_14(NOMINAL).
VARIABLE LEVEL d1_15(SCALE).
VARIABLE LEVEL d1_18(NOMINAL).
VARIABLE LEVEL d1_19(NOMINAL).
VARIABLE LEVEL d1_21(NOMINAL).
VARIABLE LEVEL d2_12(NOMINAL).
VARIABLE LEVEL d2_13(SCALE).
VARIABLE LEVEL d2_14(NOMINAL).
VARIABLE LEVEL d2_15(SCALE).
VARIABLE LEVEL d2_18(NOMINAL).
VARIABLE LEVEL d2_19(NOMINAL).
VARIABLE LEVEL d2_21(NOMINAL).
VARIABLE LEVEL dx7(NOMINAL).
VARIABLE LEVEL d26(NOMINAL).
VARIABLE LEVEL d91(NOMINAL).
VARIABLE LEVEL d93(NOMINAL).
VARIABLE LEVEL d96(NOMINAL).
VARIABLE LEVEL dx8a(NOMINAL).
VARIABLE LEVEL dx8b(NOMINAL).
VARIABLE LEVEL dx8c(NOMINAL).
VARIABLE LEVEL dx8d(NOMINAL).
VARIABLE LEVEL dx8e(NOMINAL).
VARIABLE LEVEL dx8f(NOMINAL).
VARIABLE LEVEL dx8g(NOMINAL).
VARIABLE LEVEL dx8h(NOMINAL).
VARIABLE LEVEL senfviv(SCALE).
VARIABLE LEVEL sansfam(NOMINAL).
VARIABLE LEVEL snfratri(SCALE).
VARIABLE LEVEL lgenfviv(NOMINAL).
VARIABLE LEVEL lgfratri(NOMINAL).
VARIABLE LEVEL ptsenfan(NOMINAL).
VARIABLE LEVEL frersoeu(NOMINAL).
VARIABLE LEVEL parents(NOMINAL).
VARIABLE LEVEL bparents(NOMINAL).
VARIABLE LEVEL agemere(SCALE).
VARIABLE LEVEL agepere(SCALE).
VARIABLE LEVEL agebmere(SCALE).
VARIABLE LEVEL agebpere(SCALE).
VARIABLE LEVEL ageascd(SCALE).
VARIABLE LEVEL ageascal(SCALE).
VARIABLE LEVEL agmari1(SCALE).
VARIABLE LEVEL agveuv1(SCALE).
VARIABLE LEVEL pondrep5(SCALE).

* Définition des missings

MISSING VALUES dateentr (LO THRU -1).
MISSING VALUES tb1 (LO THRU -1).
MISSING VALUES tb2 (LO THRU -1).
MISSING VALUES nb3 (LO THRU -1).
MISSING VALUES tb4 (LO THRU -1).
MISSING VALUES mb5 (LO THRU -1).
MISSING VALUES mb6a (LO THRU -1).
MISSING VALUES mb6b (LO THRU -1).
MISSING VALUES mb7a (LO THRU -1).
MISSING VALUES mb7b (LO THRU -1).
MISSING VALUES mb8a (LO THRU -1).
MISSING VALUES mb8b (LO THRU -1).
MISSING VALUES mb9 (LO THRU -1).
MISSING VALUES sb10 (LO THRU -1).
MISSING VALUES sb11 (LO THRU -1).
MISSING VALUES mb12 (LO THRU -1).
MISSING VALUES mb13a (LO THRU -1).
MISSING VALUES sb13b (LO THRU -1).
MISSING VALUES mb14 (LO THRU -1).
MISSING VALUES mb15 (LO THRU -1).
MISSING VALUES b16 (LO THRU -1).
MISSING VALUES mb17 (LO THRU -1).
MISSING VALUES sb18 (LO THRU -1).
MISSING VALUES sb19 (LO THRU -1).
MISSING VALUES sb20 (LO THRU -1).
MISSING VALUES sb21 (LO THRU -1).
MISSING VALUES b22 (LO THRU -1).
MISSING VALUES mb23 (LO THRU -1).
MISSING VALUES mb24a (LO THRU -1).
MISSING VALUES sb24b (LO THRU -1).
MISSING VALUES mb25 (LO THRU -1).
MISSING VALUES mb26 (LO THRU -1).
MISSING VALUES b27 (LO THRU -1).
MISSING VALUES mb28 (LO THRU -1).
MISSING VALUES sb29 (LO THRU -1).
MISSING VALUES sb30 (LO THRU -1).
MISSING VALUES sb31 (LO THRU -1).
MISSING VALUES sb32 (LO THRU -1).
MISSING VALUES b33 (LO THRU -1).
MISSING VALUES mb34 (LO THRU -1).
MISSING VALUES mb35a (LO THRU -1).
MISSING VALUES sb35b (LO THRU -1).
MISSING VALUES mb36 (LO THRU -1).
MISSING VALUES mb37 (LO THRU -1).
MISSING VALUES b38 (LO THRU -1).
MISSING VALUES mb39 (LO THRU -1).
MISSING VALUES sb40 (LO THRU -1).
MISSING VALUES sb41 (LO THRU -1).
MISSING VALUES sb42 (LO THRU -1).
MISSING VALUES sb43 (LO THRU -1).
MISSING VALUES b44 (LO THRU -1).
MISSING VALUES mb45 (LO THRU -1).
MISSING VALUES mb46a (LO THRU -1).
MISSING VALUES sb46b (LO THRU -1).
MISSING VALUES mb47 (LO THRU -1).
MISSING VALUES mb48 (LO THRU -1).
MISSING VALUES b49 (LO THRU -1).
MISSING VALUES mb50 (LO THRU -1).
MISSING VALUES sb51 (LO THRU -1).
MISSING VALUES sb52 (LO THRU -1).
MISSING VALUES sb53 (LO THRU -1).
MISSING VALUES sb54 (LO THRU -1).
MISSING VALUES b55 (LO THRU -1).
MISSING VALUES b56a (LO THRU -1).
MISSING VALUES b56b (LO THRU -1).
MISSING VALUES b56c (LO THRU -1).
MISSING VALUES b57 (LO THRU -1).
MISSING VALUES b58 (LO THRU -1).
MISSING VALUES b59a (LO THRU -1).
MISSING VALUES b59b (LO THRU -1).
MISSING VALUES b59c (LO THRU -1).
MISSING VALUES b59d (LO THRU -1).
MISSING VALUES b59e (LO THRU -1).
MISSING VALUES b59f (LO THRU -1).
MISSING VALUES b60 (LO THRU -1).
MISSING VALUES b61 (LO THRU -1).
MISSING VALUES b62a (LO THRU -1).
MISSING VALUES b62b (LO THRU -1).
MISSING VALUES b62c (LO THRU -1).
MISSING VALUES b63 (LO THRU -1).
MISSING VALUES b64 (LO THRU -1).
MISSING VALUES b65a (LO THRU -1).
MISSING VALUES b65b (LO THRU -1).
MISSING VALUES b65c (LO THRU -1).
MISSING VALUES b65d (LO THRU -1).
MISSING VALUES b65e (LO THRU -1).
MISSING VALUES b65f (LO THRU -1).
MISSING VALUES b66 (LO THRU -1).
MISSING VALUES b67 (LO THRU -1).
MISSING VALUES b68a (LO THRU -1).
MISSING VALUES b68b (LO THRU -1).
MISSING VALUES b68c (LO THRU -1).
MISSING VALUES b69 (LO THRU -1).
MISSING VALUES b70 (LO THRU -1).
MISSING VALUES b71a (LO THRU -1).
MISSING VALUES b71b (LO THRU -1).
MISSING VALUES b71c (LO THRU -1).
MISSING VALUES b71d (LO THRU -1).
MISSING VALUES b71e (LO THRU -1).
MISSING VALUES b71f (LO THRU -1).
MISSING VALUES b72 (LO THRU -1).
MISSING VALUES b73 (LO THRU -1).
MISSING VALUES b74a (LO THRU -1).
MISSING VALUES b74b (LO THRU -1).
MISSING VALUES b74c (LO THRU -1).
MISSING VALUES b75 (LO THRU -1).
MISSING VALUES b76 (LO THRU -1).
MISSING VALUES b77a (LO THRU -1).
MISSING VALUES b77b (LO THRU -1).
MISSING VALUES b77c (LO THRU -1).
MISSING VALUES b77d (LO THRU -1).
MISSING VALUES b77e (LO THRU -1).
MISSING VALUES b77f (LO THRU -1).
MISSING VALUES b78 (LO THRU -1).
MISSING VALUES b79 (LO THRU -1).
MISSING VALUES sb80 (LO THRU -1).
MISSING VALUES sb81a (LO THRU -1).
MISSING VALUES sb81b (LO THRU -1).
MISSING VALUES sb81ab (LO THRU -1).
MISSING VALUES sb82 (LO THRU -1).
MISSING VALUES sb83a (LO THRU -1).
MISSING VALUES mb83b (LO THRU -1).
MISSING VALUES mb83c (LO THRU -1).
MISSING VALUES mb83d (LO THRU -1).
MISSING VALUES mb83e (LO THRU -1).
MISSING VALUES mb83f (LO THRU -1).
MISSING VALUES mb83g (LO THRU -1).
MISSING VALUES sb84 (LO THRU -1).
MISSING VALUES pb85 (LO THRU -1).
MISSING VALUES pb86 (LO THRU -1).
MISSING VALUES pb87 (LO THRU -1).
MISSING VALUES pb88a (LO THRU -1).
MISSING VALUES pb88b (LO THRU -1).
MISSING VALUES q88c (LO THRU -1).
MISSING VALUES pb88d (LO THRU -1).
MISSING VALUES pb88e (LO THRU -1).
MISSING VALUES pb88f (LO THRU -1).
MISSING VALUES pb88g (LO THRU -1).
MISSING VALUES sb89 (LO THRU -1).
MISSING VALUES sb90 (LO THRU -1).
MISSING VALUES sb91 (LO THRU -1).
MISSING VALUES sb92 (LO THRU -1).
MISSING VALUES sb93 (LO THRU -1).
MISSING VALUES pb94a (LO THRU -1).
MISSING VALUES pb94b (LO THRU -1).
MISSING VALUES pb94c (LO THRU -1).
MISSING VALUES pb94d (LO THRU -1).
MISSING VALUES pb94e (LO THRU -1).
MISSING VALUES pb94f (LO THRU -1).
MISSING VALUES pb94g (LO THRU -1).
MISSING VALUES pb94h (LO THRU -1).
MISSING VALUES pb94i (LO THRU -1).
MISSING VALUES pb94j (LO THRU -1).
MISSING VALUES sb94k (LO THRU -1).
MISSING VALUES pb95 (LO THRU -1).
MISSING VALUES mb96a (LO THRU -1).
MISSING VALUES mb96b (LO THRU -1).
MISSING VALUES mb96c (LO THRU -1).
MISSING VALUES mb96d (LO THRU -1).
MISSING VALUES mb96e (LO THRU -1).
MISSING VALUES mb96f (LO THRU -1).
MISSING VALUES mb97a (LO THRU -1).
MISSING VALUES mb97b (LO THRU -1).
MISSING VALUES sb97c (LO THRU -1).
MISSING VALUES pb98a (LO THRU -1).
MISSING VALUES pb98b (LO THRU -1).
MISSING VALUES pb98c (LO THRU -1).
MISSING VALUES pb98d (LO THRU -1).
MISSING VALUES pb98e (LO THRU -1).
MISSING VALUES pb98f (LO THRU -1).
MISSING VALUES pb98g (LO THRU -1).
MISSING VALUES pb98h (LO THRU -1).
MISSING VALUES sb98i (LO THRU -1).
MISSING VALUES pb98j (LO THRU -1).
MISSING VALUES sb99 (LO THRU -1).
MISSING VALUES sb100a (LO THRU -1).
MISSING VALUES sb100b (LO THRU -1).
MISSING VALUES sb100c (LO THRU -1).
MISSING VALUES sb100d (LO THRU -1).
MISSING VALUES sb100e (LO THRU -1).
MISSING VALUES sb100f (LO THRU -1).
MISSING VALUES sb100g (LO THRU -1).
MISSING VALUES sb100h (LO THRU -1).
MISSING VALUES sb100i (LO THRU -1).
MISSING VALUES sb100j (LO THRU -1).
MISSING VALUES sb100k (LO THRU -1).
MISSING VALUES sb101a (LO THRU -1).
MISSING VALUES sb101b (LO THRU -1).
MISSING VALUES sb101c (LO THRU -1).
MISSING VALUES sb101d (LO THRU -1).
MISSING VALUES sb101e (LO THRU -1).
MISSING VALUES sb101f (LO THRU -1).
MISSING VALUES sb101g (LO THRU -1).
MISSING VALUES sb101h (LO THRU -1).
MISSING VALUES sb101i (LO THRU -1).
MISSING VALUES sb101j (LO THRU -1).
MISSING VALUES sb101k (LO THRU -1).
MISSING VALUES sb102a (LO THRU -1).
MISSING VALUES sb102b (LO THRU -1).
MISSING VALUES sb102c (LO THRU -1).
MISSING VALUES sb102d (LO THRU -1).
MISSING VALUES tb103 (LO THRU -1).
MISSING VALUES sb104 (LO THRU -1).
MISSING VALUES tb105 (LO THRU -1).
MISSING VALUES tb106 (LO THRU -1).
MISSING VALUES mb107a (LO THRU -1).
MISSING VALUES mb107b (LO THRU -1).
MISSING VALUES mb107c (LO THRU -1).
MISSING VALUES mb107d (LO THRU -1).
MISSING VALUES mb107e (LO THRU -1).
MISSING VALUES mb107f (LO THRU -1).
MISSING VALUES mb108a (LO THRU -1).
MISSING VALUES mb108b (LO THRU -1).
MISSING VALUES mb108c (LO THRU -1).
MISSING VALUES mb108d (LO THRU -1).
MISSING VALUES mb108e (LO THRU -1).
MISSING VALUES mb108f (LO THRU -1).
MISSING VALUES mb109a (LO THRU -1).
MISSING VALUES mb109b (LO THRU -1).
MISSING VALUES mb109c (LO THRU -1).
MISSING VALUES mb109d (LO THRU -1).
MISSING VALUES mb109e (LO THRU -1).
MISSING VALUES mb109f (LO THRU -1).
MISSING VALUES mb109g (LO THRU -1).
MISSING VALUES mb109h (LO THRU -1).
MISSING VALUES mb110a (LO THRU -1).
MISSING VALUES mb110b (LO THRU -1).
MISSING VALUES mb110c (LO THRU -1).
MISSING VALUES mb110d (LO THRU -1).
MISSING VALUES mb110e (LO THRU -1).
MISSING VALUES mb110f (LO THRU -1).
MISSING VALUES mb110g (LO THRU -1).
MISSING VALUES mb110h (LO THRU -1).
MISSING VALUES mb110i (LO THRU -1).
MISSING VALUES sb111 (LO THRU -1).
MISSING VALUES sb112 (LO THRU -1).
MISSING VALUES sb113 (LO THRU -1).
MISSING VALUES sb113a (LO THRU -1).
MISSING VALUES sb113b (LO THRU -1).
MISSING VALUES sb113c (LO THRU -1).
MISSING VALUES sb113d (LO THRU -1).
MISSING VALUES sb114 (LO THRU -1).
MISSING VALUES b115 (LO THRU -1).
MISSING VALUES b116a (LO THRU -1).
MISSING VALUES b116b (LO THRU -1).
MISSING VALUES b116c (LO THRU -1).
MISSING VALUES b116d (LO THRU -1).
MISSING VALUES tb117a (LO THRU -1).
MISSING VALUES tb117b (LO THRU -1).
MISSING VALUES tb117c (LO THRU -1).
MISSING VALUES tb117d (LO THRU -1).
MISSING VALUES mb118a (LO THRU -1).
MISSING VALUES mb118b (LO THRU -1).
MISSING VALUES mb118c (LO THRU -1).
MISSING VALUES mb119a (LO THRU -1).
MISSING VALUES mb119b (LO THRU -1).
MISSING VALUES mb119c (LO THRU -1).
MISSING VALUES b120 (LO THRU -1).
MISSING VALUES b121 (LO THRU -1).
MISSING VALUES b122 (LO THRU -1).
MISSING VALUES b123 (LO THRU -1).
MISSING VALUES mb124 (LO THRU -1).
MISSING VALUES b126 (LO THRU -1).
MISSING VALUES b127 (LO THRU -1).
MISSING VALUES b128 (LO THRU -1).
MISSING VALUES b129 (LO THRU -1).
MISSING VALUES b130 (LO THRU -1).
MISSING VALUES b131 (LO THRU -1).
MISSING VALUES tb132aa (LO THRU -1).
MISSING VALUES tb132ab (LO THRU -1).
MISSING VALUES tb132ac (LO THRU -1).
MISSING VALUES tb132ad (LO THRU -1).
MISSING VALUES tb132ae (LO THRU -1).
MISSING VALUES tb132af (LO THRU -1).
MISSING VALUES tb132ag (LO THRU -1).
MISSING VALUES tb132ah (LO THRU -1).
MISSING VALUES tb132ai (LO THRU -1).
MISSING VALUES tb132aj (LO THRU -1).
MISSING VALUES tb132ak (LO THRU -1).
MISSING VALUES sb132ba (LO THRU -1).
MISSING VALUES sb132bb (LO THRU -1).
MISSING VALUES sb132bc (LO THRU -1).
MISSING VALUES sb132bd (LO THRU -1).
MISSING VALUES sb132be (LO THRU -1).
MISSING VALUES sb132bf (LO THRU -1).
MISSING VALUES sb132bg (LO THRU -1).
MISSING VALUES sb132bh (LO THRU -1).
MISSING VALUES sb132bi (LO THRU -1).
MISSING VALUES sb132bj (LO THRU -1).
MISSING VALUES sb132bk (LO THRU -1).
MISSING VALUES tb133 (LO THRU -1).
MISSING VALUES tb134 (LO THRU -1).
MISSING VALUES tb135 (LO THRU -1).
MISSING VALUES tb136 (LO THRU -1).
MISSING VALUES tb137 (LO THRU -1).
MISSING VALUES tb138 (LO THRU -1).
MISSING VALUES tb139 (LO THRU -1).
MISSING VALUES tb140 (LO THRU -1).
MISSING VALUES tb141 (LO THRU -1).
MISSING VALUES sb142 (LO THRU -1).
MISSING VALUES tb143 (LO THRU -1).
MISSING VALUES nbmedic (LO THRU -1).
MISSING VALUES mb144 (LO THRU -1).
MISSING VALUES sb146 (LO THRU -1).
MISSING VALUES sb147a (LO THRU -1).
MISSING VALUES sb147b (LO THRU -1).
MISSING VALUES sb147c (LO THRU -1).
MISSING VALUES sb147d (LO THRU -1).
MISSING VALUES sb148 (LO THRU -1).
MISSING VALUES tb149 (LO THRU -1).
MISSING VALUES tb150 (LO THRU -1).
MISSING VALUES tb151 (LO THRU -1).
MISSING VALUES tb152 (LO THRU -1).
MISSING VALUES tb153 (LO THRU -1).
MISSING VALUES tb154 (LO THRU -1).
MISSING VALUES tb155 (LO THRU -1).
MISSING VALUES tb156 (LO THRU -1).
MISSING VALUES tb157 (LO THRU -1).
MISSING VALUES tb158 (LO THRU -1).
MISSING VALUES tb159 (LO THRU -1).
MISSING VALUES sb160 (LO THRU -1).
MISSING VALUES sb161a (LO THRU -1).
MISSING VALUES sb161b (LO THRU -1).
MISSING VALUES sb161c (LO THRU -1).
MISSING VALUES sb161d (LO THRU -1).
MISSING VALUES sb161e (LO THRU -1).
MISSING VALUES sb161f (LO THRU -1).
MISSING VALUES sb161g (LO THRU -1).
MISSING VALUES sb161h (LO THRU -1).
MISSING VALUES sb161i (LO THRU -1).
MISSING VALUES sb161j (LO THRU -1).
MISSING VALUES sb162a (LO THRU -1).
MISSING VALUES sb162b (LO THRU -1).
MISSING VALUES sb162c (LO THRU -1).
MISSING VALUES sb162d (LO THRU -1).
MISSING VALUES sb162e (LO THRU -1).
MISSING VALUES sb162f (LO THRU -1).
MISSING VALUES sb162g (LO THRU -1).
MISSING VALUES mb163 (LO THRU -1).
MISSING VALUES mb164 (LO THRU -1).
MISSING VALUES mb165 (LO THRU -1).
MISSING VALUES mb166 (LO THRU -1).
MISSING VALUES mb167 (LO THRU -1).
MISSING VALUES sb168a (LO THRU -1).
MISSING VALUES sb168b (LO THRU -1).
MISSING VALUES pb169aa (LO THRU -1).
MISSING VALUES pb169ab (LO THRU -1).
MISSING VALUES pb169ac (LO THRU -1).
MISSING VALUES pb169ad (LO THRU -1).
MISSING VALUES pb169ae (LO THRU -1).
MISSING VALUES pb169af (LO THRU -1).
MISSING VALUES pb169ag (LO THRU -1).
MISSING VALUES pb169ah (LO THRU -1).
MISSING VALUES pb169ba (LO THRU -1).
MISSING VALUES pb169bb (LO THRU -1).
MISSING VALUES pb169bc (LO THRU -1).
MISSING VALUES pb169bd (LO THRU -1).
MISSING VALUES pb169be (LO THRU -1).
MISSING VALUES pb169bf (LO THRU -1).
MISSING VALUES pb169bg (LO THRU -1).
MISSING VALUES pb169bh (LO THRU -1).
MISSING VALUES sb170aa (LO THRU -1).
MISSING VALUES sb170ab (LO THRU -1).
MISSING VALUES sb170ac (LO THRU -1).
MISSING VALUES sb170ad (LO THRU -1).
MISSING VALUES sb170ae (LO THRU -1).
MISSING VALUES sb170af (LO THRU -1).
MISSING VALUES sb170ag (LO THRU -1).
MISSING VALUES sb170ah (LO THRU -1).
MISSING VALUES sb170ba (LO THRU -1).
MISSING VALUES sb170bb (LO THRU -1).
MISSING VALUES sb170bc (LO THRU -1).
MISSING VALUES sb170bd (LO THRU -1).
MISSING VALUES sb170be (LO THRU -1).
MISSING VALUES sb170bf (LO THRU -1).
MISSING VALUES sb170bg (LO THRU -1).
MISSING VALUES sb170bh (LO THRU -1).
MISSING VALUES mb171a (LO THRU -1).
MISSING VALUES mb171b (LO THRU -1).
MISSING VALUES mb171c (LO THRU -1).
MISSING VALUES mb171d (LO THRU -1).
MISSING VALUES mb171e (LO THRU -1).
MISSING VALUES mb171f (LO THRU -1).
MISSING VALUES tb172a (LO THRU -1).
MISSING VALUES mb172b (LO THRU -1).
MISSING VALUES lb172c (LO THRU -1).
MISSING VALUES mb172d (LO THRU -1).
MISSING VALUES mb172e (LO THRU -1).
MISSING VALUES mb172f (LO THRU -1).
MISSING VALUES tb173a (LO THRU -1).
MISSING VALUES mb173b (LO THRU -1).
MISSING VALUES lb173c (LO THRU -1).
MISSING VALUES mb173d (LO THRU -1).
MISSING VALUES mb173e (LO THRU -1).
MISSING VALUES b174 (LO THRU -1).
MISSING VALUES sb175a (LO THRU -1).
MISSING VALUES sb175b (LO THRU -1).
MISSING VALUES sb175c (LO THRU -1).
MISSING VALUES sb175d (LO THRU -1).
MISSING VALUES sb175e (LO THRU -1).
MISSING VALUES sb176aa (LO THRU -1).
MISSING VALUES sb176ba (LO THRU -1).
MISSING VALUES sb176ca (LO THRU -1).
MISSING VALUES sb176ab (LO THRU -1).
MISSING VALUES sb176bb (LO THRU -1).
MISSING VALUES sb176cb (LO THRU -1).
MISSING VALUES sb176ac (LO THRU -1).
MISSING VALUES sb176bc (LO THRU -1).
MISSING VALUES sb176cc (LO THRU -1).
MISSING VALUES sb176ad (LO THRU -1).
MISSING VALUES sb176bd (LO THRU -1).
MISSING VALUES sb176cd (LO THRU -1).
MISSING VALUES sb176ae (LO THRU -1).
MISSING VALUES sb176be (LO THRU -1).
MISSING VALUES sb176ce (LO THRU -1).
MISSING VALUES sb176af (LO THRU -1).
MISSING VALUES sb176bf (LO THRU -1).
MISSING VALUES sb176cf (LO THRU -1).
MISSING VALUES sb176ag (LO THRU -1).
MISSING VALUES sb176bg (LO THRU -1).
MISSING VALUES sb176cg (LO THRU -1).
MISSING VALUES sb176ah (LO THRU -1).
MISSING VALUES sb176bh (LO THRU -1).
MISSING VALUES sb176ch (LO THRU -1).
MISSING VALUES sb176ai (LO THRU -1).
MISSING VALUES sb176bi (LO THRU -1).
MISSING VALUES sb176ci (LO THRU -1).
MISSING VALUES sb176aj (LO THRU -1).
MISSING VALUES sb176bj (LO THRU -1).
MISSING VALUES sb176cj (LO THRU -1).
MISSING VALUES sb176ak (LO THRU -1).
MISSING VALUES sb176bk (LO THRU -1).
MISSING VALUES sb176ck (LO THRU -1).
MISSING VALUES sb176al (LO THRU -1).
MISSING VALUES sb176bl (LO THRU -1).
MISSING VALUES sb176cl (LO THRU -1).
MISSING VALUES sb176am (LO THRU -1).
MISSING VALUES sb176bm (LO THRU -1).
MISSING VALUES sb176cm (LO THRU -1).
MISSING VALUES sb177 (LO THRU -1).
MISSING VALUES b178 (LO THRU -1).
MISSING VALUES b179 (LO THRU -1).
MISSING VALUES b180 (LO THRU -1).
MISSING VALUES b181 (LO THRU -1).
MISSING VALUES sb182aa (LO THRU -1).
MISSING VALUES sb182ba (LO THRU -1).
MISSING VALUES sb182ca (LO THRU -1).
MISSING VALUES b182ab (LO THRU -1).
MISSING VALUES b182bb (LO THRU -1).
MISSING VALUES b182cb (LO THRU -1).
MISSING VALUES sb182ac (LO THRU -1).
MISSING VALUES sb182bc (LO THRU -1).
MISSING VALUES sb182cc (LO THRU -1).
MISSING VALUES sb182ad (LO THRU -1).
MISSING VALUES sb182bd (LO THRU -1).
MISSING VALUES sb182cd (LO THRU -1).
MISSING VALUES sb182ae (LO THRU -1).
MISSING VALUES sb182be (LO THRU -1).
MISSING VALUES sb182ce (LO THRU -1).
MISSING VALUES sb182af (LO THRU -1).
MISSING VALUES sb182bf (LO THRU -1).
MISSING VALUES sb182cf (LO THRU -1).
MISSING VALUES sb182ag (LO THRU -1).
MISSING VALUES sb182bg (LO THRU -1).
MISSING VALUES sb182cg (LO THRU -1).
MISSING VALUES sb183aa (LO THRU -1).
MISSING VALUES sb183ba (LO THRU -1).
MISSING VALUES sb183ab (LO THRU -1).
MISSING VALUES sb183bb (LO THRU -1).
MISSING VALUES sb183ac (LO THRU -1).
MISSING VALUES sb183bc (LO THRU -1).
MISSING VALUES sb183ad (LO THRU -1).
MISSING VALUES sb183bd (LO THRU -1).
MISSING VALUES sb184 (LO THRU -1).
MISSING VALUES b185 (LO THRU -1).
MISSING VALUES b186aa (LO THRU -1).
MISSING VALUES b186ba (LO THRU -1).
MISSING VALUES b186ab (LO THRU -1).
MISSING VALUES b186bb (LO THRU -1).
MISSING VALUES b186ac (LO THRU -1).
MISSING VALUES b186bc (LO THRU -1).
MISSING VALUES b186ad (LO THRU -1).
MISSING VALUES b186bd (LO THRU -1).
MISSING VALUES b186ae (LO THRU -1).
MISSING VALUES b186be (LO THRU -1).
MISSING VALUES b186af (LO THRU -1).
MISSING VALUES b186bf (LO THRU -1).
MISSING VALUES b186ag (LO THRU -1).
MISSING VALUES b186bg (LO THRU -1).
MISSING VALUES b186ah (LO THRU -1).
MISSING VALUES b186bh (LO THRU -1).
MISSING VALUES b186ai (LO THRU -1).
MISSING VALUES b186bi (LO THRU -1).
MISSING VALUES b186aj (LO THRU -1).
MISSING VALUES b186bj (LO THRU -1).
MISSING VALUES b186ak (LO THRU -1).
MISSING VALUES b186bk (LO THRU -1).
MISSING VALUES b186al (LO THRU -1).
MISSING VALUES b186bl (LO THRU -1).
MISSING VALUES b186am (LO THRU -1).
MISSING VALUES b186bm (LO THRU -1).
MISSING VALUES b187a (LO THRU -1).
MISSING VALUES b187b (LO THRU -1).
MISSING VALUES b187c (LO THRU -1).
MISSING VALUES b187d (LO THRU -1).
MISSING VALUES b187e (LO THRU -1).
MISSING VALUES b187f (LO THRU -1).
MISSING VALUES b187g (LO THRU -1).
MISSING VALUES b187h (LO THRU -1).
MISSING VALUES b187i (LO THRU -1).
MISSING VALUES b187j (LO THRU -1).
MISSING VALUES b187k (LO THRU -1).
MISSING VALUES b187l (LO THRU -1).
MISSING VALUES b187m (LO THRU -1).
MISSING VALUES tb188 (LO THRU -1).
MISSING VALUES mb191 (LO THRU -1).
MISSING VALUES mb192a (LO THRU -1).
MISSING VALUES sb192b (LO THRU -1).
MISSING VALUES mb193 (LO THRU -1).
MISSING VALUES mb194 (LO THRU -1).
MISSING VALUES sb195 (LO THRU -1).
MISSING VALUES mb196 (LO THRU -1).
MISSING VALUES sb197 (LO THRU -1).
MISSING VALUES sb198 (LO THRU -1).
MISSING VALUES sb199 (LO THRU -1).
MISSING VALUES sb200 (LO THRU -1).
MISSING VALUES sb201 (LO THRU -1).
MISSING VALUES mb202 (LO THRU -1).
MISSING VALUES mb203a (LO THRU -1).
MISSING VALUES sb203b (LO THRU -1).
MISSING VALUES mb204 (LO THRU -1).
MISSING VALUES mb205 (LO THRU -1).
MISSING VALUES sb206 (LO THRU -1).
MISSING VALUES mb207 (LO THRU -1).
MISSING VALUES sb208 (LO THRU -1).
MISSING VALUES sb209 (LO THRU -1).
MISSING VALUES sb210 (LO THRU -1).
MISSING VALUES sb211 (LO THRU -1).
MISSING VALUES sb212 (LO THRU -1).
MISSING VALUES mb213 (LO THRU -1).
MISSING VALUES mb214a (LO THRU -1).
MISSING VALUES sb214b (LO THRU -1).
MISSING VALUES mb215 (LO THRU -1).
MISSING VALUES mb216 (LO THRU -1).
MISSING VALUES sb217 (LO THRU -1).
MISSING VALUES mb218 (LO THRU -1).
MISSING VALUES sb219 (LO THRU -1).
MISSING VALUES sb220 (LO THRU -1).
MISSING VALUES sb221 (LO THRU -1).
MISSING VALUES sb222 (LO THRU -1).
MISSING VALUES sb223 (LO THRU -1).
MISSING VALUES mb224 (LO THRU -1).
MISSING VALUES mb225a (LO THRU -1).
MISSING VALUES sb225b (LO THRU -1).
MISSING VALUES mb226 (LO THRU -1).
MISSING VALUES mb227 (LO THRU -1).
MISSING VALUES sb228 (LO THRU -1).
MISSING VALUES mb229 (LO THRU -1).
MISSING VALUES sb230 (LO THRU -1).
MISSING VALUES sb231 (LO THRU -1).
MISSING VALUES sb232 (LO THRU -1).
MISSING VALUES sb233 (LO THRU -1).
MISSING VALUES sb234 (LO THRU -1).
MISSING VALUES mv1 (LO THRU -1).
MISSING VALUES tv2 (LO THRU -1).
MISSING VALUES mv3a (LO THRU -1).
MISSING VALUES mv3b (LO THRU -1).
MISSING VALUES mv3c (LO THRU -1).
MISSING VALUES mv3d (LO THRU -1).
MISSING VALUES mv3e (LO THRU -1).
MISSING VALUES mv4 (LO THRU -1).
MISSING VALUES mv5 (LO THRU -1).
MISSING VALUES v6 (LO THRU -1).
MISSING VALUES sv7 (LO THRU -1).
MISSING VALUES v8 (LO THRU -1).
MISSING VALUES v9 (LO THRU -1).
MISSING VALUES v10 (LO THRU -1).
MISSING VALUES v11 (LO THRU -1).
MISSING VALUES v12 (LO THRU -1).
MISSING VALUES v13 (LO THRU -1).
MISSING VALUES v14 (LO THRU -1).
MISSING VALUES v15 (LO THRU -1).
MISSING VALUES v16 (LO THRU -1).
MISSING VALUES v17 (LO THRU -1).
MISSING VALUES v18 (LO THRU -1).
MISSING VALUES v19 (LO THRU -1).
MISSING VALUES sv20 (LO THRU -1).
MISSING VALUES v21 (LO THRU -1).
MISSING VALUES sv22 (LO THRU -1).
MISSING VALUES sv23 (LO THRU -1).
MISSING VALUES sv24 (LO THRU -1).
MISSING VALUES sv25 (LO THRU -1).
MISSING VALUES sv26 (LO THRU -1).
MISSING VALUES v27a (LO THRU -1).
MISSING VALUES v27b (LO THRU -1).
MISSING VALUES v27c (LO THRU -1).
MISSING VALUES v27d (LO THRU -1).
MISSING VALUES v27e (LO THRU -1).
MISSING VALUES v27f (LO THRU -1).
MISSING VALUES v28a (LO THRU -1).
MISSING VALUES v28b (LO THRU -1).
MISSING VALUES v29 (LO THRU -1).
MISSING VALUES v30 (LO THRU -1).
MISSING VALUES v31 (LO THRU -1).
MISSING VALUES v32a (LO THRU -1).
MISSING VALUES v32b (LO THRU -1).
MISSING VALUES v32c (LO THRU -1).
MISSING VALUES v32d (LO THRU -1).
MISSING VALUES v32e (LO THRU -1).
MISSING VALUES mv33 (LO THRU -1).
MISSING VALUES v34 (LO THRU -1).
MISSING VALUES v35 (LO THRU -1).
MISSING VALUES v36a (LO THRU -1).
MISSING VALUES v36b (LO THRU -1).
MISSING VALUES v37 (LO THRU -1).
MISSING VALUES mv38a (LO THRU -1).
MISSING VALUES mv38b (LO THRU -1).
MISSING VALUES mv39a (LO THRU -1).
MISSING VALUES mv39b (LO THRU -1).
MISSING VALUES mv39c (LO THRU -1).
MISSING VALUES mv39d (LO THRU -1).
MISSING VALUES mv39e (LO THRU -1).
MISSING VALUES mv39f (LO THRU -1).
MISSING VALUES mv40a (LO THRU -1).
MISSING VALUES mv40b (LO THRU -1).
MISSING VALUES mv40c (LO THRU -1).
MISSING VALUES mv40d (LO THRU -1).
MISSING VALUES mv40e (LO THRU -1).
MISSING VALUES mv40f (LO THRU -1).
MISSING VALUES mv40g (LO THRU -1).
MISSING VALUES mv40h (LO THRU -1).
MISSING VALUES v41a (LO THRU -1).
MISSING VALUES v41b (LO THRU -1).
MISSING VALUES v41c (LO THRU -1).
MISSING VALUES v42 (LO THRU -1).
MISSING VALUES v43 (LO THRU -1).
MISSING VALUES v44a (LO THRU -1).
MISSING VALUES v44b (LO THRU -1).
MISSING VALUES v44c (LO THRU -1).
MISSING VALUES v44d (LO THRU -1).
MISSING VALUES v44e (LO THRU -1).
MISSING VALUES v44f (LO THRU -1).
MISSING VALUES v44g (LO THRU -1).
MISSING VALUES v45a (LO THRU -1).
MISSING VALUES v45b (LO THRU -1).
MISSING VALUES v45c (LO THRU -1).
MISSING VALUES v45d (LO THRU -1).
MISSING VALUES v45e (LO THRU -1).
MISSING VALUES v45f (LO THRU -1).
MISSING VALUES v45g (LO THRU -1).
MISSING VALUES v46 (LO THRU -1).
MISSING VALUES v47a (LO THRU -1).
MISSING VALUES v47b (LO THRU -1).
MISSING VALUES v48a (LO THRU -1).
MISSING VALUES v48b (LO THRU -1).
MISSING VALUES v48c (LO THRU -1).
MISSING VALUES v48d (LO THRU -1).
MISSING VALUES v48e (LO THRU -1).
MISSING VALUES v48f (LO THRU -1).
MISSING VALUES v48g (LO THRU -1).
MISSING VALUES v49a (LO THRU -1).
MISSING VALUES v49b (LO THRU -1).
MISSING VALUES v49c (LO THRU -1).
MISSING VALUES v49d (LO THRU -1).
MISSING VALUES v49e (LO THRU -1).
MISSING VALUES v49f (LO THRU -1).
MISSING VALUES v50a (LO THRU -1).
MISSING VALUES v50b (LO THRU -1).
MISSING VALUES v51a (LO THRU -1).
MISSING VALUES v51b (LO THRU -1).
MISSING VALUES v51c (LO THRU -1).
MISSING VALUES v52a (LO THRU -1).
MISSING VALUES v52b (LO THRU -1).
MISSING VALUES v52c (LO THRU -1).
MISSING VALUES v52d (LO THRU -1).
MISSING VALUES v52e (LO THRU -1).
MISSING VALUES v52f (LO THRU -1).
MISSING VALUES v53 (LO THRU -1).
MISSING VALUES v54a (LO THRU -1).
MISSING VALUES v54b (LO THRU -1).
MISSING VALUES v55 (LO THRU -1).
MISSING VALUES v56a (LO THRU -1).
MISSING VALUES v56b (LO THRU -1).
MISSING VALUES v57 (LO THRU -1).
MISSING VALUES v58a (LO THRU -1).
MISSING VALUES v58b (LO THRU -1).
MISSING VALUES sv59 (LO THRU -1).
MISSING VALUES sv60 (LO THRU -1).
MISSING VALUES sv61 (LO THRU -1).
MISSING VALUES sv62 (LO THRU -1).
MISSING VALUES sv63 (LO THRU -1).
MISSING VALUES sv64 (LO THRU -1).
MISSING VALUES sv65a (LO THRU -1).
MISSING VALUES sv65b (LO THRU -1).
MISSING VALUES v66a (LO THRU -1).
MISSING VALUES v66b (LO THRU -1).
MISSING VALUES v66c (LO THRU -1).
MISSING VALUES v66d (LO THRU -1).
MISSING VALUES v66e (LO THRU -1).
MISSING VALUES v66f (LO THRU -1).
MISSING VALUES v67a (LO THRU -1).
MISSING VALUES v67b (LO THRU -1).
MISSING VALUES v67c (LO THRU -1).
MISSING VALUES v67d (LO THRU -1).
MISSING VALUES v67e (LO THRU -1).
MISSING VALUES v67f (LO THRU -1).
MISSING VALUES v67g (LO THRU -1).
MISSING VALUES v67h (LO THRU -1).
MISSING VALUES v68 (LO THRU -1).
MISSING VALUES v69 (LO THRU -1).
MISSING VALUES v70a (LO THRU -1).
MISSING VALUES v70b (LO THRU -1).
MISSING VALUES v70c (LO THRU -1).
MISSING VALUES v70d (LO THRU -1).
MISSING VALUES v71 (LO THRU -1).
MISSING VALUES v72a (LO THRU -1).
MISSING VALUES v72b (LO THRU -1).
MISSING VALUES v73a (LO THRU -1).
MISSING VALUES v73b (LO THRU -1).
MISSING VALUES v73c (LO THRU -1).
MISSING VALUES v73d (LO THRU -1).
MISSING VALUES v73e (LO THRU -1).
MISSING VALUES v73f (LO THRU -1).
MISSING VALUES v73g (LO THRU -1).
MISSING VALUES v74a (LO THRU -1).
MISSING VALUES v74b (LO THRU -1).
MISSING VALUES v74c (LO THRU -1).
MISSING VALUES v74d (LO THRU -1).
MISSING VALUES v74e (LO THRU -1).
MISSING VALUES v74f (LO THRU -1).
MISSING VALUES v75a (LO THRU -1).
MISSING VALUES v75b (LO THRU -1).
MISSING VALUES v76a (LO THRU -1).
MISSING VALUES v76b (LO THRU -1).
MISSING VALUES v76c (LO THRU -1).
MISSING VALUES v76d (LO THRU -1).
MISSING VALUES v76e (LO THRU -1).
MISSING VALUES v76f (LO THRU -1).
MISSING VALUES v77 (LO THRU -1).
MISSING VALUES v78a (LO THRU -1).
MISSING VALUES v78b (LO THRU -1).
MISSING VALUES v79 (LO THRU -1).
MISSING VALUES v80 (LO THRU -1).
MISSING VALUES mv81 (LO THRU -1).
MISSING VALUES mv82 (LO THRU -1).
MISSING VALUES mv83 (LO THRU -1).
MISSING VALUES mv84 (LO THRU -1).
MISSING VALUES mv85 (LO THRU -1).
MISSING VALUES v86a (LO THRU -1).
MISSING VALUES v86b (LO THRU -1).
MISSING VALUES v86c (LO THRU -1).
MISSING VALUES v86d (LO THRU -1).
MISSING VALUES v86e (LO THRU -1).
MISSING VALUES v86f (LO THRU -1).
MISSING VALUES v86g (LO THRU -1).
MISSING VALUES v87a (LO THRU -1).
MISSING VALUES v87b (LO THRU -1).
MISSING VALUES v87c (LO THRU -1).
MISSING VALUES v87d (LO THRU -1).
MISSING VALUES v87e (LO THRU -1).
MISSING VALUES v87f (LO THRU -1).
MISSING VALUES v87g (LO THRU -1).
MISSING VALUES mv88 (LO THRU -1).
MISSING VALUES mv89 (LO THRU -1).
MISSING VALUES mv90 (LO THRU -1).
MISSING VALUES mv91 (LO THRU -1).
MISSING VALUES mv92a (LO THRU -1).
MISSING VALUES uv92b (LO THRU -1).
MISSING VALUES mv92c (LO THRU -1).
MISSING VALUES mv92d (LO THRU -1).
MISSING VALUES mv92e (LO THRU -1).
MISSING VALUES mv92f (LO THRU -1).
MISSING VALUES mv92g (LO THRU -1).
MISSING VALUES mv92h (LO THRU -1).
MISSING VALUES mv93a (LO THRU -1).
MISSING VALUES mv93b (LO THRU -1).
MISSING VALUES mv93c (LO THRU -1).
MISSING VALUES mv93d (LO THRU -1).
MISSING VALUES mv93e (LO THRU -1).
MISSING VALUES mv93f (LO THRU -1).
MISSING VALUES mv93g (LO THRU -1).
MISSING VALUES mv93h (LO THRU -1).
MISSING VALUES mv94 (LO THRU -1).
MISSING VALUES mv95a (LO THRU -1).
MISSING VALUES uv95b (LO THRU -1).
MISSING VALUES mv95c (LO THRU -1).
MISSING VALUES mv95d (LO THRU -1).
MISSING VALUES mv95e (LO THRU -1).
MISSING VALUES mv95f (LO THRU -1).
MISSING VALUES mv95g (LO THRU -1).
MISSING VALUES mv95h (LO THRU -1).
MISSING VALUES v96 (LO THRU -1).
MISSING VALUES v97 (LO THRU -1).
MISSING VALUES v98 (LO THRU -1).
MISSING VALUES v99 (LO THRU -1).
MISSING VALUES v100 (LO THRU -1).
MISSING VALUES mv101 (LO THRU -1).
MISSING VALUES mv102 (LO THRU -1).
MISSING VALUES v103a (LO THRU -1).
MISSING VALUES v103b (LO THRU -1).
MISSING VALUES v103c (LO THRU -1).
MISSING VALUES v103d (LO THRU -1).
MISSING VALUES v103e (LO THRU -1).
MISSING VALUES v103f (LO THRU -1).
MISSING VALUES v103g (LO THRU -1).
MISSING VALUES v103h (LO THRU -1).
MISSING VALUES v103i (LO THRU -1).
MISSING VALUES v103j (LO THRU -1).
MISSING VALUES v103k (LO THRU -1).
MISSING VALUES v103l (LO THRU -1).
MISSING VALUES v103m (LO THRU -1).
MISSING VALUES v103n (LO THRU -1).
MISSING VALUES v103o (LO THRU -1).
MISSING VALUES v103p (LO THRU -1).
MISSING VALUES tv104aa (LO THRU -1).
MISSING VALUES sv104ca (LO THRU -1).
MISSING VALUES tv104ab (LO THRU -1).
MISSING VALUES sv104cb (LO THRU -1).
MISSING VALUES tv104ac (LO THRU -1).
MISSING VALUES sv104cc (LO THRU -1).
MISSING VALUES sv105aa (LO THRU -1).
MISSING VALUES sv105ba (LO THRU -1).
MISSING VALUES sv105ab (LO THRU -1).
MISSING VALUES sv105bb (LO THRU -1).
MISSING VALUES sv105ar (LO THRU -1).
MISSING VALUES sv105br (LO THRU -1).
MISSING VALUES lv106 (LO THRU -1).
MISSING VALUES sv107 (LO THRU -1).
MISSING VALUES sv107r (LO THRU -1).
MISSING VALUES sv108 (LO THRU -1).
MISSING VALUES sv109aa (LO THRU -1).
MISSING VALUES sv109ab (LO THRU -1).
MISSING VALUES sv109ac (LO THRU -1).
MISSING VALUES v109ad (LO THRU -1).
MISSING VALUES sv109ae (LO THRU -1).
MISSING VALUES sv109af (LO THRU -1).
MISSING VALUES sv109ag (LO THRU -1).
MISSING VALUES sv109ah (LO THRU -1).
MISSING VALUES sv109ai (LO THRU -1).
MISSING VALUES sv109aj (LO THRU -1).
MISSING VALUES sv109ba (LO THRU -1).
MISSING VALUES sv109bb (LO THRU -1).
MISSING VALUES sv109bc (LO THRU -1).
MISSING VALUES v109bd (LO THRU -1).
MISSING VALUES sv109be (LO THRU -1).
MISSING VALUES sv109bf (LO THRU -1).
MISSING VALUES sv109bg (LO THRU -1).
MISSING VALUES sv109bh (LO THRU -1).
MISSING VALUES sv109bi (LO THRU -1).
MISSING VALUES sv109bj (LO THRU -1).
MISSING VALUES sv109ca (LO THRU -1).
MISSING VALUES sv109cb (LO THRU -1).
MISSING VALUES sv109cc (LO THRU -1).
MISSING VALUES v109cd (LO THRU -1).
MISSING VALUES sv109ce (LO THRU -1).
MISSING VALUES sv109cf (LO THRU -1).
MISSING VALUES sv109cg (LO THRU -1).
MISSING VALUES sv109ch (LO THRU -1).
MISSING VALUES sv109ci (LO THRU -1).
MISSING VALUES sv109cj (LO THRU -1).
MISSING VALUES sv110a (LO THRU -1).
MISSING VALUES sv110b (LO THRU -1).
MISSING VALUES sv110c (LO THRU -1).
MISSING VALUES mv111 (LO THRU -1).
MISSING VALUES mv112 (LO THRU -1).
MISSING VALUES mv113 (LO THRU -1).
MISSING VALUES sv114aa (LO THRU -1).
MISSING VALUES sv114ab (LO THRU -1).
MISSING VALUES sv114ac (LO THRU -1).
MISSING VALUES v114ad (LO THRU -1).
MISSING VALUES sv114ae (LO THRU -1).
MISSING VALUES sv114af (LO THRU -1).
MISSING VALUES sv114ag (LO THRU -1).
MISSING VALUES sv114ah (LO THRU -1).
MISSING VALUES sv114ai (LO THRU -1).
MISSING VALUES sv114aj (LO THRU -1).
MISSING VALUES sv114ak (LO THRU -1).
MISSING VALUES sv114ba (LO THRU -1).
MISSING VALUES sv114bb (LO THRU -1).
MISSING VALUES sv114bc (LO THRU -1).
MISSING VALUES v114bd (LO THRU -1).
MISSING VALUES sv114be (LO THRU -1).
MISSING VALUES sv114bf (LO THRU -1).
MISSING VALUES sv114bg (LO THRU -1).
MISSING VALUES sv114bh (LO THRU -1).
MISSING VALUES sv114bi (LO THRU -1).
MISSING VALUES sv114bj (LO THRU -1).
MISSING VALUES sv114bk (LO THRU -1).
MISSING VALUES sv114ca (LO THRU -1).
MISSING VALUES sv114cb (LO THRU -1).
MISSING VALUES sv114cc (LO THRU -1).
MISSING VALUES v114cd (LO THRU -1).
MISSING VALUES sv114ce (LO THRU -1).
MISSING VALUES sv114cf (LO THRU -1).
MISSING VALUES sv114cg (LO THRU -1).
MISSING VALUES sv114ch (LO THRU -1).
MISSING VALUES sv114ci (LO THRU -1).
MISSING VALUES sv114cj (LO THRU -1).
MISSING VALUES sv114ck (LO THRU -1).
MISSING VALUES sv114d (LO THRU -1).
MISSING VALUES mv115a (LO THRU -1).
MISSING VALUES mv115b (LO THRU -1).
MISSING VALUES mv115c (LO THRU -1).
MISSING VALUES mv115d (LO THRU -1).
MISSING VALUES mv115e (LO THRU -1).
MISSING VALUES mv115f (LO THRU -1).
MISSING VALUES mv115g (LO THRU -1).
MISSING VALUES mv115h (LO THRU -1).
MISSING VALUES mv115i (LO THRU -1).
MISSING VALUES mv115j (LO THRU -1).
MISSING VALUES mv115k (LO THRU -1).
MISSING VALUES mv115l (LO THRU -1).
MISSING VALUES mv115m (LO THRU -1).
MISSING VALUES tv116 (LO THRU -1).
MISSING VALUES sv117 (LO THRU -1).
MISSING VALUES sv118a (LO THRU -1).
MISSING VALUES sv118b (LO THRU -1).
MISSING VALUES sv118c (LO THRU -1).
MISSING VALUES sv118d (LO THRU -1).
MISSING VALUES sv118e (LO THRU -1).
MISSING VALUES sv118f (LO THRU -1).
MISSING VALUES sv118g (LO THRU -1).
MISSING VALUES sv118h (LO THRU -1).
MISSING VALUES sv118i (LO THRU -1).
MISSING VALUES sv118j (LO THRU -1).
MISSING VALUES v119a (LO THRU -1).
MISSING VALUES v119b (LO THRU -1).
MISSING VALUES v119c (LO THRU -1).
MISSING VALUES v119d (LO THRU -1).
MISSING VALUES v119e (LO THRU -1).
MISSING VALUES v119f (LO THRU -1).
MISSING VALUES sv120a (LO THRU -1).
MISSING VALUES sv120b (LO THRU -1).
MISSING VALUES sv120c (LO THRU -1).
MISSING VALUES sv120d (LO THRU -1).
MISSING VALUES sv120e (LO THRU -1).
MISSING VALUES sv120f (LO THRU -1).
MISSING VALUES sv120g (LO THRU -1).
MISSING VALUES sv120h (LO THRU -1).
MISSING VALUES v121a (LO THRU -1).
MISSING VALUES sv121b (LO THRU -1).
MISSING VALUES sv121c (LO THRU -1).
MISSING VALUES sv121d (LO THRU -1).
MISSING VALUES sv121e (LO THRU -1).
MISSING VALUES sv121f (LO THRU -1).
MISSING VALUES sv121g (LO THRU -1).
MISSING VALUES sv122a (LO THRU -1).
MISSING VALUES sv122b (LO THRU -1).
MISSING VALUES sv122c (LO THRU -1).
MISSING VALUES sv122d (LO THRU -1).
MISSING VALUES sv122e (LO THRU -1).
MISSING VALUES sv122f (LO THRU -1).
MISSING VALUES sv122g (LO THRU -1).
MISSING VALUES sv123a (LO THRU -1).
MISSING VALUES sv123b (LO THRU -1).
MISSING VALUES sv123c (LO THRU -1).
MISSING VALUES sv123d (LO THRU -1).
MISSING VALUES sv123e (LO THRU -1).
MISSING VALUES sv123f (LO THRU -1).
MISSING VALUES sv124a (LO THRU -1).
MISSING VALUES sv124b (LO THRU -1).
MISSING VALUES sv124c (LO THRU -1).
MISSING VALUES sv124d (LO THRU -1).
MISSING VALUES sv124e (LO THRU -1).
MISSING VALUES sv124f (LO THRU -1).
MISSING VALUES sv124g (LO THRU -1).
MISSING VALUES sv124h (LO THRU -1).
MISSING VALUES sv124i (LO THRU -1).
MISSING VALUES mv125a (LO THRU -1).
MISSING VALUES mv125b (LO THRU -1).
MISSING VALUES mv125c (LO THRU -1).
MISSING VALUES mv125d (LO THRU -1).
MISSING VALUES v125e (LO THRU -1).
MISSING VALUES v125f (LO THRU -1).
MISSING VALUES mv125g (LO THRU -1).
MISSING VALUES mv125h (LO THRU -1).
MISSING VALUES mv125i (LO THRU -1).
MISSING VALUES mv125j (LO THRU -1).
MISSING VALUES mv125k (LO THRU -1).
MISSING VALUES mv125l (LO THRU -1).
MISSING VALUES sv126a (LO THRU -1).
MISSING VALUES sv126b (LO THRU -1).
MISSING VALUES mv127a (LO THRU -1).
MISSING VALUES mv127b (LO THRU -1).
MISSING VALUES tv128 (LO THRU -1).
MISSING VALUES sv129a (LO THRU -1).
MISSING VALUES sv129b (LO THRU -1).
MISSING VALUES sv129c (LO THRU -1).
MISSING VALUES sv129d (LO THRU -1).
MISSING VALUES sv129e (LO THRU -1).
MISSING VALUES sv129f (LO THRU -1).
MISSING VALUES sv129g (LO THRU -1).
MISSING VALUES sv130a (LO THRU -1).
MISSING VALUES sv130b (LO THRU -1).
MISSING VALUES sv130c (LO THRU -1).
MISSING VALUES sv130d (LO THRU -1).
MISSING VALUES sv130e (LO THRU -1).
MISSING VALUES sv130f (LO THRU -1).
MISSING VALUES sv131 (LO THRU -1).
MISSING VALUES sv132a (LO THRU -1).
MISSING VALUES sv132b (LO THRU -1).
MISSING VALUES sv133a (LO THRU -1).
MISSING VALUES sv133b (LO THRU -1).
MISSING VALUES mv134 (LO THRU -1).
MISSING VALUES tv135aa (LO THRU -1).
MISSING VALUES sv135b1a (LO THRU -1).
MISSING VALUES sv135b2a (LO THRU -1).
MISSING VALUES sv135b3a (LO THRU -1).
MISSING VALUES sv135b4a (LO THRU -1).
MISSING VALUES sv135ca (LO THRU -1).
MISSING VALUES v135da (LO THRU -1).
MISSING VALUES v135ea (LO THRU -1).
MISSING VALUES sv135ab (LO THRU -1).
MISSING VALUES sv135b1b (LO THRU -1).
MISSING VALUES sv135b2b (LO THRU -1).
MISSING VALUES sv135b3b (LO THRU -1).
MISSING VALUES sv135b4b (LO THRU -1).
MISSING VALUES sv135cb (LO THRU -1).
MISSING VALUES v135db (LO THRU -1).
MISSING VALUES v135eb (LO THRU -1).
MISSING VALUES tv135ac (LO THRU -1).
MISSING VALUES sv135b1c (LO THRU -1).
MISSING VALUES sv135b2c (LO THRU -1).
MISSING VALUES sv135b3c (LO THRU -1).
MISSING VALUES sv135b4c (LO THRU -1).
MISSING VALUES sv135cc (LO THRU -1).
MISSING VALUES v135dc (LO THRU -1).
MISSING VALUES v135ec (LO THRU -1).
MISSING VALUES sv135ad (LO THRU -1).
MISSING VALUES sv135b1d (LO THRU -1).
MISSING VALUES sv135b2d (LO THRU -1).
MISSING VALUES sv135b3d (LO THRU -1).
MISSING VALUES sv135b4d (LO THRU -1).
MISSING VALUES sv135cd (LO THRU -1).
MISSING VALUES v135dd (LO THRU -1).
MISSING VALUES v135ed (LO THRU -1).
MISSING VALUES sv135ae (LO THRU -1).
MISSING VALUES sv135b1e (LO THRU -1).
MISSING VALUES sv135b2e (LO THRU -1).
MISSING VALUES sv135b3e (LO THRU -1).
MISSING VALUES sv135b4e (LO THRU -1).
MISSING VALUES sv135ce (LO THRU -1).
MISSING VALUES v135de (LO THRU -1).
MISSING VALUES v135ee (LO THRU -1).
MISSING VALUES sv135af (LO THRU -1).
MISSING VALUES sv135b1f (LO THRU -1).
MISSING VALUES sv135b2f (LO THRU -1).
MISSING VALUES sv135b3f (LO THRU -1).
MISSING VALUES sv135b4f (LO THRU -1).
MISSING VALUES sv135cf (LO THRU -1).
MISSING VALUES v135df (LO THRU -1).
MISSING VALUES v135ef (LO THRU -1).
MISSING VALUES tv135ag (LO THRU -1).
MISSING VALUES sv135b1g (LO THRU -1).
MISSING VALUES sv135b2g (LO THRU -1).
MISSING VALUES sv135b3g (LO THRU -1).
MISSING VALUES sv135b4g (LO THRU -1).
MISSING VALUES sv135cg (LO THRU -1).
MISSING VALUES v135dg (LO THRU -1).
MISSING VALUES v135eg (LO THRU -1).
MISSING VALUES sv135ah (LO THRU -1).
MISSING VALUES sv135b1h (LO THRU -1).
MISSING VALUES sv135b2h (LO THRU -1).
MISSING VALUES sv135b3h (LO THRU -1).
MISSING VALUES sv135b4h (LO THRU -1).
MISSING VALUES sv135ch (LO THRU -1).
MISSING VALUES v135dh (LO THRU -1).
MISSING VALUES v135eh (LO THRU -1).
MISSING VALUES sv135ai (LO THRU -1).
MISSING VALUES sv135b1i (LO THRU -1).
MISSING VALUES sv135b2i (LO THRU -1).
MISSING VALUES sv135b3i (LO THRU -1).
MISSING VALUES sv135b4i (LO THRU -1).
MISSING VALUES sv135ci (LO THRU -1).
MISSING VALUES v135di (LO THRU -1).
MISSING VALUES v135ei (LO THRU -1).
MISSING VALUES sv135aj (LO THRU -1).
MISSING VALUES sv135b1j (LO THRU -1).
MISSING VALUES sv135b2j (LO THRU -1).
MISSING VALUES sv135b3j (LO THRU -1).
MISSING VALUES sv135b4j (LO THRU -1).
MISSING VALUES sv135cj (LO THRU -1).
MISSING VALUES v135dj (LO THRU -1).
MISSING VALUES v135ej (LO THRU -1).
MISSING VALUES sv135ak (LO THRU -1).
MISSING VALUES sv135b1k (LO THRU -1).
MISSING VALUES sv135b2k (LO THRU -1).
MISSING VALUES sv135b3k (LO THRU -1).
MISSING VALUES sv135b4k (LO THRU -1).
MISSING VALUES sv135ck (LO THRU -1).
MISSING VALUES v135dk (LO THRU -1).
MISSING VALUES v135ek (LO THRU -1).
MISSING VALUES tv135al (LO THRU -1).
MISSING VALUES sv135b1l (LO THRU -1).
MISSING VALUES sv135b2l (LO THRU -1).
MISSING VALUES sv135b3l (LO THRU -1).
MISSING VALUES sv135b4l (LO THRU -1).
MISSING VALUES sv135cl (LO THRU -1).
MISSING VALUES v135dl (LO THRU -1).
MISSING VALUES v135el (LO THRU -1).
MISSING VALUES tv135am (LO THRU -1).
MISSING VALUES sv135b1m (LO THRU -1).
MISSING VALUES sv135b2m (LO THRU -1).
MISSING VALUES sv135b3m (LO THRU -1).
MISSING VALUES sv135b4m (LO THRU -1).
MISSING VALUES sv135cm (LO THRU -1).
MISSING VALUES v135dm (LO THRU -1).
MISSING VALUES v135em (LO THRU -1).
MISSING VALUES tv135an (LO THRU -1).
MISSING VALUES sv135b1n (LO THRU -1).
MISSING VALUES sv135b2n (LO THRU -1).
MISSING VALUES sv135b3n (LO THRU -1).
MISSING VALUES sv135b4n (LO THRU -1).
MISSING VALUES sv135cn (LO THRU -1).
MISSING VALUES v135dn (LO THRU -1).
MISSING VALUES v135en (LO THRU -1).
MISSING VALUES tv135ao (LO THRU -1).
MISSING VALUES sv135b1o (LO THRU -1).
MISSING VALUES sv135b2o (LO THRU -1).
MISSING VALUES sv135b3o (LO THRU -1).
MISSING VALUES sv135b4o (LO THRU -1).
MISSING VALUES sv135co (LO THRU -1).
MISSING VALUES v135do (LO THRU -1).
MISSING VALUES v135eo (LO THRU -1).
MISSING VALUES sv135ap (LO THRU -1).
MISSING VALUES sv135b1p (LO THRU -1).
MISSING VALUES sv135b2p (LO THRU -1).
MISSING VALUES sv135b3p (LO THRU -1).
MISSING VALUES sv135b4p (LO THRU -1).
MISSING VALUES sv135cp (LO THRU -1).
MISSING VALUES v135dp (LO THRU -1).
MISSING VALUES v135ep (LO THRU -1).
MISSING VALUES sv135aq (LO THRU -1).
MISSING VALUES sv135b1q (LO THRU -1).
MISSING VALUES sv135b2q (LO THRU -1).
MISSING VALUES sv135b3q (LO THRU -1).
MISSING VALUES sv135b4q (LO THRU -1).
MISSING VALUES sv135cq (LO THRU -1).
MISSING VALUES v135dq (LO THRU -1).
MISSING VALUES v135eq (LO THRU -1).
MISSING VALUES tv135ar (LO THRU -1).
MISSING VALUES sv135b1r (LO THRU -1).
MISSING VALUES sv135b2r (LO THRU -1).
MISSING VALUES sv135b3r (LO THRU -1).
MISSING VALUES sv135b4r (LO THRU -1).
MISSING VALUES sv135cr (LO THRU -1).
MISSING VALUES v135dr (LO THRU -1).
MISSING VALUES v135er (LO THRU -1).
MISSING VALUES tv135as (LO THRU -1).
MISSING VALUES sv135b1s (LO THRU -1).
MISSING VALUES sv135b2s (LO THRU -1).
MISSING VALUES sv135b3s (LO THRU -1).
MISSING VALUES sv135b4s (LO THRU -1).
MISSING VALUES sv135cs (LO THRU -1).
MISSING VALUES v135ds (LO THRU -1).
MISSING VALUES v135es (LO THRU -1).
MISSING VALUES tv135at (LO THRU -1).
MISSING VALUES sv135b1t (LO THRU -1).
MISSING VALUES sv135b2t (LO THRU -1).
MISSING VALUES sv135b3t (LO THRU -1).
MISSING VALUES sv135b4t (LO THRU -1).
MISSING VALUES sv135ct (LO THRU -1).
MISSING VALUES v135dt (LO THRU -1).
MISSING VALUES v135et (LO THRU -1).
MISSING VALUES pv136 (LO THRU -1).
MISSING VALUES sv137 (LO THRU -1).
MISSING VALUES sv138a (LO THRU -1).
MISSING VALUES sv138b (LO THRU -1).
MISSING VALUES sv138c (LO THRU -1).
MISSING VALUES sv138d (LO THRU -1).
MISSING VALUES sv138e (LO THRU -1).
MISSING VALUES sv138f (LO THRU -1).
MISSING VALUES sv138g (LO THRU -1).
MISSING VALUES sv138h (LO THRU -1).
MISSING VALUES sv138i (LO THRU -1).
MISSING VALUES sv138j (LO THRU -1).
MISSING VALUES sv138k (LO THRU -1).
MISSING VALUES sv139 (LO THRU -1).
MISSING VALUES sv140 (LO THRU -1).
MISSING VALUES mv141a (LO THRU -1).
MISSING VALUES v141b (LO THRU -1).
MISSING VALUES mv141c (LO THRU -1).
MISSING VALUES mv141d (LO THRU -1).
MISSING VALUES mv141e (LO THRU -1).
MISSING VALUES mv141f (LO THRU -1).
MISSING VALUES v141g (LO THRU -1).
MISSING VALUES mv141h (LO THRU -1).
MISSING VALUES mv141i (LO THRU -1).
MISSING VALUES mv141j (LO THRU -1).
MISSING VALUES mv141k (LO THRU -1).
MISSING VALUES v141l (LO THRU -1).
MISSING VALUES mv141m (LO THRU -1).
MISSING VALUES mv141n (LO THRU -1).
MISSING VALUES mv141o (LO THRU -1).
MISSING VALUES mv141p (LO THRU -1).
MISSING VALUES mv141q (LO THRU -1).
MISSING VALUES mv141r (LO THRU -1).
MISSING VALUES v141s (LO THRU -1).
MISSING VALUES mv141t (LO THRU -1).
MISSING VALUES mv141u (LO THRU -1).
MISSING VALUES mv141v (LO THRU -1).
MISSING VALUES mv141w (LO THRU -1).
MISSING VALUES v141x (LO THRU -1).
MISSING VALUES mv141y (LO THRU -1).
MISSING VALUES mv141z (LO THRU -1).
MISSING VALUES mv141aa (LO THRU -1).
MISSING VALUES mv141ab (LO THRU -1).
MISSING VALUES sv142 (LO THRU -1).
MISSING VALUES mv143aa (LO THRU -1).
MISSING VALUES mv143ab (LO THRU -1).
MISSING VALUES mv143ac (LO THRU -1).
MISSING VALUES mv143ad (LO THRU -1).
MISSING VALUES mv143ae (LO THRU -1).
MISSING VALUES mv143af (LO THRU -1).
MISSING VALUES mv143ag (LO THRU -1).
MISSING VALUES mv143ah (LO THRU -1).
MISSING VALUES mv143ai (LO THRU -1).
MISSING VALUES mv143aj (LO THRU -1).
MISSING VALUES mv143ba (LO THRU -1).
MISSING VALUES mv143bb (LO THRU -1).
MISSING VALUES mv143bc (LO THRU -1).
MISSING VALUES mv143bd (LO THRU -1).
MISSING VALUES mv143be (LO THRU -1).
MISSING VALUES mv143bf (LO THRU -1).
MISSING VALUES mv143bg (LO THRU -1).
MISSING VALUES mv143bh (LO THRU -1).
MISSING VALUES mv143bi (LO THRU -1).
MISSING VALUES mv143bj (LO THRU -1).
MISSING VALUES mv143ca (LO THRU -1).
MISSING VALUES mv143cb (LO THRU -1).
MISSING VALUES mv143cc (LO THRU -1).
MISSING VALUES mv143cd (LO THRU -1).
MISSING VALUES mv143ce (LO THRU -1).
MISSING VALUES mv143cf (LO THRU -1).
MISSING VALUES mv143cg (LO THRU -1).
MISSING VALUES mv143ch (LO THRU -1).
MISSING VALUES mv143ci (LO THRU -1).
MISSING VALUES mv143cj (LO THRU -1).
MISSING VALUES mv144aa (LO THRU -1).
MISSING VALUES mv144ab (LO THRU -1).
MISSING VALUES mv144ac (LO THRU -1).
MISSING VALUES mv144ad (LO THRU -1).
MISSING VALUES mv144ae (LO THRU -1).
MISSING VALUES mv144af (LO THRU -1).
MISSING VALUES mv144ag (LO THRU -1).
MISSING VALUES mv144ah (LO THRU -1).
MISSING VALUES mv144ai (LO THRU -1).
MISSING VALUES mv144aj (LO THRU -1).
MISSING VALUES mv144ba (LO THRU -1).
MISSING VALUES mv144bb (LO THRU -1).
MISSING VALUES mv144bc (LO THRU -1).
MISSING VALUES mv144bd (LO THRU -1).
MISSING VALUES mv144be (LO THRU -1).
MISSING VALUES mv144bf (LO THRU -1).
MISSING VALUES mv144bg (LO THRU -1).
MISSING VALUES mv144bh (LO THRU -1).
MISSING VALUES mv144bi (LO THRU -1).
MISSING VALUES mv144bj (LO THRU -1).
MISSING VALUES mv144ca (LO THRU -1).
MISSING VALUES mv144cb (LO THRU -1).
MISSING VALUES mv144cc (LO THRU -1).
MISSING VALUES mv144cd (LO THRU -1).
MISSING VALUES mv144ce (LO THRU -1).
MISSING VALUES mv144cf (LO THRU -1).
MISSING VALUES mv144cg (LO THRU -1).
MISSING VALUES mv144ch (LO THRU -1).
MISSING VALUES mv144ci (LO THRU -1).
MISSING VALUES mv144cj (LO THRU -1).
MISSING VALUES mv145 (LO THRU -1).
MISSING VALUES svf1 (LO THRU -1).
MISSING VALUES svf2 (LO THRU -1).
MISSING VALUES svf3 (LO THRU -1).
MISSING VALUES svf4 (LO THRU -1).
MISSING VALUES svf5 (LO THRU -1).
MISSING VALUES mvf6a (LO THRU -1).
MISSING VALUES mvf6b (LO THRU -1).
MISSING VALUES mvf6c (LO THRU -1).
MISSING VALUES mvf7 (LO THRU -1).
MISSING VALUES mvf8 (LO THRU -1).
MISSING VALUES mvf9 (LO THRU -1).
MISSING VALUES mvf9a (LO THRU -1).
MISSING VALUES mvf9b (LO THRU -1).
MISSING VALUES mvf9c (LO THRU -1).
MISSING VALUES mvf9d (LO THRU -1).
MISSING VALUES mvf10 (LO THRU -1).
MISSING VALUES mvf11 (LO THRU -1).
MISSING VALUES svfdate (LO THRU -1).
MISSING VALUES ix1a (LO THRU -1).
MISSING VALUES ix1b (LO THRU -1).
MISSING VALUES i2b (LO THRU -1).
MISSING VALUES i10 (LO THRU -1).
MISSING VALUES i11 (LO THRU -1).
MISSING VALUES i1_18 (LO THRU -1).
MISSING VALUES i1_19 (LO THRU -1).
MISSING VALUES i1_21 (LO THRU -1).
MISSING VALUES i2_18 (LO THRU -1).
MISSING VALUES i2_19 (LO THRU -1).
MISSING VALUES i2_21 (LO THRU -1).
MISSING VALUES i3_18 (LO THRU -1).
MISSING VALUES i3_19 (LO THRU -1).
MISSING VALUES i3_21 (LO THRU -1).
MISSING VALUES i4_18 (LO THRU -1).
MISSING VALUES i4_19 (LO THRU -1).
MISSING VALUES i4_21 (LO THRU -1).
MISSING VALUES i5_18 (LO THRU -1).
MISSING VALUES i5_19 (LO THRU -1).
MISSING VALUES i5_21 (LO THRU -1).
MISSING VALUES i6_18 (LO THRU -1).
MISSING VALUES i6_19 (LO THRU -1).
MISSING VALUES i6_21 (LO THRU -1).
MISSING VALUES i7_18 (LO THRU -1).
MISSING VALUES i7_19 (LO THRU -1).
MISSING VALUES i7_21 (LO THRU -1).
MISSING VALUES i8_18 (LO THRU -1).
MISSING VALUES i8_19 (LO THRU -1).
MISSING VALUES i8_21 (LO THRU -1).
MISSING VALUES ix2a (LO THRU -1).
MISSING VALUES ix2b (LO THRU -1).
MISSING VALUES i81 (LO THRU -1).
MISSING VALUES i83a (LO THRU -1).
MISSING VALUES i83b (LO THRU -1).
MISSING VALUES i83c (LO THRU -1).
MISSING VALUES i83d (LO THRU -1).
MISSING VALUES i83e (LO THRU -1).
MISSING VALUES i83f (LO THRU -1).
MISSING VALUES i83g (LO THRU -1).
MISSING VALUES i84 (LO THRU -1).
MISSING VALUES i91 (LO THRU -1).
MISSING VALUES i91mois (LO THRU -1).
MISSING VALUES i91annee (LO THRU -1).
MISSING VALUES i93 (LO THRU -1).
MISSING VALUES i95 (LO THRU -1).
MISSING VALUES ix3a (LO THRU -1).
MISSING VALUES ix3b (LO THRU -1).
MISSING VALUES i117d (LO THRU -1).
MISSING VALUES ix4 (LO THRU -1).
MISSING VALUES i22 (LO THRU -1).
MISSING VALUES i24 (LO THRU -1).
MISSING VALUES i25 (LO THRU -1).
MISSING VALUES i26 (LO THRU -1).
MISSING VALUES ix5 (LO THRU -1).
MISSING VALUES i60 (LO THRU -1).
MISSING VALUES i62 (LO THRU -1).
MISSING VALUES i63 (LO THRU -1).
MISSING VALUES i64 (LO THRU -1).
MISSING VALUES i65 (LO THRU -1).
MISSING VALUES i59 (LO THRU -1).
MISSING VALUES i136 (LO THRU -1).
MISSING VALUES ix6a (LO THRU -1).
MISSING VALUES ix6b (LO THRU -1).
MISSING VALUES ix6c (LO THRU -1).
MISSING VALUES i98 (LO THRU -1).
MISSING VALUES i175a (LO THRU -1).
MISSING VALUES i175b (LO THRU -1).
MISSING VALUES i175c (LO THRU -1).
MISSING VALUES i175d (LO THRU -1).
MISSING VALUES i182aa (LO THRU -1).
MISSING VALUES i182ba (LO THRU -1).
MISSING VALUES i182ac (LO THRU -1).
MISSING VALUES i182bc (LO THRU -1).
MISSING VALUES i182ad (LO THRU -1).
MISSING VALUES i182bd (LO THRU -1).
MISSING VALUES i182ae (LO THRU -1).
MISSING VALUES i182be (LO THRU -1).
MISSING VALUES i182ag (LO THRU -1).
MISSING VALUES i182bg (LO THRU -1).
MISSING VALUES i125a (LO THRU -1).
MISSING VALUES i125l (LO THRU -1).
MISSING VALUES i135e (LO THRU -1).
MISSING VALUES i135f (LO THRU -1).
MISSING VALUES i135j (LO THRU -1).
MISSING VALUES rjpb (LO THRU -1).
MISSING VALUES rjpc (LO THRU -1).
MISSING VALUES jpd (LO THRU -1).
MISSING VALUES jpg (LO THRU -1).
MISSING VALUES j9 (LO THRU -1).
MISSING VALUES j12a (LO THRU -1).
MISSING VALUES j12b (LO THRU -1).
MISSING VALUES j12c (LO THRU -1).
MISSING VALUES j23a (LO THRU -1).
MISSING VALUES j23b (LO THRU -1).
MISSING VALUES j23c (LO THRU -1).
MISSING VALUES j34a (LO THRU -1).
MISSING VALUES j34b (LO THRU -1).
MISSING VALUES j34c (LO THRU -1).
MISSING VALUES j45a (LO THRU -1).
MISSING VALUES j45b (LO THRU -1).
MISSING VALUES j45c (LO THRU -1).
MISSING VALUES j26 (LO THRU -1).
MISSING VALUES j115a (LO THRU -1).
MISSING VALUES j115b (LO THRU -1).
MISSING VALUES j115c (LO THRU -1).
MISSING VALUES j115d (LO THRU -1).
MISSING VALUES j115e (LO THRU -1).
MISSING VALUES j115f (LO THRU -1).
MISSING VALUES j115g (LO THRU -1).
MISSING VALUES j115h (LO THRU -1).
MISSING VALUES j115i (LO THRU -1).
MISSING VALUES j115j (LO THRU -1).
MISSING VALUES j115k (LO THRU -1).
MISSING VALUES j115m (LO THRU -1).
MISSING VALUES j172a (LO THRU -1).
MISSING VALUES j172b (LO THRU -1).
MISSING VALUES dpd (LO THRU -1).
MISSING VALUES dpe (LO THRU -1).
MISSING VALUES dpf (LO THRU -1).
MISSING VALUES dpg (LO THRU -1).
MISSING VALUES dph (LO THRU -1).
MISSING VALUES dpi (LO THRU -1).
MISSING VALUES dpl (LO THRU -1).
MISSING VALUES d1_12 (LO THRU -1).
MISSING VALUES d1_13 (LO THRU -1).
MISSING VALUES d1_14 (LO THRU -1).
MISSING VALUES d1_15 (LO THRU -1).
MISSING VALUES d1_18 (LO THRU -1).
MISSING VALUES d1_19 (LO THRU -1).
MISSING VALUES d1_21 (LO THRU -1).
MISSING VALUES d2_12 (LO THRU -1).
MISSING VALUES d2_13 (LO THRU -1).
MISSING VALUES d2_14 (LO THRU -1).
MISSING VALUES d2_15 (LO THRU -1).
MISSING VALUES d2_18 (LO THRU -1).
MISSING VALUES d2_19 (LO THRU -1).
MISSING VALUES d2_21 (LO THRU -1).
MISSING VALUES dx7 (LO THRU -1).
MISSING VALUES d26 (LO THRU -1).
MISSING VALUES d91 (LO THRU -1).
MISSING VALUES d93 (LO THRU -1).
MISSING VALUES d96 (LO THRU -1).
MISSING VALUES dx8a (LO THRU -1).
MISSING VALUES dx8b (LO THRU -1).
MISSING VALUES dx8c (LO THRU -1).
MISSING VALUES dx8d (LO THRU -1).
MISSING VALUES dx8e (LO THRU -1).
MISSING VALUES dx8f (LO THRU -1).
MISSING VALUES dx8g (LO THRU -1).
MISSING VALUES dx8h (LO THRU -1).
MISSING VALUES senfviv (LO THRU -1).
MISSING VALUES sansfam (LO THRU -1).
MISSING VALUES snfratri (LO THRU -1).
MISSING VALUES lgenfviv (LO THRU -1).
MISSING VALUES lgfratri (LO THRU -1).
MISSING VALUES ptsenfan (LO THRU -1).
MISSING VALUES frersoeu (LO THRU -1).
MISSING VALUES parents (LO THRU -1).
MISSING VALUES bparents (LO THRU -1).
MISSING VALUES agemere (LO THRU -1).
MISSING VALUES agepere (LO THRU -1).
MISSING VALUES agebmere (LO THRU -1).
MISSING VALUES agebpere (LO THRU -1).
MISSING VALUES ageascd (LO THRU -1).
MISSING VALUES ageascal (LO THRU -1).
MISSING VALUES agmari1 (LO THRU -1).
MISSING VALUES agveuv1 (LO THRU -1).
MISSING VALUES pondrep5 (LO THRU -1).