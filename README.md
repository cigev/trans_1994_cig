# L'autonomie de la personne vieillissante dans son environnement socio-culturel

La recherche porte sur des facteurs (internes et environnementaux) et des processus qui favorisent ou soutiennent l'autonomie de la personne vieillisante, son intégrité physique et psychique et sa participation à la vie en société, ou au contraire qui entravent ou portent atteinte à celles-ci. Cet objectif est poursuivi au moyen d'une enquête transversale auprès d'un échantillon représentatif de la population âgée de 63 ans et plus de deux régions typées (canton de Genève et Valais central) en 1979. Le choix des deux régions d'enquête s'explique par le fait qu'elles constituent typologiquement les deux extrêmes de la "fourchette" représentant la diversité des situations des personnes âgées en Suisse. Cette enquête transversale est répétée en 1994 et complétée par une enquête longitudinale (1994-1999) sur la cohorte des 80-84 ans vivant à domicile au moment du passage de l'enquête transversale de 1994. Ce design autorise une comparaison permettant d'établir l'évolution et les changements de la condition âgée sur 15 ans.

La recherche s'inscrit dans la perspective théorique du parcours de vie (life course). A la stratification sociale "horizontale" (de classes) s'articule une stratification "verticale", une organisation sociale du temps de vie, définissant étapes et transitions. L'hypothèse est que la position occupée dans le parcours de vie est un meilleur indicateur que l'âge tant pour définir des situations et problèmes-types que par rapport aux pratiques des personnes vieillissantes. L'idée est donc de construire et de valider un indicateur de position dans le parcours de vie (IPV) combinant la position individuelle dans trois dimensions: le parcours de santé, la trajectoire familiale, la trajectoire professionnelle.

Par les résultats de la recherche, on veut contribuer à une réflexion sur les fondements éthiques de la recherche en gérontologie et d'une politique de la vieillesse, à la définition des besoins présents et à venir des personnes vieillissantes, et à la mise en lumière des possibles articulations entre le soutien que peuvent apporter la famille ou les réseaux d'entraide, et les diverses formes d'aides institutionnelles.

## Pour commencer

Nous proposons ici un jeu de données mis à jour et corrigé en 2019-2020, ainsi qu'une documentation complète. La liste des fichiers disponibles est [détaillée ci-dessous](#contenu), et la documentation se trouve dans le fichier [trans_1994_doc.md](trans_1994_doc.md).

Les données de l'enquête avaient été déposées auprès du SIDOS en 2005 et sont aujourd'hui [disponibles auprès de FORS](https://forsbase.unil.ch/project/study-public-overview/15414/0/), accompagnées de leur documentation de l'époque. L'établissement du jeu de données avait fait l'objet d'un travail important (voir la documentation qui est fournie par FORS), mais avait laissé de côté un certain nombre d'étapes que nous avons reprises en 2019-2020. La [documentation fournie](trans_1994_doc.md) se base sur le travail effectué par le SIDOS en 2005, complété d'une vérification plus approfondie sur les variables en 2019-2020.

En clonant ce dépôt, vous vous engagez à respecter la [licence d'utilisation](LICENSE) et à citer [les auteur·e·s](#auteures) et le titre de l'enquête ([voir la citation proposée](#citation)).

### Citation

(FR)
> Lalive d'Épinay, C. (dir.). (1994). *L'autonomie de la personne vieillissante dans son environnement socio-culturel* [Jeu de données et documentation]. Tiré de https://gitlab.unige.ch/cigev/

(EN)
> Lalive d'Épinay, C. (Dir.). (1994). *L'autonomie de la personne vieillissante dans son environnement socio-culturel* [Data file and documentation]. Retrieved from https://gitlab.unige.ch/cigev/

### Prérequis

Vous pouvez télécharger les données fournies par le CIGEV et les utiliser librement, selon la [licence d'utilisation](LICENSE). Afin de faire profiter les autres utilisateurs/trices de vos corrections éventuelles, utilisez Git: voir le livre [Pro Git](https://git-scm.com/book/fr/v2) pour bien commencer. Pour utiliser les commandes de Git, vous devez vous [créer une paire de clés SSH](https://git-scm.com/book/fr/v2/Git-sur-le-serveur-Génération-des-clés-publiques-SSH) et intégrer la clé publique à votre profil (Settings > SSH Keys).

Le [jeu de données](trans_1994_data.csv) est fourni au format [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values), mais peut être importé dans [SPSS](https://fr.wikipedia.org/wiki/SPSS) grâce à la [syntaxe d'importation](trans_1994_data.sps) qui l'accompagne (modifiez la ligne appelant le fichier CSV en indiquant le chemin complet du fichier). La [documentation](trans_1994_doc.md) est fournie sous la forme d'un fichier [MD](https://fr.wikipedia.org/wiki/Markdown). Le [codebook](trans_1994_codebook.ods) a été exporté depuis SPSS et est fourni au format [OpenDocument](https://fr.wikipedia.org/wiki/OpenDocument). Les questionnaires (XXXXX) sont au format [PDF](https://fr.wikipedia.org/wiki/Portable_Document_Format).


### Obtenir les données

Pour utiliser les données de ce projet, vous pouvez copier le jeu de données et la documentation en les téléchargeant depuis ce site (voir lien «Download» en haut à droite de la liste des fichiers) ou en clonant le dépôt:

```
git clone git@gitlab.unige.ch:cigev/trans_1994_cig.git
```

Si vous souhaitez apporter des modifications à ce projet, il faut le *forker*, c'est-à-dire vous créer une copie des fichiers dans votre espace personnel, auxquels vous pourrez apporter des modifications. Vous pourrez par la suite partager ces modifications ou demander à la personne responsable de ce projet de les intégrer dans la branche principale. Utilisez pour cela le bouton «Fork» qui se trouve en haut de [la page principale](https://gitlab.unige.ch/cigev/trans_1994_cig).

## Contenu

* [LICENSE](LICENSE) – Licence d'utilisation des données
* [README.md](README.md) – Indications de base (ce fichier)
* [trans_1994_annexe_codebook79-94.ods](trans_1994_annexe_codebook79-94.ods) – Codebook annexe permettant la comparaison entre les enquêtes de 1979 et 1994
* [trans_1994_annexe_csp.pdf](trans_1994_annexe_csp.pdf) – Explications sur la catégorie socio-professionnelle en 1994, avec comparaison 1979
* [trans_1994_annexe_listethematiquevariables.pdf](trans_1994_annexe_listethematiquevariables.pdf) – Liste thématique des variables
* [trans_1994_annexe_souspopulations.pdf](trans_1994_annexe_souspopulations.pdf) – Sous-populations de l'enquête selon le groupe d'âge, le domicile et l'aptitude à répondre au questionnaire
* [trans_1994_annexe_variablesquestionnaires.ods](trans_1994_annexe_variablesquestionnaires.ods) – Liste détaillée des variables et différences pour chaque questionnaire
* [trans_1994_codebook.ods](trans_1994_codebook.ods) – Codebook au format OpenDocument (feuille de calcul)
* [trans_1994_data.csv](trans_1994_data.csv) – Jeu de données au format CSV
* [trans_1994_data.sps](trans_1994_data.sps) – Syntaxe d'importation du jeu de données au format SPSS
* [trans_1994_doc.md](trans_1994_doc.md) – Documentation et rapport de traitement au format MD
* [trans_1994_q1bleu.pdf](trans_1994_q1bleu.pdf) – Questionnaire pour personnes aptes à domicile, 1ère partie (auto-administré)
* [trans_1994_q1vert.pdf](trans_1994_q1vert.pdf) – Questionnaire pour personnes aptes à domicile, 2e partie (face-à-face)
* [trans_1994_q2bleu.pdf](trans_1994_q2bleu.pdf) – Questionnaire pour personnes aptes à domicile, version simplifiée plus de 80 ans, 1ère partie (auto-administré)
* [trans_1994_q2vert.pdf](trans_1994_q2vert.pdf) – Questionnaire pour personnes aptes à domicile, version simplifiée plus de 80 ans (face-à-face)
* [trans_1994_q3.pdf](trans_1994_q3.pdf) – Questionnaire pour personnes aptes en institution (auto-administré)
* [trans_1994_q4.pdf](trans_1994_q4.pdf) – Questionnaire pour personnes non-aptes en institution (rempli par personnel)
* [trans_1994_q5.pdf](trans_1994_q5.pdf) – Questionnaire pour personnes non-aptes à domicile (rempli par proxi)

## Contribuer

Si vous désirez contribuer à l'amélioration de ce jeu de données, de sa documentation, ou laisser des commentaires, vous pouvez contacter le CIGEV à l'adresse: [cigev-data@unige.ch](mailto:cigev-data@unige.ch). Le système Git vous permettra de proposer des modifications que nous pourrons valider, documenter et partager.

Pour toute utilisation des données (analyse, comparaison, enseignement, publication...), nous vous serions reconnaissants de bien vouloir nous informer: [cigev-data@unige.ch](mailto:cigev-data@unige.ch).

## Auteur·e·s

* **Christian Lalive d'Épinay** – *Chef de projet*
* **Eric Fuchs**
* **Eugen Horber**
* **Jean-Pierre Michel**
* **André Rougemont**

### Collaboratrices et collaborateurs

* **Christine Bétemps**
* **Jean-François Bickel**
* **Matthias Brunner**
* **Cornelia Hummel**
* **Carole Maystre**
* **Jean-François Riand**
* **Dario Spini**
* **Astrid Stuckelberger**
* **Manfred Urben**
* **Nathalie Vollenwyder**

## Licence

Ce projet est distribué selon les termes de la licence de CreativeCommons CC-BY-SA. Voir le fichier [LICENSE](LICENSE) pour plus de détails.

## Remerciements

Merci à toutes les personnes (notamment secrétaires, enquêtrices et enquêteurs) qui ont également collaboré à cette recherche.
